<?php
namespace app\common\pay;
use ptpaysdk\core\PtSdk;
use think\Log;
use think\Request;
use app\common\Pay;

class PtPayWechat extends Pay
{

    protected $gateway = 'https://pay.xdqj.net';
    protected $code = '';
    protected $error = '';

    public function return_url($order)
    {
        header("Location:" . url('/orderquery',['orderid'=>$order->trade_no]));
    }

    public function notify_callback($params,$order, $type = '')
    {

        $pay = new PtSdk();
        //验证签名
        if($pay->isSign())
        {
            //再次判断订单状态,如过服务器有这个订单就处理业务
            if($pay->isCheckOrder() || $pay->checkOrderState())
            {
                //签名验证成功,订单验证成功
                //---------开始业务逻辑----------------
                $request_data =get_notify_parameter();

                $order->transaction_id =$request_data['payId'];
                $this->completeOrder($order);
                //----------业务逻辑结束---------------
                //告诉服务器已经收到通知
                echo 'success';
            }
            else
            {
                exit('fail');
            }
        }
        else
        {
            exit('fail');
        }


    }

    public function order($outTradeNo,$subject,$totalAmount,$paytype='')
    {
        $parameter = array(
            //支付方式 1.是微信，2是支付宝
            "type" => '1',
            //商户订单号
            "payId" => $outTradeNo,
            //自定义参数
            "param" => $subject,
            //金额
            "price" => $totalAmount,
            'returnUrl'=>Request::instance()->domain().'/orderquery?orderid='.$outTradeNo.'&',
            'notifyUrl'=>Request::instance()->domain().'/pay/notify/PtPayAliPay'

        );
        $this->code    = 0;
        $pay =new PtSdk();
        $obj           = new \stdClass();
        $obj->pay_url  = $pay->createOrder($parameter);
        $obj->content_type = 3;
        return $obj;

    }
}
?>