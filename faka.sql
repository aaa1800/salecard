-- phpMyAdmin SQL Dump
-- version 4.9.5
-- https://www.phpmyadmin.net/
--
-- 主机： localhost
-- 生成日期： 2022-12-10 07:45:03
-- 服务器版本： 5.7.27-log
-- PHP 版本： 7.0.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- 数据库： `zblog`
--

-- --------------------------------------------------------

--
-- 表的结构 `announce_log`
--

CREATE TABLE `announce_log` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL DEFAULT '0' COMMENT '用户ID',
  `article_id` int(11) NOT NULL DEFAULT '0' COMMENT '公告ID',
  `create_at` int(11) NOT NULL DEFAULT '0' COMMENT '阅读时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `app_menu`
--

CREATE TABLE `app_menu` (
  `id` int(11) NOT NULL,
  `function_id` int(11) NOT NULL COMMENT '唯一值',
  `menu` text NOT NULL COMMENT '菜单项'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='客户端菜单';

--
-- 转存表中的数据 `app_menu`
--

INSERT INTO `app_menu` (`id`, `function_id`, `menu`) VALUES
(1, 0, '{\"title\":\"\\u5546\\u54c1\\u5206\\u7c7b\",\"img_url\":\"http://api.faka.zuy.cn/static/upload/5b5ad8346b8f7/5b5ad8346b933.png\",\"function_id\":\"0\",\"sort\":0,\"is_show\":true,\"function_links\":\"cc.zuy.faka_android.ui.activity.goods.GoodsGenreActivity\",\"iOS_ViewType\":\"1\",\"iOS_sotryBoard\":\"Home\",\"iOS_ViewController\":\"ClassifyListViewController\",\"android_Ver\":\"1.0\",\"iOS_Ver\":\"1.0\",\"wxapp_page\":\"/pages/goodsCategory/lists/lists\"}'),
(2, 1, '{\"title\":\"\\u6dfb\\u52a0\\u5546\\u54c1\",\"img_url\":\"http://api.faka.zuy.cn/static/upload/5b5ad8d9671f5/5b5ad8d96722e.png\",\"function_id\":\"1\",\"sort\":0,\"is_show\":true,\"function_links\":\"cc.zuy.faka_android.ui.activity.goods.AddGoodsActivity\",\"iOS_ViewType\":\"1\",\"iOS_sotryBoard\":\"Home\",\"iOS_ViewController\":\"AddGoodsViewController\",\"android_Ver\":\"1.0\",\"iOS_Ver\":\"1.0\",\"wxapp_page\":\"/pages/goods/add/add\"}'),
(3, 2, '{\"title\":\"\\u5546\\u54c1\\u5217\\u8868\",\"img_url\":\"http:\\/\\/api.faka.zuy.cn\\/static\\/upload\\/5b5ad920eed40\\/5b5ad920eed7a.png\",\"function_id\":\"2\",\"sort\":0,\"is_show\":true,\"function_links\":\"cc.zuy.faka_android.ui.activity.goods.GoodsListActivity\",\"iOS_ViewType\":\"1\",\"iOS_sotryBoard\":\"Home\",\"iOS_ViewController\":\"GoodsListViewController\",\"android_Ver\":\"1.0\",\"iOS_Ver\":\"1.0\",\"wxapp_page\":\"/pages/goods/lists/lists\"}'),
(4, 3, '{\"title\":\"\\u5361\\u5bc6\\u5217\\u8868\",\"img_url\":\"http:\\/\\/api.faka.zuy.cn\\/static\\/upload\\/5b5ada3c8c52f\\/5b5ada3c8c568.png\",\"function_id\":\"5\",\"sort\":0,\"is_show\":true,\"function_links\":\"cc.zuy.faka_android.ui.activity.menu.CardListActivity\",\"iOS_ViewType\":\"1\",\"iOS_sotryBoard\":\"Cards\",\"iOS_ViewController\":\"CardListViewController\",\"android_Ver\":\"1.0\",\"iOS_Ver\":\"1.0\",\"wxapp_page\":\"/pages/goodsCard/lists/lists\"}'),
(5, 4, '{\"title\":\"\\u5e97\\u94fa\\u94fe\\u63a5\",\"img_url\":\"http:\\/\\/api.faka.zuy.cn\\/static\\/upload\\/5b5adc07170e2\\/5b5adc071711c.png\",\"function_id\":\"3\",\"sort\":0,\"is_show\":true,\"function_links\":\"cc.zuy.faka_android.ui.activity.goods.StoreLinksActivity\",\"iOS_ViewType\":\"1\",\"iOS_sotryBoard\":\"Home\",\"iOS_ViewController\":\"StoreLinkViewController\",\"android_Ver\":\"1.0\",\"iOS_Ver\":\"1.0\",\"wxapp_page\":\"/pages/user/link/link\"}'),
(6, 5, '{\"title\":\"\\u6dfb\\u52a0\\u5361\\u5bc6\",\"img_url\":\"http:\\/\\/api.faka.zuy.cn\\/static\\/upload\\/5b5adc6b692fb\\/5b5adc6b69335.png\",\"function_id\":\"4\",\"sort\":0,\"is_show\":true,\"function_links\":\"cc.zuy.faka_android.ui.activity.menu.CardAddActivity\",\"iOS_ViewType\":\"1\",\"iOS_sotryBoard\":\"Cards\",\"iOS_ViewController\":\"AddCardsViewController\",\"android_Ver\":\"1.0\",\"iOS_Ver\":\"1.0\",\"wxapp_page\":\"/pages/goodsCard/edit/edit\"}'),
(7, 6, '{\"title\":\"\\u8ba2\\u5355\\u5217\\u8868\",\"img_url\":\"http:\\/\\/api.faka.zuy.cn\\/static\\/upload\\/5b5adce9bdd2e\\/5b5adce9bdd68.png\",\"function_id\":\"6\",\"sort\":0,\"is_show\":true,\"function_links\":\"cc.zuy.faka_android.ui.activity.order.OrderListActivity\",\"iOS_ViewType\":\"1\",\"iOS_sotryBoard\":\"Order\",\"iOS_ViewController\":\"OrderListViewController\",\"android_Ver\":\"1.0\",\"iOS_Ver\":\"1.0\",\"wxapp_page\":\"/pages/order/lists/lists\"}'),
(8, 7, '{\"title\":\"\\u4f18\\u60e0\\u5238\",\"img_url\":\"http:\\/\\/api.faka.zuy.cn\\/static\\/upload\\/5b5add2b1ed3c\\/5b5add2b1ed75.png\",\"function_id\":\"7\",\"sort\":0,\"is_show\":true,\"function_links\":\"cc.zuy.faka_android.ui.activity.menu.CouponsActivity\",\"iOS_ViewType\":\"1\",\"iOS_sotryBoard\":\"Coupon\",\"iOS_ViewController\":\"CouponListViewController\",\"android_Ver\":\"1.0\",\"iOS_Ver\":\"1.0\",\"wxapp_page\":\"/pages/goodsCoupon/lists/lists\"}'),
(9, 8, '{\"title\":\"\\u63d0\\u73b0\\u7ba1\\u7406\",\"img_url\":\"http:\\/\\/api.faka.zuy.cn\\/static\\/upload\\/5b5add6aafc89\\/5b5add6aafcc3.png\",\"function_id\":\"8\",\"sort\":0,\"is_show\":true,\"function_links\":\"cc.zuy.faka_android.ui.activity.menu.ApplyMoneyListActivity\",\"iOS_ViewType\":\"1\",\"iOS_sotryBoard\":\"Home\",\"iOS_ViewController\":\"EnchashmentListViewController\",\"android_Ver\":\"1.0\",\"iOS_Ver\":\"1.0\",\"wxapp_page\":\"/pages/user/cashList/cashList\"}'),
(10, 9, '{\"title\":\"\\u4ed8\\u6b3e\\u65b9\\u5f0f\",\"img_url\":\"http:\\/\\/api.faka.zuy.cn\\/static\\/upload\\/5b5addb00d1b5\\/5b5addb00d1ee.png\",\"function_id\":\"9\",\"sort\":0,\"is_show\":true,\"function_links\":\"cc.zuy.faka_android.ui.activity.menu.PayWayActivity\",\"iOS_ViewType\":\"1\",\"iOS_sotryBoard\":\"Home\",\"iOS_ViewController\":\"PayTypeListViewController\",\"android_Ver\":\"1.0\",\"iOS_Ver\":\"1.0\",\"wxapp_page\":\"/pages/pay/lists/lists\"}'),
(11, 10, '{\"title\":\"\\u5361\\u5bc6\\u56de\\u6536\",\"img_url\":\"http:\\/\\/api.faka.zuy.cn\\/static\\/upload\\/5b5addcf3fab3\\/5b5addcf3faec.png\",\"function_id\":\"10\",\"sort\":0,\"is_show\":true,\"function_links\":\"cc.zuy.faka_android.ui.activity.menu.RecycleBinActivity\",\"iOS_ViewType\":\"1\",\"iOS_sotryBoard\":\"Cards\",\"iOS_ViewController\":\"CardRecycleViewController\",\"android_Ver\":\"1.0\",\"iOS_Ver\":\"1.0\",\"wxapp_page\":\"/pages/goodsCard/trash/trash\"}'),
(12, 11, '{\"title\":\"\\u6536\\u76ca\\u5206\\u6790\",\"img_url\":\"http:\\/\\/api.faka.zuy.cn\\/static\\/upload\\/5b5addf6a6548\\/5b5addf6a6581.png\",\"function_id\":\"11\",\"sort\":0,\"is_show\":true,\"function_links\":\"cc.zuy.faka_android.ui.activity.menu.IncomeAnalysisActivity\",\"iOS_ViewType\":\"1\",\"iOS_sotryBoard\":\"Order\",\"iOS_ViewController\":\"InComeAnalyzeViewController\",\"android_Ver\":\"1.0\",\"iOS_Ver\":\"1.0\",\"wxapp_page\":\"/pages/user/statics/statics\"}'),
(13, 12, '{\"title\":\"\\u6e20\\u9053\\u5206\\u6790\",\"img_url\":\"http:\\/\\/api.faka.zuy.cn\\/static\\/upload\\/5b5ade8db3465\\/5b5ade8db349e.png\",\"function_id\":\"12\",\"sort\":0,\"is_show\":true,\"function_links\":\"cc.zuy.faka_android.ui.activity.menu.ChannelAnalysisActivity\",\"iOS_ViewType\":\"1\",\"iOS_sotryBoard\":\"Home\",\"iOS_ViewController\":\"ChannelAnalysisViewController\",\"android_Ver\":\"1.0\",\"iOS_Ver\":\"1.0\",\"wxapp_page\":\"/pages/pay/detail/detail\"}'),
(16, 14, '{\"title\":\"\\u767b\\u5f55\\u65e5\\u5fd7\",\"img_url\":\"http:\\/\\/api.faka.zuy.cn\\/static\\/upload\\/5b5b4e6037138\\/5b5b4e6037171.png\",\"function_id\":\"14\",\"sort\":0,\"is_show\":true,\"function_links\":\"cc.zuy.faka_android.ui.activity.menu.LoginDiaryActivity\",\"iOS_ViewType\":\"1\",\"iOS_sotryBoard\":\"Home\",\"iOS_ViewController\":\"LoginDiaryViewController\",\"android_Ver\":\"1.0\",\"iOS_Ver\":\"1.0\",\"wxapp_page\":\"/pages/user/loginLogs/loginLogs\"}'),
(17, 15, '{\"title\":\"\\u63a8\\u5e7f\\u7ba1\\u7406\",\"img_url\":\"http:\\/\\/api.faka.zuy.cn\\/static\\/upload\\/5b5bccc8c9960\\/5b5bccc8c99a0.png\",\"function_id\":\"15\",\"sort\":0,\"is_show\":true,\"function_links\":\"cc.zuy.faka_android.ui.activity.menu.SpreadManageActivity\",\"iOS_ViewType\":\"1\",\"iOS_sotryBoard\":\"Home\",\"iOS_ViewController\":\"PromoteViewController\",\"android_Ver\":\"1.0\",\"iOS_Ver\":\"1.0\",\"wxapp_page\":\"/pages/spread/lists/lists\"}'),
(18, 16, '{\"title\":\"\\u6295\\u8bc9\\u7ba1\\u7406\",\"img_url\":\"http:\\/\\/api.faka.zuy.cn\\/static\\/upload\\/5b5bce9b6e4ed\\/5b5bce9b6e526.png\",\"function_id\":\"16\",\"sort\":0,\"is_show\":true,\"function_links\":\"cc.zuy.faka_android.ui.activity.menu.ComplainActivity\",\"iOS_ViewType\":\"1\",\"iOS_sotryBoard\":\"Home\",\"iOS_ViewController\":\"ComplainViewController\",\"android_Ver\":\"1.0\",\"iOS_Ver\":\"1.0\",\"wxapp_page\":\"/pages/complaint/lists/lists\"}');

-- --------------------------------------------------------

--
-- 表的结构 `app_version`
--

CREATE TABLE `app_version` (
  `id` int(11) NOT NULL,
  `platform` enum('android','ios') NOT NULL COMMENT '平台',
  `package` varchar(255) NOT NULL COMMENT '安装包下载地址',
  `create_at` int(10) NOT NULL COMMENT '发布时间',
  `version` varchar(255) NOT NULL COMMENT '安装包版本',
  `remark` text NOT NULL COMMENT '更新说明',
  `create_ip` varchar(255) NOT NULL COMMENT '上传 IP'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='客户端版本';

-- --------------------------------------------------------

--
-- 表的结构 `article`
--

CREATE TABLE `article` (
  `id` int(10) UNSIGNED NOT NULL,
  `cate_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `title` varchar(50) NOT NULL DEFAULT '',
  `title_img` varchar(255) NOT NULL DEFAULT '' COMMENT '标题图',
  `description` text NOT NULL COMMENT '文章描述',
  `content` text NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0',
  `views` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `create_at` int(10) UNSIGNED NOT NULL,
  `update_at` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `is_system` tinyint(4) NOT NULL DEFAULT '0' COMMENT '1表示系统调用到的页面，禁止删除',
  `top` int(10) NOT NULL DEFAULT '0' COMMENT '置顶时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `article`
--

INSERT INTO `article` (`id`, `cate_id`, `title`, `title_img`, `description`, `content`, `status`, `views`, `create_at`, `update_at`, `is_system`, `top`) VALUES
(4, 2, '1.如何入驻自动发卡平台成为商户？', '', '', '&lt;p&gt;通过平台的账户注册功能，即可免费入驻自动发卡平台。&lt;/p&gt;\r\n', 1, 0, 1670584300, 0, 0, 0),
(5, 2, '2.如何登录自动发卡平台商户中心？', '', '', '&lt;p&gt;点击右上角&amp;ldquo;登录&amp;rdquo;按钮进入&lt;/p&gt;\r\n', 1, 0, 1670584383, 0, 0, 0),
(6, 2, '3.如何在平台上销售卡密商品？', '', '', '&lt;p&gt;商户添加的每个卡密商品，平台都会分配一个购买链接，商户只要将这个链接发送给买家，买家付款后平台自动发货，即可完成本次交易。&lt;/p&gt;\r\n', 1, 0, 1670584462, 0, 0, 0),
(7, 2, '4.平台商户可以卖些什么？', '', '', '&lt;p&gt;可以出售卡密商品（例如软件注册码，论坛帐号，等等），不可以卖实物商品（例如衣服，水果，等等）。&lt;/p&gt;\r\n', 1, 0, 1670584548, 0, 0, 0),
(8, 2, '5.账户的金额满多少自动结算？', '', '', '&lt;p&gt;商户账户金额满100元，当天晚上12点后，系统自动帮您提现，平台将于次日12点前，结算到您预留的账户，账户金额不满100元，可以申请手动提现。&lt;/p&gt;\r\n', 1, 0, 1670584673, 0, 0, 0),
(9, 2, '6.平台结算方式有哪些？', '', '', '&lt;p&gt;共有三种结算方式，支付宝、微信和银行卡。&lt;/p&gt;\r\n', 1, 0, 1670584745, 0, 0, 0),
(10, 2, '7.如果买家已经付款，但是TA说没有收到卡密该怎么办？', '', '', '&lt;p&gt;请直接联系平台客服解决。&lt;/p&gt;\r\n', 1, 0, 1670584806, 0, 0, 0),
(11, 2, '8.自动发卡平台安全吗？', '', '', '&lt;p&gt;平台拥有完善的安全监测系统，可以及时发现网站的非正常访问，并且做出相应的安全响应操作。平台将会采取一切先进的技术手段，同时完善各项安全检测措施，全力保护在平台中存储的个人信息、账户信息以及交易记录的数据安全。 &lt;span style=&quot;display: none;&quot;&gt;&amp;nbsp;&lt;/span&gt;&lt;/p&gt;\r\n', 1, 0, 1670584861, 0, 0, 0),
(12, 3, '平台禁售商品类目', '', '', '&lt;table&gt;\r\n	&lt;colgroup&gt;\r\n		&lt;col width=&quot;17%&quot; /&gt;\r\n		&lt;col width=&quot;22%&quot; /&gt;\r\n		&lt;col width=&quot;61%&quot; /&gt;\r\n	&lt;/colgroup&gt;\r\n	&lt;tbody&gt;\r\n		&lt;tr&gt;\r\n			&lt;td align=&quot;center&quot; rowspan=&quot;4&quot;&gt;政治类&lt;/td&gt;\r\n			&lt;td align=&quot;center&quot; rowspan=&quot;2&quot;&gt;反动信息&lt;/td&gt;\r\n			&lt;td align=&quot;left&quot;&gt;含有反动、破坏国家统一、破坏主权及领土完整、破坏社会稳定，涉及国家机密、扰乱社会秩序，宣扬邪教迷信，宣扬宗教、种族歧视、藏独、法轮功、违反伦理等信息，或法律法规禁止出版发行的书籍、音像制品、视频、文件资料等；&lt;/td&gt;\r\n		&lt;/tr&gt;\r\n		&lt;tr&gt;\r\n			&lt;td align=&quot;left&quot;&gt;其他反动物品及言论等；&lt;/td&gt;\r\n		&lt;/tr&gt;\r\n		&lt;tr&gt;\r\n			&lt;td align=&quot;center&quot; rowspan=&quot;2&quot;&gt;政治物品&lt;/td&gt;\r\n			&lt;td align=&quot;left&quot;&gt;国家机密文件资料等；&lt;/td&gt;\r\n		&lt;/tr&gt;\r\n		&lt;tr&gt;\r\n			&lt;td align=&quot;left&quot;&gt;带有宗教歧视、种族歧视的相关商品或信息。&lt;/td&gt;\r\n		&lt;/tr&gt;\r\n		&lt;tr&gt;\r\n			&lt;td align=&quot;center&quot; rowspan=&quot;2&quot;&gt;军警类&lt;/td&gt;\r\n			&lt;td align=&quot;center&quot;&gt;军火武器/枪械及配件&lt;/td&gt;\r\n			&lt;td align=&quot;left&quot;&gt;枪支、弹药、军火及其相关器材、配件、附属产品，仿制品的衍生工艺品等；包括枪瞄、枪套等枪支配件以及90%大小相似的仿真枪；&lt;/td&gt;\r\n		&lt;/tr&gt;\r\n		&lt;tr&gt;\r\n			&lt;td align=&quot;center&quot;&gt;军警用品&lt;/td&gt;\r\n			&lt;td align=&quot;left&quot;&gt;冒用警用、军用制服、标志、设备及制品；带有警用标志（警徽）物品；描述为军队、警队使用物品；&lt;/td&gt;\r\n		&lt;/tr&gt;\r\n		&lt;tr&gt;\r\n			&lt;td align=&quot;center&quot; rowspan=&quot;8&quot;&gt;治安类（危险品）&lt;/td&gt;\r\n			&lt;td align=&quot;center&quot;&gt;危险品&lt;/td&gt;\r\n			&lt;td align=&quot;left&quot;&gt;烟花爆竹、易燃、易爆物品；介绍制作易燃易爆品方法的相关教程、书籍。&lt;/td&gt;\r\n		&lt;/tr&gt;\r\n		&lt;tr&gt;\r\n			&lt;td align=&quot;center&quot; rowspan=&quot;2&quot;&gt;危险武器&lt;/td&gt;\r\n			&lt;td align=&quot;left&quot;&gt;可致使他人暂时失去反抗能力，对他人身体造成重大伤害的管制器具；&lt;/td&gt;\r\n		&lt;/tr&gt;\r\n		&lt;tr&gt;\r\n			&lt;td align=&quot;left&quot;&gt;甩棍、电棍、电击棍等危险物品；&lt;/td&gt;\r\n		&lt;/tr&gt;\r\n		&lt;tr&gt;\r\n			&lt;td align=&quot;center&quot; rowspan=&quot;2&quot;&gt;危险化学品&lt;/td&gt;\r\n			&lt;td align=&quot;left&quot;&gt;剧毒化学品；&lt;/td&gt;\r\n		&lt;/tr&gt;\r\n		&lt;tr&gt;\r\n			&lt;td align=&quot;left&quot;&gt;国家名录中禁止出售的危险化学品（剧毒化学品除外）；&lt;/td&gt;\r\n		&lt;/tr&gt;\r\n		&lt;tr&gt;\r\n			&lt;td align=&quot;center&quot; rowspan=&quot;2&quot;&gt;剧毒物品&lt;/td&gt;\r\n			&lt;td align=&quot;left&quot;&gt;毒品、制毒原料、制毒化学品及致瘾性药物；&lt;/td&gt;\r\n		&lt;/tr&gt;\r\n		&lt;tr&gt;\r\n			&lt;td align=&quot;left&quot;&gt;毒品吸食工具及配件；&lt;/td&gt;\r\n		&lt;/tr&gt;\r\n		&lt;tr&gt;\r\n			&lt;td align=&quot;center&quot;&gt;毒品、毒品检测工具&lt;/td&gt;\r\n			&lt;td align=&quot;left&quot;&gt;毒品检测试剂准入只允许有资质的戒毒所机构。&lt;/td&gt;\r\n		&lt;/tr&gt;\r\n		&lt;tr&gt;\r\n			&lt;td align=&quot;center&quot; rowspan=&quot;8&quot;&gt;黄赌毒类&lt;/td&gt;\r\n			&lt;td align=&quot;center&quot; rowspan=&quot;3&quot;&gt;黄色低俗色情服务及信息&lt;/td&gt;\r\n			&lt;td align=&quot;left&quot;&gt;含有色情淫秽内容的音像制品及视频；色情陪聊服务；成人网站论坛的帐号及邀请码；&lt;/td&gt;\r\n		&lt;/tr&gt;\r\n		&lt;tr&gt;\r\n			&lt;td align=&quot;left&quot;&gt;用于传播色情信息的软件及图片；含有情色、暴力、低俗内容的音像制品；原味内衣及相关产品；&lt;/td&gt;\r\n		&lt;/tr&gt;\r\n		&lt;tr&gt;\r\n			&lt;td align=&quot;left&quot;&gt;含有情色、暴力、低俗内容的动漫、读物、游戏和图片；&lt;/td&gt;\r\n		&lt;/tr&gt;\r\n		&lt;tr&gt;\r\n			&lt;td align=&quot;center&quot;&gt;赌博器具&lt;/td&gt;\r\n			&lt;td align=&quot;left&quot;&gt;如电子老虎机、百家乐桌子，或者百家乐的筹码等；&lt;/td&gt;\r\n		&lt;/tr&gt;\r\n		&lt;tr&gt;\r\n			&lt;td align=&quot;center&quot;&gt;赌博/博彩服务&lt;/td&gt;\r\n			&lt;td align=&quot;left&quot;&gt;棋牌类网站，游戏货币能直接兑换现金。或者棋牌类网站，游戏货币能直接通过账户流转，且有专门银商收购贩卖游戏币。如：捕鱼游戏、金鲨银鲨、彩金、牛牛等；&lt;/td&gt;\r\n		&lt;/tr&gt;\r\n		&lt;tr&gt;\r\n			&lt;td align=&quot;center&quot;&gt;私彩&lt;/td&gt;\r\n			&lt;td align=&quot;left&quot;&gt;私彩是指私人坐庄，由个人或组织发行的，以诈取钱财为目的的非法彩票。一般以公益彩票的开奖结果进行赌博，骗取高额利润，如地下六合彩，以香港六合彩的开奖号码进行变相赌博，以1赔40左右的高额赔率欺骗民众；&lt;/td&gt;\r\n		&lt;/tr&gt;\r\n		&lt;tr&gt;\r\n			&lt;td align=&quot;center&quot;&gt;毒品、吸毒工具、毒品检测工具&lt;/td&gt;\r\n			&lt;td align=&quot;left&quot;&gt;毒品检测试剂准入只允许有资质的戒毒所机构；毒品检测工具，不允许出售。&lt;/td&gt;\r\n		&lt;/tr&gt;\r\n		&lt;tr&gt;\r\n			&lt;td align=&quot;center&quot;&gt;其他黄色低俗物品或服务&lt;/td&gt;\r\n			&lt;td align=&quot;left&quot;&gt;泡妞秘籍、恋足、恋童、人体摄影、丝袜、原味、捆绑等带有不正当引导倾向的；可致使他人暂时失去反抗能力、意识模糊的口服或外用的催情类商品及人造处女膜。&lt;/td&gt;\r\n		&lt;/tr&gt;\r\n		&lt;tr&gt;\r\n			&lt;td align=&quot;center&quot; rowspan=&quot;7&quot;&gt;侵害隐私类&lt;/td&gt;\r\n			&lt;td align=&quot;center&quot; rowspan=&quot;4&quot;&gt;间谍器材&lt;/td&gt;\r\n			&lt;td align=&quot;left&quot;&gt;用于监听、窃取隐私或机密的软件及设备；&lt;/td&gt;\r\n		&lt;/tr&gt;\r\n		&lt;tr&gt;\r\n			&lt;td align=&quot;left&quot;&gt;用于非法摄像、录音、取证等用途的设备；&lt;/td&gt;\r\n		&lt;/tr&gt;\r\n		&lt;tr&gt;\r\n			&lt;td align=&quot;left&quot;&gt;身份证及身份证验证、阅读设备；&lt;/td&gt;\r\n		&lt;/tr&gt;\r\n		&lt;tr&gt;\r\n			&lt;td align=&quot;left&quot;&gt;盗取或破解账号密码的软件、工具、教程及产物。&lt;/td&gt;\r\n		&lt;/tr&gt;\r\n		&lt;tr&gt;\r\n			&lt;td align=&quot;center&quot; rowspan=&quot;2&quot;&gt;身份信息等其他侵犯个人隐私的信息&lt;/td&gt;\r\n			&lt;td align=&quot;left&quot;&gt;个人隐私信息及企业内部数据买卖；提供个人手机定位、电话清单查询、银行账户查询等服务；&lt;/td&gt;\r\n		&lt;/tr&gt;\r\n		&lt;tr&gt;\r\n			&lt;td align=&quot;left&quot;&gt;出售、转让、回收，包括已作废或者作为收藏用途的银行卡；&lt;/td&gt;\r\n		&lt;/tr&gt;\r\n		&lt;tr&gt;\r\n			&lt;td align=&quot;center&quot;&gt;其他危害隐私的物品或服务&lt;/td&gt;\r\n			&lt;td align=&quot;left&quot;&gt;如间谍服务，私人侦探等。&lt;/td&gt;\r\n		&lt;/tr&gt;\r\n		&lt;tr&gt;\r\n			&lt;td align=&quot;center&quot; rowspan=&quot;6&quot;&gt;医药器械类&lt;/td&gt;\r\n			&lt;td align=&quot;center&quot;&gt;麻醉药品和精神类药品&lt;/td&gt;\r\n			&lt;td align=&quot;left&quot;&gt;精神类、麻醉类、有毒类、放射类、兴奋剂类、计生类药品；非药品添加药品成分；国家公示已查处、药品监督管理局认定禁止生产、使用的药品；用于预防、治疗人体疾病的药物、血液制品或医疗器械；&lt;/td&gt;\r\n		&lt;/tr&gt;\r\n		&lt;tr&gt;\r\n			&lt;td align=&quot;center&quot;&gt;毒性药品&lt;/td&gt;\r\n			&lt;td align=&quot;left&quot;&gt;灭鼠药、蟑螂药、兽药不做限制&lt;/td&gt;\r\n		&lt;/tr&gt;\r\n		&lt;tr&gt;\r\n			&lt;td align=&quot;center&quot;&gt;处方药&lt;/td&gt;\r\n			&lt;td align=&quot;left&quot;&gt;参照《互联网药品交易服务审批暂行规定》&lt;/td&gt;\r\n		&lt;/tr&gt;\r\n		&lt;tr&gt;\r\n			&lt;td align=&quot;center&quot;&gt;无批号药品&lt;/td&gt;\r\n			&lt;td align=&quot;left&quot;&gt;未经药品监督管理部门批准生产、进口，或未经检验即销售的药品&lt;/td&gt;\r\n		&lt;/tr&gt;\r\n		&lt;tr&gt;\r\n			&lt;td align=&quot;center&quot;&gt;胎儿性别鉴定&lt;/td&gt;\r\n			&lt;td align=&quot;left&quot;&gt;违反国家及世界卫生组织的人道精神，任何涉及胎儿性别鉴定商品、服务；&lt;/td&gt;\r\n		&lt;/tr&gt;\r\n		&lt;tr&gt;\r\n			&lt;td align=&quot;center&quot;&gt;成人药品（春药）&lt;/td&gt;\r\n			&lt;td align=&quot;left&quot;&gt;口服催情类药品。&lt;/td&gt;\r\n		&lt;/tr&gt;\r\n		&lt;tr&gt;\r\n			&lt;td align=&quot;center&quot; rowspan=&quot;7&quot;&gt;国家保护动植物&lt;/td&gt;\r\n			&lt;td align=&quot;center&quot; rowspan=&quot;7&quot;&gt;国家保护动植物&lt;/td&gt;\r\n			&lt;td align=&quot;left&quot;&gt;人体器官、遗体；&lt;/td&gt;\r\n		&lt;/tr&gt;\r\n		&lt;tr&gt;\r\n			&lt;td align=&quot;left&quot;&gt;国家重点保护类动物、濒危动物的活体、内脏、任何肢体、皮毛、标本或其他制成品，已灭绝动物与现有国家二级以上保护动物的化石。&lt;/td&gt;\r\n		&lt;/tr&gt;\r\n		&lt;tr&gt;\r\n			&lt;td align=&quot;left&quot;&gt;国家保护类植物活体（树苗除外）；&lt;/td&gt;\r\n		&lt;/tr&gt;\r\n		&lt;tr&gt;\r\n			&lt;td align=&quot;left&quot;&gt;国家保护的有益的或者有重要经济、科学研究价值的陆生野生动物的活体、内脏、任何肢体、皮毛、标本或其他制成品；&lt;/td&gt;\r\n		&lt;/tr&gt;\r\n		&lt;tr&gt;\r\n			&lt;td align=&quot;left&quot;&gt;捕鱼器相关设备及配件；&lt;/td&gt;\r\n		&lt;/tr&gt;\r\n		&lt;tr&gt;\r\n			&lt;td align=&quot;left&quot;&gt;其他动物捕杀工具；&lt;/td&gt;\r\n		&lt;/tr&gt;\r\n		&lt;tr&gt;\r\n			&lt;td align=&quot;left&quot;&gt;猫狗肉、猫狗皮毛、鱼翅、熊胆及其制品；&lt;/td&gt;\r\n		&lt;/tr&gt;\r\n		&lt;tr&gt;\r\n			&lt;td align=&quot;center&quot; rowspan=&quot;26&quot;&gt;虚拟类&lt;/td&gt;\r\n			&lt;td align=&quot;center&quot; rowspan=&quot;10&quot;&gt;网络服务&lt;/td&gt;\r\n			&lt;td align=&quot;left&quot;&gt;未经国家备案的网络游戏、游戏点卡、货币等相关服务类商品；&lt;/td&gt;\r\n		&lt;/tr&gt;\r\n		&lt;tr&gt;\r\n			&lt;td align=&quot;left&quot;&gt;外挂、私服相关的网游类商品；&lt;/td&gt;\r\n		&lt;/tr&gt;\r\n		&lt;tr&gt;\r\n			&lt;td align=&quot;left&quot;&gt;官方已停止经营的游戏点卡或平台卡商品；&lt;/td&gt;\r\n		&lt;/tr&gt;\r\n		&lt;tr&gt;\r\n			&lt;td align=&quot;left&quot;&gt;时间不可查询的虚拟服务类商品；&lt;/td&gt;\r\n		&lt;/tr&gt;\r\n		&lt;tr&gt;\r\n			&lt;td align=&quot;left&quot;&gt;网络账户死保帐号以及腾讯QQ帐号；&lt;/td&gt;\r\n		&lt;/tr&gt;\r\n		&lt;tr&gt;\r\n			&lt;td align=&quot;left&quot;&gt;Itunes帐号及用户充值类商品；&lt;/td&gt;\r\n		&lt;/tr&gt;\r\n		&lt;tr&gt;\r\n			&lt;td align=&quot;left&quot;&gt;各类短信、邮件、QQ/微信群发设备、软件及服务，如短信轰炸机；&lt;/td&gt;\r\n		&lt;/tr&gt;\r\n		&lt;tr&gt;\r\n			&lt;td align=&quot;left&quot;&gt;虚拟代刷、炒信、恶意刷店铺流量等服务类商品；&lt;/td&gt;\r\n		&lt;/tr&gt;\r\n		&lt;tr&gt;\r\n			&lt;td align=&quot;left&quot;&gt;黑客相关&lt;/td&gt;\r\n		&lt;/tr&gt;\r\n		&lt;tr&gt;\r\n			&lt;td align=&quot;left&quot;&gt;涉外婚介&lt;/td&gt;\r\n		&lt;/tr&gt;\r\n		&lt;tr&gt;\r\n			&lt;td align=&quot;center&quot; rowspan=&quot;5&quot;&gt;电信欺诈&lt;/td&gt;\r\n			&lt;td align=&quot;left&quot;&gt;不可查询的分期返还话费类商品；&lt;/td&gt;\r\n		&lt;/tr&gt;\r\n		&lt;tr&gt;\r\n			&lt;td align=&quot;left&quot;&gt;不限时间与流量的、时间不可查询的以及被称为漏洞卡、集团卡、内部卡、测试卡的上网资费卡或资费套餐及SIM 卡；&lt;/td&gt;\r\n		&lt;/tr&gt;\r\n		&lt;tr&gt;\r\n			&lt;td align=&quot;left&quot;&gt;一卡多号；有蹭网功能的无线网卡，以及描述信息中有告知会员能用于蹭网的设备；&lt;/td&gt;\r\n		&lt;/tr&gt;\r\n		&lt;tr&gt;\r\n			&lt;td align=&quot;left&quot;&gt;带破解功能的手机卡贴；&lt;/td&gt;\r\n		&lt;/tr&gt;\r\n		&lt;tr&gt;\r\n			&lt;td align=&quot;left&quot;&gt;SP业务自消费类商品；&lt;/td&gt;\r\n		&lt;/tr&gt;\r\n		&lt;tr&gt;\r\n			&lt;td align=&quot;center&quot; rowspan=&quot;6&quot;&gt;非法服务、票证&lt;/td&gt;\r\n			&lt;td align=&quot;left&quot;&gt;伪造变造国家机关或特定机构颁发的文件、证书、公章、防伪标签等，仅限国家机关或特定机构方可提供的服务；&lt;/td&gt;\r\n		&lt;/tr&gt;\r\n		&lt;tr&gt;\r\n			&lt;td align=&quot;left&quot;&gt;伪造各类公章、图章扰乱市场业务；&lt;/td&gt;\r\n		&lt;/tr&gt;\r\n		&lt;tr&gt;\r\n			&lt;td align=&quot;left&quot;&gt;尚可使用或用于报销的票据（及服务）,尚可使用的外贸单证以及代理报关、清单、商检、单证手续的服务；&lt;/td&gt;\r\n		&lt;/tr&gt;\r\n		&lt;tr&gt;\r\n			&lt;td align=&quot;left&quot;&gt;未公开发行的国家级正式考试答案；&lt;/td&gt;\r\n		&lt;/tr&gt;\r\n		&lt;tr&gt;\r\n			&lt;td align=&quot;left&quot;&gt;代写论文、代考试类相关服务；&lt;/td&gt;\r\n		&lt;/tr&gt;\r\n		&lt;tr&gt;\r\n			&lt;td align=&quot;left&quot;&gt;炒作博客人气、炒作网站人气、代投票类商品或信息；&lt;/td&gt;\r\n		&lt;/tr&gt;\r\n		&lt;tr&gt;\r\n			&lt;td align=&quot;center&quot; rowspan=&quot;5&quot;&gt;其他高风险的服务&lt;/td&gt;\r\n			&lt;td align=&quot;left&quot;&gt;弹窗广告虚假中奖信息；&lt;/td&gt;\r\n		&lt;/tr&gt;\r\n		&lt;tr&gt;\r\n			&lt;td align=&quot;left&quot;&gt;友情链接名称涉及禁售不合作内容；&lt;/td&gt;\r\n		&lt;/tr&gt;\r\n		&lt;tr&gt;\r\n			&lt;td align=&quot;left&quot;&gt;模板网站且网站可以打开，只有框架（无商品、无效商品、无帖子）；&lt;/td&gt;\r\n		&lt;/tr&gt;\r\n		&lt;tr&gt;\r\n			&lt;td align=&quot;left&quot;&gt;恶意舞弊投票类型；&lt;/td&gt;\r\n		&lt;/tr&gt;\r\n		&lt;tr&gt;\r\n			&lt;td align=&quot;left&quot;&gt;微交易，云交易类商户；&lt;/td&gt;\r\n		&lt;/tr&gt;\r\n		&lt;tr&gt;\r\n			&lt;td align=&quot;center&quot; rowspan=&quot;20&quot;&gt;金融类&lt;/td&gt;\r\n			&lt;td align=&quot;center&quot;&gt;非法传销&lt;/td&gt;\r\n			&lt;td align=&quot;left&quot;&gt;传销，金融互助平台。虚假宣传致富，出售书籍、碟片、成功学、消费返利、多级分销、发展下线&amp;ldquo;金字塔&amp;rdquo;型提成等；&lt;/td&gt;\r\n		&lt;/tr&gt;\r\n		&lt;tr&gt;\r\n			&lt;td align=&quot;center&quot;&gt;彩票销售&lt;/td&gt;\r\n			&lt;td align=&quot;left&quot;&gt;除O2O类的平台彩票销售；&lt;/td&gt;\r\n		&lt;/tr&gt;\r\n		&lt;tr&gt;\r\n			&lt;td align=&quot;center&quot;&gt;虚拟货币&lt;/td&gt;\r\n			&lt;td align=&quot;left&quot;&gt;虚拟货币，比特币、莱特币、元宝币等虚拟货币交易。&lt;/td&gt;\r\n		&lt;/tr&gt;\r\n		&lt;tr&gt;\r\n			&lt;td align=&quot;center&quot; rowspan=&quot;10&quot;&gt;货币业务&lt;/td&gt;\r\n			&lt;td align=&quot;left&quot;&gt;假币或制造机器；&lt;/td&gt;\r\n		&lt;/tr&gt;\r\n		&lt;tr&gt;\r\n			&lt;td align=&quot;left&quot;&gt;买卖银行账户（银行卡）；&lt;/td&gt;\r\n		&lt;/tr&gt;\r\n		&lt;tr&gt;\r\n			&lt;td align=&quot;left&quot;&gt;非法集资；&lt;/td&gt;\r\n		&lt;/tr&gt;\r\n		&lt;tr&gt;\r\n			&lt;td align=&quot;left&quot;&gt;外汇兑换服务；&lt;/td&gt;\r\n		&lt;/tr&gt;\r\n		&lt;tr&gt;\r\n			&lt;td align=&quot;left&quot;&gt;境外账户中的虚拟货币；&lt;/td&gt;\r\n		&lt;/tr&gt;\r\n		&lt;tr&gt;\r\n			&lt;td align=&quot;left&quot;&gt;期货；&lt;/td&gt;\r\n		&lt;/tr&gt;\r\n		&lt;tr&gt;\r\n			&lt;td align=&quot;left&quot;&gt;有价证券或凭证；&lt;/td&gt;\r\n		&lt;/tr&gt;\r\n		&lt;tr&gt;\r\n			&lt;td align=&quot;left&quot;&gt;P2P等理财类网站；&lt;/td&gt;\r\n		&lt;/tr&gt;\r\n		&lt;tr&gt;\r\n			&lt;td align=&quot;left&quot;&gt;大量流通中的外币及外币兑换服务；&lt;/td&gt;\r\n		&lt;/tr&gt;\r\n		&lt;tr&gt;\r\n			&lt;td align=&quot;left&quot;&gt;借贷平台，融资租赁平台，非实物众筹；&lt;/td&gt;\r\n		&lt;/tr&gt;\r\n		&lt;tr&gt;\r\n			&lt;td align=&quot;center&quot;&gt;二清无牌机构&lt;/td&gt;\r\n			&lt;td align=&quot;left&quot;&gt;未取得《支付业务许可证》、非法开展资金支付结算业务的机构，存在挪用、占用资金的风险；禁止随意变更使用场景和范围，出租、出借、出售接口；&lt;/td&gt;\r\n		&lt;/tr&gt;\r\n		&lt;tr&gt;\r\n			&lt;td align=&quot;center&quot; rowspan=&quot;4&quot;&gt;支付业务&lt;/td&gt;\r\n			&lt;td align=&quot;left&quot;&gt;禁止网上销售POS机具和提供POS收单业务；&lt;/td&gt;\r\n		&lt;/tr&gt;\r\n		&lt;tr&gt;\r\n			&lt;td align=&quot;left&quot;&gt;随意变更使用场景和范围；&lt;/td&gt;\r\n		&lt;/tr&gt;\r\n		&lt;tr&gt;\r\n			&lt;td align=&quot;left&quot;&gt;信用卡套现服务；&lt;/td&gt;\r\n		&lt;/tr&gt;\r\n		&lt;tr&gt;\r\n			&lt;td align=&quot;left&quot;&gt;违规聚合支付业务；&lt;/td&gt;\r\n		&lt;/tr&gt;\r\n		&lt;tr&gt;\r\n			&lt;td align=&quot;center&quot;&gt;非法交易所&lt;/td&gt;\r\n			&lt;td align=&quot;left&quot;&gt;微盘类交易；省级及以上政府批文且经营范围不符；&lt;/td&gt;\r\n		&lt;/tr&gt;\r\n		&lt;tr&gt;\r\n			&lt;td align=&quot;center&quot;&gt;一元购&lt;/td&gt;\r\n			&lt;td align=&quot;left&quot;&gt;购买抽奖码，商户摇奖公布中奖号码，可能获得实物或是贵金属饰品等物品；&lt;/td&gt;\r\n		&lt;/tr&gt;\r\n		&lt;tr&gt;\r\n			&lt;td align=&quot;center&quot;&gt;烟草类&lt;/td&gt;\r\n			&lt;td align=&quot;center&quot;&gt;烟草类&lt;/td&gt;\r\n			&lt;td align=&quot;left&quot;&gt;烟草专卖品及烟草专用机械（线上禁售）。可参照《烟草专用机械名录》（国烟法[2004]）。&lt;/td&gt;\r\n		&lt;/tr&gt;\r\n		&lt;tr&gt;\r\n			&lt;td align=&quot;center&quot; rowspan=&quot;2&quot;&gt;收藏类&lt;/td&gt;\r\n			&lt;td align=&quot;center&quot;&gt;考古文物&lt;/td&gt;\r\n			&lt;td align=&quot;left&quot;&gt;《中华人民共和国文物保护法》第五十一条第五十二条，国有文物不得买卖&lt;/td&gt;\r\n		&lt;/tr&gt;\r\n		&lt;tr&gt;\r\n			&lt;td align=&quot;center&quot;&gt;收藏品&lt;/td&gt;\r\n			&lt;td align=&quot;left&quot;&gt;国家禁止的集邮票品以及未经邮政行业管理部门批准制作的集邮品，以及一九四九年之后发行的包含&amp;ldquo;中华民国&amp;rdquo;字样的邮品。&lt;/td&gt;\r\n		&lt;/tr&gt;\r\n		&lt;tr&gt;\r\n			&lt;td align=&quot;center&quot; rowspan=&quot;3&quot;&gt;商品质量类&lt;/td&gt;\r\n			&lt;td align=&quot;center&quot; rowspan=&quot;3&quot;&gt;假冒伪劣产品&lt;/td&gt;\r\n			&lt;td align=&quot;left&quot;&gt;由不具备生产资质的生产商生产的或不符合国家、地方、行业、企业强制性标准的商品；&lt;/td&gt;\r\n		&lt;/tr&gt;\r\n		&lt;tr&gt;\r\n			&lt;td align=&quot;left&quot;&gt;经权威质检部门或生产商认定、公布或召回的商品，国家明令淘汰或停止销售的商品，过期、失效、变质的商品，以及含有罂粟籽的食品、调味品、护肤品等制成品；&lt;/td&gt;\r\n		&lt;/tr&gt;\r\n		&lt;tr&gt;\r\n			&lt;td align=&quot;left&quot;&gt;商品本身或外包装上所注明的产品标准、认证标志、成份及含量不符合国家规定的商品；&lt;/td&gt;\r\n		&lt;/tr&gt;\r\n		&lt;tr&gt;\r\n			&lt;td align=&quot;center&quot; rowspan=&quot;21&quot;&gt;其他类&lt;/td&gt;\r\n			&lt;td align=&quot;center&quot; rowspan=&quot;8&quot;&gt;非法所得及非法用工具&lt;/td&gt;\r\n			&lt;td align=&quot;left&quot;&gt;走私、盗窃、抢劫等非法所得；&lt;/td&gt;\r\n		&lt;/tr&gt;\r\n		&lt;tr&gt;\r\n			&lt;td align=&quot;left&quot;&gt;赌博用具、考试作弊工具、汽车跑表器材等非法用途工具；&lt;/td&gt;\r\n		&lt;/tr&gt;\r\n		&lt;tr&gt;\r\n			&lt;td align=&quot;left&quot;&gt;摩卡等，蹭网卡、一卡多号的手机卡等&lt;/td&gt;\r\n		&lt;/tr&gt;\r\n		&lt;tr&gt;\r\n			&lt;td align=&quot;left&quot;&gt;卫星信号收发装置及软件；用于无线电信号屏蔽的仪器或设备；&lt;/td&gt;\r\n		&lt;/tr&gt;\r\n		&lt;tr&gt;\r\n			&lt;td align=&quot;left&quot;&gt;撬锁工具、开锁服务及其相关教程、书籍；&lt;/td&gt;\r\n		&lt;/tr&gt;\r\n		&lt;tr&gt;\r\n			&lt;td align=&quot;left&quot;&gt;涉嫌欺诈等非法用途的软件；&lt;/td&gt;\r\n		&lt;/tr&gt;\r\n		&lt;tr&gt;\r\n			&lt;td align=&quot;left&quot;&gt;可能用于逃避交通管理的商品；&lt;/td&gt;\r\n		&lt;/tr&gt;\r\n		&lt;tr&gt;\r\n			&lt;td align=&quot;left&quot;&gt;猫狗肉、猫狗皮毛、鱼翅、熊胆及其制品；&lt;/td&gt;\r\n		&lt;/tr&gt;\r\n		&lt;tr&gt;\r\n			&lt;td align=&quot;center&quot; rowspan=&quot;13&quot;&gt;特殊时期特殊规定&lt;/td&gt;\r\n			&lt;td align=&quot;left&quot;&gt;未经许可的募捐类商品；&lt;/td&gt;\r\n		&lt;/tr&gt;\r\n		&lt;tr&gt;\r\n			&lt;td align=&quot;left&quot;&gt;原油；&lt;/td&gt;\r\n		&lt;/tr&gt;\r\n		&lt;tr&gt;\r\n			&lt;td align=&quot;left&quot;&gt;代孕服务；&lt;/td&gt;\r\n		&lt;/tr&gt;\r\n		&lt;tr&gt;\r\n			&lt;td align=&quot;left&quot;&gt;替考、代考、代写论文服务；&lt;/td&gt;\r\n		&lt;/tr&gt;\r\n		&lt;tr&gt;\r\n			&lt;td align=&quot;left&quot;&gt;未经许可发布的奥林匹克运动会、世界博览会、亚洲运动会等特许商品；&lt;/td&gt;\r\n		&lt;/tr&gt;\r\n		&lt;tr&gt;\r\n			&lt;td align=&quot;left&quot;&gt;邮局包裹、EMS 专递、快递等物流单据凭证及单号；&lt;/td&gt;\r\n		&lt;/tr&gt;\r\n		&lt;tr&gt;\r\n			&lt;td align=&quot;left&quot;&gt;国家补助或无偿发放的不得私自转让的商品；&lt;/td&gt;\r\n		&lt;/tr&gt;\r\n		&lt;tr&gt;\r\n			&lt;td align=&quot;left&quot;&gt;军需、国家机关专供、特供等商品。&lt;/td&gt;\r\n		&lt;/tr&gt;\r\n		&lt;tr&gt;\r\n			&lt;td align=&quot;left&quot;&gt;股权类众筹商户；&lt;/td&gt;\r\n		&lt;/tr&gt;\r\n		&lt;tr&gt;\r\n			&lt;td align=&quot;left&quot;&gt;购物返利类商户；&lt;/td&gt;\r\n		&lt;/tr&gt;\r\n		&lt;tr&gt;\r\n			&lt;td align=&quot;left&quot;&gt;虚假交易；&lt;/td&gt;\r\n		&lt;/tr&gt;\r\n		&lt;tr&gt;\r\n			&lt;td align=&quot;left&quot;&gt;套现行为；&lt;/td&gt;\r\n		&lt;/tr&gt;\r\n		&lt;tr&gt;\r\n			&lt;td align=&quot;left&quot;&gt;其它违反法律法规、社会公序良俗而不宜接入有卡啦的物品或服务&lt;/td&gt;\r\n		&lt;/tr&gt;\r\n	&lt;/tbody&gt;\r\n&lt;/table&gt;\r\n', 1, 8, 1670589651, 0, 0, 0),
(13, 3, '注册协议', '', '', '&lt;p&gt;&lt;b&gt;注册协议&lt;/b&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;strong&gt;【审慎阅读】&lt;/strong&gt;您在申请注册流程中点击同意前，应当认真阅读以下协议。&lt;strong&gt;请您务必审慎阅读、充分理解协议中相关条款内容，其中包括：&lt;/strong&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;1、&lt;strong&gt;与您约定免除或限制责任的条款；&lt;/strong&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;2、&lt;strong&gt;与您约定法律适用和管辖的条款；&lt;/strong&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;3、&lt;strong&gt;其他以粗体下划线标识的重要条款。&lt;/strong&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;如您对协议有任何疑问，可向平台客服咨询。&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;strong&gt;【特别提示】&lt;/strong&gt;当您按照注册页面提示填写信息、阅读并同意协议且完成全部注册程序后，即表示您已充分阅读、理解并接受协议的全部内容。如您因平台服务与自动发卡发生争议的，适用《自动发卡平台服务协议》处理。如您在使用平台服务过程中与其他用户发生争议的，依您与其他用户达成的协议处理。&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;strong&gt;阅读协议的过程中，如果您不同意相关协议或其中任何条款约定，您应立即停止注册程序。&lt;/strong&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;《自动发卡平台服务协议》&lt;/p&gt;\r\n\r\n&lt;p&gt;《法律声明及隐私权政策》&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;a href=&quot;/index/index/content/id/12&quot; target=&quot;_blank&quot;&gt;《平台禁售商品目录》&lt;/a&gt;&lt;/p&gt;\r\n', 1, 0, 1670589672, 0, 1, 0),
(14, 3, '企业资质', '', '', '', 1, 111, 1670583964, 0, 0, 0),
(15, 3, '用户协议', '', '', '&lt;p&gt;用户协议&lt;/p&gt;\r\n', 1, 1, 1670589694, 0, 1, 0);

-- --------------------------------------------------------

--
-- 表的结构 `article_category`
--

CREATE TABLE `article_category` (
  `id` int(10) UNSIGNED NOT NULL,
  `pid` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `path` varchar(1024) NOT NULL,
  `name` varchar(30) NOT NULL DEFAULT '',
  `alias` varchar(30) NOT NULL DEFAULT '',
  `remark` varchar(255) NOT NULL DEFAULT '',
  `status` tinyint(4) NOT NULL DEFAULT '0',
  `create_at` int(10) UNSIGNED NOT NULL,
  `update_at` int(10) UNSIGNED DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `article_category`
--

INSERT INTO `article_category` (`id`, `pid`, `path`, `name`, `alias`, `remark`, `status`, `create_at`, `update_at`) VALUES
(1, 0, '0', '系统公告', 'notice', '系统公告', 1, 1520268395, 0),
(2, 0, '0', '常见问题', 'faq', '常见问题', 1, 1520268562, 0),
(3, 0, '0', '系统单页', 'single', '系统单页禁止删除', 1, 1524220912, 0);

-- --------------------------------------------------------

--
-- 表的结构 `auto_unfreeze`
--

CREATE TABLE `auto_unfreeze` (
  `id` int(11) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT '用户id',
  `money` decimal(10,3) UNSIGNED NOT NULL DEFAULT '0.000' COMMENT '冻结金额',
  `unfreeze_time` int(11) NOT NULL DEFAULT '0' COMMENT '解冻时间',
  `created_at` int(11) NOT NULL DEFAULT '0' COMMENT '更新时间',
  `trade_no` varchar(255) NOT NULL DEFAULT '0' COMMENT '冻结资金来源订单号',
  `status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '冻结资金记录状态，1：可用，-1：不可用（订单申诉中等情况）'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='订单金额T+1日自动解冻表';

--
-- 转存表中的数据 `auto_unfreeze`
--

INSERT INTO `auto_unfreeze` (`id`, `user_id`, `money`, `unfreeze_time`, `created_at`, `trade_no`, `status`) VALUES
(1, 10001, '0.090', 1539014400, 1538990812, 'T1810081726376105', 1),
(2, 10002, '0.980', 1562860800, 1562846534, 'T190711200007200022', 1),
(3, 10002, '0.990', 1562860800, 1562846543, 'T190711194702200079', 1),
(4, 10002, '0.980', 1562860800, 1562846664, 'T190711200401200063', 1),
(5, 10002, '1.000', 1562947200, 1562924705, 'Tc190712174409200012', 1),
(6, 10002, '1.000', 1562947200, 1562924705, 'Tc190712174046200059', 1);

-- --------------------------------------------------------

--
-- 表的结构 `cash`
--

CREATE TABLE `cash` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `type` tinyint(3) UNSIGNED NOT NULL COMMENT '收款产品类型 1支付宝 2微信',
  `collect_info` varchar(1024) NOT NULL DEFAULT '' COMMENT '提现信息',
  `money` decimal(10,2) UNSIGNED NOT NULL COMMENT '提现金额',
  `fee` decimal(10,2) UNSIGNED NOT NULL DEFAULT '0.00' COMMENT '手续费',
  `actual_money` decimal(10,2) UNSIGNED NOT NULL DEFAULT '0.00' COMMENT '实际到账',
  `status` tinyint(3) UNSIGNED NOT NULL DEFAULT '0' COMMENT '状态 0审核中 1审核通过 2审核未通过',
  `create_at` int(10) UNSIGNED NOT NULL COMMENT '创建时间',
  `complete_at` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT '完成时间',
  `collect_img` tinytext COMMENT '收款二维码',
  `auto_cash` tinyint(4) NOT NULL DEFAULT '0' COMMENT '1 表示自动提现',
  `bank_name` varchar(50) NOT NULL DEFAULT '' COMMENT '银行名称',
  `bank_branch` varchar(150) NOT NULL DEFAULT '' COMMENT '开户地址',
  `bank_card` varchar(50) NOT NULL DEFAULT '' COMMENT '银行卡号',
  `realname` varchar(50) NOT NULL DEFAULT '' COMMENT '真实姓名',
  `idcard_number` varchar(50) NOT NULL DEFAULT '' COMMENT '身份证号码',
  `orderid` varchar(50) NOT NULL DEFAULT '' COMMENT '订单号',
  `account` int(11) NOT NULL DEFAULT '0' COMMENT '代付账号',
  `daifu_status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '代付状态（0，未申请，1，已申请）'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `cash_channel`
--

CREATE TABLE `cash_channel` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL COMMENT '代付渠道名',
  `code` varchar(255) NOT NULL COMMENT '代付渠道代码',
  `account_fields` text NOT NULL COMMENT '代付所需的字段',
  `type` tinyint(4) NOT NULL DEFAULT '1' COMMENT '1支付宝，2微信，3银行',
  `status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '渠道状态，0关闭，1开启'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `cash_channel`
--

INSERT INTO `cash_channel` (`id`, `title`, `code`, `account_fields`, `type`, `status`) VALUES
(1, '千游银行卡代付', 'qianyoupay', 'appKey:appKey|screctKey:screctKey', 3, 1),
(2, '千游支付宝代付', 'QianyouAlipay', 'appKey:appKey|screctKey:screctKey', 1, 1);

-- --------------------------------------------------------

--
-- 表的结构 `cash_channel_account`
--

CREATE TABLE `cash_channel_account` (
  `id` int(10) UNSIGNED NOT NULL,
  `channel_id` int(10) UNSIGNED NOT NULL COMMENT '渠道ID',
  `name` varchar(32) NOT NULL DEFAULT '' COMMENT '账户名',
  `params` text NOT NULL COMMENT '参数',
  `status` tinyint(3) UNSIGNED NOT NULL DEFAULT '0' COMMENT '状态 1启用 0禁用'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `channel`
--

CREATE TABLE `channel` (
  `id` int(11) UNSIGNED NOT NULL COMMENT '通道ID',
  `title` varchar(255) NOT NULL DEFAULT '' COMMENT '通道名称',
  `code` varchar(50) NOT NULL DEFAULT '' COMMENT '通道代码',
  `status` tinyint(1) UNSIGNED NOT NULL DEFAULT '0' COMMENT '状态 1 开启 0 关闭',
  `mch_id` varchar(50) NOT NULL DEFAULT '' COMMENT '商户号',
  `signkey` varchar(255) NOT NULL DEFAULT '' COMMENT '签名KEY',
  `appid` varchar(50) NOT NULL DEFAULT '' COMMENT 'APPID(账号)',
  `appsecret` varchar(50) NOT NULL DEFAULT '' COMMENT 'APPSECRET(密钥)',
  `gateway` varchar(255) NOT NULL DEFAULT '' COMMENT '网关地址',
  `return_url` varchar(255) NOT NULL DEFAULT '' COMMENT '页面通知（优先级）',
  `notify_url` varchar(255) NOT NULL DEFAULT '' COMMENT '服务器通知（优先级）',
  `lowrate` decimal(10,4) UNSIGNED NOT NULL DEFAULT '0.0000' COMMENT '充值费率',
  `highrate` decimal(10,4) UNSIGNED NOT NULL DEFAULT '0.0000' COMMENT '封顶费率',
  `costrate` decimal(10,4) UNSIGNED NOT NULL DEFAULT '0.0000' COMMENT '成本费率',
  `accounting_date` tinyint(3) UNSIGNED NOT NULL DEFAULT '1' COMMENT '结算时间 1、D0 2、D1 3、T0 4、T1',
  `max_money` decimal(10,2) UNSIGNED NOT NULL DEFAULT '0.00' COMMENT '单笔限额 0为最高不限额',
  `min_money` decimal(10,2) UNSIGNED NOT NULL DEFAULT '0.00' COMMENT '单笔限额',
  `limit_time` varchar(19) NOT NULL DEFAULT '' COMMENT '限时',
  `account_fields` varchar(1024) NOT NULL DEFAULT '' COMMENT '账户字段',
  `polling` tinyint(1) UNSIGNED NOT NULL DEFAULT '0' COMMENT '接口模式 0单独 1轮询',
  `account_id` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT '账号ID',
  `weight` text COMMENT '权重',
  `updatetime` int(11) UNSIGNED NOT NULL DEFAULT '0' COMMENT '更新时间',
  `paytype` tinyint(1) UNSIGNED NOT NULL DEFAULT '0' COMMENT '支付类型 1 微信扫码 2 微信公众号 3 支付宝扫码 4 支付宝手机 5 网银支付 6',
  `show_name` varchar(255) NOT NULL DEFAULT '' COMMENT '前台展示名称',
  `is_available` tinyint(4) NOT NULL DEFAULT '0' COMMENT '接口可用 0通用 1手机 2电脑',
  `default_fields` varchar(1024) NOT NULL DEFAULT '' COMMENT '字段默认值',
  `applyurl` varchar(255) NOT NULL DEFAULT '' COMMENT '申请地址',
  `is_install` tinyint(4) NOT NULL DEFAULT '0',
  `sort` int(10) NOT NULL DEFAULT '0' COMMENT '渠道排序'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='支付供应商';

--
-- 转存表中的数据 `channel`
--

INSERT INTO `channel` (`id`, `title`, `code`, `status`, `mch_id`, `signkey`, `appid`, `appsecret`, `gateway`, `return_url`, `notify_url`, `lowrate`, `highrate`, `costrate`, `accounting_date`, `max_money`, `min_money`, `limit_time`, `account_fields`, `polling`, `account_id`, `weight`, `updatetime`, `paytype`, `show_name`, `is_available`, `default_fields`, `applyurl`, `is_install`, `sort`) VALUES
(58, '支付宝扫码（官方）', 'AlipayScan', 1, '', '', '', '', '', '', '', '0.0300', '0.0000', '0.0040', 1, '0.00', '0.00', '', 'alipay_public_key:alipay_public_key|merchant_private_key:merchant_private_key|app_id:app_id|防封域名（可选）:refer', 0, 4, '[]', 0, 3, '', 2, 'charset=UTF-8', 'https://open.alipay.com/platform/homeRoleSelection.htm', 1, 0),
(59, '支付宝H5（官方）', 'AlipayWap', 0, '', '', '', '', '', '', '', '0.0300', '0.0000', '0.0040', 1, '0.00', '0.00', '', 'alipay_public_key:alipay_public_key|merchant_private_key:merchant_private_key|app_id:app_id|防封域名（可选）:refer', 0, 5, '[]', 0, 4, '', 1, 'charset=UTF-8', 'https://open.alipay.com/platform/homeRoleSelection.htm', 1, 0),
(60, '点缀微信扫码', 'DzWxScan', 0, '', '', '', '', '', '', '', '0.0000', '0.0000', '0.0000', 1, '0.00', '0.00', '', 'appid:appid|appsecret:appsecret|防封域名（可选）:refer', 0, 0, '[]', 0, 1, '', 0, '', '', 1, 0),
(61, '点缀支付宝扫码', 'DzAliScan', 0, '', '', '', '', '', '', '', '0.0000', '0.0000', '0.0000', 1, '0.00', '0.00', '', 'appid:appid|appsecret:appsecret|防封域名（可选）:refer', 0, 0, '[]', 0, 3, '', 0, '', '', 1, 0),
(62, '点缀微信公众号', 'DzWxGzh', 0, '', '', '', '', '', '', '', '0.0000', '0.0000', '0.0000', 1, '0.00', '0.00', '', 'appid:appid|appsecret:appsecret|是否使用自有公众号（0否，1是）:usepublic|防封域名（可选）:refer', 0, 0, '[]', 0, 2, '', 0, '', '', 1, 0),
(63, '15173Wap微信支付', 'Lh15173WapPay', 0, '', '', '', '', '', '', '', '0.0100', '0.0100', '0.0100', 1, '0.00', '0.00', '', 'bargainor_id:bargainor_id|key:key|防封域名（可选）:refer', 0, 16, '[]', 0, 18, '', 1, '', '', 1, 0),
(64, '拉卡微信支付', 'LkWxPay', 0, '', '', '', '', '', '', '', '0.0000', '0.0000', '0.0000', 1, '0.00', '0.00', '', 'userid:userid|userkey:userkey|防封域名（可选）:refer', 0, 17, '[]', 0, 19, '', 0, '', '', 1, 0),
(65, '拉卡微信H5支付', 'LkWxH5Pay', 0, '', '', '', '', '', '', '', '0.0000', '0.0000', '0.0000', 1, '0.00', '0.00', '', 'userid:userid|userkey:userkey|防封域名（可选）:refer', 0, 18, '[]', 0, 20, '', 0, '', '', 1, 0),
(66, '拉卡支付宝扫码支付', 'LkAlipayScanPay', 0, '', '', '', '', '', '', '', '0.0000', '0.0000', '0.0000', 1, '0.00', '0.00', '', 'userid:userid|userkey:userkey|防封域名（可选）:refer', 0, 19, '[]', 0, 21, '', 0, '', '', 1, 0),
(67, '快接微信扫码支付', 'KjWxSanPay', 0, '', '', '', '', '', '', '', '0.0000', '0.0000', '0.0000', 1, '0.00', '0.00', '', 'merchant_no:merchant_no|appkey:appkey|api_url:api_url|防封域名（可选）:refer', 0, 20, '[]', 0, 22, '', 0, '', '', 1, 0),
(68, '快接微信H5支付', 'KjWxH5Pay', 0, '', '', '', '', '', '', '', '0.0000', '0.0000', '0.0000', 1, '0.00', '0.00', '', 'merchant_no:merchant_no|appkey:appkey|api_url:api_url|防封域名（可选）:refer', 0, 21, '[]', 0, 23, '', 0, '', '', 1, 0),
(69, '快接支付宝即时到账', 'KjAlipayScanPay', 0, '', '', '', '', '', '', '', '0.0000', '0.0000', '0.0000', 1, '0.00', '0.00', '', 'merchant_no:merchant_no|appkey:appkey|api_url:api_url|防封域名（可选）:refer', 0, 22, '[]', 0, 24, '', 0, '', '', 1, 0),
(70, '快接微信H5支付', 'KjAlipayH5Pay', 0, '', '', '', '', '', '', '', '0.0000', '0.0000', '0.0000', 1, '0.00', '0.00', '', 'merchant_no:merchant_no|appkey:appkey|api_url:api_url|防封域名（可选）:refer', 0, 0, '[]', 0, 26, '', 0, '', '', 1, 0),
(71, '微信官方H5支付（官方）', 'WxpayH5', 0, '', '', '', '', '', '', '', '0.0000', '0.0000', '0.0000', 1, '0.00', '0.00', '', 'appid:appid|mch_id:mch_id|signkey:signkey|防封域名（可选）:refer', 0, 0, '[]', 0, 27, '', 0, '', '', 1, 0),
(72, '微信扫码（官方）', 'WxpayScan', 0, '', '', '', '', '', '', '', '0.0300', '0.0000', '0.0040', 1, '0.00', '0.00', '', 'appid:appid|mch_id:mch_id|signkey:signkey|notify_url:notify_url|防封域名（可选）:refer', 0, 6, '[]', 0, 1, '', 0, '', '', 1, 0),
(73, '12kaQQ钱包扫码', 'Ka12QqNative', 0, '', '', '', '', '', '', '', '0.0300', '0.0500', '0.0200', 1, '0.00', '0.00', '', '商户编号:customerid|api秘钥:apikey|防封域名（可选）:refer', 0, 0, '', 0, 30, '12kaQQ钱包扫码', 1, '', '', 1, 0),
(74, '12kaQQ钱包wap', 'Ka12QqWap', 0, '', '', '', '', '', '', '', '0.0000', '0.0000', '0.0000', 1, '0.00', '0.00', '', '商户编号:customerid|api秘钥:apikey|防封域名（可选）:refer', 0, 0, '', 0, 31, '12kaQQ钱包wap', 2, '', '', 1, 0),
(75, '12ka网银快捷', 'Ka12QuickBank', 0, '', '', '', '', '', '', '', '0.0000', '0.0000', '0.0000', 1, '0.00', '0.00', '', '商户编号:customerid|api秘钥:apikey|防封域名（可选）:refer', 0, 0, '', 0, 32, '12ka网银快捷', 1, '', '', 1, 0),
(76, '12卡网银快捷WAP', 'Ka12QuickWap', 0, '', '', '', '', '', '', '', '0.0000', '0.0000', '0.0000', 1, '0.00', '0.00', '', '商户编号:customerid|api秘钥:apikey|防封域名（可选）:refer', 0, 0, '', 0, 33, '12卡网银快捷WAP', 1, '', '', 1, 0),
(77, '12ka支付宝扫码', 'Ka12AlipayScan', 0, '', '', '', '', '', '', '', '0.0000', '0.0000', '0.0000', 1, '0.00', '0.00', '', '商户编号:customerid|api秘钥:apikey|防封域名（可选）:refer', 0, 0, '', 0, 34, '12ka支付宝扫码', 2, '', '', 1, 0),
(78, '12ka支付宝wap', 'Ka12AlipayWap', 0, '', '', '', '', '', '', '', '0.0000', '0.0000', '0.0000', 1, '0.00', '0.00', '', '商户编号:customerid|api秘钥:apikey|防封域名（可选）:refer', 0, 0, '', 0, 35, '12ka支付宝wap', 0, '', '', 1, 0),
(79, '12ka微信扫码', 'Ka12WxScan', 0, '', '', '', '', '', '', '', '0.0000', '0.0000', '0.0000', 1, '0.00', '0.00', '', '商户编号:customerid|api秘钥:apikey|防封域名（可选）:refer', 0, 0, '', 0, 36, '12ka微信扫码', 2, '', '', 1, 0),
(80, '12ka微信wap', 'Ka12WxWap', 0, '', '', '', '', '', '', '', '0.0000', '0.0000', '0.0000', 1, '0.00', '0.00', '', '商户编号:customerid|api秘钥:apikey|防封域名（可选）:refer', 0, 0, '', 0, 37, '12ka微信wap', 2, '', '', 1, 0),
(81, '15173QQ扫码支付', 'Lh15173QqPay', 0, '', '', '', '', '', '', '', '0.0000', '0.0000', '0.0000', 1, '0.00', '0.00', '', 'bargainor_id:bargainor_id|key:key|防封域名（可选）:refer', 0, 0, '', 0, 39, '15173QQ扫码支付', 0, '', '', 1, 0),
(82, '码支付微信扫码', 'CodePayWxScan', 0, '', '', '', '', '', '', '', '0.0000', '0.0000', '0.0000', 1, '0.00', '0.00', '', '码支付ID:id|通信秘钥:key|防封域名（可选）:refer', 0, 0, '', 0, 38, '码支付微信扫码', 2, '', '', 1, 0),
(83, '码支付qq扫码', 'CodePayQqScan', 0, '', '', '', '', '', '', '', '0.0000', '0.0000', '0.0000', 1, '0.00', '0.00', '', '码支付ID:id|通信秘钥:key|防封域名（可选）:refer', 0, 0, '', 0, 40, '码支付qq扫码', 0, '', '', 1, 0),
(84, '码支付支付宝扫码', 'CodePayAliScan', 0, '', '', '', '', '', '', '', '0.0200', '0.0000', '0.0200', 1, '0.00', '0.00', '', '码支付ID:id|通信秘钥:key|防封域名（可选）:refer', 0, 0, '', 0, 41, '码支付支付宝扫码', 0, '', '', 1, 0),
(85, '点缀QQ扫码', 'DzQqScan', 0, '', '', '', '', '', '', '', '0.0000', '0.0000', '0.0000', 1, '0.00', '0.00', '', 'appid:appid|appsecret:appsecret|防封域名（可选）:refer', 0, 0, '[]', 0, 8, '点缀支付PC', 1, '', '', 1, 0),
(86, '黔贵金服支付宝扫码', 'QgjfAlipayScan', 0, '', '', '', '', '', '', '', '0.0000', '0.0000', '0.0000', 1, '0.00', '0.00', '', '商户号:mch_id|密钥:key|防封域名（可选）:refer', 0, 0, '[]', 0, 51, '黔贵金服支付宝扫码', 2, '', '', 1, 0),
(87, '黔贵金服微信扫码', 'QgjfWxScan', 0, '', '', '', '', '', '', '', '0.0000', '0.0000', '0.0000', 1, '0.00', '0.00', '', '商户号:mch_id|密钥:key|防封域名（可选）:refer', 0, 0, '[]', 0, 52, '黔贵金服微信扫码', 2, '', '', 1, 0),
(88, '黔贵金服QQ钱包扫码', 'QgjfQqNative', 0, '', '', '', '', '', '', '', '0.0000', '0.0000', '0.0000', 1, '0.00', '0.00', '', '商户号:mch_id|密钥:key|防封域名（可选）:refer', 0, 0, '[]', 0, 53, '黔贵金服QQ钱包扫码', 2, '', '', 1, 0),
(89, '黔贵金服公众号', 'QgjfWxGzh', 0, '', '', '', '', '', '', '', '0.0000', '0.0000', '0.0000', 1, '0.00', '0.00', '', '商户号:mch_id|密钥:key|防封域名（可选）:refer', 0, 0, '[]', 0, 54, '黔贵金服公众号', 2, '', '', 1, 0),
(90, '黔贵金服支付宝Wap', 'QgjfAlipayWap', 0, '', '', '', '', '', '', '', '0.0000', '0.0000', '0.0000', 1, '0.00', '0.00', '', '商户号:mch_id|密钥:key|防封域名（可选）:refer', 0, 0, '[]', 0, 55, '黔贵金服支付宝WAP', 2, '', '', 1, 0),
(91, '黔贵金服微信WAP', 'QgjfWxWap', 0, '', '', '', '', '', '', '', '0.0000', '0.0000', '0.0000', 1, '0.00', '0.00', '', '商户号:mch_id|密钥:key|防封域名（可选）:refer', 0, 0, '[]', 0, 56, '黔贵金服微信WAP', 2, '', '', 1, 0),
(92, '点缀支付宝即时到账', 'DzAliToPay', 0, '', '', '', '', '', '', '', '0.0000', '0.0000', '0.0000', 1, '0.00', '0.00', '', 'appid:appid|appsecret:appsecret|防封域名（可选）:refer', 0, 0, '[]', 0, 57, '点缀支付宝即时到账', 1, '', '', 1, 0),
(93, '点缀支付京东钱包扫码', 'DzJdScan', 0, '', '', '', '', '', '', '', '0.0000', '0.0000', '0.0000', 1, '0.00', '0.00', '', 'appid:appid|appsecret:appsecret|防封域名（可选）:refer', 0, 0, '[]', 0, 58, '点缀支付京东钱包扫码', 2, '', '', 1, 0),
(94, '掌灵付微信H5', 'ZlfWxH5', 0, '', '', '', '', '', '', '', '0.0000', '0.0000', '0.0000', 1, '0.00', '0.00', '', 'appid:appid|key:key|防封域名（可选）:refer', 0, 0, '[]', 0, 59, '掌灵付微信H5', 0, '', '', 1, 0),
(95, '掌灵付京东H5', 'ZlfJdH5', 0, '', '', '', '', '', '', '', '0.0000', '0.0000', '0.0000', 1, '0.00', '0.00', '', 'appid:appid|key:key|防封域名（可选）:refer', 0, 0, '[]', 0, 60, '掌灵付京东H5', 0, '', '', 1, 0),
(96, '掌灵付京东扫码', 'ZlfJdScan', 0, '', '', '', '', '', '', '', '0.0000', '0.0000', '0.0000', 1, '0.00', '0.00', '', 'appid:appid|key:key|防封域名（可选）:refer', 0, 0, '[]', 0, 89, '掌灵付京东扫码', 2, '', '', 1, 0),
(97, '掌灵付微信扫码', 'ZlfWxScan', 0, '', '', '', '', '', '', '', '0.0350', '0.0000', '0.0100', 1, '0.00', '0.00', '', 'appid:appid|key:key|防封域名（可选）:refer|防封域名（可选）:refer', 0, 0, '[]', 0, 47, '掌灵付微信扫码', 2, '', '', 1, 0),
(98, '掌灵付QQ钱包扫码', 'ZlfQqScan', 0, '', '', '', '', '', '', '', '0.0350', '0.0000', '0.0100', 1, '0.00', '0.00', '', 'appid:appid|key:key|防封域名（可选）:refer|防封域名（可选）:refer', 0, 0, '[]', 0, 48, '掌灵付QQ扫码', 0, '', '', 1, 0),
(99, 'QQ钱包扫码（官方）', 'QqNative', 0, '', '', '', '', '', '', '', '0.0300', '0.0000', '0.0060', 1, '0.00', '0.00', '', 'mch_id:mch_id|key:key|防封域名（可选）:refer', 0, 0, '[]', 0, 8, '官方QQ扫码', 0, '', '', 1, 0),
(100, '点缀微信H5', 'DzWxH5', 0, '', '', '', '', '', '', '', '0.0000', '0.0000', '0.0000', 1, '0.00', '0.00', '', 'appid:appid|appsecret:appsecret|防封域名（可选）:refer', 0, 0, '[]', 0, 61, '点缀微信H5', 0, '', '', 1, 0),
(101, '优畅上海微信H5', 'YcshWxH5', 0, '', '', '', '', '', '', '', '0.0000', '0.0000', '0.0000', 1, '0.00', '0.00', '', '商户Id:mch_id|商户密钥:appsecret|防封域名（可选）:refer', 0, 0, '[]', 0, 62, '优畅微信H5', 0, '', '', 1, 0),
(102, '优畅上海微信扫码', 'YcshWxScan', 0, '', '', '', '', '', '', '', '0.0000', '0.0000', '0.0000', 1, '0.00', '0.00', '', '商户Id:mch_id|商户密钥:appsecret|防封域名（可选）:refer', 0, 0, '[]', 0, 63, '优畅上海微信扫码', 0, '', '', 1, 0),
(103, '优畅上海微信公众号', 'YcshWxGzh', 0, '', '', '', '', '', '', '', '0.0000', '0.0000', '0.0000', 1, '0.00', '0.00', '', '商户Id:mch_id|商户密钥:appsecret|微信公众号appid:appid|防封域名（可选）:refer', 0, 0, '[]', 0, 64, '优畅上海微信公众号', 0, '', '', 1, 0),
(104, '海鸟微信公众号', 'HnPayWxGzh', 0, '', '', '', '', '', '', '', '0.0000', '0.0000', '0.0000', 1, '0.00', '0.00', '', '商户Id:appid| 商户号:merchant|商户密钥:key|是否使用自有公众号（0否，1是）:publicappid|防封域名（可选）:refer', 0, 0, '[]', 0, 67, '海鸟微信公众号', 0, '', '', 1, 0),
(105, '海鸟微信扫码', 'HnPayWxScan', 0, '', '', '', '', '', '', '', '0.0000', '0.0000', '0.0000', 1, '0.00', '0.00', '', '商户Id:appid|商户号:merchant|商户密钥:key|防封域名（可选）:refer', 0, 0, '[]', 0, 68, '海鸟微信扫码', 0, '', '', 1, 0),
(106, '海鸟微信H5', 'HnPayWxH5', 0, '', '', '', '', '', '', '', '0.0000', '0.0000', '0.0000', 1, '0.00', '0.00', '', '商户Id:appid|商户号:merchant|商户密钥:key|防封域名（可选）:refer', 0, 0, '[]', 0, 69, '海鸟微信H5', 0, '', '', 1, 0),
(107, '海鸟微信qq扫码', 'HnPayQqScan', 0, '', '', '', '', '', '', '', '0.0000', '0.0000', '0.0000', 1, '0.00', '0.00', '', '商户Id:appid|商户号:merchant|商户密钥:key|防封域名（可选）:refer', 0, 0, '[]', 0, 70, '海鸟微信qq扫码', 0, '', '', 1, 0),
(108, '海鸟支付宝扫码', 'HnPayAliScan', 0, '', '', '', '', '', '', '', '0.0000', '0.0000', '0.0000', 1, '0.00', '0.00', '', '商户Id:appid|商户号:merchant|商户密钥:key|防封域名（可选）:refer', 0, 0, '[]', 0, 71, '海鸟支付宝扫码', 0, '', '', 1, 0),
(109, '完美数卡微信扫码', 'WmskWxScan', 0, '', '', '', '', '', '', '', '0.0000', '0.0000', '0.0000', 1, '0.00', '0.00', '', '商户Id:customerid|商户密钥:key|防封域名（可选）:refer', 0, 0, '[]', 0, 78, '完美数卡微信扫码', 0, '', '', 0, 0),
(110, '完美数卡支付宝扫码', 'WmskAliScan', 0, '', '', '', '', '', '', '', '0.0000', '0.0000', '0.0000', 1, '0.00', '0.00', '', '商户Id:customerid|商户密钥:key|防封域名（可选）:refer', 0, 0, '[]', 0, 79, '完美数卡支付宝扫码', 0, '', '', 0, 0),
(111, '完美数卡QQ扫码', 'WmskQqScan', 0, '', '', '', '', '', '', '', '0.0000', '0.0000', '0.0000', 1, '0.00', '0.00', '', '商户Id:customerid|商户密钥:key|防封域名（可选）:refer', 0, 0, '[]', 0, 80, '完美数卡QQ扫码', 0, '', '', 0, 0),
(112, '完美数卡QQWap', 'WmskQqWap', 0, '', '', '', '', '', '', '', '0.0000', '0.0000', '0.0000', 1, '0.00', '0.00', '', '商户Id:customerid|商户密钥:key|防封域名（可选）:refer', 0, 0, '[]', 0, 81, '完美数卡QQWap', 0, '', '', 0, 0),
(113, '完美数卡微信Wap', 'WmskWxWap', 0, '', '', '', '', '', '', '', '0.0000', '0.0000', '0.0000', 1, '0.00', '0.00', '', '商户Id:customerid|商户密钥:key|防封域名（可选）:refer', 0, 0, '[]', 0, 82, '完美数卡微信Wap', 0, '', '', 0, 0),
(114, '完美数卡支付宝Wap', 'WmskAliWap', 0, '', '', '', '', '', '', '', '0.0000', '0.0000', '0.0000', 1, '0.00', '0.00', '', '商户Id:customerid|商户密钥:key|防封域名（可选）:refer', 0, 0, '[]', 0, 83, '完美数卡支付宝Wap', 0, '', '', 0, 0),
(115, '掌灵付支付宝扫码', 'ZlfAliScan', 0, '', '', '', '', '', '', '', '0.0000', '0.0000', '0.0000', 1, '0.00', '0.00', '', 'appid:appid|key:key|防封域名（可选）:refer', 0, 0, '[]', 0, 87, '掌灵付支付宝扫码', 0, '', '', 0, 0),
(116, 'QPay支付宝', 'QPayAli', 0, '', '', '', '', '', '', '', '0.0300', '0.0000', '0.0040', 1, '0.00', '0.00', '', '网关:gateway|用户id:uid|token:token|防封域名（可选）:refer', 0, 7, '[]', 0, 14, '', 0, '', '', 0, 0),
(117, 'QPay微信', 'QPayWx', 0, '', '', '', '', '', '', '', '0.0100', '0.0000', '0.0010', 1, '0.00', '0.00', '', '网关:gateway|用户id:uid|token:token|防封域名（可选）:refer', 0, 0, '[]', 0, 15, '', 0, 'gateway=https://pay.qpayapi.com', '', 0, 0),
(118, '15173微信扫描支付', 'Lh15173PcPay', 0, '', '', '', '', '', '', '', '0.0000', '0.0000', '0.0000', 1, '0.00', '0.00', '', 'bargainor_id:bargainor_id|key:key|防封域名（可选）:refer', 0, 15, '[]', 0, 17, '', 0, '', '', 0, 0),
(119, '蜂鸟支付宝扫码支付', 'FnAliScan', 0, '', '', '', '', '', '', '', '0.0000', '0.0000', '0.0000', 1, '0.00', '0.00', '', 'appid:appid|secret:secret|merchant_no:merchant_no|防封域名（可选）:refer', 0, 0, '[]', 0, 42, '', 2, '', '', 0, 0),
(120, '蜂鸟支付宝WAP支付', 'FnAliWap', 0, '', '', '', '', '', '', '', '0.0000', '0.0000', '0.0000', 1, '0.00', '0.00', '', 'appid:appid|secret:secret|merchant_no:merchant_no|防封域名（可选）:refer', 0, 0, '[]', 0, 43, '', 1, '', '', 0, 0),
(121, '蜂鸟微信扫码支付', 'FnWxScan', 0, '', '', '', '', '', '', '', '0.0000', '0.0000', '0.0000', 1, '0.00', '0.00', '', 'appid:appid|secret:secret|merchant_no:merchant_no|sub_appid:sub_appid|防封域名（可选）:refer', 0, 0, '[]', 0, 44, '', 2, '', '', 0, 0),
(122, '蜂鸟微信公众号支付', 'FnWxJspay', 0, '', '', '', '', '', '', '', '0.0000', '0.0000', '0.0000', 1, '0.00', '0.00', '', 'appid:appid|secret:secret|merchant_no:merchant_no|wx_appid:wx_appid|wx_secret:wx_secret|防封域名（可选）:refer', 0, 0, '[]', 0, 45, '', 1, '', '', 0, 0),
(123, '蜂鸟QQ钱包扫码支付', 'FnQqScan', 0, '', '', '', '', '', '', '', '0.0000', '0.0000', '0.0000', 1, '0.00', '0.00', '', 'appid:appid|secret:secret|merchant_no:merchant_no|防封域名（可选）:refer', 0, 0, '[]', 0, 46, '', 2, '', '', 0, 0),
(124, '优畅上海支付宝扫码', 'YcshAliScan', 0, '', '', '', '', '', '', '', '0.0000', '0.0000', '0.0000', 1, '0.00', '0.00', '', '商户Id:mch_id|商户密钥:appsecret|防封域名（可选）:refer', 0, 0, '[]', 0, 90, '优畅上海支付宝扫码', 0, '', '', 0, 0),
(125, '优畅上海支付宝Wap', 'YcshAliWap', 0, '', '', '', '', '', '', '', '0.0000', '0.0000', '0.0000', 1, '0.00', '0.00', '', '商户Id:mch_id|商户密钥:appsecret|防封域名（可选）:refer', 0, 0, '[]', 0, 91, '优畅上海支付宝Wap', 0, '', '', 0, 0),
(126, '汉口银行微信公众号', 'HkyhWxGzh', 0, '', '', '', '', '', '', '', '0.0000', '0.0000', '0.0000', 1, '0.00', '0.00', '', 'appid:appid|appkey:appkey|是否使用自有公众号(可选，1是，0否):usePublic|门店（可选）:store|防封域名（可选）:refer', 0, 0, '[]', 0, 98, '', 1, '', '', 0, 0),
(127, '汉口银行微信扫码', 'HkyhWxScan', 0, '', '', '', '', '', '', '', '0.0000', '0.0000', '0.0000', 1, '0.00', '0.00', '', 'appid:appid|appkey:appkey|门店（可选）:store|防封域名（可选）:refer', 0, 0, '[]', 0, 99, '', 0, '', '', 0, 0),
(128, '汉口银行微信wap', 'HkyhWxWap', 0, '', '', '', '', '', '', '', '0.0000', '0.0000', '0.0000', 1, '0.00', '0.00', '', 'appid:appid|appkey:appkey|门店（可选）:store|防封域名（可选）:refer', 0, 0, '[]', 0, 100, '', 0, '', '', 0, 0),
(129, '汉口银行支付宝扫码', 'HkyhAliScan', 0, '', '', '', '', '', '', '', '0.0000', '0.0000', '0.0000', 1, '0.00', '0.00', '', 'appid:appid|appkey:appkey|门店（可选）:store|防封域名（可选）:refer', 0, 0, '[]', 0, 101, '', 0, '', '', 0, 0),
(130, '汉口银行支付宝Wap', 'HkyhAliWap', 0, '', '', '', '', '', '', '', '0.0000', '0.0000', '0.0000', 1, '0.00', '0.00', '', 'appid:appid|appkey:appkey|门店（可选）:store|防封域名（可选）:refer', 0, 0, '[]', 0, 102, '', 0, '', '', 0, 0),
(131, '优畅上海QQ扫码', 'YcshQqScan', 0, '', '', '', '', '', '', '', '0.0000', '0.0000', '0.0000', 1, '0.00', '0.00', '', '商户Id:mch_id|商户密钥:appsecret|防封域名（可选）:refer', 0, 0, '[]', 0, 103, '', 0, '', '', 0, 0),
(132, '优畅上海QQWap', 'YcshQqWap', 0, '', '', '', '', '', '', '', '0.0000', '0.0000', '0.0000', 1, '0.00', '0.00', '', '商户Id:mch_id|商户密钥:appsecret|防封域名（可选）:refer', 0, 0, '[]', 0, 104, '', 0, '', '', 0, 0),
(133, '平安付微信扫码', 'PafbWxScan', 0, '', '', '', '', '', '', '', '0.0000', '0.0000', '0.0000', 1, '0.00', '0.00', '', 'appid:appid|key:key|门店:store|防封域名（可选）:refer', 0, 0, '[]', 0, 105, '', 0, '', '', 0, 0),
(134, '平安付支付宝扫码', 'PafbAliScan', 0, '', '', '', '', '', '', '', '0.0000', '0.0000', '0.0000', 1, '0.00', '0.00', '', 'appid:appid|key:key|门店:store|防封域名（可选）:refer', 0, 0, '[]', 0, 106, '', 1, '', '', 0, 0),
(135, '平安付支付宝wap', 'PafbAliWap', 0, '', '', '', '', '', '', '', '0.0000', '0.0000', '0.0000', 1, '0.00', '0.00', '', 'appid:appid|key:key|门店:store|防封域名（可选）:refer', 0, 0, '[]', 0, 107, '', 0, '', '', 0, 0),
(136, '平安付微信wap', 'PafbWxWap', 0, '', '', '', '', '', '', '', '0.0000', '0.0000', '0.0000', 1, '0.00', '0.00', '', 'appid:appid|key:key|门店:store|防封域名（可选）:refer', 0, 0, '[]', 0, 108, '', 0, '', '', 0, 0),
(137, '网商银行微信', 'WsyhWxScan', 0, '', '', '', '', '', '', '', '0.0000', '0.0000', '0.0000', 1, '0.00', '0.00', '', '机构AppId:Appid|机构号:IsvOrgId|浙商私钥:private_key|浙商公钥:public_key|商户号:merchantid|清算方式:settleType|服务商类型（咨询上游，不确定填03）:ProviderType|防封域名（可选）:refer', 0, 0, '[]', 0, 109, '', 0, '', '', 0, 0),
(138, '网商银行支付宝', 'WsyhAliScan', 0, '', '', '', '', '', '', '', '0.0000', '0.0000', '0.0000', 1, '0.00', '0.00', '', '机构AppId:Appid|机构号:IsvOrgId|浙商私钥:private_key|浙商公钥:public_key|商户号:merchantid|清算方式:settleType|服务商类型（咨询上游，不确定填03）:ProviderType|防封域名（可选）:refer', 0, 0, '[]', 0, 110, '', 0, '', '', 0, 0),
(139, '牛支付支付宝扫码', 'NZFAliqrcode', 0, '', '', '', '', '', '', '', '0.0000', '0.0000', '0.0000', 1, '0.00', '0.00', '', '商户号:memberid|appid:appid|app 密钥:key|子商户 id(没有请留空):submchid|防封域名（可选）:refer|防封域名（可选）:refer', 0, 0, '[]', 0, 88, '牛支付支付宝扫码', 0, '', '', 1, 0),
(152, '吉易支付微信扫码', 'JyWxPay', 0, '', '', '', '', '', '', '', '0.0000', '0.0000', '0.0000', 1, '0.00', '0.00', '', '商户号:key|key:refer|渠道号:channelcode', 0, 0, '[]', 0, 111, '', 2, '', '', 0, 0),
(153, '吉易支付微信公众号', 'JyWxGzhPay', 0, '', '', '', '', '', '', '', '0.0000', '0.0000', '0.0000', 1, '0.00', '0.00', '', '商户号:key|key:refer|渠道号:channelcode', 0, 0, '[]', 0, 112, '', 1, '', '', 0, 0),
(154, 'PayApi支付宝', 'PayapiAli', 0, '', '', '', '', '', '', '', '0.0000', '0.0000', '0.0000', 1, '0.00', '0.00', '', '商户号:uid|秘钥:key', 0, 0, '[]', 0, 113, '', 0, '', '', 0, 0),
(155, 'PayApi微信', 'PayapiWx', 0, '', '', '', '', '', '', '', '0.0000', '0.0000', '0.0000', 1, '0.00', '0.00', '', '商户号:uid|秘钥:key', 0, 0, '[]', 0, 114, '', 0, '', '', 0, 0),
(156, '威富通支付宝扫码', 'SwiftAliScan', 0, '', '', '', '', '', '', '', '0.0000', '0.0000', '0.0000', 1, '0.00', '0.00', '', '商户Id:mch_id|商户密钥:key|防封域名（可选）:refer', 0, 0, '[]', 0, 92, '威富通支付宝扫码', 0, '', '', 0, 0),
(157, '威富通支付宝Wap', 'SwiftAliWap', 0, '', '', '', '', '', '', '', '0.0000', '0.0000', '0.0000', 1, '0.00', '0.00', '', '商户Id:mch_id|商户密钥:key|防封域名（可选）:refer', 0, 0, '[]', 0, 93, '威富通支付宝Wap', 0, '', '', 0, 0),
(158, '威富通微信扫码', 'SwiftWxScan', 0, '', '', '', '', '', '', '', '0.0000', '0.0000', '0.0000', 1, '0.00', '0.00', '', '商户Id:mch_id|商户密钥:key|防封域名（可选）:refer', 0, 0, '[]', 0, 94, '威富通微信扫码', 0, '', '', 0, 0),
(159, '威富通京东扫码', 'SwiftJd', 0, '', '', '', '', '', '', '', '0.0000', '0.0000', '0.0000', 1, '0.00', '0.00', '', '商户Id:mch_id|商户密钥:key|防封域名（可选）:refer', 0, 0, '[]', 0, 95, '威富通京东扫码', 0, '', '', 0, 0),
(160, '威富通微信 Wap', 'SwiftWxWap', 0, '', '', '', '', '', '', '', '0.0000', '0.0000', '0.0000', 1, '0.00', '0.00', '', '商户Id:mch_id|商户密钥:key|防封域名（可选）:refer', 0, 0, '[]', 0, 96, '威富通微信Wap', 0, '', '', 0, 0),
(161, '威富通微信公众号', 'SwiftWxGzh', 0, '', '', '', '', '', '', '', '0.0000', '0.0000', '0.0000', 1, '0.00', '0.00', '', '商户Id:mch_id|商户密钥:key|防封域名（可选）:refer', 0, 0, '[]', 0, 97, '威富通微信公众号', 0, '', '', 0, 0),
(162, '易云支付宝扫码', 'YiyunAliScan', 0, '', '', '', '', '', '', '', '0.0000', '0.0000', '0.0000', 1, '0.00', '0.00', '', '商户Id:userid|商户密钥:userkey|防封域名（可选）:refer', 0, 0, '[]', 0, 115, '易云支付宝扫码', 0, '', '', 0, 0),
(163, '易云支付宝WAP', 'YiyunAliWap', 0, '', '', '', '', '', '', '', '0.0000', '0.0000', '0.0000', 1, '0.00', '0.00', '', '商户Id:userid|商户密钥:userkey|防封域名（可选）:refer', 0, 0, '[]', 0, 116, '易云支付宝WAP', 0, '', '', 0, 0),
(164, '易云微信扫码', 'YiyunWxScan', 0, '', '', '', '', '', '', '', '0.0000', '0.0000', '0.0000', 1, '0.00', '0.00', '', '商户Id:userid|商户密钥:userkey|防封域名（可选）:refer', 0, 0, '[]', 0, 117, '易云微信扫码', 0, '', '', 0, 0),
(165, '易云微信WAP', 'YiyunWxWap', 0, '', '', '', '', '', '', '', '0.0000', '0.0000', '0.0000', 1, '0.00', '0.00', '', '商户Id:userid|商户密钥:userkey|防封域名（可选）:refer', 0, 0, '[]', 0, 118, '易云微信WAP', 0, '', '', 0, 0),
(166, '易云微信公众号', 'YiyunWxGzh', 0, '', '', '', '', '', '', '', '0.0000', '0.0000', '0.0000', 1, '0.00', '0.00', '', '商户Id:userid|商户密钥:userkey|防封域名（可选）:refer', 0, 0, '[]', 0, 119, '易云微信公众号', 0, '', '', 0, 0),
(167, '恒隆支付宝扫码', 'HenglongAliScan', 0, '', '', '', '', '', '', '', '0.0000', '0.0000', '0.0000', 1, '0.00', '0.00', '', '商户Id:memberid|商户密钥:key|防封域名（可选）:refer', 0, 0, '[]', 0, 120, '恒隆支付宝扫码', 0, '', '', 0, 0),
(168, '恒隆支付宝WAP', 'HenglongAliWap', 0, '', '', '', '', '', '', '', '0.0000', '0.0000', '0.0000', 1, '0.00', '0.00', '', '商户Id:memberid|商户密钥:key|防封域名（可选）:refer', 0, 0, '[]', 0, 121, '恒隆支付宝WAP', 0, '', '', 0, 0),
(169, '恒隆微信公众号支付', 'HenglongWxGzh', 0, '', '', '', '', '', '', '', '0.0000', '0.0000', '0.0000', 1, '0.00', '0.00', '', '商户Id:memberid|商户密钥:key|防封域名（可选）:refer', 0, 0, '[]', 0, 122, '恒隆微信公众号支付', 0, '', '', 0, 0),
(170, '恒隆微信扫码', 'HenglongWxScan', 0, '', '', '', '', '', '', '', '0.0000', '0.0000', '0.0000', 1, '0.00', '0.00', '', '商户Id:memberid|商户密钥:key|防封域名（可选）:refer', 0, 0, '[]', 0, 123, '恒隆微信扫码', 0, '', '', 0, 0),
(171, '深度支付宝扫码', 'ShenduAliScan', 0, '', '', '', '', '', '', '', '0.0000', '0.0000', '0.0000', 1, '0.00', '0.00', '', '商户Id:mch_id|商户密钥:md5_key|防封域名（可选）:refer', 0, 0, '[]', 0, 124, '深度支付宝扫码', 0, '', '', 0, 0),
(172, '深度支付宝服务窗支付', 'ShenduAliJspay', 0, '', '', '', '', '', '', '', '0.0000', '0.0000', '0.0000', 1, '0.00', '0.00', '', '商户Id:mch_id|商户密钥:md5_key|防封域名（可选）:refer', 0, 0, '[]', 0, 125, '深度支付宝服务窗支付', 0, '', '', 0, 0),
(173, '深度微信扫码', 'ShenduWxScan', 0, '', '', '', '', '', '', '', '0.0000', '0.0000', '0.0000', 1, '0.00', '0.00', '', '商户Id:mch_id|商户密钥:md5_key|防封域名（可选）:refer', 0, 0, '[]', 0, 126, '深度微信扫码', 0, '', '', 0, 0),
(174, '深度微信公众号', 'ShenduWxGzh', 0, '', '', '', '', '', '', '', '0.0000', '0.0000', '0.0000', 1, '0.00', '0.00', '', '商户Id:mch_id|商户密钥:md5_key|防封域名（可选）:refer', 0, 0, '[]', 0, 127, '深度微信公众号', 0, '', '', 0, 0),
(175, '聚合支付', 'Juhezhifu', 0, '', '', '', '', '', '', '', '0.0000', '0.0000', '0.0000', 1, '0.00', '0.00', '', '商户Id:memberid|商户密钥:key|产品编号:bankcode|网关地址:geteway|防封域名（可选）:refer', 0, 0, '[]', 0, 128, '聚合支付', 0, '', '', 0, 0);

-- --------------------------------------------------------

--
-- 表的结构 `channel_account`
--

CREATE TABLE `channel_account` (
  `id` int(10) UNSIGNED NOT NULL,
  `channel_id` int(10) UNSIGNED NOT NULL COMMENT '渠道ID',
  `name` varchar(32) NOT NULL DEFAULT '' COMMENT '账户名',
  `params` text NOT NULL COMMENT '参数',
  `max_money` decimal(10,2) UNSIGNED NOT NULL DEFAULT '0.00' COMMENT '下线额度',
  `cur_money` decimal(10,2) UNSIGNED NOT NULL DEFAULT '0.00' COMMENT '当前额度',
  `limit_time` varchar(19) NOT NULL DEFAULT '' COMMENT '限时',
  `status` tinyint(3) UNSIGNED NOT NULL DEFAULT '0' COMMENT '状态 1启用 0禁用',
  `lowrate` decimal(10,4) UNSIGNED NOT NULL DEFAULT '0.0000' COMMENT '充值费率',
  `highrate` decimal(10,4) UNSIGNED NOT NULL DEFAULT '0.0000' COMMENT '封顶费率',
  `costrate` decimal(10,4) UNSIGNED NOT NULL DEFAULT '0.0000' COMMENT '成本费率',
  `rate_type` tinyint(4) NOT NULL DEFAULT '0' COMMENT '费率设置 0 继承接口  1单独设置'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `channel_account`
--

INSERT INTO `channel_account` (`id`, `channel_id`, `name`, `params`, `max_money`, `cur_money`, `limit_time`, `status`, `lowrate`, `highrate`, `costrate`, `rate_type`) VALUES
(1, 139, '演示账号', '{\"memberid\":\"2\",\"appid\":\"12340004\",\"key\":\"0e0a47c54df24e3015eb78475bfae915\",\"submchid\":\"\",\"refer\":\"\"}', '0.00', '0.00', '', 1, '0.0000', '0.0000', '0.0000', 0);

-- --------------------------------------------------------

--
-- 表的结构 `complaint`
--

CREATE TABLE `complaint` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `trade_no` char(50) NOT NULL DEFAULT '',
  `type` varchar(20) NOT NULL DEFAULT '',
  `qq` varchar(15) NOT NULL DEFAULT '',
  `mobile` varchar(15) NOT NULL DEFAULT '',
  `desc` varchar(1000) NOT NULL DEFAULT '',
  `status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0待处理 1已处理',
  `admin_read` tinyint(3) UNSIGNED NOT NULL DEFAULT '0' COMMENT '管理员查看状态',
  `create_at` int(10) UNSIGNED NOT NULL,
  `create_ip` varchar(15) NOT NULL DEFAULT '',
  `pwd` char(10) NOT NULL DEFAULT '123456' COMMENT '投诉单查询密码',
  `result` tinyint(4) NOT NULL DEFAULT '0' COMMENT '申诉结果',
  `expire_at` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT '申诉过期时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `complaint_message`
--

CREATE TABLE `complaint_message` (
  `id` int(10) UNSIGNED NOT NULL,
  `trade_no` varchar(255) NOT NULL DEFAULT '0' COMMENT '投诉所属订单',
  `from` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT '发送人，0为管理员发送的消息',
  `content` varchar(1024) NOT NULL DEFAULT '' COMMENT '对话内容',
  `status` tinyint(3) NOT NULL DEFAULT '0' COMMENT '0未读  1已读',
  `create_at` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT '发送时间',
  `content_type` varchar(255) NOT NULL DEFAULT '0' COMMENT '投诉内容类型：0：文本消息，1：图片消息'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='投诉会话信息';

-- --------------------------------------------------------

--
-- 表的结构 `email_code`
--

CREATE TABLE `email_code` (
  `id` int(10) UNSIGNED NOT NULL,
  `email` varchar(50) NOT NULL DEFAULT '' COMMENT '邮箱',
  `screen` varchar(30) NOT NULL DEFAULT '' COMMENT '场景',
  `code` char(6) NOT NULL DEFAULT '' COMMENT '验证码',
  `status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '状态：0：未使用 1：已使用',
  `create_at` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT '创建时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `email_code`
--

INSERT INTO `email_code` (`id`, `email`, `screen`, `code`, `status`, `create_at`) VALUES
(1, '3500533323@qq.com', 'register', '392251', 0, 1571041659);

-- --------------------------------------------------------

--
-- 表的结构 `goods`
--

CREATE TABLE `goods` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `cate_id` int(10) UNSIGNED NOT NULL,
  `theme` varchar(15) NOT NULL DEFAULT 'default' COMMENT '主题',
  `sort` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT '排序',
  `name` varchar(500) NOT NULL DEFAULT '',
  `price` decimal(10,2) UNSIGNED NOT NULL DEFAULT '0.00',
  `cost_price` decimal(10,2) UNSIGNED NOT NULL DEFAULT '0.00' COMMENT '成本价',
  `wholesale_discount` tinyint(3) UNSIGNED NOT NULL DEFAULT '0' COMMENT '批发优惠',
  `wholesale_discount_list` text NOT NULL COMMENT '批发价',
  `limit_quantity` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT '起购数量',
  `inventory_notify` tinyint(3) UNSIGNED NOT NULL DEFAULT '0' COMMENT '库存预警 0表示不报警',
  `inventory_notify_type` tinyint(3) UNSIGNED NOT NULL DEFAULT '1' COMMENT '库存预警通知方式 1站内信 2邮件',
  `coupon_type` tinyint(3) UNSIGNED NOT NULL DEFAULT '0' COMMENT '优惠券 0不支持 1支持',
  `sold_notify` tinyint(3) UNSIGNED NOT NULL DEFAULT '0' COMMENT '售出通知',
  `take_card_type` tinyint(3) UNSIGNED NOT NULL DEFAULT '0' COMMENT '取卡密码 0关闭 1必填 2选填',
  `visit_type` tinyint(3) UNSIGNED NOT NULL DEFAULT '0' COMMENT '访问密码',
  `visit_password` varchar(30) NOT NULL DEFAULT '' COMMENT '访问密码',
  `contact_limit` enum('mobile','email','qq','any','default') NOT NULL DEFAULT 'default',
  `content` varchar(200) NOT NULL DEFAULT '' COMMENT '商品说明',
  `remark` varchar(200) NOT NULL DEFAULT '',
  `status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0下架 1上架',
  `create_at` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `is_freeze` tinyint(4) DEFAULT '0',
  `sms_payer` tinyint(3) UNSIGNED NOT NULL DEFAULT '0' COMMENT '短信付费方：0买家 1商户',
  `delete_at` int(11) DEFAULT NULL COMMENT '删除标记'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `goods_card`
--

CREATE TABLE `goods_card` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `goods_id` int(10) UNSIGNED NOT NULL,
  `number` varchar(500) NOT NULL DEFAULT '',
  `secret` varchar(500) NOT NULL DEFAULT '',
  `status` tinyint(3) NOT NULL DEFAULT '1' COMMENT '0不可用 1可用 2已使用',
  `create_at` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `delete_at` int(11) DEFAULT NULL COMMENT '删除标记',
  `sell_time` int(11) DEFAULT NULL COMMENT '售出时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `goods_card`
--

INSERT INTO `goods_card` (`id`, `user_id`, `goods_id`, `number`, `secret`, `status`, `create_at`, `delete_at`, `sell_time`) VALUES
(114, 10002, 4, '111111111', '', 2, 1562845427, NULL, 1562846541),
(115, 10002, 4, 'adaadadfgffffffffffffffffff', '', 2, 1562846572, NULL, 1562846669),
(116, 10002, 4, 'dadadadadadssss', '', 2, 1562848219, NULL, 1562924705),
(117, 10002, 4, 'ssfdfafcxvxvxxvxvx', '', 2, 1562848219, NULL, 1562924705),
(118, 10002, 4, 'adacvbdgsfsfsffsfs', '', 1, 1562848219, NULL, NULL),
(119, 10002, 5, 'wOvgnsAM1vLrwAVsTwJkiq12zWYVU1im', '', 1, 1563063927, NULL, NULL),
(120, 10002, 5, 'dW1OBqcgF66CpqOKCxJPbbdu8QXu3d9H', '', 1, 1563063927, NULL, NULL),
(121, 10002, 5, '8OPvXAWtdtTgxROHlGOpHO9YMrRyTDff', '', 1, 1563063927, NULL, NULL),
(122, 10002, 5, 'WVZbwug84u7B6pIh439njp8rAAuFAmi9', '', 1, 1563063927, NULL, NULL),
(123, 10002, 5, '9eMb3klWLGDXa4YH8Lp0FZYz6wxl54Cl', '', 1, 1563063927, NULL, NULL),
(124, 10002, 5, 'fHGJtQ6pfynVAh64qrj7hUhgyTzG1Ddy', '', 1, 1563063927, NULL, NULL),
(125, 10002, 5, 'kAxrbRIOKuTbbE2U8GPdokauvUgYEo8q', '', 1, 1563063927, NULL, NULL),
(126, 10002, 5, 'ln3vY2Crcq9w0OBnlpt2La3McqaR5JeQ', '', 1, 1563063927, NULL, NULL),
(127, 10002, 5, '19SQ3FJkcZKgVIojjuFEk4jLODRA9W3E', '', 1, 1563063927, NULL, NULL),
(128, 10002, 5, 'kGeEvkI17AUfMjcUKhcPVBNAIpB2BbMU', '', 1, 1563063927, NULL, NULL),
(129, 10002, 5, 'HXyrWPCRQMU7suMKNiCjxNxotr75joQ2', '', 1, 1563063927, NULL, NULL),
(130, 10002, 5, 'Blj5OAYwZJnAxaIkBHJYmBjtD7ufcOtP', '', 1, 1563063927, NULL, NULL),
(131, 10002, 5, 'jdaflS6Bn1nlKXDbdo0HYCxOgZVTCtb9', '', 1, 1563063927, NULL, NULL),
(132, 10002, 5, 'dk3wgaUUf6YwW7TmZEzsYqplCxJcvqzo', '', 1, 1563063927, NULL, NULL),
(133, 10002, 5, 'OXQ9EXVGF8BjsKXmolVweo5qKtN2tPAr', '', 1, 1563063927, NULL, NULL),
(134, 10002, 5, 'ZPY6DQvnL9FEbQ4hmPkVyRP1yVTFfeU3', '', 1, 1563063927, NULL, NULL),
(135, 10002, 5, '67VYxHJKrFtxKAr4IRcqotPKvtBbgNhw', '', 1, 1563063927, NULL, NULL),
(136, 10002, 5, 'T2KcYvQDNHHKlUQVeqPLNBv3o4FoDd2q', '', 1, 1563063927, NULL, NULL),
(137, 10002, 5, 'FiLzmDKSvDCYW1ZJ9j95dldAJrstw5Vu', '', 1, 1563063927, NULL, NULL),
(138, 10002, 5, 'EMRBVDCidgWE9MF8PJ1ayCGbQVDh2sug', '', 1, 1563063927, NULL, NULL),
(139, 10002, 5, 'PxMSveXOPLOMhBj64BItKAx0WhqdNB7V', '', 1, 1563063927, NULL, NULL),
(140, 10002, 5, 'PTPnZ1KiMJiEY5cLXsWtUnaIxr6uIBsW', '', 1, 1563063927, NULL, NULL),
(141, 10002, 5, 'HbmZaIGvnCo8Gb3VphXI1OjgLSppDYY6', '', 1, 1563063927, NULL, NULL),
(142, 10002, 5, '25pKyu2GkfqQmFJP7nkdGFUUrUoyOskV', '', 1, 1563063927, NULL, NULL),
(143, 10002, 5, 'RCNg3BhxcWEsIxnT1NCyMHXznCROMvlo', '', 1, 1563063927, NULL, NULL),
(144, 10002, 5, 'ms15J2VzM5akjgfFrdSHiwP279phpNQE', '', 1, 1563063927, NULL, NULL),
(145, 10002, 5, 'KrkOOpeswMoVedldErfDaZ6UCoFN6Y5f', '', 1, 1563063927, NULL, NULL),
(146, 10002, 5, 'fDmpW4puhvaBsJEQ3kjJyRSNaT3XGPxR', '', 1, 1563063927, NULL, NULL),
(147, 10002, 5, '40xNzR7HX5AxifdbNe3dGArYxe3X5dJE', '', 1, 1563063927, NULL, NULL),
(148, 10002, 5, 'AU7OFcieXWUfRZbI7JOzyBhkcIGXWzXA', '', 1, 1563063927, NULL, NULL),
(149, 10002, 5, 'dHSwD3CvntQQHvetE7KPsBjcbQOICqwr', '', 1, 1563063927, NULL, NULL),
(150, 10002, 5, 'M8GMwn2PnQGeQoo53MKkUzQI1HMT8Ivl', '', 1, 1563063927, NULL, NULL),
(151, 10002, 5, 'aBfVfYHBV7nYtWs2kAXrXy1XungHDIuk', '', 1, 1563063927, NULL, NULL),
(152, 10002, 5, 'jNajQFXbBs0eOyLqSKEAMJ8rqu3tfffl', '', 1, 1563063927, NULL, NULL),
(153, 10002, 5, 'o1Tcvgs8GDMyWmzLgVcDsKGFE9fH9J3Y', '', 1, 1563063927, NULL, NULL),
(154, 10002, 5, 'TzXzcOszAhJHpCj4ku6elotS11ZdkTtO', '', 1, 1563063927, NULL, NULL),
(155, 10002, 5, 'lMCKOLHLLRmMjcAkQrqxGd7uqRo3yVIR', '', 1, 1563063927, NULL, NULL),
(156, 10002, 5, 'wD4f6j5m9Kb4N4hDs5oCDiY8vTdMRbiE', '', 1, 1563063927, NULL, NULL),
(157, 10002, 5, '51f4qG9fVk5yaO1jSLiTbG83mYAcHApd', '', 1, 1563063927, NULL, NULL),
(158, 10002, 5, 'CsReFhQPJK1TNRTG47a8qNPt8Ja4RBdt', '', 1, 1563063927, NULL, NULL),
(159, 10002, 5, 'lpLKp6KvqqoFupjiS2YTNX5nIJjOJCVT', '', 1, 1563063927, NULL, NULL),
(160, 10002, 5, '0bfuGjrBrj3EBpIQvoYHps4xO4UoUP3r', '', 1, 1563063927, NULL, NULL),
(161, 10002, 5, 'ooSviqTgTemazAHg56UnbaGHbLECxmyg', '', 1, 1563063927, NULL, NULL),
(162, 10002, 5, 'CfQ9W6K6oRAmuDN6TriJhrMUZJfrhFdj', '', 1, 1563063927, NULL, NULL),
(163, 10002, 5, 'E5psxuXnCTHOTQKGN6Vw4cQVd6Hia4M1', '', 1, 1563063927, NULL, NULL),
(164, 10002, 5, 'IWirMi84DukWtYEvPBfOgy3buaFRpRzR', '', 1, 1563063927, NULL, NULL),
(165, 10002, 5, '58D4m2XSX7lDwxbRFLMgEzbSEKrPVLrA', '', 1, 1563063927, NULL, NULL),
(166, 10002, 5, 'YyyQHfDWwLzoglolx4itkwUUODlkDSHL', '', 1, 1563063927, NULL, NULL),
(167, 10002, 5, 'CDRZ6TFhzmdSd8RpD8o8oH1s781UW9mg', '', 1, 1563063927, NULL, NULL),
(168, 10002, 5, 'yBNqUBW48rkgNgwa0qpnodVDALsGqPYy', '', 1, 1563063927, NULL, NULL),
(169, 10002, 5, 'wiJdkp0tjYx9nryWLGji198MPD1IywJY', '', 1, 1563063927, NULL, NULL),
(170, 10002, 5, 'WE18CIpmuMCxoLBzKVroWy1tuP8vdfj6', '', 1, 1563063927, NULL, NULL),
(171, 10002, 5, 'Ytt9joNDuA41YIETQYRL7W4SP2KXBr3V', '', 1, 1563063927, NULL, NULL),
(172, 10002, 5, 'dJSCFgELGh2ZS4beFVwqCCr6Nm5bDRvx', '', 1, 1563063927, NULL, NULL),
(173, 10002, 5, 'kTTGWxsEEaJFvRzyeuHGou2lD9CCSLHe', '', 1, 1563063927, NULL, NULL),
(174, 10002, 5, '3lbdhNbQiyn798Myo5mqiI86YKXJOfEx', '', 1, 1563063927, NULL, NULL),
(175, 10002, 5, 'BKVWD67HuQPo6QXLK3o8WNQWi5BRclg0', '', 1, 1563063927, NULL, NULL),
(176, 10002, 5, '4sai56VjzSyz4qTBk1Z87WwZEmvYesW8', '', 1, 1563063927, NULL, NULL),
(177, 10002, 5, 'z5YVtpQgvpKBFuYbXzCedFx8ht572l2i', '', 1, 1563063927, NULL, NULL),
(178, 10002, 5, 'uwiRqHne125KQDPNekbbzBlgcrjrbn7P', '', 1, 1563063927, NULL, NULL),
(179, 10002, 5, '8fv7xiDUTnj1DXYIz6qn2PCgajNGsT1l', '', 1, 1563063927, NULL, NULL),
(180, 10002, 5, '53RlWpkB9i7EoR7xy1mTGVfEIrpbI4KI', '', 1, 1563063927, NULL, NULL),
(181, 10002, 5, 'y3auGmwss9fvHJZ9MY9d6cSEqVrVmAnR', '', 1, 1563063927, NULL, NULL),
(182, 10002, 5, 'WuXAE1pwKz7blO0m44uXF3mhiznuPWA1', '', 1, 1563063927, NULL, NULL),
(183, 10002, 5, 'gPtUXk3g1Vv9td3qFqcZMo7Xnjuu6rpT', '', 1, 1563063927, NULL, NULL),
(184, 10002, 5, 'cuDFpfMWADJVdXcmK6gcrSBlJo88Bjza', '', 1, 1563063927, NULL, NULL),
(185, 10002, 5, 'XEZV0DYDXsTOkgJPxAyD7VtqHbFaTPDd', '', 1, 1563063927, NULL, NULL),
(186, 10002, 5, 'laW989oRmsqJrH3iqZePwvTsHC7vc6PN', '', 1, 1563063927, NULL, NULL),
(187, 10002, 5, 'tJ5fnCQzvdsh7orbXX6vNjTKVy8ZyUZk', '', 1, 1563063927, NULL, NULL),
(188, 10002, 5, 'BuPj6LXGguXmozAC4xnl5VPLU4DpnNK8', '', 1, 1563063927, NULL, NULL),
(189, 10002, 5, 'DJtwqyt1vXv5jqsBD8iNf3OvYh4MTIxn', '', 1, 1563063927, NULL, NULL),
(190, 10002, 5, 'xPEJSp20cKj2ZpJjRHM1dU06QjxmBL5P', '', 1, 1563063927, NULL, NULL),
(191, 10002, 5, 'VeV87X2rPdEjEdEez5xSsY15rCVbbacT', '', 1, 1563063927, NULL, NULL),
(192, 10002, 5, 'BfqEssZh8MfR7d9UWQAEeYNeDITxszt7', '', 1, 1563063927, NULL, NULL),
(193, 10002, 5, 'AHDGtBRUVpjTydhTDbRCr2ufhoxex5Ir', '', 1, 1563063927, NULL, NULL),
(194, 10002, 5, 'GAWJV1m37ja9tw81zS4Z5iy4IdBikaP1', '', 1, 1563063927, NULL, NULL),
(195, 10002, 5, 'O7sCDhBGiOsd4hmuTdK12TRLrbGVq7Bn', '', 1, 1563063927, NULL, NULL),
(196, 10002, 5, 'fD8r9AdNl2RUkS9OdJA2E9OhxfdF3KnB', '', 1, 1563063927, NULL, NULL),
(197, 10002, 5, '2sSgUAriSCdBJqoZoWcGYw9GHRDkcEpv', '', 1, 1563063927, NULL, NULL),
(198, 10002, 5, '5UTTfs6IhMDuN0qzc3Pef1EoBD50OuCn', '', 1, 1563063927, NULL, NULL),
(199, 10002, 5, 'fUvsknqwUDXoA1IcnOZNppNJN4Hib93G', '', 1, 1563063927, NULL, NULL),
(200, 10002, 5, '3C1zfvgFgl5SLC78Ogb2FqBqysYwtyn2', '', 1, 1563063927, NULL, NULL),
(201, 10002, 5, 'FqZqdQj4Dadpz6pI98sUHoiaihpNbMk0', '', 1, 1563063927, NULL, NULL),
(202, 10002, 5, 'm0vptkTsrtJ9mRRGEnSffhcesMGSKvbK', '', 1, 1563063927, NULL, NULL),
(203, 10002, 5, 'Uj6J7X5fAI7pNsj4SuzFdxQgHIDTlLTu', '', 1, 1563063927, NULL, NULL),
(204, 10002, 5, 'Cvp9MHaNI1SE3NOeXtquLlwHmA4PXshm', '', 1, 1563063927, NULL, NULL),
(205, 10002, 5, 'H6WQX1usMAuL7tjMJ5ey4evHILSp7Zff', '', 1, 1563063927, NULL, NULL),
(206, 10002, 5, 'bhSMO4lj8rhem9oahewJ6vO2CH1z9Kw6', '', 1, 1563063927, NULL, NULL),
(207, 10002, 5, 'sJxhwDFfHlB4WR1iyEY2zpttIbQfNK9I', '', 1, 1563063927, NULL, NULL),
(208, 10002, 5, 'NM9heQzLxqFnzFuB9Rum9H2q4FJvHfqT', '', 1, 1563063927, NULL, NULL),
(209, 10002, 5, 'NpI3yWOSMpPW2ZMVoUTbcGWxQIjF2qjH', '', 1, 1563063927, NULL, NULL),
(210, 10002, 5, '5P3427rfuAUMYIuQyyGXHyX905GtbOur', '', 1, 1563063927, NULL, NULL),
(211, 10002, 5, 'YGxC33ydryzj9GpgNAfgtKOZBuPFPxmn', '', 1, 1563063927, NULL, NULL),
(212, 10002, 5, 'nCeEXx8BuwNY259YFZJ5OTGqni6NF9hr', '', 1, 1563063927, NULL, NULL),
(213, 10002, 5, 'zjCE3nRt3EhEJmveXCRlQsUNwaQz9Gc4', '', 1, 1563063927, NULL, NULL),
(214, 10002, 5, 'ox2k7CNB1ui1jpkqrxlJxeXqxfgWM8lP', '', 1, 1563063927, NULL, NULL),
(215, 10002, 5, 'RLTs8QrjHaWX9EcbBQ1h3YOeuriitRZv', '', 1, 1563063927, NULL, NULL),
(216, 10002, 5, 'AnmbCXkMoWwWXyxxFvK0fvLWQyxvHzUd', '', 1, 1563063927, NULL, NULL),
(217, 10002, 5, 'MznA0jq9vWTr3hKfe0XDhaRnMHtbp1wH', '', 1, 1563063927, NULL, NULL),
(218, 10002, 5, '8CMCj7pB1N7SJFV7fGlw7bp61zOSlzR7', '', 1, 1563063927, NULL, NULL),
(219, 10002, 5, 'Xroyk1V22unvYeuRdGzaAjKuXiN9Joq4', '', 1, 1563063927, NULL, NULL),
(220, 10002, 5, 'PPMUVB4ibqNGwe5hjvbLMsK7RRJ5kVCz', '', 1, 1563063927, NULL, NULL),
(221, 10002, 5, 'tyg8DmVVJQpfcl5aEvkQx44mET22pREq', '', 1, 1563063927, NULL, NULL),
(222, 10002, 5, 'LlYsyHNGTaiK5x3N92QCpqW21lYUlWI8', '', 1, 1563063927, NULL, NULL),
(223, 10002, 5, 'v3QPq51rs1hbJ3ZCQSRtvoMgRMaKbsBP', '', 1, 1563063927, NULL, NULL),
(224, 10002, 5, 'CWl3Dg33G3L3SKseZra1VmPrZr2mY8cK', '', 1, 1563063927, NULL, NULL),
(225, 10002, 5, 'YmYu5TDNejXJqoJg9IUdRRgejauZh7qB', '', 1, 1563063927, NULL, NULL),
(226, 10002, 5, 'WGRC79OtLnoGdW7jWCpHUBUyihUOPGU5', '', 1, 1563063927, NULL, NULL),
(227, 10002, 5, 'HGjhfnd9nbkQLhanQspTD3SfXOLF8vXC', '', 1, 1563063927, NULL, NULL),
(228, 10002, 5, 'BpQvWkHebZRakahJJ4kV5uAo36mEVP9N', '', 1, 1563063927, NULL, NULL),
(229, 10002, 5, '4c3DJnpQH5j8pNTYk3KRhii4X5a1RLIa', '', 1, 1563063927, NULL, NULL),
(230, 10002, 5, 'nSGyVOSdXVf7VdpXcDXPxm5EB3GmJtvw', '', 1, 1563063927, NULL, NULL),
(231, 10002, 5, 'W1eshxv2IZHPft8nHBzsLEUcFl4Xy5mp', '', 1, 1563063927, NULL, NULL),
(232, 10002, 5, 'WOIHf9VhedYhyKKSWdHVUvuyoIPNQx6w', '', 1, 1563063927, NULL, NULL),
(233, 10002, 5, 'vb3whwqtqtpzeExCKyqj6pAjbC2iGmxG', '', 1, 1563063927, NULL, NULL),
(234, 10002, 5, 'oaw7L3cGLfnJ1r13KsqVSnYtKtnHwKlJ', '', 1, 1563063927, NULL, NULL),
(235, 10002, 5, 'gnLvK3R3F8tEocFQUZAjpQZl5JMEIDSa', '', 1, 1563063927, NULL, NULL),
(236, 10002, 5, 'asp7lCBUYw6oyEN6HbyvUzNet31S332K', '', 1, 1563063927, NULL, NULL),
(237, 10002, 5, 'oP1W9VpeLqS4iHL97AbqJvJzb6QHMqQ7', '', 1, 1563063927, NULL, NULL),
(238, 10002, 5, '1BSBYtjambnfN9opz4me43qKjwBk1hVI', '', 1, 1563063927, NULL, NULL),
(239, 10002, 5, 'gpaJ4k9KRqGHH0SdyFBqnyC61VoAuB6j', '', 1, 1563063927, NULL, NULL),
(240, 10002, 5, 'wdbXJF3xpQ2wdIZuTmOj8rJ4RYWBL3Dk', '', 1, 1563063927, NULL, NULL),
(241, 10002, 5, 'EhBaEmzoQrIUZ1tym75uo3SLpjThURzw', '', 1, 1563063927, NULL, NULL),
(242, 10002, 5, 'nfDKIoGuFzPR8vLYyzVpYC4WRk5zpNra', '', 1, 1563063927, NULL, NULL),
(243, 10002, 5, 'lY5Av8QuN366xhJO67mBugUDhbjU3VMO', '', 1, 1563063927, NULL, NULL),
(244, 10002, 5, 'TFMqOv5QjMHBU5h6QutP8jidyogDY8gH', '', 1, 1563063927, NULL, NULL),
(245, 10002, 5, 'Yq9bDgIBINBCOwtktw2AjjOy7f46abnU', '', 1, 1563063927, NULL, NULL),
(246, 10002, 5, 'azVERBmj8Y1oS189xV2sTMtexcl1Tm7z', '', 1, 1563063927, NULL, NULL),
(247, 10002, 5, 'pOXj0dFDHAQPMBmq2HWFXzB86TLC1eeF', '', 1, 1563063927, NULL, NULL),
(248, 10002, 5, 'aVADBlE8EnTz4I9PXDOcOP4Nj16VWIIh', '', 1, 1563063927, NULL, NULL),
(249, 10002, 5, 'zvznLdlUaz3USKf9vwla45YrDsz5gKvU', '', 1, 1563063927, NULL, NULL),
(250, 10002, 5, 'dsRUXDf3gx6hLq13rxafcCBY8UuQxeJj', '', 1, 1563063927, NULL, NULL),
(251, 10002, 5, 'A81E4BEXjJVNYR4gvu5DSFtXAD7nmdnc', '', 1, 1563063927, NULL, NULL),
(252, 10002, 5, 'Pf9oBEneKxHQTPNdD4sqQ4OXqSWNBjnF', '', 1, 1563063927, NULL, NULL),
(253, 10002, 5, 'X4XMe1AFMVyschAYRiTjsLbpMdgqfqE1', '', 1, 1563063927, NULL, NULL),
(254, 10002, 5, 'o7bXlnftGOnRvsTlUQkO1Gp6oXKM7Fch', '', 1, 1563063927, NULL, NULL),
(255, 10002, 5, '9IqB5HXjKo5D1LJ5SmMyosNxhpvzfpqe', '', 1, 1563063927, NULL, NULL),
(256, 10002, 5, 'duaybklsTEFpc91PReHDHsrsDGjRx64a', '', 1, 1563063927, NULL, NULL),
(257, 10002, 5, '8OHaCRBiSIMszkSbS85ooQyqBFiscawv', '', 1, 1563063927, NULL, NULL),
(258, 10002, 5, 'g7Je1k58fDezgg8ye3tQT5CTesUGJ0l4', '', 1, 1563063927, NULL, NULL),
(259, 10002, 5, 'SOYv4qXdyuYck6LLEFguT2DANwBcXVvi', '', 1, 1563063927, NULL, NULL),
(260, 10002, 5, '72vbpMopywG8XzfOaO44mzrlvkaw8cvT', '', 1, 1563063927, NULL, NULL),
(261, 10002, 5, 'uQ7iQVy0gYl3x2tdpLOpyXU0t6qfa1xF', '', 1, 1563063927, NULL, NULL),
(262, 10002, 5, '7wGlK2rktul8rAnH2nXYjHI1osboIJkK', '', 1, 1563063927, NULL, NULL),
(263, 10002, 5, 'kKEU6q7qmOkBbHYd2ChBHt7ymcodB5Od', '', 1, 1563063927, NULL, NULL),
(264, 10002, 5, 'aiG9hWcP6pnJ8GzhCu52peUijRBqkqOn', '', 1, 1563063927, NULL, NULL),
(265, 10002, 5, 'NJzunXctLTBicu6vfBdr0jjnddQFLKsk', '', 1, 1563063927, NULL, NULL),
(266, 10002, 5, '48ArLKHAd5wjBUxi04HoLhepnuUJA3IH', '', 1, 1563063927, NULL, NULL),
(267, 10002, 5, 'paeRlehseGuANCoCo5VI4aoLNS9NYg0Y', '', 1, 1563063927, NULL, NULL),
(268, 10002, 5, 'b8ePtSIuPWfybtSq676CAanHid7jKVlq', '', 1, 1563063927, NULL, NULL),
(269, 10002, 5, 'tB3ys9IuyZF4rAloGy1R3ySmEiy9fNRE', '', 1, 1563063927, NULL, NULL),
(270, 10002, 5, 'pgu4yUrKYAM1GauahO3VdZQkRceldITI', '', 1, 1563063927, NULL, NULL),
(271, 10002, 5, 'mKurPPgQBaORRpUNku4WFbvj22f326SR', '', 1, 1563063927, NULL, NULL),
(272, 10002, 5, 'yosgWbPLul9yroOVBymWE3bT9I6Ha41D', '', 1, 1563063927, NULL, NULL),
(273, 10002, 5, 'x6gfHzkBR4eBzVmTq4oA2D5fnrp6Rdmo', '', 1, 1563063927, NULL, NULL),
(274, 10002, 5, 'Q2HUbnysB4q7Nar1mZbTGKDNPuIn8f2c', '', 1, 1563063927, NULL, NULL),
(275, 10002, 5, 'Dqp1hEIGtHkUgjHcfDC5vj7ptK27Ifni', '', 1, 1563063927, NULL, NULL),
(276, 10002, 5, 'BPrJACEfJyOv9a2vDIKSupu6I4137r2L', '', 1, 1563063927, NULL, NULL),
(277, 10002, 5, 'WU8aD6DBHE7nSPCIc53aV0zwknKpoowo', '', 1, 1563063927, NULL, NULL),
(278, 10002, 5, 'PP4Bj2uCAUzZHccLIrnP7PnUalaX2dcx', '', 1, 1563063927, NULL, NULL),
(279, 10002, 5, 'q6Alp9a7wKlufC5zjDEtiiZLUNvdsNKl', '', 1, 1563063927, NULL, NULL),
(280, 10002, 5, 'HBUg8z4CMzigdGRWhfd87WfTbgQS42Cc', '', 1, 1563063927, NULL, NULL),
(281, 10002, 5, 'nIoUm5cntSdVYbLgck0GpDHEw1BMXsqB', '', 1, 1563063927, NULL, NULL),
(282, 10002, 5, 'bMKmcIIhJbEiOqeEl66XfDV6GF1MHvhS', '', 1, 1563063927, NULL, NULL),
(283, 10002, 5, 'H22bOKlzD8Q1qwZ1Gw67UUsWlrPgPmDh', '', 1, 1563063927, NULL, NULL),
(284, 10002, 5, 'NGDI6DqkvSdcfqGAbVU8ywpW1qHHh1GP', '', 1, 1563063927, NULL, NULL),
(285, 10002, 5, 'g8IO9YfV9zuWRmxlWExxhCu9M38gpn7M', '', 1, 1563063927, NULL, NULL),
(286, 10002, 5, 's3AEHtbQFGTHYV32ubMVyYj7aZlTPnuN', '', 1, 1563063927, NULL, NULL),
(287, 10002, 5, '3rI2R2OTYuBRKK5jkEBkkXNNDp8r2Pia', '', 1, 1563063927, NULL, NULL),
(288, 10002, 5, '8ui2Z9E82NUDdRKLnZSfcOQaVn7GHJrH', '', 1, 1563063927, NULL, NULL),
(289, 10002, 5, 'kI7q22LYpVDRGL4iy9aDM4PjOcwxkJS5', '', 1, 1563063927, NULL, NULL),
(290, 10002, 5, 'c2lW5rnHSJjNkE7hhcDv4xqWMvkn2vx2', '', 1, 1563063927, NULL, NULL),
(291, 10002, 5, 'CJhRngN0PXnUf8YDq6ui2aIPJDX8RBfE', '', 1, 1563063927, NULL, NULL),
(292, 10002, 5, 'wSVGotjWlcsEW6tbdoqPBjlTeEdiRcXa', '', 1, 1563063927, NULL, NULL),
(293, 10002, 5, '9fVD4kzqp7fqfr4xElkckRmo3uFWuMCv', '', 1, 1563063927, NULL, NULL),
(294, 10002, 5, 'YutJxey6OUUlNXTyAOxWmN7C6XtMiJKo', '', 1, 1563063927, NULL, NULL),
(295, 10002, 5, 'AhozmzHpGDvYGlg9hoeMH4WEnaDPBAnz', '', 1, 1563063927, NULL, NULL),
(296, 10002, 5, 'b3Nc7hp9k8grVi1JnOr3A7fFvtHL3ixm', '', 1, 1563063927, NULL, NULL),
(297, 10002, 5, '8JPEoiU14GHHJ6ofH4RYKqMRQVDCQqzi', '', 1, 1563063927, NULL, NULL),
(298, 10002, 5, 'Qws34pY554myGFYiWhhAG2bgXJCIyvsC', '', 1, 1563063927, NULL, NULL),
(299, 10002, 5, '4sYqecqFzMOKKfbGY1c8WKEgCtZmFvTu', '', 1, 1563063927, NULL, NULL),
(300, 10002, 5, 'NPFI9oisY8pbyG7r8612cZyroSI2nFyg', '', 1, 1563063927, NULL, NULL),
(301, 10002, 5, 'Suh8l9j7M6cYKgrQOGCpJjKrPkOjN3wE', '', 1, 1563063927, NULL, NULL),
(302, 10002, 5, 'BDbCI2pkiIXKjiUGBKSaouqLhfyGWFaY', '', 1, 1563063927, NULL, NULL),
(303, 10002, 5, '08bTDbYtySYioJsDViosjGgoKTcfVenq', '', 1, 1563063927, NULL, NULL),
(304, 10002, 5, '53O2PaSiIxDUQrEu89TfHAkuKPxDrBRI', '', 1, 1563063927, NULL, NULL),
(305, 10002, 5, '7DpiVkbG0Dk8cgwFF6HakquIYSQG26bv', '', 1, 1563063927, NULL, NULL),
(306, 10002, 5, 'V8MqKfk7zxYv7Iozh2ocksaoSbreAO63', '', 1, 1563063927, NULL, NULL),
(307, 10002, 5, 'DweJtvUD1GGyHdOJ1ch4QkLsmC0u1nj2', '', 1, 1563063927, NULL, NULL),
(308, 10002, 5, 'lCe4FOimG6ckAB48ny6LKb7nW48pVRDF', '', 1, 1563063927, NULL, NULL),
(309, 10002, 5, 'zEPcJHIXtYgurW66c22ySYDLfWtFe35c', '', 1, 1563063927, NULL, NULL),
(310, 10002, 5, '5Pzqt28SshclTjIkPom3CLXmbEzzJWMo', '', 1, 1563063927, NULL, NULL),
(311, 10002, 5, 'tfjrt3Uh9E7z3Ss284b6nKM1D4juKJVp', '', 1, 1563063927, NULL, NULL),
(312, 10002, 5, 'pXP3JJwzkq8asuOAWvpX8gMT3iX1QJOD', '', 1, 1563063927, NULL, NULL),
(313, 10002, 5, '8KOCIurEV69Hm9peTFSUG7ANJvtxWmDF', '', 1, 1563063927, NULL, NULL),
(314, 10002, 5, '2qpVinNgb6BTwJIWFaAFm89bfpGBGa3X', '', 1, 1563063927, NULL, NULL),
(315, 10002, 5, 'VtG19tVxSmMFfel4NinWQ4RaYvki5cpv', '', 1, 1563063927, NULL, NULL),
(316, 10002, 5, '7TJsGDKzYkLCXEmcnOtzkEme2YAcSPmN', '', 1, 1563063927, NULL, NULL),
(317, 10002, 5, 'ipidB54tIFyl1hX8cJLVRWJMqCN8eoqe', '', 1, 1563063927, NULL, NULL),
(318, 10002, 5, 'taOkzYiJgSQKqLbmbXvTOFvldPtz4hVQ', '', 1, 1563063927, NULL, NULL),
(319, 10002, 5, 'hYV0TjaCv1lWWSp8oVKXfgS7gzIN4M5I', '', 1, 1563063927, NULL, NULL),
(320, 10002, 5, 'P42qx0o29ZBehakKgUE8ytLAdRSvLer7', '', 1, 1563063927, NULL, NULL),
(321, 10002, 5, 'LH6MW8lIGH4h4LtMHtQW6NU8KtRnBfqd', '', 1, 1563063927, NULL, NULL),
(322, 10002, 5, 'l9SN5rmClFArSymMPv1F3HZpwrY4aNTA', '', 1, 1563063927, NULL, NULL),
(323, 10002, 5, 'TxsTlWXQNN6qrcfOGf6yhcJeRghn81gx', '', 1, 1563063927, NULL, NULL),
(324, 10002, 5, 'dY3tlCZAVofNzUwbZ1zrBJgdvk3h0ODo', '', 1, 1563063927, NULL, NULL),
(325, 10002, 5, 'JT5N3Ro4sHaSFEX1DAuiq68V1mtq5sXA', '', 1, 1563063927, NULL, NULL),
(326, 10002, 5, 'LfoeT2Eaos9c99jEmWz758DpgPKGsgpM', '', 1, 1563063927, NULL, NULL),
(327, 10002, 5, 'il86TabsmSZqQE9vkE4PWevdgF7s5bUO', '', 1, 1563063927, NULL, NULL),
(328, 10002, 5, '5udnN4O5D5RylGGPU9cAxDvC5lHUtd2Q', '', 1, 1563063927, NULL, NULL),
(329, 10002, 5, 'e1sc5sX59WBybHhXswdJKwgdaTySOsg4', '', 1, 1563063927, NULL, NULL),
(330, 10002, 5, '79ixoSi4ymgxPWlyVo6ttagT6rylDz5j', '', 1, 1563063927, NULL, NULL),
(331, 10002, 5, 'Z83XVDiJthe84eepNiYFLe83VCQIDEqv', '', 1, 1563063927, NULL, NULL),
(332, 10002, 5, 'RdgYOjLEt5WWRuvuKTwSSoo7V4S3VMH1', '', 1, 1563063927, NULL, NULL),
(333, 10002, 5, 'xvkJ3LRh98pRh2L7TPxag62kUZGrnyAX', '', 1, 1563063927, NULL, NULL),
(334, 10002, 5, 'Kw977Ta2KXD8JInQJLzLIRQjAwXZRqyg', '', 1, 1563063927, NULL, NULL),
(335, 10002, 5, 'Hs5MRcg2C3pwJOfgV0jpObgRlCSom9dB', '', 1, 1563063927, NULL, NULL),
(336, 10002, 5, 'qx3E4BJwiuPDIR6YgkFqmSnDgyWhmXSQ', '', 1, 1563063927, NULL, NULL),
(337, 10002, 5, '9sdnmUyUucty86Lazhl6schw0MQoeUyp', '', 1, 1563063927, NULL, NULL),
(338, 10002, 5, 'BHFBJ5EW2DusyFb4i0YmUtF3hJyJ5SSK', '', 1, 1563063927, NULL, NULL),
(339, 10002, 5, 'BTEhZbr4zXsY1Gtz4IzE5G3SoiAVrwlW', '', 1, 1563063927, NULL, NULL),
(340, 10002, 5, 'X1eQlbthqpuVT3GdbNoONanxEwhsG4kc', '', 1, 1563063927, NULL, NULL),
(341, 10002, 5, 'oz67I6OAsHg8Oukv1y8FOJitZqjHF7uB', '', 1, 1563063927, NULL, NULL),
(342, 10002, 5, 'C9xNFFTL7UTWLE8BFwS5yFQCgNcgqcT2', '', 1, 1563063927, NULL, NULL),
(343, 10002, 5, 'c9P35JHfZgdjUPeHrOt2EKIe7XbALtTe', '', 1, 1563063927, NULL, NULL),
(344, 10002, 5, 'aSHBCfxLjoRBgzRGYr6UBIjAkTXbynaK', '', 1, 1563063927, NULL, NULL),
(345, 10002, 5, 'eBQ68492euXP49pSiI2DycImuAskafqH', '', 1, 1563063927, NULL, NULL),
(346, 10002, 5, 'p9omQ7G73d0HeSqFWd2E5r2mTFGUnFNi', '', 1, 1563063927, NULL, NULL),
(347, 10002, 5, 'Zo9L9C6rUVnYMJgV9UEawamefFObkoEx', '', 1, 1563063927, NULL, NULL),
(348, 10002, 5, 'wbbqozGbrI7rN3eOpYPiCFz5k1ylhSNi', '', 1, 1563063927, NULL, NULL),
(349, 10002, 5, 'kzNQTqJ9JYnVtlLbGAKRtJiGSNUOCMee', '', 1, 1563063927, NULL, NULL),
(350, 10002, 5, 'aY02hNzydNi1Z2gkihLckLe1BiHHKurl', '', 1, 1563063927, NULL, NULL),
(351, 10002, 5, 'aDMMNFnJkPbikucL2yGhmBQIpIzAAdXt', '', 1, 1563063927, NULL, NULL),
(352, 10002, 5, 'R5KEyMuzgMr9UHM1aitU92zh6XkKLkyk', '', 1, 1563063927, NULL, NULL),
(353, 10002, 5, 'wdb8TTGYfYZ7od1czqeQojW9stUrr6TP', '', 1, 1563063927, NULL, NULL),
(354, 10002, 5, 'MgI75BnKwLhzaI5olbi4CoidYURgDQfk', '', 1, 1563063927, NULL, NULL),
(355, 10002, 5, 'zmTy4gMVyF8ixJH5fW7GyMFgraCf657b', '', 1, 1563063927, NULL, NULL),
(356, 10002, 5, 'eiY3otCbxrLOvMz9DTSseuUnVOi1AJvx', '', 1, 1563063927, NULL, NULL),
(357, 10002, 5, 'Rqn6GaQMXwNiw5ZenWAPMzWPiEXDMb8K', '', 1, 1563063927, NULL, NULL),
(358, 10002, 5, 'osogks6LIegWLUrpOEcH6XteBRoxGLxm', '', 1, 1563063927, NULL, NULL),
(359, 10002, 5, 'eIXHpf2DlLwd9VNYvuvYBPNYk16DJx5n', '', 1, 1563063927, NULL, NULL),
(360, 10002, 5, 'aUwlUhxzYsHjb15jUvGfImDsNjzwVTus', '', 1, 1563063927, NULL, NULL),
(361, 10002, 5, '1xYrjDld9amgC95iSzHTjhFQCoyILiS6', '', 1, 1563063927, NULL, NULL),
(362, 10002, 5, '0UajSd3DalzA4MqW67Ox6rDhYKPJxqjQ', '', 1, 1563063927, NULL, NULL),
(363, 10002, 5, 'Bl7VvejD0DGHHJO76c7LXACDRsku2Rsn', '', 1, 1563063927, NULL, NULL),
(364, 10002, 5, 'iRAcMSbXlikwF8NNHcyRq4kOe2yKUmlt', '', 1, 1563063927, NULL, NULL),
(365, 10002, 5, '5NNo5Zwgz6JzXeJfOM1nzyDodUqMIb8o', '', 1, 1563063927, NULL, NULL),
(366, 10002, 5, 'AK6RS6sUWLbb05cuEx1KzahxtteKSDVn', '', 1, 1563063927, NULL, NULL),
(367, 10002, 5, 'ppyulLWkE6fmJWZdBe0KerYkWOgk7tiY', '', 1, 1563063927, NULL, NULL),
(368, 10002, 5, 'Kgs1Onq8iuzLAKYxYkQTrybF3vBQX5B7', '', 1, 1563063927, NULL, NULL),
(369, 10002, 5, '5UIrCGRvEVi4uUtJs0x31HKwSKB7A3iO', '', 1, 1563063927, NULL, NULL),
(370, 10002, 5, 'UlqbBy6rHUlWVW4QYUU38ztJGksLJXb0', '', 1, 1563063927, NULL, NULL),
(371, 10002, 5, 'snrdOqAdJLrmdAdDLVjw8BcZNy1haPM8', '', 1, 1563063927, NULL, NULL),
(372, 10002, 5, 'uzWkf4PqygwL4K0R5OEZkPPdZ9SGw6m7', '', 1, 1563063927, NULL, NULL),
(373, 10002, 5, 'g9Ablaci5vSBuYclTimAMUIbcdEp4EpJ', '', 1, 1563063927, NULL, NULL),
(374, 10002, 5, 'ATcO846WqCdcijYinapbrc0W6bbdqMee', '', 1, 1563063927, NULL, NULL),
(375, 10002, 5, 'gVfegjcsNAZR5LnSgiDYmVgHFIe61fEr', '', 1, 1563063927, NULL, NULL),
(376, 10002, 5, 'cAXohMDuAXIJZmaN4p6pTZ8m9V9QKU8T', '', 1, 1563063927, NULL, NULL),
(377, 10002, 5, 'G7NTEXr9jIFW3CwqTlMrOpQfSCQ8Pl9Q', '', 1, 1563063927, NULL, NULL),
(378, 10002, 5, 'mXlN27mTcr5pzrg9jdMIxuN82d46xowB', '', 1, 1563063927, NULL, NULL),
(379, 10002, 5, 'rEj45z2GRRMw1W1oY3T2YTBPMI1QbhOn', '', 1, 1563063927, NULL, NULL),
(380, 10002, 5, 'VpMINYwthHa7B5VNHNa3ee16tL51QL0W', '', 1, 1563063927, NULL, NULL),
(381, 10002, 5, '83dC2ApgVXMwyXwZNH5wijsokE6kQ1C8', '', 1, 1563063927, NULL, NULL),
(382, 10002, 5, 'yJ8rMzQEvY58PwteYmaiMukwnSEi2pa9', '', 1, 1563063927, NULL, NULL),
(383, 10002, 5, 'M53bvpYOapI69in9fPXY6vavP3NPWa1l', '', 1, 1563063927, NULL, NULL),
(384, 10002, 5, 'owyI6sHrQI87KyjgrKGqNqBY9B5YVkdD', '', 1, 1563063927, NULL, NULL),
(385, 10002, 5, 'Qn8PVKjKlN9vuT78WGPQW61WOy19DQMs', '', 1, 1563063927, NULL, NULL),
(386, 10002, 5, 'SBc0rcSkXjIvH53RYrpoLzWCdEnR6tpC', '', 1, 1563063927, NULL, NULL),
(387, 10002, 5, 'FERP7datNs5s8prEnS0qFILBleJ8Bz37', '', 1, 1563063927, NULL, NULL),
(388, 10002, 5, 'Vao6AudFjjed1xIDaflP3w6a51wROh7U', '', 1, 1563063927, NULL, NULL),
(389, 10002, 5, '4XTjYH1iBX0oYruBcecO7pdj62wjsaiE', '', 1, 1563063927, NULL, NULL),
(390, 10002, 5, 'kKq8XPQbG2BbdT6LN7N1Mv3DmPZ3md1H', '', 1, 1563063927, NULL, NULL),
(391, 10002, 5, 'GF7M7tnVQXzdMBQmXUaiqkoSBsB3Kjcq', '', 1, 1563063927, NULL, NULL),
(392, 10002, 5, 'N0JCQ2qmEtJdlCy1SJ42QOvlKBaz7xki', '', 1, 1563063927, NULL, NULL),
(393, 10002, 5, 'jTicS9wF7Ae7Soocw8Sk4fye18KzAazi', '', 1, 1563063927, NULL, NULL),
(394, 10002, 5, 'LqU5Il756wkElidrvLrtIZFz3u4fz8pi', '', 1, 1563063927, NULL, NULL),
(395, 10002, 5, 'mUwpImxid4YEgO1Tr4Z0iNEO18QElPlS', '', 1, 1563063927, NULL, NULL),
(396, 10002, 5, 'EIIOLriOI6rBro4N9ADUkYTSKz2gpLuz', '', 1, 1563063927, NULL, NULL),
(397, 10002, 5, 'SrbSxgb8SvOpG3HuuItsIatCkf6ZEjQV', '', 1, 1563063927, NULL, NULL),
(398, 10002, 5, 'wtrbNMMWvKhJvp4LUwykDoI2Ho32KTrp', '', 1, 1563063927, NULL, NULL),
(399, 10002, 5, 'iiIvHgAJr1NfTLQSpil2JiIKVnOKP3Rj', '', 1, 1563063927, NULL, NULL),
(400, 10002, 5, 'Z5TlMHKW82zvSZJnyM8a4L5RiXHxvSl1', '', 1, 1563063927, NULL, NULL),
(401, 10002, 5, 'J5nOKK18Ka5xprrGcj3RgTQn1Gxo0q7w', '', 1, 1563063927, NULL, NULL),
(402, 10002, 5, 'VXKNn1pLwPdkp7OA8WMwxwABgQEV8yRH', '', 1, 1563063927, NULL, NULL),
(403, 10002, 5, 'GK7JyMpzU0sjydwFPJbmEk33JEDIz1aB', '', 1, 1563063927, NULL, NULL),
(404, 10002, 5, 'dB10Lm2WjQBNuuHtvYjWA36aIoS9rwKE', '', 1, 1563063927, NULL, NULL),
(405, 10002, 5, '5QhEpHylIYhgJEVab2zckTvAgteOIyYv', '', 1, 1563063927, NULL, NULL),
(406, 10002, 5, 'wgaHGTaqwvkWuWtbhrTo6WQTJ98ryRSN', '', 1, 1563063927, NULL, NULL),
(407, 10002, 5, 'VNLcSbe2rmODxMsO2tuD2BdSJaX9gXzO', '', 1, 1563063927, NULL, NULL),
(408, 10002, 5, 'QycShpzNryiOmDYtzJtwLKgrPJszJNy9', '', 1, 1563063927, NULL, NULL),
(409, 10002, 5, 's4cmDFQirJ8HFFpWDfxYf4X2Gg8hWC76', '', 1, 1563063927, NULL, NULL),
(410, 10002, 5, '3DwshvjFvvfmtlY2rBttz8s7VqKn5TD8', '', 1, 1563063927, NULL, NULL),
(411, 10002, 5, 'QOIWTvUrglgTp4ve5lrqpRaRNnrkW0oJ', '', 1, 1563063927, NULL, NULL),
(412, 10002, 5, 'NctYCrDBomjVhGBepINekt5DrJnFD05W', '', 1, 1563063927, NULL, NULL),
(413, 10002, 5, '6kijCCWPfF52FhQrhlVXia6SvTIATlar', '', 1, 1563063927, NULL, NULL),
(414, 10002, 5, 'Toi1mOLdnXV9cvN3lJD27wtOA9BFo0v3', '', 1, 1563063927, NULL, NULL),
(415, 10002, 5, 'prarQ7RoZd0XmdrowxYEkRYmOoUsvFOu', '', 1, 1563063927, NULL, NULL),
(416, 10002, 5, '8zllp3k7yyrlD6vleCGkkCLajqtlnpvr', '', 1, 1563063927, NULL, NULL),
(417, 10002, 5, '83XUfTkN6IHvFYSTNTPxwCSy5XFaBtfq', '', 1, 1563063927, NULL, NULL),
(418, 10002, 5, 'GNyK2fqCBRwufZtrJJCcvLHFyhRwphr3', '', 1, 1563063927, NULL, NULL),
(419, 10002, 5, 'OCp6GwdPcz3KVvpGGC9SxpqU3fbkurTb', '', 1, 1563063927, NULL, NULL),
(420, 10002, 5, 'ZfKWUkJFkAQqD3cIuaaMB7v5F5IGynZv', '', 1, 1563063927, NULL, NULL),
(421, 10002, 5, 'CQoVxlDLIYmRdQ9HSYCwPcKjn6B5zxRi', '', 1, 1563063927, NULL, NULL),
(422, 10002, 5, 'lpRNNmHi3dreadOntckcPjJJ58ExNuhJ', '', 1, 1563063927, NULL, NULL),
(423, 10002, 5, 'KbLWtU8uaS5GQ2DBtULpLjPLwSyuoi2z', '', 1, 1563063927, NULL, NULL),
(424, 10002, 5, 'alAKtJ1PxxzVDniGTPFqNfLh45uFJn21', '', 1, 1563063927, NULL, NULL),
(425, 10002, 5, 'iVt37LJwvBkTYzWA31e4qlRfQiS7QYWZ', '', 1, 1563063927, NULL, NULL),
(426, 10002, 5, '7WNMnBqFUHzQ6ho4SBWWqInlowhmitX6', '', 1, 1563063927, NULL, NULL),
(427, 10002, 5, 'mzkfd5UmQa5arKLEjIkCAsz5DPqo6lzJ', '', 1, 1563063927, NULL, NULL),
(428, 10002, 5, 'OOocp4EgnB9WAQCrvJPLzSBQlMnLnriH', '', 1, 1563063927, NULL, NULL),
(429, 10002, 5, 'tN5JYd1n8eiW945Ku7IrWPUJZ32rMQdd', '', 1, 1563063927, NULL, NULL),
(430, 10002, 5, 'vU6JpxWY5jbhUxbBKRlcHQiwH3f4O8H6', '', 1, 1563063927, NULL, NULL),
(431, 10002, 5, 'XHv6UxpvgwjxMVyUO7eqsqkwfAz3P5nW', '', 1, 1563063927, NULL, NULL),
(432, 10002, 5, 'Kvh9Y2FArMVnNHDJlsa8C2jmYdW62FuQ', '', 1, 1563063927, NULL, NULL),
(433, 10002, 5, 'YpPV9tqTRoQ7Rod44v1kd7PS9KERIOgD', '', 1, 1563063927, NULL, NULL),
(434, 10002, 5, '4UlpptAY27yHtuWP4Xw6veNi7MbwEb3z', '', 1, 1563063927, NULL, NULL),
(435, 10002, 5, 'Yub7qeOsyeFMkIDXMV0OH6QCqCVLVbyb', '', 1, 1563063927, NULL, NULL),
(436, 10002, 5, 'Fj6XqVG8p57sSu3ueUltxaCAJcZEUhDC', '', 1, 1563063927, NULL, NULL),
(437, 10002, 5, 'BxZhIQNR9f5lHach9ooOVX5QaWebzshM', '', 1, 1563063927, NULL, NULL),
(438, 10002, 5, 'gCvgwKHClJ4eOuKLhHIrXmH0wQlEtPCT', '', 1, 1563063927, NULL, NULL),
(439, 10002, 5, 'HFbtewzY8Gl7WqNXiB15xWofSfhng7HA', '', 1, 1563063927, NULL, NULL),
(440, 10002, 5, 'LiluPQASDQXzhQQPYyvB79C1mjVV2MhA', '', 1, 1563063927, NULL, NULL),
(441, 10002, 5, '2LhfKNfWwDBUuqD1TUTFAWeIfEEcprvG', '', 1, 1563063927, NULL, NULL),
(442, 10002, 5, 'nsATFqMW1SPQPijpyFTn83pwFv96Shxr', '', 1, 1563063927, NULL, NULL),
(443, 10002, 5, 'IDYGCa1AN29HA4pzgLKiCKd32HObmvQI', '', 1, 1563063927, NULL, NULL),
(444, 10002, 5, 'T9q2uq12X7tJJO4VSjMAuRUyp1aRSP99', '', 1, 1563063927, NULL, NULL),
(445, 10002, 5, '3nSomBD4pb2rHGgf9tgd7nNcGhz4aalT', '', 1, 1563063927, NULL, NULL),
(446, 10002, 5, 'LIkbhyS7AI4PjYU5nMlphRRToQczP27H', '', 1, 1563063927, NULL, NULL),
(447, 10002, 5, 'DUS9mA3AjXBkoBJ4ICfYpIMYAjNXQWO2', '', 1, 1563063927, NULL, NULL),
(448, 10002, 5, 'w2AJXRvYHZutm1X3UtnPDrntHqngNR8X', '', 1, 1563063927, NULL, NULL),
(449, 10002, 5, 'KRigEQVxdzLOkyYRdVADWdpLkNpVdq3s', '', 1, 1563063927, NULL, NULL),
(450, 10002, 5, 'xsjQ6VrRYlh51NBGxyA8C5prCXBBbKYm', '', 1, 1563063927, NULL, NULL),
(451, 10002, 5, '8P8VwfW6dqyZQzCmh4r0M1GzF1UToPZ2', '', 1, 1563063927, NULL, NULL),
(452, 10002, 5, 'nmYhoA9SPjMWW8Bns0xHDkdfc6SpnHHR', '', 1, 1563063927, NULL, NULL),
(453, 10002, 5, 'aps2K9ZAbeuDaV8urQvvq2MtkAyui3NW', '', 1, 1563063927, NULL, NULL),
(454, 10002, 5, 'GARfKslBktICbc4sYm4M5SgpksHv6Ep1', '', 1, 1563063927, NULL, NULL),
(455, 10002, 5, 'ziQfuLxOx84x3pJ2yd4eqGuIiHyWpqo2', '', 1, 1563063927, NULL, NULL),
(456, 10002, 5, 'aNcIurLRoaAdSKMkAmtArYnYtRd9LWxO', '', 1, 1563063927, NULL, NULL),
(457, 10002, 5, 'ORsQYKsoTohQwfGa168vO9Gt8FmA1ChD', '', 1, 1563063927, NULL, NULL),
(458, 10002, 5, 'RVvub3ciZSJAMHBrXyB96JXjNlBHCHCm', '', 1, 1563063927, NULL, NULL),
(459, 10002, 5, 'F3FBE39GuLoiPspncWy2m6sRMMBnKIiz', '', 1, 1563063927, NULL, NULL),
(460, 10002, 5, 'TNVvBLeP8Mexb7gITQbCLtZnZhcJJ94l', '', 1, 1563063927, NULL, NULL),
(461, 10002, 5, 'E9Q4QUR5GYQxWLtJP5wXsMFXw03d3LXn', '', 1, 1563063927, NULL, NULL),
(462, 10002, 5, 'cY6PL9pQS8EpQPwUrO37DDLsLowdxSXB', '', 1, 1563063927, NULL, NULL),
(463, 10002, 5, 'sKk0DUs6vHFJ3V5sidbxFaq3sMqq4C2h', '', 1, 1563063927, NULL, NULL),
(464, 10002, 5, '0lu8Pt3xi895liT4F6AdCICfMrniaERO', '', 1, 1563063927, NULL, NULL),
(465, 10002, 5, 'y6kTRPhiIOUg12einPj69ef9oeFgdRQn', '', 1, 1563063927, NULL, NULL),
(466, 10002, 5, 'pBVJ68iFU1rFsk04COnWSwjiap6PTve1', '', 1, 1563063927, NULL, NULL),
(467, 10002, 5, 'xuAnMgMaCJYY7ehtVSh9craEaLHV859l', '', 1, 1563063927, NULL, NULL),
(468, 10002, 5, '2XfcLdj3GxKEnd46Ks4eRVzNAvNsNaGM', '', 1, 1563063927, NULL, NULL),
(469, 10002, 5, 'fILc6AGb90KDodUmVi46VC9GBGgRNZWC', '', 1, 1563063927, NULL, NULL),
(470, 10002, 5, 'lOjXLfiAcZrCSTHwLmBIoI8f5aZec9V9', '', 1, 1563063927, NULL, NULL),
(471, 10002, 5, 'cfqIFYqJKptBaKT82FSO6o25Ia3E48LH', '', 1, 1563063927, NULL, NULL),
(472, 10002, 5, 'pYglAPs2CcOJ5CVQJ8ncX1Pu4j9LI6T9', '', 1, 1563063927, NULL, NULL),
(473, 10002, 5, 'flBoOzswGc4IQsAy3OQYUtGNr97yrqbo', '', 1, 1563063927, NULL, NULL),
(474, 10002, 5, '1xpVr1trwuDNECYrzpLH9lFY2o7plWWn', '', 1, 1563063927, NULL, NULL),
(475, 10002, 5, 'rrHU4dpd3RAR7XFhQod5PIXpmTqvJVAI', '', 1, 1563063927, NULL, NULL),
(476, 10002, 5, 'EXUVPFxRVwiVT23wajQH30cuSZ85GJbz', '', 1, 1563063927, NULL, NULL),
(477, 10002, 5, 'bQtulY9i6GUTMvoneiM0iMTcnffFZsOU', '', 1, 1563063927, NULL, NULL),
(478, 10002, 5, '4z6chuCVke6jPAQPyQ547iRjhETYMIP9', '', 1, 1563063927, NULL, NULL),
(479, 10002, 5, 'WaVzaBIAbbnW4w1o5hh1P3n2pbSmMLPD', '', 1, 1563063927, NULL, NULL),
(480, 10002, 5, 'dxohvX88JK7Y6Vyw7Jw8HLX2UDDDUCqO', '', 1, 1563063927, NULL, NULL),
(481, 10002, 5, 'CG6frLgcW5nHJCLpNKGDuW1r83wd8eJd', '', 1, 1563063927, NULL, NULL),
(482, 10002, 5, '4MjEgXtftty8syi5eI6N1tA8F0G74mGd', '', 1, 1563063927, NULL, NULL),
(483, 10002, 5, '5rXLVQCSdB5217YtAtCJDRhbew3ApCLR', '', 1, 1563063927, NULL, NULL),
(484, 10002, 5, 'qKIi7th3tNW6siex0cW5EWp8GXkJrBTF', '', 1, 1563063927, NULL, NULL),
(485, 10002, 5, 'CEUKaP3oZbqsnm8u8P9XBTaFpWdt1CU6', '', 1, 1563063927, NULL, NULL),
(486, 10002, 5, 'Q5FRmDCqSPd1spt1wV13swK8JEoFWSds', '', 1, 1563063927, NULL, NULL),
(487, 10002, 5, 'HlwnZOkln1g5OwgNF4YRAghweo4KPOEE', '', 1, 1563063927, NULL, NULL),
(488, 10002, 5, 'DaOPGMEEvg64cnswVBSpnlse3phu8dQ2', '', 1, 1563063927, NULL, NULL),
(489, 10002, 5, 'abxWCzw7rpfGnjplsUr8lkC6mrKmEegv', '', 1, 1563063927, NULL, NULL),
(490, 10002, 5, 'w5TY9HekcV9uOpUAvQfv9pXRBHO8yS4c', '', 1, 1563063927, NULL, NULL),
(491, 10002, 5, 'f8Osa50UWtuyFS55zIKUJ5QqrmeB4jZH', '', 1, 1563063927, NULL, NULL),
(492, 10002, 5, 'Pvcpwjs5r2RVgALLABzULFElTiFLFNp1', '', 1, 1563063927, NULL, NULL),
(493, 10002, 5, 'vd8JsRus8Uj8Ii8UH5MTzn9Zg9JmQGip', '', 1, 1563063927, NULL, NULL),
(494, 10002, 5, 'QDFp5dFVgwTN1lRnuSTaXFu6lUPeLcJk', '', 1, 1563063927, NULL, NULL),
(495, 10002, 5, 'p7MAhR96vaGJ2cUxDxIxvpGi3be3uP6m', '', 1, 1563063927, NULL, NULL),
(496, 10002, 5, '81ib5rsqpXPGkPSEhWNIa8eNxGo4FkHN', '', 1, 1563063927, NULL, NULL),
(497, 10002, 5, '6ReFp9EcUVugP6R5vb9H7qbFljDxB69S', '', 1, 1563063927, NULL, NULL),
(498, 10002, 5, 'vO3FxN1eBTWDoHMoNoSLrbtBmE1S10pQ', '', 1, 1563063927, NULL, NULL),
(499, 10002, 5, 'cbNkVETWDuH9yvLwjTyxuoI19dEqI5vu', '', 1, 1563063927, NULL, NULL),
(500, 10002, 5, '5AgFj7YgixzaFeKWLQqtP3OMqngt3IqX', '', 1, 1563063927, NULL, NULL),
(501, 10002, 5, 'LXyLz9mvvAPbcawWpJUb3NMM9Diu7tyQ', '', 1, 1563063927, NULL, NULL),
(502, 10002, 5, '4AQxPvPhHK9VuKKJOORPDJXMlOhghhIU', '', 1, 1563063927, NULL, NULL),
(503, 10002, 5, 'HoyXlcPqabif5slMnXHMOP1eFrs9eW8Q', '', 1, 1563063927, NULL, NULL),
(504, 10002, 5, 'VixvdhbqPiPc8D7WWd5mZI5gt1S9vpUs', '', 1, 1563063927, NULL, NULL),
(505, 10002, 5, 'lf9NddS6n7UOZ7Q2pJdBSEdLgY3UrHYk', '', 1, 1563063927, NULL, NULL),
(506, 10002, 5, 'AmCUu3GpDrBPUoqXUyvJAzSD9rgNd5Zo', '', 1, 1563063927, NULL, NULL),
(507, 10002, 5, 'IF3Vk16KTVA1hboz2JpoILHt3qLkYBk9', '', 1, 1563063927, NULL, NULL),
(508, 10002, 5, 'l6mkWcsbW389XhN93Rm1fxRID3nNWePz', '', 1, 1563063927, NULL, NULL),
(509, 10002, 5, '6HOCZGcUFHpLL4KlkDyhmgHTnEqGjUaF', '', 1, 1563063927, NULL, NULL),
(510, 10002, 5, '7xDEc1Rd581YXNmkgmCQhKQuJ1k9rpJ6', '', 1, 1563063927, NULL, NULL),
(511, 10002, 5, 'qPDUKlMK2qqYc5QPWhfLLgXdsk7S7obS', '', 1, 1563063927, NULL, NULL),
(512, 10002, 5, 'xpkSYEmtYJbklSJzQ3rmXutwXF3noYWk', '', 1, 1563063927, NULL, NULL),
(513, 10002, 5, 'ntk8xYsT6Fwg3OEdeOhHA2LVRRImgxgN', '', 1, 1563063927, NULL, NULL),
(514, 10002, 5, 'CtFAtTd8jAabRlfARFKz3zQdKqusWMVE', '', 1, 1563063927, NULL, NULL),
(515, 10002, 5, 'aAEnOOlyNiNk6p7AHC8qteropuOJzGjp', '', 1, 1563063927, NULL, NULL),
(516, 10002, 5, 'CfyWs2xvXxa3yU6kM6WQcu5DV3ihAxaT', '', 1, 1563063927, NULL, NULL),
(517, 10002, 5, 'X7aI1abU2XSUiicOcDSBsuPp1m9KsplF', '', 1, 1563063927, NULL, NULL),
(518, 10002, 5, 'YhVsve8oW12X9U8bOdNgG6E8dLL67OUq', '', 1, 1563063927, NULL, NULL),
(519, 10002, 5, 'HsJ4hRLEEMzqAEssXkNRABuEakN1bGkx', '', 1, 1563063927, NULL, NULL),
(520, 10002, 5, 'udumjS0kaeJiork9Z7Vc6OUU2DIHN9Sv', '', 1, 1563063927, NULL, NULL),
(521, 10002, 5, 'dbKssHIgKQEr17v3zQJVUXs7a9hmMN0x', '', 1, 1563063927, NULL, NULL),
(522, 10002, 5, 'HkqKJkOFPWQ2XHczmoYEQFsILmTdZzTq', '', 1, 1563063927, NULL, NULL),
(523, 10002, 5, 'sC427b4Ualkek95MPGadhWTQTAHiQdU7', '', 1, 1563063927, NULL, NULL),
(524, 10002, 5, '4ljayMQziwgAUe8liQAMdWQ4XwIybtuK', '', 1, 1563063927, NULL, NULL),
(525, 10002, 5, 'CAjjaF5biTMNP5iDYUyU3gG9cHc2AVIO', '', 1, 1563063927, NULL, NULL),
(526, 10002, 5, 'qqbOohRKD8V635LoRUNFSJNKp2TVAjru', '', 1, 1563063927, NULL, NULL),
(527, 10002, 5, 'BSYHUkEqDwKk7Nw5fRWCixgO47eEkdxo', '', 1, 1563063927, NULL, NULL),
(528, 10002, 5, 'JOIUQJh52Oimo6bubqPV1a8Iavrwig8J', '', 1, 1563063927, NULL, NULL),
(529, 10002, 5, 'aqSwekVGAwES3RfJmccp42dqBtqTlUbq', '', 1, 1563063927, NULL, NULL),
(530, 10002, 5, 'lxPgkJhHHg0jsG4BRFztsYgGjKa1z1jO', '', 1, 1563063927, NULL, NULL),
(531, 10002, 5, 'u8rEmYEQyUe1fA8JzsBoGJdhRDM3qvnG', '', 1, 1563063927, NULL, NULL),
(532, 10002, 5, 'vHQntZkMhxOHcnuooXkLiOUCCov5eCkL', '', 1, 1563063927, NULL, NULL),
(533, 10002, 5, 'Y9KpTlkx3Xo0Vhko4wBMXOIxUavidWCb', '', 1, 1563063927, NULL, NULL),
(534, 10002, 5, '3fa08UXtI85Snqj2xUxfXYek9hkvY4yk', '', 1, 1563063927, NULL, NULL),
(535, 10002, 5, 'XGHv5TgQdvvfLYBfByK8g9go17c3TrUY', '', 1, 1563063927, NULL, NULL),
(536, 10002, 5, '01srgtRBXqkRJtAfkh5kimpdAMcyYkbn', '', 1, 1563063927, NULL, NULL),
(537, 10002, 5, 'c7McBogeH4cAkqHrNQ6VO8TClW5kq06F', '', 1, 1563063927, NULL, NULL),
(538, 10002, 5, '9yd6KvP9e7jOY34p8WEdMBgXoLE9MlW4', '', 1, 1563063927, NULL, NULL),
(539, 10002, 5, '5l6BeK9qi8ywvjCQ21QmLTtxfyRRXsMI', '', 1, 1563063927, NULL, NULL),
(540, 10002, 5, '4PPmvR2KJ7enR5brqYMXIOBYIi9j14IB', '', 1, 1563063927, NULL, NULL),
(541, 10002, 5, 'qGOmSHs3eTx9wBXLYkQCUa87cIW3H1a6', '', 1, 1563063927, NULL, NULL),
(542, 10002, 5, 'UxS0IF6C31evUX0fQOulgTFm8dh6RbWk', '', 1, 1563063927, NULL, NULL),
(543, 10002, 5, 'lzHaQNdc6K0tsCD28t34KTrrSvarVvbZ', '', 1, 1563063927, NULL, NULL),
(544, 10002, 5, 'HMsmn1BwLZcSN7VGhqFmwAGwXJafwYjn', '', 1, 1563063927, NULL, NULL),
(545, 10002, 5, 'YQwePVXmEh66i5P7ZxuPi3Uk6cH4ofR2', '', 1, 1563063927, NULL, NULL),
(546, 10002, 5, 'h86Edb32dcx5Nr6WmBUDybemidJ8hebc', '', 1, 1563063927, NULL, NULL),
(547, 10002, 5, 'CqwCyRhUxZcosChVXhkVZ2fq5AMlBkzI', '', 1, 1563063927, NULL, NULL),
(548, 10002, 5, 'TeY8B5I63GWNABNlt6PKUryjuUYuszpb', '', 1, 1563063927, NULL, NULL),
(549, 10002, 5, 'xg0pi2XwfI38UmOY8xctCWe4TP9GOQBh', '', 1, 1563063927, NULL, NULL),
(550, 10002, 5, 'TAsIVYJX4fXrsDVhXHxT7al4UpHd1YOU', '', 1, 1563063927, NULL, NULL),
(551, 10002, 5, 'KICMg7x9zw1vd1T9WvOOpMcgo8V3O74f', '', 1, 1563063927, NULL, NULL),
(552, 10002, 5, 'pOmUQgIOsNGCfILiGMmLI9eQOp8uJwRd', '', 1, 1563063927, NULL, NULL),
(553, 10002, 5, 'INRDBwYcsF8iAois7oLPHGyeJq74imfX', '', 1, 1563063927, NULL, NULL),
(554, 10002, 5, 'gpPzYaUlHMkPikMih5wd6ZCCJmQK1tgY', '', 1, 1563063927, NULL, NULL),
(555, 10002, 5, '8al8GaTjOMEYKVhfsYy0p9j4NCprrI6h', '', 1, 1563063927, NULL, NULL),
(556, 10002, 5, 'R4qBFw7BMiVWfntZEphWWWAM2s9sMP2j', '', 1, 1563063927, NULL, NULL),
(557, 10002, 5, 'WlnDeC0DjHzT7rj3b16bHDtOzOR75Se2', '', 1, 1563063927, NULL, NULL),
(558, 10002, 5, '7JjnA791bT9WxVzo0Vca68anhi7bO9wM', '', 1, 1563063927, NULL, NULL),
(559, 10002, 5, 'zHQ7mAnFHXaDPBaL6UOtMSc7xFmaq6w9', '', 1, 1563063927, NULL, NULL),
(560, 10002, 5, 'wzumnHEDyxOQI3H07max9TP7YB9PD8eI', '', 1, 1563063927, NULL, NULL),
(561, 10002, 5, 'ebg7WM9bhdmlIYMayXsCy1JycGQmr73T', '', 1, 1563063927, NULL, NULL),
(562, 10002, 5, 'DEMzwWFz6tCOoPmHsF5IjlLOsQQguuWo', '', 1, 1563063927, NULL, NULL),
(563, 10002, 5, 'JnlrTiyrLBU6gbtYKc1fV3Rr6f9W9xAM', '', 1, 1563063927, NULL, NULL),
(564, 10002, 5, 'SbqnB6HOJ4p7Awd9By1UFVmZryfAU6RM', '', 1, 1563063927, NULL, NULL),
(565, 10002, 5, 'wFZjwG7r46zh1BorWdxJBxxSuswtUTF1', '', 1, 1563063927, NULL, NULL),
(566, 10002, 5, '2f1ZNa3hd5HHAFebzcO9uW3c3JzYaCj3', '', 1, 1563063927, NULL, NULL),
(567, 10002, 5, 'cRzGcfuFPMKg2SJoOfKqlJZswjV8Jik2', '', 1, 1563063927, NULL, NULL),
(568, 10002, 5, 'yvioTok3ylDRPwOboCq1TAq2VixqK8Mz', '', 1, 1563063927, NULL, NULL),
(569, 10002, 5, 'zRpdrbaSG5zRdEfvUzobNlLz5IRJJ152', '', 1, 1563063927, NULL, NULL),
(570, 10002, 5, 'LCjQNNvXUO9gE78CQnHni3YLzHiStVok', '', 1, 1563063927, NULL, NULL),
(571, 10002, 5, 'VXzCiPalHUj1vtCIULufM2kI3QHSU7CQ', '', 1, 1563063927, NULL, NULL),
(572, 10002, 5, 'pqwuWjynXbE4OMHvkU6NHzJCjfPubtUr', '', 1, 1563063927, NULL, NULL),
(573, 10002, 5, 'FYTUxwoxzOcIyKpuAWDu246Caw3m4nKw', '', 1, 1563063927, NULL, NULL),
(574, 10002, 5, 'lhRqSLz1T6XikTjwvgE3dpHySQ0oLz5a', '', 1, 1563063927, NULL, NULL),
(575, 10002, 5, 'hBZissSZRsc5LGPm491Rps9OBCyZ8CDw', '', 1, 1563063927, NULL, NULL),
(576, 10002, 5, 'TCaFR2N7PkcZXsPqIJHTb3LDDzcLODdF', '', 1, 1563063927, NULL, NULL),
(577, 10002, 5, 'fNYsUMI1Z3gUk2TzFFtSsAE7czAuYOqc', '', 1, 1563063927, NULL, NULL),
(578, 10002, 5, 'cv3PoGEW7HXSyzxDGiZS31a96HhEQxOQ', '', 1, 1563063927, NULL, NULL),
(579, 10002, 5, 'vV3ltiMtLqY0qC5WV3j7ntxxGXkcCfXG', '', 1, 1563063927, NULL, NULL),
(580, 10002, 5, 'wpVHGf5HI5Nh9ppT9nOxkGoije4YI3OA', '', 1, 1563063927, NULL, NULL),
(581, 10002, 5, 'OyH43KBFxwGEpUNr8i6gnV4vV6yLLHCB', '', 1, 1563063927, NULL, NULL),
(582, 10002, 5, 'ntn29PoqQNF3oFRkKtjb2B8AibzJzo7o', '', 1, 1563063927, NULL, NULL),
(583, 10002, 5, 'hztNLadKssaCfuRoa4vAtv5Pgy2esg5D', '', 1, 1563063927, NULL, NULL),
(584, 10002, 5, '3BzHGyDs248GQvY6PT8VxRsm92vlQUWU', '', 1, 1563063927, NULL, NULL),
(585, 10002, 5, '5xNkTOUlELOiMrhhE2oBTIqNSPFYN4Qq', '', 1, 1563063927, NULL, NULL),
(586, 10002, 5, 'MK2wBxw8XKKEmMrIGq1DDsXm9RmMK2Ri', '', 1, 1563063927, NULL, NULL),
(587, 10002, 5, 'x5AZdwd7LEjDcqqBS5WvgN4YiXrlknZg', '', 1, 1563063927, NULL, NULL),
(588, 10002, 5, 'YxYznPncFPDuULoIQZaUkcAzrqWwRpTL', '', 1, 1563063927, NULL, NULL),
(589, 10002, 5, 'RnAix8tg3Z9uRfFZZfz4VJtnG9LDH8qR', '', 1, 1563063927, NULL, NULL),
(590, 10002, 5, 'SKGfRhBlDg2CdJcLshZ408CFt7awcTOW', '', 1, 1563063927, NULL, NULL),
(591, 10002, 5, '3A4FGsQFq5jSdJ2LF6IYIyDyEt1ESQYe', '', 1, 1563063927, NULL, NULL),
(592, 10002, 5, 'KfCKMgNSvHRO4lRyFedjktWwmDXWiR4R', '', 1, 1563063927, NULL, NULL),
(593, 10002, 5, 'AXVQQrcwwhVDWTdyw3qW5H1GuqtihVna', '', 1, 1563063927, NULL, NULL),
(594, 10002, 5, 'h7JGdQVJzzgbPY2MVIKzwsYApVtyKz8U', '', 1, 1563063927, NULL, NULL),
(595, 10002, 5, 'kZyoYOfgJ6FCRJ0GfK8C8WSNGgtnwF55', '', 1, 1563063927, NULL, NULL),
(596, 10002, 5, 'KPsNlJDaGRGVHbBRIWryAvxCCeWRPBLk', '', 1, 1563063927, NULL, NULL),
(597, 10002, 5, 'toItzH4J189Do1NLceY7WPrtO3S2yx4i', '', 1, 1563063927, NULL, NULL),
(598, 10002, 5, 't9pylLk2QgV4ijsvnmrOkeOTNn7flBWq', '', 1, 1563063927, NULL, NULL),
(599, 10002, 5, 'w2rgjEaQMONvjizOTkUTjel4Kd5aNzNw', '', 1, 1563063927, NULL, NULL),
(600, 10002, 5, 'R2JUwdW6XHNLjqGXuIrfzBTExdPgGwWi', '', 1, 1563063927, NULL, NULL),
(601, 10002, 5, 'qH4jah0bGi3nukULhP2FCHpNTy38reA1', '', 1, 1563063927, NULL, NULL),
(602, 10002, 5, 'j1Cm8bHqS6RTr2SkV9WUj94EBfvfeBbO', '', 1, 1563063927, NULL, NULL),
(603, 10002, 5, 'IGP5blO2tKbV53LzvBrbhIv9vYr7NLdb', '', 1, 1563063927, NULL, NULL),
(604, 10002, 5, 'ooTQvOX93jjmmKHlmJMnrRwX4BgiWzjH', '', 1, 1563063927, NULL, NULL),
(605, 10002, 5, 'cVYVJ27b9XNiLKcVpuPseLstjxcDpqUk', '', 1, 1563063927, NULL, NULL),
(606, 10002, 5, 'jd3urhMtALObY3OVQ5MGaFt14ury7hhL', '', 1, 1563063927, NULL, NULL),
(607, 10002, 5, 'R3rB272TSmHdkx3zaGgxC6uCns09d5LD', '', 1, 1563063927, NULL, NULL),
(608, 10002, 5, '9HLHMGysr4MqQyM16ACimOIQHVgAIrvR', '', 1, 1563063927, NULL, NULL),
(609, 10002, 5, 'SuaFEfMMbht8ubXPLje9gxXnNiraQCDt', '', 1, 1563063927, NULL, NULL),
(610, 10002, 5, '2qQ6lpPn0Fvs3ljviXtsWFDmvnlmxs82', '', 1, 1563063927, NULL, NULL),
(611, 10002, 5, 'T5xUHidKHSoPn5ACp63kBPRigL49B9mh', '', 1, 1563063927, NULL, NULL),
(612, 10002, 5, 'ogCIwcbHOJomZ64IrM8Mo0f7osOi11s7', '', 1, 1563063927, NULL, NULL),
(613, 10002, 5, 'wLivhDViOxD0bX3faFnbsPnPsAhCo7Ul', '', 1, 1563063927, NULL, NULL),
(614, 10002, 5, 'NLDIBB2B0CH3FWQLJJJ4oDdgJsvnp6BT', '', 1, 1563063927, NULL, NULL),
(615, 10002, 5, '8UrmcNAY8vPm565sce9ky0ShNoDYn6ja', '', 1, 1563063927, NULL, NULL),
(616, 10002, 5, '6qDj53lOirHhHw7YEm9LKqnrRal6rHeo', '', 1, 1563063927, NULL, NULL),
(617, 10002, 5, 'l5mN1jPrTLndK5MoGgIqzIglxqoitKmw', '', 1, 1563063927, NULL, NULL),
(618, 10002, 5, 'V8FM4E1Y6yY7g5PufXD96x32fUKAsAwz', '', 1, 1563063927, NULL, NULL),
(619, 10002, 6, 'sCN1VDd2RsJjDqSrOijTb96WT0gWTC7W', '', 1, 1563063957, NULL, NULL),
(620, 10002, 6, '1S19Ye2ArDTfXztXMc9t3o8C0jidCOps', '', 1, 1563063957, NULL, NULL),
(621, 10002, 6, 'xJeOIfkXg5kdtObcXPNxkM4bzNoiLd7A', '', 1, 1563063957, NULL, NULL),
(622, 10002, 6, 'LIhNwqoku3hhsmDn90AFbs1TU58NBt7e', '', 1, 1563063957, NULL, NULL),
(623, 10002, 6, 'CCLColw6VhYXHluHSP2UR7tSyTfUBcgW', '', 1, 1563063957, NULL, NULL),
(624, 10002, 6, 'jVCsYRNNuPHSi3jBE7I5j34MKaltv24E', '', 1, 1563063957, NULL, NULL),
(625, 10002, 6, 'tPodqNN6zsBExfWKo84GYNiFQyuyJr60', '', 1, 1563063957, NULL, NULL),
(626, 10002, 6, 'xN9pBirdeoRhdtW3j4sekqMLAaGEAIZy', '', 1, 1563063957, NULL, NULL),
(627, 10002, 6, 'ay62LktA7RPKnBqyTSOGAtwzDr7nmzwB', '', 1, 1563063957, NULL, NULL),
(628, 10002, 6, 'Owu0EU4UxumPWFqiaDhxacIIOXEAv9Z1', '', 1, 1563063957, NULL, NULL),
(629, 10002, 6, 'c1psgvbrKE7mvmaHDl4Y2qyvoeJKAClF', '', 1, 1563063957, NULL, NULL),
(630, 10002, 6, 'S8iVkQWMlP1vrLUe3p5c5OOIiy5QNbSu', '', 1, 1563063957, NULL, NULL),
(631, 10002, 6, 'DRlPbdy5T7oLUTZEjwBYv4lWPYZXjUPd', '', 1, 1563063957, NULL, NULL),
(632, 10002, 6, 'JdIhUllEWfSOUbKEmOW0DB5MIkabjmb2', '', 1, 1563063957, NULL, NULL),
(633, 10002, 6, '45r4NYorhtHWzveKXcOBB7fky6708Zrr', '', 1, 1563063957, NULL, NULL),
(634, 10002, 6, 'hUFVluXb21KNP8BF8JQBE6NQU8vJvzY8', '', 1, 1563063957, NULL, NULL),
(635, 10002, 6, 'PULyplN6btfurULoi3ku0NHktIdJXrni', '', 1, 1563063957, NULL, NULL),
(636, 10002, 6, 'tFTIch3M9OSA1fW3pJhlOF47bSrSC3zS', '', 1, 1563063957, NULL, NULL),
(637, 10002, 6, 'VqMmMRDLusCTccS769munv3Pl95ZOUAw', '', 1, 1563063957, NULL, NULL),
(638, 10002, 6, 'YCq0oTrvgwiRxfa9FlnJ5GjH3cEALyRl', '', 1, 1563063957, NULL, NULL),
(639, 10002, 6, 'pv8M74YejyYpeXBcVPzWGafvbxQWb3s3', '', 1, 1563063957, NULL, NULL),
(640, 10002, 6, 'R4p7CCFzs2gHWfMr2ZyiljsRPYEVf241', '', 1, 1563063957, NULL, NULL),
(641, 10002, 6, 'FCZWI2msmp4ik1ECffwbJXMC27siqcm2', '', 1, 1563063957, NULL, NULL),
(642, 10002, 6, 'YGjwQ6iuunYLaKmDux4FkpNFJMxJr6dH', '', 1, 1563063957, NULL, NULL),
(643, 10002, 6, 'whNEnTky3XiT5CP2hrQEPBLcN1sMY7zt', '', 1, 1563063957, NULL, NULL),
(644, 10002, 6, 'Hr39AYIRlh4Ix7qch9WQnU4yQQ8G8X98', '', 1, 1563063957, NULL, NULL),
(645, 10002, 6, 'ibiDmdQLdA1HuBRbIANGljWnhUQEIV1y', '', 1, 1563063957, NULL, NULL),
(646, 10002, 6, 'lKj6t8cDsjZQtTXijaQEVlgx9yVTAFiR', '', 1, 1563063957, NULL, NULL),
(647, 10002, 6, 'HE8INnTQb6UqjNhcJhmJYpO5Q5GhcOBF', '', 1, 1563063957, NULL, NULL),
(648, 10002, 6, '8e55A0GOzBd3zU3OHmsATUjk1hIRIlIq', '', 1, 1563063957, NULL, NULL),
(649, 10002, 6, 'GpGvGHe57J1wQPTUfsJLcbcepXdxFXr4', '', 1, 1563063957, NULL, NULL),
(650, 10002, 6, 'y85H4qDoh1sn4ixphUSOGXwUq1gQPXg5', '', 1, 1563063957, NULL, NULL),
(651, 10002, 6, '1Z2VGsVQpoF24kbqd3BQnocIvANaQ6Lb', '', 1, 1563063957, NULL, NULL),
(652, 10002, 6, 'xlDYFdpeilYriuciJfw7J7MSueVhGyC6', '', 1, 1563063957, NULL, NULL),
(653, 10002, 6, 'm1FA33wveonreUkdMepsSG8aHGBETWhG', '', 1, 1563063957, NULL, NULL),
(654, 10002, 6, 'FVmtxgpxo3N1Ec8E654LCm61FifyvnCq', '', 1, 1563063957, NULL, NULL),
(655, 10002, 6, 'V42LQE3H36GyebPqwybjwpIb6iYvwX5W', '', 1, 1563063957, NULL, NULL),
(656, 10002, 6, 'NlAyeVW75wz3lRp32FNqpJHRYmnC53IJ', '', 1, 1563063957, NULL, NULL),
(657, 10002, 6, 'ecEIeSTI9LVPIJ7b2of8mOjDqZIbVpLq', '', 1, 1563063957, NULL, NULL),
(658, 10002, 6, 'A5iiiPfGk49p7GKRvM8RVIGL0xYvgpxS', '', 1, 1563063957, NULL, NULL),
(659, 10002, 6, 'N23LfvVT26S7GfSkMJVS1xr5w6lUTIcE', '', 1, 1563063957, NULL, NULL),
(660, 10002, 6, 'I25FMS3KvSwgzJB09PRXcMEf7S7oT8yQ', '', 1, 1563063957, NULL, NULL),
(661, 10002, 6, 'C8rWHgbnBYU5Gaxf6eNifT4HXKzTnrnO', '', 1, 1563063957, NULL, NULL),
(662, 10002, 6, 'BQwFSdhWD8CqliaAc2hplQI2jQWzis5h', '', 1, 1563063957, NULL, NULL),
(663, 10002, 6, 'nV6Ef51jamdPeEvo3kbhRTqJAopY08it', '', 1, 1563063957, NULL, NULL),
(664, 10002, 6, 'VhyhrfQwDoLMmHwoqrYvvxL8BPR7rqOW', '', 1, 1563063957, NULL, NULL),
(665, 10002, 6, 'KpcspUI1lLQ7v3GmbDvq4lIfG7TSuqOf', '', 1, 1563063957, NULL, NULL),
(666, 10002, 6, '27rPFB31h5mzJXFY8V3xYbGtAQUGVSGF', '', 1, 1563063957, NULL, NULL),
(667, 10002, 6, 'I2MdxoxA5Ntpp4osIVwsWa2V33PQ66Hg', '', 1, 1563063957, NULL, NULL),
(668, 10002, 6, 'uW9liQkyCUwYfDMOID6MXUCHu2mxao0u', '', 1, 1563063957, NULL, NULL),
(669, 10002, 6, 'bririJZtvBC7itabZNNosO8FuYUt2f0Y', '', 1, 1563063957, NULL, NULL),
(670, 10002, 6, 'A8IKB9sQnWilPrvzLRSUGECxbmkS7uAP', '', 1, 1563063957, NULL, NULL),
(671, 10002, 6, 'SIOPJADMwAnrUcAnVgxG3iI94jEOIrzI', '', 1, 1563063957, NULL, NULL),
(672, 10002, 6, 'W7HpOPXHH93z65rPWgD9qcNgKYscXDgu', '', 1, 1563063957, NULL, NULL),
(673, 10002, 6, 'BcAu7wvzoT5t8XAem9dZNgHFFVxSxJAi', '', 1, 1563063957, NULL, NULL),
(674, 10002, 6, 'v1EIWbqM7cVQWJ7lT0Fdcw4Oljz3Njf4', '', 1, 1563063957, NULL, NULL),
(675, 10002, 6, 'm2CJRK2g83Ltbw7QgjbFE4E7jab9IgDu', '', 1, 1563063957, NULL, NULL),
(676, 10002, 6, 'HotS8PlcypHFfW9tBau8reQcwfn4JvZa', '', 1, 1563063957, NULL, NULL),
(677, 10002, 6, 'KgNpeVOUCRCY7aLJwo8brZv2GQadv7gs', '', 1, 1563063957, NULL, NULL),
(678, 10002, 6, 'pvuVSY9hMlOU8VB7rwO3bclBrurd6rla', '', 1, 1563063957, NULL, NULL),
(679, 10002, 6, 'SmOsgrHYB3M4YyMB7YZAae9T9mnYfRkf', '', 1, 1563063957, NULL, NULL),
(680, 10002, 6, 'Gfg46kT4QWG4klXssmSTJhXn4hngLI4m', '', 1, 1563063957, NULL, NULL),
(681, 10002, 6, 'bYP0ISrwfBFmhx1yzNoqHLKAJ15IN5wc', '', 1, 1563063957, NULL, NULL),
(682, 10002, 6, 'tr0NJYu6PL4OtuEcG6R4NXXzImArOL5s', '', 1, 1563063957, NULL, NULL),
(683, 10002, 6, 'gVyy652vrwIrEKKQTSsaNrGHExEPoCHM', '', 1, 1563063957, NULL, NULL),
(684, 10002, 6, 'nTPANxnIyz65GWGTnlFT8T5I76jpyaUo', '', 1, 1563063957, NULL, NULL),
(685, 10002, 6, 'Iw5ikVSSTz6BwecMVp5vfoRTf6VeFSen', '', 1, 1563063957, NULL, NULL),
(686, 10002, 6, 'CoKsMAWIhXbXmFSG0UcgNi7h2ouzuwIE', '', 1, 1563063957, NULL, NULL),
(687, 10002, 6, 'vWqaIJXFSTy8yn3FRVYFPe2Zjhm9ognB', '', 1, 1563063957, NULL, NULL),
(688, 10002, 6, 'TzndtT5xvstzS7xdITxc5EgzUSVpC5Ft', '', 1, 1563063957, NULL, NULL),
(689, 10002, 6, 'cYw5a9ETABYNzVDKm4hi3RZzFQUlGCPy', '', 1, 1563063957, NULL, NULL),
(690, 10002, 6, 'VpuzIxHj5N16nzU9WiKl0tKGbIDNzL10', '', 1, 1563063957, NULL, NULL),
(691, 10002, 6, 'PzysbJeeSkJxG0L4UGVFTjcufek1V1vv', '', 1, 1563063957, NULL, NULL),
(692, 10002, 6, 'mrahJIvpJgtY2Chc2nQNPWUvHhbQ3aR4', '', 1, 1563063957, NULL, NULL),
(693, 10002, 6, 'IRIUlsA5soFvJa6N5dPwevHW0uaYUhJM', '', 1, 1563063957, NULL, NULL),
(694, 10002, 6, 'fWAlVwyPSPBYmIYX2G3nHdSzocc2XpXE', '', 1, 1563063957, NULL, NULL),
(695, 10002, 6, 'vL25bWF3QIwCdTDH4RDRIJp9lwAJ3xgv', '', 1, 1563063957, NULL, NULL),
(696, 10002, 6, 'tExSElkCWs0sinqsJsuGP4AGEHyoBNsM', '', 1, 1563063957, NULL, NULL),
(697, 10002, 6, 'HSJp3bhCfPKeurQk5fCAjRBgz24uGmz9', '', 1, 1563063957, NULL, NULL),
(698, 10002, 6, 'MPP9Af3iPX43EG96uCOeSgjO2366DS9c', '', 1, 1563063957, NULL, NULL),
(699, 10002, 6, '1Ts43TwLkt6xiJ65qF6gp1eji2bzxdTI', '', 1, 1563063957, NULL, NULL),
(700, 10002, 6, 'b27alP5aZ2AD8KfgEqCSgOD59sNgPiU8', '', 1, 1563063957, NULL, NULL),
(701, 10002, 6, 'NEpNFYmkIxL2TQpdeY2WQCNgYlfxYSdL', '', 1, 1563063957, NULL, NULL),
(702, 10002, 6, 'jAhJu41SKKFsuqJAOIVl2DCCoNI1qBtU', '', 1, 1563063957, NULL, NULL),
(703, 10002, 6, 'mVXNx2ki4BAilxgdMeaij1tyUy8wTRSG', '', 1, 1563063957, NULL, NULL),
(704, 10002, 6, 'OhcFfk4vK4fX5mNIuYwub82t9q2AbIGa', '', 1, 1563063957, NULL, NULL),
(705, 10002, 6, 'PPWJ6aP4rX3uasuCH0iIXw8KZcnncy5a', '', 1, 1563063957, NULL, NULL),
(706, 10002, 6, '68ccFkxoVDYKxPd324LP9MWMvRMJdbpm', '', 1, 1563063957, NULL, NULL),
(707, 10002, 6, 's7oDx6OLkBtcn1rGU4cl36e2euNge2J6', '', 1, 1563063957, NULL, NULL),
(708, 10002, 6, 'PNMgTFIy2EPpIeSy73WkuuBz6dpTVnwS', '', 1, 1563063957, NULL, NULL),
(709, 10002, 6, 'Ctj0KVz9zWsxai9Xk35BWWdwbgVuULsE', '', 1, 1563063957, NULL, NULL),
(710, 10002, 6, '7UlUedvjiT2li2tGPRtheiybZ4HnzELx', '', 1, 1563063957, NULL, NULL),
(711, 10002, 6, 'CUfEEGWaXurMcfySVB9Xeen6Ac4kB6IO', '', 1, 1563063957, NULL, NULL),
(712, 10002, 6, 'oBniS81VT9w2kmOKl44l8ADpF8YFZTkT', '', 1, 1563063957, NULL, NULL),
(713, 10002, 6, 'jUyPVjlfWhtXcoWSPshdyKlahHBxYtFY', '', 1, 1563063957, NULL, NULL),
(714, 10002, 6, 'bgVzjYrj7vTzYHKTyIqAES5Un19vtiP1', '', 1, 1563063957, NULL, NULL),
(715, 10002, 6, 'aKMpQu6nOpjLJyYvXyG7Vq4jgm5F78ik', '', 1, 1563063957, NULL, NULL),
(716, 10002, 6, '91RKh8oEsFl0UNgB64x5s5EpnW3C22MO', '', 1, 1563063957, NULL, NULL),
(717, 10002, 6, 'ouqOb255czgKfrJM9Z5nTm99ub4IpXFB', '', 1, 1563063957, NULL, NULL),
(718, 10002, 6, 'jG114o4LlcQk157MVHVHthnESVcBJzJV', '', 1, 1563063957, NULL, NULL),
(719, 10002, 6, 'ua43eMo1L7jjlC3Xccuo3cOFPo4IDIZK', '', 1, 1563063957, NULL, NULL),
(720, 10002, 6, 'rbq4OQJUhStCJyKbcIVNHsYmCTrGlPY1', '', 1, 1563063957, NULL, NULL),
(721, 10002, 6, 'Elenf21GnpDOpxpLPpUH2JSPVtB5jUDE', '', 1, 1563063957, NULL, NULL);
INSERT INTO `goods_card` (`id`, `user_id`, `goods_id`, `number`, `secret`, `status`, `create_at`, `delete_at`, `sell_time`) VALUES
(722, 10002, 6, 'bV2kTxCBvbY6CjpMMt8RONI5a9r4Iwx8', '', 1, 1563063957, NULL, NULL),
(723, 10002, 6, 'olmjGMFDvGgtzeFT8Gbdcg4csG9vbjQ2', '', 1, 1563063957, NULL, NULL),
(724, 10002, 6, 'zK7DOkSHwr3oaCdYqCMLF90VgN36xdlg', '', 1, 1563063957, NULL, NULL),
(725, 10002, 6, 'PV9jMQObdKWR1BL8dyI6mNtEm4mVHGga', '', 1, 1563063957, NULL, NULL),
(726, 10002, 6, 'Ev5UyaJWzRlioosLimH57exjQqXHvAoU', '', 1, 1563063957, NULL, NULL),
(727, 10002, 6, 'fdNjxbuTCM3TzE4HWreqH6YQ1DwzXgoT', '', 1, 1563063957, NULL, NULL),
(728, 10002, 6, 'rBFVZooqn2dU7w4LvR2sNmOCqO6nV5rS', '', 1, 1563063957, NULL, NULL),
(729, 10002, 6, 'HVUs1D5j1f8RpR1lLOy9uQ6lzKhlDoeT', '', 1, 1563063957, NULL, NULL),
(730, 10002, 6, 'NHPXhWr3KaQ61CeF5HRrF06hdjb5rsca', '', 1, 1563063957, NULL, NULL),
(731, 10002, 6, 'luVUhMKg1iiyQVQirgMEOgWzl296TZI2', '', 1, 1563063957, NULL, NULL),
(732, 10002, 6, 'Xnw4q3PW9Ty9cmENbkQ15SA63sH2fARk', '', 1, 1563063957, NULL, NULL),
(733, 10002, 6, '6HBQcRTkEknM6zYK236qMp7Mf9h3fwMk', '', 1, 1563063957, NULL, NULL),
(734, 10002, 6, 'yZI5XMUCn6aqytzuVly3tLfgxnAtUePg', '', 1, 1563063957, NULL, NULL),
(735, 10002, 6, 'YN4k3cTKHj1z60Ovo4EAtW3PmkxDcQl4', '', 1, 1563063957, NULL, NULL),
(736, 10002, 6, 'MC4BEXxtRF1IerxqklKgL4rJCEvhdO0A', '', 1, 1563063957, NULL, NULL),
(737, 10002, 6, 'cU65BJrDgtgJhuVGP8yecl1QlfOC8vfd', '', 1, 1563063957, NULL, NULL),
(738, 10002, 6, 'RQfpy1a5kYmVonTwbnkts2YhWiybeF0g', '', 1, 1563063957, NULL, NULL),
(739, 10002, 6, 'XsmudwwEHGYkMWDqVx6izBmIKhNXnr89', '', 1, 1563063957, NULL, NULL),
(740, 10002, 6, 'EmGptlYL3YNwTOq1FWXJzgCIO5tv8T9I', '', 1, 1563063957, NULL, NULL),
(741, 10002, 6, 'KC42WKy3OWZQa9NeVr1vWdWvnt8SGEVN', '', 1, 1563063957, NULL, NULL),
(742, 10002, 6, '5elXxGtF2irfMZMOhvF9vCzD6sIKViNm', '', 1, 1563063957, NULL, NULL),
(743, 10002, 6, 'hMSRVIqreluQIsLaCW6rZd79oqufmTw7', '', 1, 1563063957, NULL, NULL),
(744, 10002, 6, '9IXDXTldm30iTXWaA8fDHcjzu2YTKnQw', '', 1, 1563063957, NULL, NULL),
(745, 10002, 6, 'x5m9BW7PqpQQ6fpj1pN9SM44hdUiuSov', '', 1, 1563063957, NULL, NULL),
(746, 10002, 6, 'gFvoqleoK2KWWmsjEmDmSSgNNOIBYKk8', '', 1, 1563063957, NULL, NULL),
(747, 10002, 6, '3I4Lu1ofTw5fMc6nHh0WWdDl1W8QXI7u', '', 1, 1563063957, NULL, NULL),
(748, 10002, 6, 'ThliKqfUAJFDJDxpDs8EgeJBucJH6ZqV', '', 1, 1563063957, NULL, NULL),
(749, 10002, 6, 'exTrI1o7PJFrHapblx1g3aGF3Jo3rbyY', '', 1, 1563063957, NULL, NULL),
(750, 10002, 6, '6mJVlItxn0zPbSzGGY433UEz9WgMu88N', '', 1, 1563063957, NULL, NULL),
(751, 10002, 6, 'P9His6xL7c3STY5jLw4l2Anx5nLJP5bu', '', 1, 1563063957, NULL, NULL),
(752, 10002, 6, 'gjf88iK8aRhb4QIAmMcdNKMEeB1jeDQB', '', 1, 1563063957, NULL, NULL),
(753, 10002, 6, 'SQpNUnggpT9iDSFIlonIFg9BVIPuCduB', '', 1, 1563063957, NULL, NULL),
(754, 10002, 6, 'pyqtQtxs0M11SzZW98e45RxdddqT4r51', '', 1, 1563063957, NULL, NULL),
(755, 10002, 6, 'v5lP96dlp4YhJWi4c2KlqjOXsVNbTzqq', '', 1, 1563063957, NULL, NULL),
(756, 10002, 6, 'NcU0QnNdzcL7ZUuPtuREkppDK63TExXn', '', 1, 1563063957, NULL, NULL),
(757, 10002, 6, 'uPPYreHEO7vrRNlhGTHFF3iMfCZASBzG', '', 1, 1563063957, NULL, NULL),
(758, 10002, 6, 'vneapouFzwQYKuegS6XL1DM9XW9gfqqF', '', 1, 1563063957, NULL, NULL),
(759, 10002, 6, 'z3SXmiKDIA2DIGmx6K8L7ZwiNzp4LKaA', '', 1, 1563063957, NULL, NULL),
(760, 10002, 6, 'nD0HOu8oF6iBh4UQ2TxHK85aO0Vfm8fq', '', 1, 1563063957, NULL, NULL),
(761, 10002, 6, '41xvdKxv3OeAcXo7VWo4Hi5KSqMMvT6M', '', 1, 1563063957, NULL, NULL),
(762, 10002, 6, 'vSQxqDfBeT2Yxh32aT8J1kjk3yuqljJV', '', 1, 1563063957, NULL, NULL),
(763, 10002, 6, 'MbqNCoyqjjqC63BDJjKCIAxMafduVIFC', '', 1, 1563063957, NULL, NULL),
(764, 10002, 6, 'LNcu4eg6dJV7gdyIaDTaLif6ChPE1oP8', '', 1, 1563063957, NULL, NULL),
(765, 10002, 6, 'joxyInsGId9MQicnhnA2AJpB64tmdc2k', '', 1, 1563063957, NULL, NULL),
(766, 10002, 6, 'JIdVk4PrtW5uJwBNkVeaCKh6tS34s4yk', '', 1, 1563063957, NULL, NULL),
(767, 10002, 6, 'lFxLAPlL5NPaj92Vv6Ns4pe42VxhqVRT', '', 1, 1563063957, NULL, NULL),
(768, 10002, 6, '6q1MQGPUjZsefKNSYYYpSwrqWfsDjtBM', '', 1, 1563063957, NULL, NULL),
(769, 10002, 6, '5Uo4mzq69BRUJiflFvoeUcsrid616h6A', '', 1, 1563063957, NULL, NULL),
(770, 10002, 6, 'D0UWnSFZCW0UgALFqWpjp1KuDFmCru9e', '', 1, 1563063957, NULL, NULL),
(771, 10002, 6, 'TmtMqHIt4gKcdAARaVUlZcKjgQwaokhW', '', 1, 1563063957, NULL, NULL),
(772, 10002, 6, 'W6n2Ih4SIlNAILRKMwL8vUPyPzlbrd33', '', 1, 1563063957, NULL, NULL),
(773, 10002, 6, 'RLbpHLwUU66lrvEAdRtfWNWECB4ZJgT6', '', 1, 1563063957, NULL, NULL),
(774, 10002, 6, 'tlrNWhq7ogzkZu2ONUa6KWqSfybf1ILS', '', 1, 1563063957, NULL, NULL),
(775, 10002, 6, 'wEKzCPQWQAWYbgcZ4GqItc9s798WccIn', '', 1, 1563063957, NULL, NULL),
(776, 10002, 6, 'DPXGSa1AxljPpEAAHKf6elfegbqFBHs2', '', 1, 1563063957, NULL, NULL),
(777, 10002, 6, 'uItapIf4GmyoAlmCCFva9jBmxC3jJhqf', '', 1, 1563063957, NULL, NULL),
(778, 10002, 6, 'aHmnrMNWxdjVgB8BDYXkLlRGErJzKl4h', '', 1, 1563063957, NULL, NULL),
(779, 10002, 6, 'JRNIus70zb8IXLBG20pMSyX17sPPD3cl', '', 1, 1563063957, NULL, NULL),
(780, 10002, 6, 'xQsd0p2fLlxz7GZoU8vDRGjgbs88vvCX', '', 1, 1563063957, NULL, NULL),
(781, 10002, 6, 'St0jKo5ndl79sqHLj6dQyjB4yB6HBf6Y', '', 1, 1563063957, NULL, NULL),
(782, 10002, 6, 'JFPHFPFwiKf3Ia9SMx7FEbU1ePGAGRnN', '', 1, 1563063957, NULL, NULL),
(783, 10002, 6, 'aAP6d73pXHUdWerah5e8mjVN5yzNLNxG', '', 1, 1563063957, NULL, NULL),
(784, 10002, 6, 'fxsDGmgp2esI6w6S64WxZR13rngmE6Vx', '', 1, 1563063957, NULL, NULL),
(785, 10002, 6, 'lUpiZqe2zu5MmztG8olEapDVB9vAlZxl', '', 1, 1563063957, NULL, NULL),
(786, 10002, 6, '9m27qxh56a7REILPhb2c9OnIbK355eDx', '', 1, 1563063957, NULL, NULL),
(787, 10002, 6, 'boVHu07Rsi4aXYCuVze93f28E9QvFFmy', '', 1, 1563063957, NULL, NULL),
(788, 10002, 6, 'ijd9kxrQzebpRTd3cSGLcXhxaFvzTMnG', '', 1, 1563063957, NULL, NULL),
(789, 10002, 6, '65altkMhbXBYLcpQoiRfRMLRnegd3Coi', '', 1, 1563063957, NULL, NULL),
(790, 10002, 6, 'J36y7h5IpPFQK3A0oF98LNc9iBACElLJ', '', 1, 1563063957, NULL, NULL),
(791, 10002, 6, 'YXvUA4baj9xvTNRpodMaBm3juaa8gs6C', '', 1, 1563063957, NULL, NULL),
(792, 10002, 6, 'Th4rFxKctp7SGlrWq6NN5ldEVXdXeScC', '', 1, 1563063957, NULL, NULL),
(793, 10002, 6, 'tFM9KeGY2bzkC8rExANe9w1uuNwGJpnf', '', 1, 1563063957, NULL, NULL),
(794, 10002, 6, 'NuFg8kh6Q90ya8KVxtXqWjulxZPsz2ny', '', 1, 1563063957, NULL, NULL),
(795, 10002, 6, 'S2I3xdGCrAEFUILwjU4Nt1uwO0ICjw45', '', 1, 1563063957, NULL, NULL),
(796, 10002, 6, 'wNvzzomYDvSExhgFO27KdKPDJ5ofajO9', '', 1, 1563063957, NULL, NULL),
(797, 10002, 6, 'lAtlJzTX1ebn5akNMpdX5YeGw53tMGOn', '', 1, 1563063957, NULL, NULL),
(798, 10002, 6, 'y7f1lEVIAHISFuvRiiyt18L9MGY5PYC4', '', 1, 1563063957, NULL, NULL),
(799, 10002, 6, 'vAqTc9qiQsfLHhTtTEjZPcjw1qN3kMPA', '', 1, 1563063957, NULL, NULL),
(800, 10002, 6, '61tvud4E4PgbRj4RA7uIygbV62MTbHK6', '', 1, 1563063957, NULL, NULL),
(801, 10002, 6, 'JvX1SpTaVyDLpilyVOamkDjmJQoLg6be', '', 1, 1563063957, NULL, NULL),
(802, 10002, 6, 'RddblLzNDmu2PtqO8IQdp6t6p2V4lvTD', '', 1, 1563063957, NULL, NULL),
(803, 10002, 6, 'xoGI1jgzXwE5sLaP4A15Jpez4c2yAaVU', '', 1, 1563063957, NULL, NULL),
(804, 10002, 6, 'HGW2DAdDyOx7h8GGgldrd2fyLYjTsval', '', 1, 1563063957, NULL, NULL),
(805, 10002, 6, 'xsH7FRkts9x9ekNrKdIPbjaJxyg9DABV', '', 1, 1563063957, NULL, NULL),
(806, 10002, 6, 'vK2ssmJmGFu2TsGNpg1cXEtv1Mi5kmvP', '', 1, 1563063957, NULL, NULL),
(807, 10002, 6, 'GNKpYv8Lj5xgoUdcL231McQRap5iD31D', '', 1, 1563063957, NULL, NULL),
(808, 10002, 6, 'LY4rL0waUP8YS3B4VQfBz4VB0BsomdU6', '', 1, 1563063957, NULL, NULL),
(809, 10002, 6, '91PBMiaugVsyUIq8BpTHqfaVpdkXyxrl', '', 1, 1563063957, NULL, NULL),
(810, 10002, 6, '2l6PI1I3AGxdEisyInaNcG8G9dAYzOhM', '', 1, 1563063957, NULL, NULL),
(811, 10002, 6, 'IzS2tHKHzuAqQ2i968X4YeVGQt054ukl', '', 1, 1563063957, NULL, NULL),
(812, 10002, 6, 'pjIrywzcBlPQdGam3tZCMmKLHwC0r2Vh', '', 1, 1563063957, NULL, NULL),
(813, 10002, 6, '0XMrO9dU01yNWWopwDid1OfUFoAFUPIC', '', 1, 1563063957, NULL, NULL),
(814, 10002, 6, 'sTjiCUuj3DQVuUmcy2QWoqF7tBykKSiF', '', 1, 1563063957, NULL, NULL),
(815, 10002, 6, 'SeTrlx3HMSCG4euH5K4VQFMgoCe0emmY', '', 1, 1563063957, NULL, NULL),
(816, 10002, 6, '34CSMhqLRetNqFDnJY3q4ggfH91DMbbO', '', 1, 1563063957, NULL, NULL),
(817, 10002, 6, 'VD2QHvsop1OCSA6lZbhp7LnoRvfYijbt', '', 1, 1563063957, NULL, NULL),
(818, 10002, 6, 'qka9nRE8TSdRppIVRqBvFv9HRtGAwouG', '', 1, 1563063957, NULL, NULL),
(819, 10002, 6, 'bUBsDNfEH2Pp8YITH0RBRIwHSu3eUhYW', '', 1, 1563063957, NULL, NULL),
(820, 10002, 6, '2fRdB18cr9c8MRQvIcM2e3MTiwGlo7S0', '', 1, 1563063957, NULL, NULL),
(821, 10002, 6, 'huNw7zarpLsyW3g6QIXecae1BKqB8RLE', '', 1, 1563063957, NULL, NULL),
(822, 10002, 6, 'GXDXbbgOqUd1nrGsUVG5ztKLoWtdSpET', '', 1, 1563063957, NULL, NULL),
(823, 10002, 6, '26q72eSjFnYknQOavDAtIBTSnPYIbWUV', '', 1, 1563063957, NULL, NULL),
(824, 10002, 6, 'nvWBm83G9z4TkWW2hq9xthmACXRs8HQa', '', 1, 1563063957, NULL, NULL),
(825, 10002, 6, 'VQ48dzgU7hpQrRrEmJmTJN091e4aXPyd', '', 1, 1563063957, NULL, NULL),
(826, 10002, 6, 'kOi83lY256qMhsJkxUyV6IDdhGtYIe62', '', 1, 1563063957, NULL, NULL),
(827, 10002, 6, 'VfCmpDAh4o9ajueIa88GLKqo1kngDzXs', '', 1, 1563063957, NULL, NULL),
(828, 10002, 6, 'IJE1BNFME4fmcLdfWKGKUWg6lfZCBxWV', '', 1, 1563063957, NULL, NULL),
(829, 10002, 6, 'NXUhLt2DD5wariGIdrx8eEeqFY5t23rq', '', 1, 1563063957, NULL, NULL),
(830, 10002, 6, 'nQo3H1iC3qCwgU4PNT9JQRVxVU8LpKvd', '', 1, 1563063957, NULL, NULL),
(831, 10002, 6, 'rsyvNcjS7mWMoAIeXVPfm56kI8ekknAh', '', 1, 1563063957, NULL, NULL),
(832, 10002, 6, 'C35n7LbuAm4lsWxOLcF0OLWljg4TnqPV', '', 1, 1563063957, NULL, NULL),
(833, 10002, 6, '74Wak0a3I629GiRa8meZbwP7bkoJq5g5', '', 1, 1563063957, NULL, NULL),
(834, 10002, 6, 'kFxS7S9R66Ww9DXpDD1bhxV72Cv68g0c', '', 1, 1563063957, NULL, NULL),
(835, 10002, 6, 'l1zQMr6Gy49RXUJa0uHkwD9AxEi71QEM', '', 1, 1563063957, NULL, NULL),
(836, 10002, 6, 'IpjfuFawiXzcKEFV4cJ4rjafp8iy2qiv', '', 1, 1563063957, NULL, NULL),
(837, 10002, 6, 'fKSLDtIa7rPeuCaK6SIHscwbUEMqwEgU', '', 1, 1563063957, NULL, NULL),
(838, 10002, 6, '9FMjQIR4vp4YIOmBX94oVVmhjxRFE2we', '', 1, 1563063957, NULL, NULL),
(839, 10002, 6, 'THc7HVxchBL95dmxeOjBQbE8aKUVzYze', '', 1, 1563063957, NULL, NULL),
(840, 10002, 6, '7URjNmsQ803o0wYQm6xOTqRwASvQCWiu', '', 1, 1563063957, NULL, NULL),
(841, 10002, 6, 'PffDi2VzzGOpUs39XPatY6FpXt7I6SV6', '', 1, 1563063957, NULL, NULL),
(842, 10002, 6, 'gMQU2ygFFL2bOgzPP4m8ME1uSracH01a', '', 1, 1563063957, NULL, NULL),
(843, 10002, 6, 'uaUKibtWgoiLa8Cm99JIp91u4A97wXTf', '', 1, 1563063957, NULL, NULL),
(844, 10002, 6, 'rzEvXvfNKMYWck46y0WnYlM56ycJmQcq', '', 1, 1563063957, NULL, NULL),
(845, 10002, 6, 'iCAyii3BxfDLHsNkN9OLtw9qJbuLIlWL', '', 1, 1563063957, NULL, NULL),
(846, 10002, 6, 'XOSrIo6pOU211vG7WW3s4ZSqDqYI8wjB', '', 1, 1563063957, NULL, NULL),
(847, 10002, 6, 'QWic6DQHtJI3vzbAanTNACrNFPrylng2', '', 1, 1563063957, NULL, NULL),
(848, 10002, 6, 'Pp4etRK7eg4FLT3TpuZewcpwXobABXdp', '', 1, 1563063957, NULL, NULL),
(849, 10002, 6, 'bVMCezme05VfJbVjWfyXYmYcPwFAJHDX', '', 1, 1563063957, NULL, NULL),
(850, 10002, 6, 'Y8iN2VXOyuW1Wrnfrnqjrl4DqsjTbuXF', '', 1, 1563063957, NULL, NULL),
(851, 10002, 6, 'pvfqbMdLMm678JJPMClSDivgnLJiDtGq', '', 1, 1563063957, NULL, NULL),
(852, 10002, 6, 'IvqQcHvXjlHUHxhD5k8mbopbMwXi2XeV', '', 1, 1563063957, NULL, NULL),
(853, 10002, 6, 'thMqe3a3uEuaK5VyjWzLxAzIWQ5wsJrk', '', 1, 1563063957, NULL, NULL),
(854, 10002, 6, 'ntXWqgyHQrJ0hXIrkNdHXAKHPMpZLPuJ', '', 1, 1563063957, NULL, NULL),
(855, 10002, 6, 'Rdw0K0cvjCfYvM9HAo9YEWJWotGvLaTm', '', 1, 1563063957, NULL, NULL),
(856, 10002, 6, 'ip5GhYnyFjJeLvI18RZyjWM1osaSNStv', '', 1, 1563063957, NULL, NULL),
(857, 10002, 6, 'QEV4hoGSwiI17woPZmQq1vQ6PNLDC1oa', '', 1, 1563063957, NULL, NULL),
(858, 10002, 6, 'FZluHL1bBmqkC6Szn60R8QhROAGTTfQS', '', 1, 1563063957, NULL, NULL),
(859, 10002, 6, 'UukJO62EEowwvaoG4oEIWXFdy3NEiyOD', '', 1, 1563063957, NULL, NULL),
(860, 10002, 6, 'G24VqxGmwgtUYxNQU9f8s4o2QWglLasu', '', 1, 1563063957, NULL, NULL),
(861, 10002, 6, 'p9OuSxuMBN8kxREv33RDOYWbTcuWJs12', '', 1, 1563063957, NULL, NULL),
(862, 10002, 6, 'HKdjhs0nlcljAJKpXiZaopduSR7A3233', '', 1, 1563063957, NULL, NULL),
(863, 10002, 6, 'EAwIfU6FRYaYqFIDjR911oOP35GicEO5', '', 1, 1563063957, NULL, NULL),
(864, 10002, 6, 'kbDKF6gzxSxEmT5UoizFCzAgqpFiL19J', '', 1, 1563063957, NULL, NULL),
(865, 10002, 6, 'y8scn7WPdSoh1dl2giutN3BOxPTXEEsI', '', 1, 1563063957, NULL, NULL),
(866, 10002, 6, 'RsyVEoep4g6ourjboqCorflfsUiUAdco', '', 1, 1563063957, NULL, NULL),
(867, 10002, 6, 'jj1FIBDSnD3XUq1NSz7se8v8fX6VlLi8', '', 1, 1563063957, NULL, NULL),
(868, 10002, 6, 'ONE16nS5viwvoUA6L7cGbP1PxIDGtiL3', '', 1, 1563063957, NULL, NULL),
(869, 10002, 6, 'LJpFR1p32wWHlJHhfhLROtTPzVlN9H7h', '', 1, 1563063957, NULL, NULL),
(870, 10002, 6, 'Vx05B6je1euvIM5tEdzyP9Ere58UK38g', '', 1, 1563063957, NULL, NULL),
(871, 10002, 6, 'ttQ7AcvYoShmqxqTwZ79DIPgQB2SHouE', '', 1, 1563063957, NULL, NULL),
(872, 10002, 6, 'axZTdpWMCbuGpwSCp776wjB6KU5HY0Fz', '', 1, 1563063957, NULL, NULL),
(873, 10002, 6, 'SdlZ2LsQl4EHSLdxugXRp4TikbpwoidM', '', 1, 1563063957, NULL, NULL),
(874, 10002, 6, 'iL2sRx3hBEcbzbDwkW9tmwvrDiZNtPH6', '', 1, 1563063957, NULL, NULL),
(875, 10002, 6, 'ey3VkUBaVJJdSCOC4zKg9ao4NfbAG6gL', '', 1, 1563063957, NULL, NULL),
(876, 10002, 6, 'q2BfZ19Cz8F5PzeFBD85MReCEkcLJxxc', '', 1, 1563063957, NULL, NULL),
(877, 10002, 6, 'OF0ycrpf9uPcdgePXbJIIPImA5nkL0Mp', '', 1, 1563063957, NULL, NULL),
(878, 10002, 6, 'PYKy2mJ566D4bvnBg94ycdZnIbUU6qOi', '', 1, 1563063957, NULL, NULL),
(879, 10002, 6, 'DIWPaOVIgVNt9nKh2Qrb7ojKMYQn4XNi', '', 1, 1563063957, NULL, NULL),
(880, 10002, 6, 'fWK1eY7wxa9YWwcQWkpmZXLQDQu5Kpyi', '', 1, 1563063957, NULL, NULL),
(881, 10002, 6, 'yPKzYGMGLktm2gzzd1HJDmr59bSEEpPl', '', 1, 1563063957, NULL, NULL),
(882, 10002, 6, 'M9nWMOK2EhnkEtl73FeCu9p7blJpPxwN', '', 1, 1563063957, NULL, NULL),
(883, 10002, 6, '8F3Xyv9EBfYMvyEPNn8vWjBKCClW3OV3', '', 1, 1563063957, NULL, NULL),
(884, 10002, 6, 'gXBduPrzv6xW78zmbiF1JNDWmPFzx7jW', '', 1, 1563063957, NULL, NULL),
(885, 10002, 6, 'WwAIJL83NAN33rCMAFVKWBmgleVYM21n', '', 1, 1563063957, NULL, NULL),
(886, 10002, 6, 'mTlQ4vivTQffWU7O8AxhG8PZutxvsbwJ', '', 1, 1563063957, NULL, NULL),
(887, 10002, 6, 'jlHLQv2DvNoO7j5pcN4i4uemcmsNYrGa', '', 1, 1563063957, NULL, NULL),
(888, 10002, 6, 'OOSXXLeiqG2LSLtdYCmAM7GHISqp1uv4', '', 1, 1563063957, NULL, NULL),
(889, 10002, 6, 'sjowqFf6L7arctCAIPAAupzwdro5asax', '', 1, 1563063957, NULL, NULL),
(890, 10002, 6, 'E25iFhzqRkfqDqoMOaBoKIYbziu0upbe', '', 1, 1563063957, NULL, NULL),
(891, 10002, 6, '87q5b21N2VMQEoXDnG1UIknzpZS2nxSC', '', 1, 1563063957, NULL, NULL),
(892, 10002, 6, '5LmQtiNTCUg5ifDPLmg4sf75zrMs0UpD', '', 1, 1563063957, NULL, NULL),
(893, 10002, 6, 'owNkeYkmWHPale979jW9eVptRFd1kTHr', '', 1, 1563063957, NULL, NULL),
(894, 10002, 6, 'i4iftv4nxfMLQJOQZp5Z2BECAn7hOwNO', '', 1, 1563063957, NULL, NULL),
(895, 10002, 6, '2PsI68l7voW1qRabQUQtclcdvOAAu5Ow', '', 1, 1563063957, NULL, NULL),
(896, 10002, 6, 'uRlGn93pv3UnrsIQkwgvqEFFBX36P1nq', '', 1, 1563063957, NULL, NULL),
(897, 10002, 6, 'RBUDWxMcwx3KB61IYNkmjjJNTEq5VqbA', '', 1, 1563063957, NULL, NULL),
(898, 10002, 6, 'wuTtRImD5FwaDopCXrfNBKch7uiS1SgK', '', 1, 1563063957, NULL, NULL),
(899, 10002, 6, 'NkQt64H4lDz1eC2xLQn17qqh1BDrHY9j', '', 1, 1563063957, NULL, NULL),
(900, 10002, 6, 'oDBVOWbdTNRao7pwamsrYTpInODPtfbo', '', 1, 1563063957, NULL, NULL),
(901, 10002, 6, 'mqlEIPRlcFdqMuWqxpHVNhQyXkqEtqv7', '', 1, 1563063957, NULL, NULL),
(902, 10002, 6, 'YGTOZEuwYX2wt4CHTE3ONFeVHkjkTVEs', '', 1, 1563063957, NULL, NULL),
(903, 10002, 6, 'CBmNh2H4kReqxfAWpuwFXHiQiNDgUw11', '', 1, 1563063957, NULL, NULL),
(904, 10002, 6, 'hW3ZoYSIaDvpExL3p9661zVLb2y44u9U', '', 1, 1563063957, NULL, NULL),
(905, 10002, 6, '69eT7M08f2koPjpSvjitFo6X31HmG5NA', '', 1, 1563063957, NULL, NULL),
(906, 10002, 6, 'C6xEyKvr6FD9jXodhfm29D9A2YeQNKCs', '', 1, 1563063957, NULL, NULL),
(907, 10002, 6, 'aIhmSeNbXR3ndjc5cCDh71f9II6g54OD', '', 1, 1563063957, NULL, NULL),
(908, 10002, 6, 'ayAQO13ACIepoqs4QozlPbK9vNJzVHb7', '', 1, 1563063957, NULL, NULL),
(909, 10002, 6, 'XJ7ljBKIjlDAXWQpuXqOKzGLtIxtRdtd', '', 1, 1563063957, NULL, NULL),
(910, 10002, 6, 'GIMAQ6SfW9WLgBTqYkG3ipaOPrdYRxQc', '', 1, 1563063957, NULL, NULL),
(911, 10002, 6, 'qK62Ar1He3AyDxHrEbHtIGAUMeJ15wKe', '', 1, 1563063957, NULL, NULL),
(912, 10002, 6, 'LIrkqLMKeW1V9INH7nCSQmhRb0uWKaCM', '', 1, 1563063957, NULL, NULL),
(913, 10002, 6, 'bKVc4RodwcdThAG4N1qy2uoh4Jhyh3z7', '', 1, 1563063957, NULL, NULL),
(914, 10002, 6, '85NhEEWJceYqTw1w3wrkmOW42hJ4sYfi', '', 1, 1563063957, NULL, NULL),
(915, 10002, 6, 'eG7KXS93D8kCy07E97smlpECTVE1OcNf', '', 1, 1563063957, NULL, NULL),
(916, 10002, 6, 'EZ16hptYJ4yWrhrsM10b5HVGxOHgVnb8', '', 1, 1563063957, NULL, NULL),
(917, 10002, 6, 'knNqD1TfybuKmVgLPxLsyWppyhoRwAVV', '', 1, 1563063957, NULL, NULL),
(918, 10002, 6, 'hosY0DcOc7maykPM9zFkYfLeHVjak8TU', '', 1, 1563063957, NULL, NULL),
(919, 10002, 6, 'lgqeoV7audSUripe611TwkyfEyHk7WGJ', '', 1, 1563063957, NULL, NULL),
(920, 10002, 6, 'n8KTXVYGfl2ZSt4atzTuHnkTOwf3rrBw', '', 1, 1563063957, NULL, NULL),
(921, 10002, 6, '2TGjcE7n2rQxIyGsuWTvoucEaHrOVlFl', '', 1, 1563063957, NULL, NULL),
(922, 10002, 6, 'ImmrLnNyaIqEwIj1PqdgdMFLXkKCGwab', '', 1, 1563063957, NULL, NULL),
(923, 10002, 6, 'gaExDow4vdaUER5bfkYhVlNdzACUX5pS', '', 1, 1563063957, NULL, NULL),
(924, 10002, 6, 'GWQudCkJXcDtXTjaITS9EPVd8tfZ7NHl', '', 1, 1563063957, NULL, NULL),
(925, 10002, 6, 'Wa182TxYafKXcGmT47JiwdVIRLhAYATB', '', 1, 1563063957, NULL, NULL),
(926, 10002, 6, 'SaDguFbsq0llrynwaHhzsU27K71Baqvs', '', 1, 1563063957, NULL, NULL),
(927, 10002, 6, 'ScDY1kiP9jOOjq4odhPA5p426nWgs8z0', '', 1, 1563063957, NULL, NULL),
(928, 10002, 6, 'jV2dRCx62rzkMXD2if9xbOEydDnyOKMy', '', 1, 1563063957, NULL, NULL),
(929, 10002, 6, 'jeIylcpQG1tNm9QNyOsOFWhswfsDiqWB', '', 1, 1563063957, NULL, NULL),
(930, 10002, 6, 'huVYQY3T6myDybvRq6WH4jSmVTLd3Crk', '', 1, 1563063957, NULL, NULL),
(931, 10002, 6, 'HYhwtOxlre6ITWGP6ymQWXLNlAa6EGB5', '', 1, 1563063957, NULL, NULL),
(932, 10002, 6, 'PCEq7nmNwSlGSeuB57ChRNuh9zfFabRh', '', 1, 1563063957, NULL, NULL),
(933, 10002, 6, 'mVNX28Nj4QR8ZuGQ7LeOqz759CYPDANS', '', 1, 1563063957, NULL, NULL),
(934, 10002, 6, 'G9ldHxEPPAw4VFmDf3E3ONupmI4Xwy8O', '', 1, 1563063957, NULL, NULL),
(935, 10002, 6, 'qqAGmik4vlxt78ckyMzqUN3SE1AAQNB3', '', 1, 1563063957, NULL, NULL),
(936, 10002, 6, 'c0r51gfJlzaPROnCAbDAcv8ru7gj60Ov', '', 1, 1563063957, NULL, NULL),
(937, 10002, 6, 'BorHUhM2NMXpazHQW4RQTYzbySIiGYy6', '', 1, 1563063957, NULL, NULL),
(938, 10002, 6, 'xJ20dPUs7z2PmNJxMQkDSy5ISk84PMUe', '', 1, 1563063957, NULL, NULL),
(939, 10002, 6, 'Ff6mSefBbOOMchBPJTMzY0b1TOWJtVns', '', 1, 1563063957, NULL, NULL),
(940, 10002, 6, 'eSF69IEU8EfYiUKcVU8vD1FaPYfXk6M8', '', 1, 1563063957, NULL, NULL),
(941, 10002, 6, '81S23drOoRL3eMyD57Q1tTyz3CCQ7mTm', '', 1, 1563063957, NULL, NULL),
(942, 10002, 6, '1OSwYqBFMvYcQ5lEBnuhIFiHMADt4kmb', '', 1, 1563063957, NULL, NULL),
(943, 10002, 6, 'kLHEySlKrKJVdSLLnp6rDGux15t6vWrP', '', 1, 1563063957, NULL, NULL),
(944, 10002, 6, 'cbgohqWA5v3VHzq2hXNHDE7Ux7Dapsqf', '', 1, 1563063957, NULL, NULL),
(945, 10002, 6, 'GmqcAwp1Uifk5BtUJgbclRuizcsvTKwY', '', 1, 1563063957, NULL, NULL),
(946, 10002, 6, 'ewK0zSgv6VfrS4b5r4PVqJK50W8Hy7ZD', '', 1, 1563063957, NULL, NULL),
(947, 10002, 6, 'uPOAFqcKpvQ62cqsvPF7okY9bzKkztPD', '', 1, 1563063957, NULL, NULL),
(948, 10002, 6, 'cU8EA7hOZzOcLjVDnqtgYV7VHywbYs9w', '', 1, 1563063957, NULL, NULL),
(949, 10002, 6, 'dKHMDNxN36huo9PTNFFkJccmd08I1Ori', '', 1, 1563063957, NULL, NULL),
(950, 10002, 6, 'H2Xn5w9yWlR77noxGxAj13JFPvQ9ovw9', '', 1, 1563063957, NULL, NULL),
(951, 10002, 6, 'EDObsT68t9MgzDeiuu3JKrmSQXcTyjAc', '', 1, 1563063957, NULL, NULL),
(952, 10002, 6, 'Y64wHbie3RvfoFyHawWDgK4a95Xx1Ko4', '', 1, 1563063957, NULL, NULL),
(953, 10002, 6, 'FrEGkXR047mmHjBMaYcrPqAWS6M8Bisx', '', 1, 1563063957, NULL, NULL),
(954, 10002, 6, 'RsopzNDDChJYm8xWclO6V3HyU7f0bh8X', '', 1, 1563063957, NULL, NULL),
(955, 10002, 6, '6Z4nExtW1tOnZUKycCWM61yqIAOpnXIw', '', 1, 1563063957, NULL, NULL),
(956, 10002, 6, 'XqtQwApmlNBUupWTSmfpAnetm1EVfsZb', '', 1, 1563063957, NULL, NULL),
(957, 10002, 6, '8mRMJy6NXw2V1Gs6u3CSnjWHu0mnhUMD', '', 1, 1563063957, NULL, NULL),
(958, 10002, 6, 'ujw1k6lnCHXE3qngEgYOvFxE4m6gMFm0', '', 1, 1563063957, NULL, NULL),
(959, 10002, 6, 'QNLsDGIGCmyIDjvX2iOxsnh75NhURCqS', '', 1, 1563063957, NULL, NULL),
(960, 10002, 6, 'uBD6kIMVFIoDiBptNnBjo5q7Kzk1s39v', '', 1, 1563063957, NULL, NULL),
(961, 10002, 6, 'EJAkhFlQYqaBCsr9xWDP3FMAVJSsOiIg', '', 1, 1563063957, NULL, NULL),
(962, 10002, 6, 'vdeQFwpScb1FKkfS3T1qzxv3Xr3vgMLF', '', 1, 1563063957, NULL, NULL),
(963, 10002, 6, 'ymQW2fsB0n74zNtJ2STqtYqcqrkiBgP5', '', 1, 1563063957, NULL, NULL),
(964, 10002, 6, 'eX46aL0toK24BTcY5qENHXgiqShSVnw2', '', 1, 1563063957, NULL, NULL),
(965, 10002, 6, '32wT2AGgLotiDDKKhfpiQEeakiMPiWFp', '', 1, 1563063957, NULL, NULL),
(966, 10002, 6, 'shHp3ss0XHDh3DcqNrytje7jNS7QkSpE', '', 1, 1563063957, NULL, NULL),
(967, 10002, 6, 'VoDVrxH8Yrcntwqn592EgGCJrubw7jie', '', 1, 1563063957, NULL, NULL),
(968, 10002, 6, 'tg9NceIkQwlYx4dq9QpG8B3ra3N5igeU', '', 1, 1563063957, NULL, NULL),
(969, 10002, 6, 'FxD4BYTX3QuRQCpD1xw4UwFubUp3hbUT', '', 1, 1563063957, NULL, NULL),
(970, 10002, 6, 'eh8rfYgE15TJENIvidAm0Ccag1xXCeiI', '', 1, 1563063957, NULL, NULL),
(971, 10002, 6, 'hT4L8F66YoF1I82hy2cw07uT8MIsRDnF', '', 1, 1563063957, NULL, NULL),
(972, 10002, 6, 'nvyyBKHhasV53bRzFLx1iIM90ENHAVtA', '', 1, 1563063957, NULL, NULL),
(973, 10002, 6, 'PIfpA1ofoZ4xfgWmUhh5OSJ7qnEYcc27', '', 1, 1563063957, NULL, NULL),
(974, 10002, 6, 'O7yvn2gVuEI2m6AeHn9uAHnGKgPf7qlq', '', 1, 1563063957, NULL, NULL),
(975, 10002, 6, 'rHhCL1t4AflgXCGISW8EEtcAwsdbKTs2', '', 1, 1563063957, NULL, NULL),
(976, 10002, 6, 'Y32mi0hseQRWCCHAuAPau3TnOUeCjRej', '', 1, 1563063957, NULL, NULL),
(977, 10002, 6, 'fS4yYnukWsiG6UPjteig8vp9iqMulHbw', '', 1, 1563063957, NULL, NULL),
(978, 10002, 6, '4scb3zZYWhzxkKFpkr4j87mKKSfia3cT', '', 1, 1563063957, NULL, NULL),
(979, 10002, 6, 'ukU2mBlJqIPJ9CuW7fdWlfs7xun7l9Oo', '', 1, 1563063957, NULL, NULL),
(980, 10002, 6, 'JDFMQDKlLX7c9BXLDlDXocyv6BcUtAE7', '', 1, 1563063957, NULL, NULL),
(981, 10002, 6, 'yjCURSL4mQxxQ5fjpgivd6pAbXuoYirM', '', 1, 1563063957, NULL, NULL),
(982, 10002, 6, 'u1Mat7uH4uUi4iU6gULiml9yyZtCz81O', '', 1, 1563063957, NULL, NULL),
(983, 10002, 6, 'f9kLV3p1EU349RR45mKVwSeTRslanL9p', '', 1, 1563063957, NULL, NULL),
(984, 10002, 6, 'LWessBHO0ryESOSpcxRhXQQ7f7lPFaAz', '', 1, 1563063957, NULL, NULL),
(985, 10002, 6, '5Ll6uL34BXFGDzhaN2JuhUWMd6X2WWF1', '', 1, 1563063957, NULL, NULL),
(986, 10002, 6, 'TLpz6ubtfqShPNrYDAZeRIAIusYbMH4j', '', 1, 1563063957, NULL, NULL),
(987, 10002, 6, 'NvYfYk5FPesZ2fuSdGIRzGRi93dHMVYl', '', 1, 1563063957, NULL, NULL),
(988, 10002, 6, 'jYiXKEJboDSLd1KOGyteCdRMFQycWDuy', '', 1, 1563063957, NULL, NULL),
(989, 10002, 6, 'amT1bsaYc077dj5Phmut2legLfmcDFCG', '', 1, 1563063957, NULL, NULL),
(990, 10002, 6, 'z1YESnWJXGCxyqzdA75NVwXDhy3d65HP', '', 1, 1563063957, NULL, NULL),
(991, 10002, 6, 'UzHThxHyOXn98KNMJiRXBekSia88toHJ', '', 1, 1563063957, NULL, NULL),
(992, 10002, 6, 'MibDK22CiEG4YhnHMigp1tjCioP7dEIq', '', 1, 1563063957, NULL, NULL),
(993, 10002, 6, 'QTVGd6OTrpP58Hol4SWkG3yqYa7E13jS', '', 1, 1563063957, NULL, NULL),
(994, 10002, 6, 'HYSgybM0snrYSqVvpgb66McLgmBgeoVQ', '', 1, 1563063957, NULL, NULL),
(995, 10002, 6, '48d8Mj97eHIyOdiXa9CM303gU8lcg0WU', '', 1, 1563063957, NULL, NULL),
(996, 10002, 6, 'SkdOKDPh97SQMRtrcBDiTK6Bn9daypxR', '', 1, 1563063957, NULL, NULL),
(997, 10002, 6, 'JinA6r69HC4DcP8MbtjmvvOddH7aQ7eq', '', 1, 1563063957, NULL, NULL),
(998, 10002, 6, 'DvrPJgHou4VAvA9Cu4E1hX7iNwiAmdh2', '', 1, 1563063957, NULL, NULL),
(999, 10002, 6, 'h2boieWaGk6dqLJ42HGPw3EHZDkuoHz3', '', 1, 1563063957, NULL, NULL),
(1000, 10002, 6, 'evIMaK85WAqR51ah7z8edpf4dLbuBCRS', '', 1, 1563063957, NULL, NULL),
(1001, 10002, 6, '4Y7tnsmQmc6NfMjzVbA79DCykt3jz2iu', '', 1, 1563063957, NULL, NULL),
(1002, 10002, 6, 'rWvRTCXgi8HnMFsYwWcRPzJKzpx2edCG', '', 1, 1563063957, NULL, NULL),
(1003, 10002, 6, 'cCTTXUR4S2egiNo7orBRAWaVvofu9GKn', '', 1, 1563063957, NULL, NULL),
(1004, 10002, 6, 'bOTeBxYEiv3HW62dHdEuQGey5Hx9NWvL', '', 1, 1563063957, NULL, NULL),
(1005, 10002, 6, 'kiccVk27DfjcZ7feuKAQzUGuaxEUvziK', '', 1, 1563063957, NULL, NULL),
(1006, 10002, 6, 'ObNKQCjBd5EMQSnrks2oRjR9O38saplf', '', 1, 1563063957, NULL, NULL),
(1007, 10002, 6, 'dCR04VOw90xNXiw8B8YnlwaRzkmm5eSZ', '', 1, 1563063957, NULL, NULL),
(1008, 10002, 6, 'v7M3b3H87hkvPVLITjWYfC6DkHGxkmDI', '', 1, 1563063957, NULL, NULL),
(1009, 10002, 6, 'rWuDlVFW2tVd5NjijZxNVPoXotHy1NmB', '', 1, 1563063957, NULL, NULL),
(1010, 10002, 6, 'FwlPEsI9aNH6487SBcFE9X6cJkzrNVlh', '', 1, 1563063957, NULL, NULL),
(1011, 10002, 6, 'WY0oBcuqNe3W0sMjIMEMRkJwgcTf3gZ2', '', 1, 1563063957, NULL, NULL),
(1012, 10002, 6, 'T69Jjzc5VzaB25YkNRCvCbjYsK6XQs8u', '', 1, 1563063957, NULL, NULL),
(1013, 10002, 6, 'SUz6vUjxGiKs2RWMLWmxhtBkIEqw7R8g', '', 1, 1563063957, NULL, NULL),
(1014, 10002, 6, 'lDwWV6Vbey8bLJ2twKI3TkAbOKpvCphE', '', 1, 1563063957, NULL, NULL),
(1015, 10002, 6, '3DEsUCAkqEtrht2dXFNU4nGKYsrBOznD', '', 1, 1563063957, NULL, NULL),
(1016, 10002, 6, 'gdXTOe1WNBVtdVndaubxfjJgGp83ENWu', '', 1, 1563063957, NULL, NULL),
(1017, 10002, 6, 'EaveMKIB1srjGRNU4zOobN7HLREVsBwa', '', 1, 1563063957, NULL, NULL),
(1018, 10002, 6, '9vAbWoGW8Sc77m4aeM4ne7fHYgQEGjVl', '', 1, 1563063957, NULL, NULL),
(1019, 10002, 6, 'xp8jcxXVWAAxpGtk3pqmqD1snL1TmuJk', '', 1, 1563063957, NULL, NULL),
(1020, 10002, 6, 'qri3fcpcS5rUeVXRA3ow9D2m55sc15bq', '', 1, 1563063957, NULL, NULL),
(1021, 10002, 6, 'wKncbCCLPM8rgBEqsPFTy1KiuAJMHwpa', '', 1, 1563063957, NULL, NULL),
(1022, 10002, 6, 'aErbakhsToiehRBnPbsoLeASwpw2I4aP', '', 1, 1563063957, NULL, NULL),
(1023, 10002, 6, 'VHbGE7pRBJ95bgoWLJS4MOv8lPEKsExD', '', 1, 1563063957, NULL, NULL),
(1024, 10002, 6, 'MsK1XVKhkPK4YmheTL58zSW29QkUKgx4', '', 1, 1563063957, NULL, NULL),
(1025, 10002, 6, 'PCzRSNvU13ba5NwupI4zjgeGl9CGt8dq', '', 1, 1563063957, NULL, NULL),
(1026, 10002, 6, 'hVhEKR64RnnOLuUMoSb1vIx65b8TJYif', '', 1, 1563063957, NULL, NULL),
(1027, 10002, 6, 'Jo3cZfk0dAWxTDS0JopaSBQmKZxOTTqK', '', 1, 1563063957, NULL, NULL),
(1028, 10002, 6, 'kygJCQPlX5Xrk93nrrDEMNDuuvC2NAYR', '', 1, 1563063957, NULL, NULL),
(1029, 10002, 6, 'kL3XBQatt1dla4rRryNozZiFOvFmrhAD', '', 1, 1563063957, NULL, NULL),
(1030, 10002, 6, 'jTzfy2rG8NXOW9H9B9NuaYNzQNAnMKyj', '', 1, 1563063957, NULL, NULL),
(1031, 10002, 6, 'xyAmu1urTtsPP8HsGIWMJfHJxY3xwDbq', '', 1, 1563063957, NULL, NULL),
(1032, 10002, 6, 'LaEfrgagvJd2Mew3jSJKKxSKp2qRb5eo', '', 1, 1563063957, NULL, NULL),
(1033, 10002, 6, '5Kf99EVVr8jBK9IQMHexMQ1HDyYogJBc', '', 1, 1563063957, NULL, NULL),
(1034, 10002, 6, 'TFdWy12358l6BNer4jRjcuDXdNtssS7B', '', 1, 1563063957, NULL, NULL),
(1035, 10002, 6, 'rNE2R5LyCRG8VRKHjzLeyDWsejlCHMJN', '', 1, 1563063957, NULL, NULL),
(1036, 10002, 6, 'OD6Ro3xkvV3C0DMENv0gnWSHPsbZutt1', '', 1, 1563063957, NULL, NULL),
(1037, 10002, 6, 'TZ3TUROdxKyaieDI301ebzfAIizFLhHn', '', 1, 1563063957, NULL, NULL),
(1038, 10002, 6, 'xbAqyVDbr78vWGdS47KePTI7V2XYSu6E', '', 1, 1563063957, NULL, NULL),
(1039, 10002, 6, 'mooXeMcKnv7MYoAWmqMnze91l4o1TS25', '', 1, 1563063957, NULL, NULL),
(1040, 10002, 6, 'CHzABvxcfmEilzyaiTH1AoiNjb9ILshO', '', 1, 1563063957, NULL, NULL),
(1041, 10002, 6, 'zwkJIE1IGnHHptGM8BkeDmCqEkukIVmM', '', 1, 1563063957, NULL, NULL),
(1042, 10002, 6, 'PRHY8HDPHgNx2WkFopGhVsCpo0wns9SR', '', 1, 1563063957, NULL, NULL),
(1043, 10002, 6, 'obB5CdOcXMfeGxJNQ11tumfVoy9QDDdy', '', 1, 1563063957, NULL, NULL),
(1044, 10002, 6, 'S1iBD4SoDdfM6LXojbSzcHGMbCJwKtSq', '', 1, 1563063957, NULL, NULL),
(1045, 10002, 6, 'yNNuMFGifnB9GQXwmBJM0cfOnXAJgfSJ', '', 1, 1563063957, NULL, NULL),
(1046, 10002, 6, 'ldyZauKgHxjvxS1rTY4PyTKpPgMbK2ks', '', 1, 1563063957, NULL, NULL),
(1047, 10002, 6, 'nFDbUquDUgVSl2nVIMXyXm99WcT0OpqF', '', 1, 1563063957, NULL, NULL),
(1048, 10002, 6, '43od1tq6pIvpcmzJ1VaIWFZM7YHHANLw', '', 1, 1563063957, NULL, NULL),
(1049, 10002, 6, 'Gv5HwHfyEIhgefUdgGtw27xGzNGsGs3M', '', 1, 1563063957, NULL, NULL),
(1050, 10002, 6, 'mclf3JTs2NXR9fn0A5s7iQg9hG2FZ3yd', '', 1, 1563063957, NULL, NULL),
(1051, 10002, 6, 'K7QOWHK8F7wvt1oqACnbpxMUT5Whx1Sy', '', 1, 1563063957, NULL, NULL),
(1052, 10002, 6, 'OmgUNSc7ISctULeRtVqhj7F63kLdzeTL', '', 1, 1563063957, NULL, NULL),
(1053, 10002, 6, 'x2lZAeNMqEcxtMsr25Op7ptFo9WBo9Xy', '', 1, 1563063957, NULL, NULL),
(1054, 10002, 6, 'RXXX8PtZ3pzH2wvkdpS7fJzJplrUM7vB', '', 1, 1563063957, NULL, NULL),
(1055, 10002, 6, 'OpVkCJC5NfEq5WeJ58PxlSRTrmE18jWx', '', 1, 1563063957, NULL, NULL),
(1056, 10002, 6, 'qmxAUxjB5HdqoYJr92oc7WGfJz6bbJJ4', '', 1, 1563063957, NULL, NULL),
(1057, 10002, 6, '4QlRbYtcl6d1Sp4nAVhnhTer8qDZz3sW', '', 1, 1563063957, NULL, NULL),
(1058, 10002, 6, 'XRhZTU5wMdkYhNGCZ9sUlFnY2MaVhjQb', '', 1, 1563063957, NULL, NULL),
(1059, 10002, 6, 'IHIqS5f7lygWb55aVoPUrqFR1j5hKPHn', '', 1, 1563063957, NULL, NULL),
(1060, 10002, 6, 'SRWbjNvXQzgGO1v8b60N5TS22dBXQ9mb', '', 1, 1563063957, NULL, NULL),
(1061, 10002, 6, 'AJIEvDxaSSY6zhhiriK1STGoW3n35YzQ', '', 1, 1563063957, NULL, NULL),
(1062, 10002, 6, 'oqUSuUg25liXndV7JuIrS6XbFYzrMzK5', '', 1, 1563063957, NULL, NULL),
(1063, 10002, 6, 'uRfpnO4QWoJruEehOF5WvMHeScz2fQkB', '', 1, 1563063957, NULL, NULL),
(1064, 10002, 6, 'aGRaP4WxHVNDM0nqy6weuzUqKp51oCNT', '', 1, 1563063957, NULL, NULL),
(1065, 10002, 6, 'xRWr9rNNKlcKH67Ji4bE3rbvpnYcIb8y', '', 1, 1563063957, NULL, NULL),
(1066, 10002, 6, 'mpGbovKDOWM8vPMvyMjnSWEMDRcqf4A8', '', 1, 1563063957, NULL, NULL),
(1067, 10002, 6, 'XCln5KE4dqipETFbRFGe4vbRM7lCxX8c', '', 1, 1563063957, NULL, NULL),
(1068, 10002, 6, 'HRi1N1d5rlatV5kIgBRYeodFmh1RpDSl', '', 1, 1563063957, NULL, NULL),
(1069, 10002, 6, '2cIh1P5SgNbfTnxS7uIzqb2cB74JiwjD', '', 1, 1563063957, NULL, NULL),
(1070, 10002, 6, 'mARmUclVJdbf1b8Kw4XY6U4k38zg4j1m', '', 1, 1563063957, NULL, NULL),
(1071, 10002, 6, 'AJn4iHuTuppxrTZ3Ev1R5FLRYUUU04sp', '', 1, 1563063957, NULL, NULL),
(1072, 10002, 6, 'tXoW7xv4sprLwDEg8BBvynCMCHtfjP46', '', 1, 1563063957, NULL, NULL),
(1073, 10002, 6, 'yl59zPV3a9B8Pjb9DgIftAKpgeY1WmUs', '', 1, 1563063957, NULL, NULL),
(1074, 10002, 6, 'QWkhu71HMzugz3VHD0Du7JUrlHB4fpV9', '', 1, 1563063957, NULL, NULL),
(1075, 10002, 6, 'HWMBGNLfSMSTcMh4oB7yO48IwRaqCaQT', '', 1, 1563063957, NULL, NULL),
(1076, 10002, 6, 'nXRGF1EWxONrhT0ssMs1TQdjEkbWusN0', '', 1, 1563063957, NULL, NULL),
(1077, 10002, 6, 'tcKVgQKqATNDzBTmObzEAHDEpd4xCCee', '', 1, 1563063957, NULL, NULL),
(1078, 10002, 6, 'yBDKHo2Az52DdaPb4WsisP93oZ8hhv53', '', 1, 1563063957, NULL, NULL),
(1079, 10002, 6, 'H4i7429QlbU0JQqUqY6AH8NIokg6GfjQ', '', 1, 1563063957, NULL, NULL),
(1080, 10002, 6, 'JSzde9fLHuG1WJ744B3cjj4qNgtiNTXJ', '', 1, 1563063957, NULL, NULL),
(1081, 10002, 6, 'dbYiFicubJQdASnxhNaX7gQb1nxKKjYi', '', 1, 1563063957, NULL, NULL),
(1082, 10002, 6, 'Ralvvo3UY7IQJHu8CvwcXaizPNXDivVS', '', 1, 1563063957, NULL, NULL),
(1083, 10002, 6, 'ruegoryWez5IdJX6eAkjy3hF5loQp0Kg', '', 1, 1563063957, NULL, NULL),
(1084, 10002, 6, '2A86v2OnzaTEwDFJIgG1ODmRJd5EcEq0', '', 1, 1563063957, NULL, NULL),
(1085, 10002, 6, 'bSQlNyI1zXIwLPjD9NtIrqxNlWUPvQ4A', '', 1, 1563063957, NULL, NULL),
(1086, 10002, 6, '81qbIgvT6MQTKBvwutKnr3b6R7bmKC4X', '', 1, 1563063957, NULL, NULL),
(1087, 10002, 6, 'WpW0RTBLEIxR5OaaWETN1aCW1tO0BmwI', '', 1, 1563063957, NULL, NULL),
(1088, 10002, 6, 't0RISKjd2wlszkG4LIQdXCj8lFLOeYbH', '', 1, 1563063957, NULL, NULL),
(1089, 10002, 6, 'l2Armrx360QxlLQBA7l0S9N3DkL3Slpg', '', 1, 1563063957, NULL, NULL),
(1090, 10002, 6, 'Htr9SvJjb0We5p7uCPy4KLmLU4eKBvwV', '', 1, 1563063957, NULL, NULL),
(1091, 10002, 6, 'iYJOcihCVFG4n0adq5FXfIyE1XF01Fm5', '', 1, 1563063957, NULL, NULL),
(1092, 10002, 6, 'lBo13Wksx5V0gTIBhnVTtjaHbohVpRdB', '', 1, 1563063957, NULL, NULL),
(1093, 10002, 6, 'QHvubrckqvpXdGogiqizN43USTIFF2lU', '', 1, 1563063957, NULL, NULL),
(1094, 10002, 6, 'UqpWaByyAeNgld47sb6D5Du51HGXDH8l', '', 1, 1563063957, NULL, NULL),
(1095, 10002, 6, 'nCxcEtW6wFuAh4u5pYolxhg45RbQyAgi', '', 1, 1563063957, NULL, NULL),
(1096, 10002, 6, 'VtiQAbeykIa9rw0xNATIiiOUmViijzaY', '', 1, 1563063957, NULL, NULL),
(1097, 10002, 6, 'ImWLcnhNkNr4PuNHdjw0DMKFJVqPoFNV', '', 1, 1563063957, NULL, NULL),
(1098, 10002, 6, '2HPFdu376aI62XVNHv1rJgJ5yYsHf8Kr', '', 1, 1563063957, NULL, NULL),
(1099, 10002, 6, 'qGIrqYG7meFLcjCAt3cWGUiy3tI5441a', '', 1, 1563063957, NULL, NULL),
(1100, 10002, 6, 'HuXr7yrgFz3odwOaSAnTkpmQKTxxo34I', '', 1, 1563063957, NULL, NULL),
(1101, 10002, 6, 'iqQRyy1ipjfGE6JHRbIKjKsUsTt9boY9', '', 1, 1563063957, NULL, NULL),
(1102, 10002, 6, '7yoEEEv3UfNUTN4wBv3taxJqFwegIheP', '', 1, 1563063957, NULL, NULL),
(1103, 10002, 6, '3jOrnT4YJQUzWtNmQ80dMEzv7qUZgKSJ', '', 1, 1563063957, NULL, NULL),
(1104, 10002, 6, 'id2EciNcsTUDYgZh6tcjgfiVcC6FVRyp', '', 1, 1563063957, NULL, NULL),
(1105, 10002, 6, 'cJFE8Qnu9FizsdU9KxpeXjFNEMqpRKvU', '', 1, 1563063957, NULL, NULL),
(1106, 10002, 6, 'fDI9rvuyCeJtepLQNXJOmPtBZ3TwVBUA', '', 1, 1563063957, NULL, NULL),
(1107, 10002, 6, '2omIP462cOF0M6LApQgF7OMuAKY8zgzn', '', 1, 1563063957, NULL, NULL),
(1108, 10002, 6, 'GnHaDbzGEAgYz2O87p382OSRkS0BUgzQ', '', 1, 1563063957, NULL, NULL),
(1109, 10002, 6, 'OBtnrPcEC1RtfR9VTyOzF2HKzMJPkfyl', '', 1, 1563063957, NULL, NULL),
(1110, 10002, 6, 'E3W1iAff5TaiVxvbGgFvdetbrrgdKQYA', '', 1, 1563063957, NULL, NULL),
(1111, 10002, 6, 'KEcOBOjo91M45NjwYbNnHpq7t3dBTFVL', '', 1, 1563063957, NULL, NULL),
(1112, 10002, 6, 'RqSNAhyiCHSftCXEnDdzTK5j6PbXJcE5', '', 1, 1563063957, NULL, NULL),
(1113, 10002, 6, 'PGIMO3MDHpLl27UrLg2TTQl0UB0rH2fs', '', 1, 1563063957, NULL, NULL),
(1114, 10002, 6, 'nP7IeqCElR4QNA3MKvnIr1wBq7P1AEuH', '', 1, 1563063957, NULL, NULL),
(1115, 10002, 6, 'xChNrIG9mfMmp4ddmjoDpD5CjePuXTie', '', 1, 1563063957, NULL, NULL),
(1116, 10002, 6, 'qLAvcv86UsVvsoDOyYl2UV4YNQDkAmYL', '', 1, 1563063957, NULL, NULL),
(1117, 10002, 6, 'ItiB75e8qC5pWRE2fxhRrIdVaRMHiCOK', '', 1, 1563063957, NULL, NULL),
(1118, 10002, 6, 'dJI9U7Zx72nVwlPHaA8KCVnRrhvkQVnO', '', 1, 1563063957, NULL, NULL),
(1119, 10002, 7, '6MmB2vyVfVVdmLQjvOkhQQmySmn121z9', '', 1, 1563063977, NULL, NULL),
(1120, 10002, 7, 'XttkbpueSRcMDjZ4XKrl3qD2iY1k3G2Q', '', 1, 1563063977, NULL, NULL),
(1121, 10002, 7, 'Cqq5Q3XnYYFEJzuRsJL2uctN1P91ErsA', '', 1, 1563063977, NULL, NULL),
(1122, 10002, 7, 'CdPvfaEAbVKsIX88X473YzRXqc5P9oCG', '', 1, 1563063977, NULL, NULL),
(1123, 10002, 7, 'RubdkLR9WwLjUfTNP5XHGvCVQTvglXXE', '', 1, 1563063977, NULL, NULL),
(1124, 10002, 7, 'UwrbylJEtjbYlWfB4k8zr7cT4Ynwd5E1', '', 1, 1563063977, NULL, NULL),
(1125, 10002, 7, 'E2b1iTLpSOtqNdQuZc3MJVtbVPOYkRVE', '', 1, 1563063977, NULL, NULL),
(1126, 10002, 7, 'tg3cXrxpkkOvQIEhcDnKnkqSsAQGNKcU', '', 1, 1563063977, NULL, NULL),
(1127, 10002, 7, 'S4phri0qlUYE8ytVGQnSfzPcQfDktQPN', '', 1, 1563063977, NULL, NULL),
(1128, 10002, 7, 'aFS6l03XjkHktjii3ZtAkPNQAjHjzXFW', '', 1, 1563063977, NULL, NULL),
(1129, 10002, 7, 'K23OUyNBZCRKDNKgbSTTNMG2DG8irM7t', '', 1, 1563063977, NULL, NULL),
(1130, 10002, 7, 'sCH9tczSYiYqS2bL2eHOtqBFDZPkaZE2', '', 1, 1563063977, NULL, NULL),
(1131, 10002, 7, 'UJJaW8mAamdw8TojaEqRLDYUg16KpAdQ', '', 1, 1563063977, NULL, NULL),
(1132, 10002, 7, 'wtM7RTcqdv6ezeXHAk5xbO7IGwoc9cNo', '', 1, 1563063977, NULL, NULL),
(1133, 10002, 7, 'UzzzyvSULlv5BJjU1CLFmbaPuSbtARP5', '', 1, 1563063977, NULL, NULL),
(1134, 10002, 7, 'P47ECfW9c8lImcgfJfoK7rR5nBpx5VcH', '', 1, 1563063977, NULL, NULL),
(1135, 10002, 7, 'NvoHW9P3cm7xPoFdgkhyyukaqKhSNQOe', '', 1, 1563063977, NULL, NULL),
(1136, 10002, 7, 'kykPwxjSGi8kClnvrUi9vpgLet2yIrET', '', 1, 1563063977, NULL, NULL),
(1137, 10002, 7, 'TLw3AoBJ9QODQ65QzfgakA243wNdsMAK', '', 1, 1563063977, NULL, NULL),
(1138, 10002, 7, 'LLaooxYlgCnntB1kjCFIAhH4lI7L3T0c', '', 1, 1563063977, NULL, NULL),
(1139, 10002, 7, 'bBdmqEoncAq2d3dHZShM8bqBDRpGKJtq', '', 1, 1563063977, NULL, NULL),
(1140, 10002, 7, 'lq2VX2gTFh8TUnpGkDRQtGWBytd5dSdv', '', 1, 1563063977, NULL, NULL),
(1141, 10002, 7, 'imZ9QHJjI3Xx9cat8IamO3IeWtYafBxo', '', 1, 1563063977, NULL, NULL),
(1142, 10002, 7, 'OCCn8HoVC9MeS93RohuvzWcjA1n3oK4N', '', 1, 1563063977, NULL, NULL),
(1143, 10002, 7, 'HSzR1sicdLhNyrka2oASmeghRl25tbb6', '', 1, 1563063977, NULL, NULL),
(1144, 10002, 7, 'SP1lz1KH8L4uEGGEckkVH0FqqS3fxXD2', '', 1, 1563063977, NULL, NULL),
(1145, 10002, 7, 'NfuHv2He3dvRSQhW7nMgclaxgGZJTNZa', '', 1, 1563063977, NULL, NULL),
(1146, 10002, 7, 'LOG6GHKjLnKlwWdvdgcGNzzWEiIwzp5d', '', 1, 1563063977, NULL, NULL),
(1147, 10002, 7, 'u4gWjYeaQf36IBqfJ4KWizFn2iH9ssRG', '', 1, 1563063977, NULL, NULL),
(1148, 10002, 7, '4dwJiBLhvnLW3qfnMxHDQX7KGMAiJMoe', '', 1, 1563063977, NULL, NULL),
(1149, 10002, 7, 'kF6jZ2XWzkhyN971fEk5CFdBhkDTll1q', '', 1, 1563063977, NULL, NULL),
(1150, 10002, 7, 'VkOmvbmJzEaMc3jZyayPrVlKT7yVYUb3', '', 1, 1563063977, NULL, NULL),
(1151, 10002, 7, 'sd12qomAm7OTBvZZTspXSqDOxQg1twnL', '', 1, 1563063977, NULL, NULL),
(1152, 10002, 7, 'OTlUKpAheO2ROYhoz8Ay1Q4Pg6yQYrxH', '', 1, 1563063977, NULL, NULL),
(1153, 10002, 7, 'u1UbvJ1fPksknbVGHvwU8Wzp3wORDNR6', '', 1, 1563063977, NULL, NULL),
(1154, 10002, 7, 'TpYv4VoYYQrxbnjDV4yjiFdmPjL29sXG', '', 1, 1563063977, NULL, NULL),
(1155, 10002, 7, '1LiYcAASbLjsCHM86VpocTX9uTVSwNVH', '', 1, 1563063977, NULL, NULL),
(1156, 10002, 7, 'GD9PFvCCMLw3wEFMAw20XuZFFYuNyBRt', '', 1, 1563063977, NULL, NULL),
(1157, 10002, 7, 'qUXZw1tNU7NPHT4bj24PvqxDlY5ToHxB', '', 1, 1563063977, NULL, NULL),
(1158, 10002, 7, '4P86S7aFUAIVSsqbBkMgTm3WpKrefIHf', '', 1, 1563063977, NULL, NULL),
(1159, 10002, 7, 'Ktrozc0vxgUV11qUjGaIqdimvoQCviRi', '', 1, 1563063977, NULL, NULL),
(1160, 10002, 7, 'HP7Vp0BNygs6aFXe0cnr10uzKq7zmRjf', '', 1, 1563063977, NULL, NULL),
(1161, 10002, 7, '5oDQBVAzl9vwAGFl9J0fXP6W9qTckKTJ', '', 1, 1563063977, NULL, NULL),
(1162, 10002, 7, '428h1zA7luygXmFCp6qQhEeDqUyLDcRB', '', 1, 1563063977, NULL, NULL),
(1163, 10002, 7, 'rkfZa6h4lqomQz1TiBprYyjM6NxiUenJ', '', 1, 1563063977, NULL, NULL),
(1164, 10002, 7, 'b9RMt5qyoHbYwhPotGZnnR2jEpUcT23V', '', 1, 1563063977, NULL, NULL),
(1165, 10002, 7, 'DcsAZcvMPTJpogM69rgpzvNfwg6h4vsJ', '', 1, 1563063977, NULL, NULL),
(1166, 10002, 7, 'BIU8I0WqNlwGoF5M8Ke54F86eheaRj2a', '', 1, 1563063977, NULL, NULL),
(1167, 10002, 7, 'f297vpxgeMrXVPYXdahb1c9zUtQdrOlu', '', 1, 1563063977, NULL, NULL),
(1168, 10002, 7, '8cgQn42F64DATJRPa3NdbijehaT0VKpD', '', 1, 1563063977, NULL, NULL),
(1169, 10002, 7, 'XuaMorYczT32HlmcMk51wWRmmdBlYMWc', '', 1, 1563063977, NULL, NULL),
(1170, 10002, 7, 'PCvfngB12x2jeb4uljY6xd50XoBjxfC1', '', 1, 1563063977, NULL, NULL),
(1171, 10002, 7, 'Wltnk5WpNGXOpLqpcQP6pILtJhwimuSv', '', 1, 1563063977, NULL, NULL),
(1172, 10002, 7, 'ilejbkqg2AxDS0KXyyLv1bAnhTE8eFNo', '', 1, 1563063977, NULL, NULL),
(1173, 10002, 7, 'vECTAOb4PokqjPEFmwdzBaVoQcux62Vg', '', 1, 1563063977, NULL, NULL),
(1174, 10002, 7, 'DWj8RLnIm0bjAQBowsQbACIyzft8SrdT', '', 1, 1563063977, NULL, NULL),
(1175, 10002, 7, 'GlcxNCLTw9vqFaOVoTQlkBs7syaraK2R', '', 1, 1563063977, NULL, NULL),
(1176, 10002, 7, 'ogW5s5cW8H7aP8zE5f81UMBwk2RfJHhq', '', 1, 1563063977, NULL, NULL),
(1177, 10002, 7, 'Xd2Knbz17FxN7QdWHUNKKTabMpwNWNvL', '', 1, 1563063977, NULL, NULL),
(1178, 10002, 7, '6JkbsWdo3ipNm1TGnzYCNxHqvYhksNsl', '', 1, 1563063977, NULL, NULL),
(1179, 10002, 7, 'lQHlXq7rRRKY1jdtplY2eTQAgedcbRqF', '', 1, 1563063977, NULL, NULL),
(1180, 10002, 7, 'cpukpTlp7Y9JzPUUEKaMKJJAl3PMcFUj', '', 1, 1563063977, NULL, NULL),
(1181, 10002, 7, '8QRaRCbTNbOJ5r2R73FlvbxrEjNfRQem', '', 1, 1563063977, NULL, NULL),
(1182, 10002, 7, 'SWVUpMjKdSEcxqa5clUl6JULVYAmpbXS', '', 1, 1563063977, NULL, NULL),
(1183, 10002, 7, 'v1yPowCjHRPF7Iozw0X5mNsY3JORRdQ5', '', 1, 1563063977, NULL, NULL),
(1184, 10002, 7, 'Aej8beyVWVJcBbYqEAUK0sjyEj8EXSNJ', '', 1, 1563063977, NULL, NULL),
(1185, 10002, 7, 'QROk2p7x7smhkj0Ofom1w93MNwfbN83p', '', 1, 1563063977, NULL, NULL),
(1186, 10002, 7, 'dKhb4EOM3fnjqXsYUDGNkffMRK1iq7Lm', '', 1, 1563063977, NULL, NULL),
(1187, 10002, 7, 'AaH1gnyimbipUCer65dmzpF0RgBCYhp5', '', 1, 1563063977, NULL, NULL),
(1188, 10002, 7, 'hZBGHmtqqGvzO9SBacyGQvQH5XbVupKD', '', 1, 1563063977, NULL, NULL),
(1189, 10002, 7, '6BQiuomaI1KWpdQtLUnnko4Dyzu212vy', '', 1, 1563063977, NULL, NULL),
(1190, 10002, 7, 'myM4IZRFB4OnoNl7Jo4lATVSy4Bz0H3b', '', 1, 1563063977, NULL, NULL),
(1191, 10002, 7, 'j0CkucGCP1fQFNDJWqP6uji7X3yG3Dwp', '', 1, 1563063977, NULL, NULL),
(1192, 10002, 7, 'hPM8ueleoh4x6PBSwmdcCo0h5bdMUzGu', '', 1, 1563063977, NULL, NULL),
(1193, 10002, 7, 'rvmM8EfBJXHVp2S31cYXWvgXNPcJJ7Im', '', 1, 1563063977, NULL, NULL),
(1194, 10002, 7, 'Vh52p7XnsefjzicdEJAgWzKfsNTa2x9C', '', 1, 1563063977, NULL, NULL),
(1195, 10002, 7, 'iQ8eSsFDIwNtDBMTLHU9NgArd8kJFViz', '', 1, 1563063977, NULL, NULL),
(1196, 10002, 7, 'aTWknoJPBA2dx3kr2e1se3jus2zHcLp4', '', 1, 1563063977, NULL, NULL),
(1197, 10002, 7, '4115HqUb2Je3ubCUgc8CV97ktmTWLIFc', '', 1, 1563063977, NULL, NULL),
(1198, 10002, 7, 'Nk423LhsEapcorqGYdLs6B7r1IUALsNs', '', 1, 1563063977, NULL, NULL),
(1199, 10002, 7, 'qakTQQBeAjHxxlrz4Bi5jC9EN6ok8T9w', '', 1, 1563063977, NULL, NULL),
(1200, 10002, 7, '7DNhZfOmBjxviZLNVx6qh1kEfxGmYJky', '', 1, 1563063977, NULL, NULL),
(1201, 10002, 7, 'xTisi9wu3HQamVrY10A3nkkLONa7pEIn', '', 1, 1563063977, NULL, NULL),
(1202, 10002, 7, 'rq27gM3jLl7fLhdpR9MEPLwq9FVMCvdr', '', 1, 1563063977, NULL, NULL),
(1203, 10002, 7, 'M9t9bOO72Yst7X7iB04jD0cpbvAWad47', '', 1, 1563063977, NULL, NULL),
(1204, 10002, 7, 'D0ESwTfrSgHqSpTFKq9brc6M5UmyLA2W', '', 1, 1563063977, NULL, NULL),
(1205, 10002, 7, 'PS61GLfLWYg1nOqHxf9rAYyChId8aO6p', '', 1, 1563063977, NULL, NULL),
(1206, 10002, 7, 'lCmCWxpxkbG2o2Qeza3nn1EIYYHlUpHt', '', 1, 1563063977, NULL, NULL),
(1207, 10002, 7, 'jtlOgBmAphOoYdWzcec9IuT7Qgl5TkS5', '', 1, 1563063977, NULL, NULL),
(1208, 10002, 7, 'HeiNjizUVu73gq6XkeBoFES2RS2nhKCe', '', 1, 1563063977, NULL, NULL),
(1209, 10002, 7, 'n5lPOhbY6dXnyYHHl1VTR5jktlPt6ai6', '', 1, 1563063977, NULL, NULL),
(1210, 10002, 7, '5Fjlc3s9KsYuu6rgHOPA7S0BlA1sr6QM', '', 1, 1563063977, NULL, NULL),
(1211, 10002, 7, '9vbqdYsOOfefdaRRiwTP4lXyILrNsH7t', '', 1, 1563063977, NULL, NULL),
(1212, 10002, 7, 'WIaZFsG5OzUpwyfd4SP8zuZn7EIFzTrF', '', 1, 1563063977, NULL, NULL),
(1213, 10002, 7, 'bnQqC0c9qUCsdiBAwPzWHkPsmF233S8d', '', 1, 1563063977, NULL, NULL),
(1214, 10002, 7, '1PsVxMviXciH4VnpXQNwo3GT4oDsGS7i', '', 1, 1563063977, NULL, NULL),
(1215, 10002, 7, 'ocUHJvC3lYXeVqSS9nS3UyN2TerjuXwS', '', 1, 1563063977, NULL, NULL),
(1216, 10002, 7, 'FB4fmisCYzhPQUmJNHI1OyPjjhwPTwRU', '', 1, 1563063977, NULL, NULL),
(1217, 10002, 7, 'fLR6HatD4kSWJEodhVEt0C7tGOULEUTN', '', 1, 1563063977, NULL, NULL),
(1218, 10002, 7, 'eluaNYu1DNmzDv7iydDoQaMK3glcrFzu', '', 1, 1563063977, NULL, NULL),
(1219, 10002, 7, 'vEL5b1mV0H5wIIHuUdrKTNekRkyr0qnl', '', 1, 1563063977, NULL, NULL),
(1220, 10002, 7, '32cwcLhSe3daqsaNFBpqliPqLsb48gqr', '', 1, 1563063977, NULL, NULL),
(1221, 10002, 7, 'UaEYA3EG0MWePN0MhWy7YsqHkQMVWgZU', '', 1, 1563063977, NULL, NULL),
(1222, 10002, 7, 'BMT9hU8MCLWtAMyjG7dXE7e5u5UoCNQO', '', 1, 1563063977, NULL, NULL),
(1223, 10002, 7, 'OFOdAOv3vtH5QRv8pXwBF19R7JeatQn4', '', 1, 1563063977, NULL, NULL),
(1224, 10002, 7, 'KB17YGPEGPGoelU4orWDULMM7RWQBJ7X', '', 1, 1563063977, NULL, NULL),
(1225, 10002, 7, 'rbyQlgwnaQfYfKjCTCBYXj6jXNC5qcST', '', 1, 1563063977, NULL, NULL),
(1226, 10002, 7, 'IYxHUqzbETlt2IATgqMaflLJM3XDExTb', '', 1, 1563063977, NULL, NULL),
(1227, 10002, 7, 'K3VdnocAvorJxvlAzkVkMsrtSsetFK1v', '', 1, 1563063977, NULL, NULL),
(1228, 10002, 7, 'Bu3ZxEUqq5E3M0qcIuLxYEJ2O9vsMIvL', '', 1, 1563063977, NULL, NULL),
(1229, 10002, 7, 'WSPrt4KUgNguYhsX2Oa1IBcYuQtCX2q9', '', 1, 1563063977, NULL, NULL),
(1230, 10002, 7, 'LQeBI92QOc8jDAI7xJPIQWOxvv1g4OiF', '', 1, 1563063977, NULL, NULL),
(1231, 10002, 7, 'Fj4pJeIw2E9WhjmkttG7r0WBjlL1EKN6', '', 1, 1563063977, NULL, NULL),
(1232, 10002, 7, 'fkAdjCq8A3e7ek3d7QuTooXP61LtS4Ui', '', 1, 1563063977, NULL, NULL),
(1233, 10002, 7, 'A85BIkqbXqM6z3kyJo11luV23thF7ww5', '', 1, 1563063977, NULL, NULL),
(1234, 10002, 7, 's1Wz55HBeYG86PwIS4Uzwo8vMXL8kPRg', '', 1, 1563063977, NULL, NULL),
(1235, 10002, 7, 'ofx8QbZFof7KRBGjm61nS5P989aX6uMK', '', 1, 1563063977, NULL, NULL),
(1236, 10002, 7, 'r61GHG5xGtSqkOC9PrItCbbFKXSok2Ac', '', 1, 1563063977, NULL, NULL),
(1237, 10002, 7, 'jbEidyPSIj5WQCrWiCA87dk1MUqDhupO', '', 1, 1563063977, NULL, NULL),
(1238, 10002, 7, 'qe99FJKfEPzLIu3dtzs25RYvB85uzbAe', '', 1, 1563063977, NULL, NULL),
(1239, 10002, 7, 'DLJjwTsx4ZidW91geGjk9azxeoxvQcKJ', '', 1, 1563063977, NULL, NULL),
(1240, 10002, 7, 'pRB8bY232fsehLXvbYeDeyH9CqQGpJSf', '', 1, 1563063977, NULL, NULL),
(1241, 10002, 7, 'SSBrrfyqyYdaVS1dIgrYF7GMLLM6SrpK', '', 1, 1563063977, NULL, NULL),
(1242, 10002, 7, '64fa0ln7zWRIuxZYBc6nn8xdRlTLmdbd', '', 1, 1563063977, NULL, NULL),
(1243, 10002, 7, 'xUnf3PV1WXfaY1n4TNrzFgJxNHHFKDIQ', '', 1, 1563063977, NULL, NULL),
(1244, 10002, 7, 'sbXp9LyJKorplYL9WDprxo4PijzDhQOH', '', 1, 1563063977, NULL, NULL),
(1245, 10002, 7, 'bGIsyznxUwuaG3qYyAk5kbXllGYfuAya', '', 1, 1563063977, NULL, NULL),
(1246, 10002, 7, 'gfJkmBN7VFM3G6o3QZVbb5xzXKfjf3Ti', '', 1, 1563063977, NULL, NULL),
(1247, 10002, 7, '4WzuNWQehnXndE8pfTP9KVFeHLKsXAm5', '', 1, 1563063977, NULL, NULL),
(1248, 10002, 7, '1E2H7SEQpCwiYgQJR4PsVlnrDEHeKNIg', '', 1, 1563063977, NULL, NULL),
(1249, 10002, 7, 'nOlrmi2t3wSjAa2FWIm7w700cErMFb9h', '', 1, 1563063977, NULL, NULL),
(1250, 10002, 7, '1DkmE1HbJ9dXtkiBqNQGLpHG4pzvJaeu', '', 1, 1563063977, NULL, NULL),
(1251, 10002, 7, 'NbriVJ5Os2TgbqdoW5YVz3F1RIEjYGTF', '', 1, 1563063977, NULL, NULL),
(1252, 10002, 7, 'lT3RPn1rvnnbRq7qeuvRIrOXjGy5z31g', '', 1, 1563063977, NULL, NULL),
(1253, 10002, 7, '45Q3YT8IFkDvU6lNe6DQyRQOpkPxwSBu', '', 1, 1563063977, NULL, NULL),
(1254, 10002, 7, '81iFjC7Xq5FY8A7WwqFJUFxgPN78eng6', '', 1, 1563063977, NULL, NULL),
(1255, 10002, 7, 'QlxJgEMSla5xlc2SXIp8HiaTFnmzWYr4', '', 1, 1563063977, NULL, NULL),
(1256, 10002, 7, 'Np3EHd9blolVhdk1S4pjB6fgZaXJj0Ev', '', 1, 1563063977, NULL, NULL),
(1257, 10002, 7, '4pBeq6zOJ7wOGtElTGCtjrRFIdbVxOSQ', '', 1, 1563063977, NULL, NULL),
(1258, 10002, 7, 'lyn78yC58493cJSFkKjfBTqp48r5FfXn', '', 1, 1563063977, NULL, NULL),
(1259, 10002, 7, 'Fxyr6dOKrGj9aYqu6YftHjiaBhrFZJnS', '', 1, 1563063977, NULL, NULL),
(1260, 10002, 7, 'TKAkHGOxv7NRuzTfNBi9TFultvSTGali', '', 1, 1563063977, NULL, NULL),
(1261, 10002, 7, 'znnMFs8pVYHYUGlFSw8WPzgu1ZtCIG1Q', '', 1, 1563063977, NULL, NULL),
(1262, 10002, 7, '5bDt35dFFwSrMjY2l3I4mciW4tSwaCxW', '', 1, 1563063977, NULL, NULL),
(1263, 10002, 7, 'MtiXHfrGz5wDEmCSw1aSdvLyJeF6uHnm', '', 1, 1563063977, NULL, NULL),
(1264, 10002, 7, 'NsMat0zZr3yhekht5NOIhkCElrocaN2g', '', 1, 1563063977, NULL, NULL),
(1265, 10002, 7, 'Un43LPjwy79Wp9d5gB2amW195wAaFADp', '', 1, 1563063977, NULL, NULL),
(1266, 10002, 7, 'q6Kdi0txAHM79hRcVVFwebN51DUcDlcx', '', 1, 1563063977, NULL, NULL),
(1267, 10002, 7, 'MX95Klrz8WSVVaEoiT17F5U8JV6C0EY9', '', 1, 1563063977, NULL, NULL),
(1268, 10002, 7, 'dCZ8hj9HSjQyPGRUEe8BkAPfIb3cga63', '', 1, 1563063977, NULL, NULL),
(1269, 10002, 7, 'lYA3pvqTT6U9N5F8rBPn5IUSfBY2vpd1', '', 1, 1563063977, NULL, NULL),
(1270, 10002, 7, 'fvYQGOwQmx8xuWIc3hn7chXAKFcdi4VV', '', 1, 1563063977, NULL, NULL),
(1271, 10002, 7, 'Lztd3F4zjSQQfnYLjVQ22JOmmI3qtRcd', '', 1, 1563063977, NULL, NULL),
(1272, 10002, 7, '421VfiEc86a6oLnGLkvwzIqXXPy6Foq7', '', 1, 1563063977, NULL, NULL),
(1273, 10002, 7, 's68RifkcqGA2lSTl9AqZfY4a1szSjObA', '', 1, 1563063977, NULL, NULL),
(1274, 10002, 7, 'fFVkgm4u3ve6xT3mBFHAzWA0QaKH7EGd', '', 1, 1563063977, NULL, NULL),
(1275, 10002, 7, 'dwqdhrF3LE4YidobpSU9Wy3QCPvJOBqK', '', 1, 1563063977, NULL, NULL),
(1276, 10002, 7, 'ZnKlyIQpDhTeX5ZgJ4lFDEVDBM91MqBh', '', 1, 1563063977, NULL, NULL),
(1277, 10002, 7, 'yI1N6J3b4If4UNlzRamVR91R3LQ6Sz1J', '', 1, 1563063977, NULL, NULL),
(1278, 10002, 7, 'VBASk38IJVnfzuLbS8546zc528KMEsPP', '', 1, 1563063977, NULL, NULL),
(1279, 10002, 7, '3M8K1rKFScOMQwaJRI2TTOONfPvCBi0C', '', 1, 1563063977, NULL, NULL),
(1280, 10002, 7, 'M37zQUdU2tfhMSXGd57k2CotMugRD1D0', '', 1, 1563063977, NULL, NULL),
(1281, 10002, 7, 'WO9sOnalJnocAAijfVy6UaCp9WRRpsvI', '', 1, 1563063977, NULL, NULL),
(1282, 10002, 7, 'eIuMcLr248t3mjk181sMh1wrpBOPdRqx', '', 1, 1563063977, NULL, NULL),
(1283, 10002, 7, 'v15MDSoHGgfvkwnMhXQ3aZh0bybgRI11', '', 1, 1563063977, NULL, NULL),
(1284, 10002, 7, 'aTX2k6JwtEEFJev4cvYayNIFDrRK5SqE', '', 1, 1563063977, NULL, NULL),
(1285, 10002, 7, 'JvfDjWoWmPhiqWlDkwVMMTtM45HNLMMm', '', 1, 1563063977, NULL, NULL),
(1286, 10002, 7, 'Oa1d4TvgLFRVkbQaEHdp4OUttstpZLYT', '', 1, 1563063977, NULL, NULL),
(1287, 10002, 7, 'KMd3oKb7u1f8CpHkl4CLH1LUxxdahJt8', '', 1, 1563063977, NULL, NULL),
(1288, 10002, 7, 'hfPW7HKUGFxJy5HGd5ch82YlcAF51pHv', '', 1, 1563063977, NULL, NULL),
(1289, 10002, 7, 'MHbuNXC382oTT3jV6blG1xW4NqJZW8oB', '', 1, 1563063977, NULL, NULL),
(1290, 10002, 7, 'CbdBI1ww4ypay6HpyCUxIe14mY99XtT6', '', 1, 1563063977, NULL, NULL),
(1291, 10002, 7, 'CIH0IE9y3CsSeTlm4le4F35gmSbExvsg', '', 1, 1563063977, NULL, NULL),
(1292, 10002, 7, 's9vgd4tz6RRvVKi320nxqMrHl3aAhThN', '', 1, 1563063977, NULL, NULL),
(1293, 10002, 7, 'QAB8X25qDWu8D8C3nLUNOqGKeTMr69Ds', '', 1, 1563063977, NULL, NULL),
(1294, 10002, 7, '3WwMQmP4HvaHIrhbIyCaTLV1ivC19xYz', '', 1, 1563063977, NULL, NULL),
(1295, 10002, 7, 'xwDdHgRp2LWXR9Lz8KZ1aSRNZdJh08Yx', '', 1, 1563063977, NULL, NULL),
(1296, 10002, 7, '1LpANWQPxPcfdAGmdLngKk1X5OuAhrAA', '', 1, 1563063977, NULL, NULL),
(1297, 10002, 7, 'ffhsV0YE5FtC6WK83zrtHchk1rEegaxf', '', 1, 1563063977, NULL, NULL),
(1298, 10002, 7, 'RNFIm3WPz4m8vKwWVN7jtbq9SL3wWPCE', '', 1, 1563063977, NULL, NULL),
(1299, 10002, 7, 'v6bun6yal34Xy4cBtTKPihDeRwtu3Q2X', '', 1, 1563063977, NULL, NULL),
(1300, 10002, 7, '6WlI8E4fvh9cvAecBtYdOPyFp0gP8yJJ', '', 1, 1563063977, NULL, NULL),
(1301, 10002, 7, 'W9YvTGywKmJtljxooHbasvgez5Zwq7VU', '', 1, 1563063977, NULL, NULL),
(1302, 10002, 7, 'wrodmTBpBabNVxIEp1zOmvFOJAtt3O6D', '', 1, 1563063977, NULL, NULL),
(1303, 10002, 7, '4p594WlPp0pQUvxC9hvqOvUsOgGfrAko', '', 1, 1563063977, NULL, NULL),
(1304, 10002, 7, 'pbmU8ctvqgVknjDpIYFsWHahnVWpRdeC', '', 1, 1563063977, NULL, NULL),
(1305, 10002, 7, 'BKg68hfUmE3AYxqMeRh0hdp1UpbFNydn', '', 1, 1563063977, NULL, NULL),
(1306, 10002, 7, 'PHbe6QmjI9CzP6BjEAYYqdaroLaY2Nnp', '', 1, 1563063977, NULL, NULL),
(1307, 10002, 7, 'sUi4lHYXn3dk6Fzg6PvcUTiXKAo8B3Gz', '', 1, 1563063977, NULL, NULL),
(1308, 10002, 7, '1Z4559EkrhDycNYQkOYoRMk5s3llduMm', '', 1, 1563063977, NULL, NULL),
(1309, 10002, 7, 'MKSBckPmRtmulgTRGombVR68YAa57LIx', '', 1, 1563063977, NULL, NULL),
(1310, 10002, 7, 'hEdiLcsBJL5ddtsvBjohfRWuKsz5utWp', '', 1, 1563063977, NULL, NULL),
(1311, 10002, 7, 'uspmue0KjGtcAIWtfg0Bf1XyDqA2c2UR', '', 1, 1563063977, NULL, NULL),
(1312, 10002, 7, 'wLK3EPeEGd6U1nIq72tpDBeW2xnn29Et', '', 1, 1563063977, NULL, NULL),
(1313, 10002, 7, 'RWAK3RgMpJkiU8UogY2tRIHCws4CACga', '', 1, 1563063977, NULL, NULL),
(1314, 10002, 7, '4n5fQ5juruYNG1ztMKQ2OSoW5F23ez24', '', 1, 1563063977, NULL, NULL),
(1315, 10002, 7, 'zyj6Wzi656xRHVXACU6jeTkuvlsDDxtr', '', 1, 1563063977, NULL, NULL),
(1316, 10002, 7, 'Xg2fwdvPkDrFTaOoeLxHAMQfGJbofKBt', '', 1, 1563063977, NULL, NULL),
(1317, 10002, 7, 'vz9zJLm2ljdPqgZt1C88VgVX56HYjhwV', '', 1, 1563063977, NULL, NULL),
(1318, 10002, 7, 'SigXRNpwXwUXKcwrJVpaVsAS2lXnTCDp', '', 1, 1563063977, NULL, NULL),
(1319, 10002, 7, 'TnzRXes8D7YRlWNn95iuPKkXA5rHRsRV', '', 1, 1563063977, NULL, NULL),
(1320, 10002, 7, '7EXu1uY8REDtaWlBHmiPYNntI5goX5Mj', '', 1, 1563063977, NULL, NULL),
(1321, 10002, 7, 'EcJfEHsmRW9y8aK9i7augE51MYBwMtve', '', 1, 1563063977, NULL, NULL),
(1322, 10002, 7, 'Rf5Ds9UYFA4D3IpT4QIg6KvdrwoUUNbE', '', 1, 1563063977, NULL, NULL),
(1323, 10002, 7, 'mAnw7lTuvgRaYSdlvjTU2KEUiGHVFysb', '', 1, 1563063977, NULL, NULL),
(1324, 10002, 7, 'QKJEFcUzuGME2gCcJBKSlwBA8llJcsL5', '', 1, 1563063977, NULL, NULL),
(1325, 10002, 7, 'cFbplnl78xKeOMRSCpg6KrCGNVS3AmHv', '', 1, 1563063977, NULL, NULL);
INSERT INTO `goods_card` (`id`, `user_id`, `goods_id`, `number`, `secret`, `status`, `create_at`, `delete_at`, `sell_time`) VALUES
(1326, 10002, 7, 'yuXt8sYVRGae9pWsWTT0OgZaDtOTOmBO', '', 1, 1563063977, NULL, NULL),
(1327, 10002, 7, 'wsthEW4Tzx2Y5iYHRlVboRgLm3cXRcsf', '', 1, 1563063977, NULL, NULL),
(1328, 10002, 7, 'oHzKM1IwMJIr8hakBsG22CaJkEvIZ2R6', '', 1, 1563063977, NULL, NULL),
(1329, 10002, 7, 'CCR1ZfCCPqqLvQflUwhC7Pzl9C8F0UwM', '', 1, 1563063977, NULL, NULL),
(1330, 10002, 7, 'mhsR3a8UR8xXZSeeoMfbgdMlbCG22qjR', '', 1, 1563063977, NULL, NULL),
(1331, 10002, 7, 'YD2Yq5WetxHBGkPhzm35wzOvIVCOqEZN', '', 1, 1563063977, NULL, NULL),
(1332, 10002, 7, '0Zv7cLgdxmbWudYX6cksNL9CFplpHtcR', '', 1, 1563063977, NULL, NULL),
(1333, 10002, 7, 'xnMQJlNlgf3CpjTIcfryhCgg5V23IFEm', '', 1, 1563063977, NULL, NULL),
(1334, 10002, 7, 'lUEd8oEiJB8XCB0rAY78Sr7rLmSJYso5', '', 1, 1563063977, NULL, NULL),
(1335, 10002, 7, 'j70928SLszDvOD3IkB7KC7K155Qqd5gC', '', 1, 1563063977, NULL, NULL),
(1336, 10002, 7, 'DENVQgCozGoggl99NBF6F4cuhuY79Cd1', '', 1, 1563063977, NULL, NULL),
(1337, 10002, 7, 'k4BAHEB1lQJAXYHr1iTE0cmT5Q7Ag6ym', '', 1, 1563063977, NULL, NULL),
(1338, 10002, 7, 'hf5RQ941csud6UD3OB8vZ67HLVlOOzOw', '', 1, 1563063977, NULL, NULL),
(1339, 10002, 7, 'loQ4MVMiLdRHixgOTggGT2dNUEUh3kOy', '', 1, 1563063977, NULL, NULL),
(1340, 10002, 7, 'pgbJAlJbQHtcdIjktfNWddFmtfz1wx5T', '', 1, 1563063977, NULL, NULL),
(1341, 10002, 7, 'M6p1DUuhawTWHnwBPDCO6LVTVjxmJLEa', '', 1, 1563063977, NULL, NULL),
(1342, 10002, 7, 'am1j58lt7w327DeEuLCqfAOVfVVxjebK', '', 1, 1563063977, NULL, NULL),
(1343, 10002, 7, 'ZPSbU6UNjQj9vyHfObiOJJJuazTi9XGz', '', 1, 1563063977, NULL, NULL),
(1344, 10002, 7, 'H1MDWC4wLVhdqEWtrDd6KcD6xhjwU5L6', '', 1, 1563063977, NULL, NULL),
(1345, 10002, 7, '3D2BDgLd47TzNqrzhMwDSl5YUzHEIEAW', '', 1, 1563063977, NULL, NULL),
(1346, 10002, 7, 'sgVKTmiwgjHYDsGV9LctFeVuNRpbUScJ', '', 1, 1563063977, NULL, NULL),
(1347, 10002, 7, '8A9lSfoDerEjDtU4Cfp8VMdfDKRoWuj7', '', 1, 1563063977, NULL, NULL),
(1348, 10002, 7, 'd08QlqDDJM3mHrR8HOHa4rGi6bcF0anj', '', 1, 1563063977, NULL, NULL),
(1349, 10002, 7, 'cKdkUl5RnPXfvkECFnPhM61B8QGLNQWI', '', 1, 1563063977, NULL, NULL),
(1350, 10002, 7, 'u6taJXHERfrsiftMSgurtcD3qymYIogO', '', 1, 1563063977, NULL, NULL),
(1351, 10002, 7, 'f5njdXJLhLuvllVywiCExscxjnkLuxnq', '', 1, 1563063977, NULL, NULL),
(1352, 10002, 7, 'RvSPkkYGnoxbiJQAXYaT9cRLrJXo3VLp', '', 1, 1563063977, NULL, NULL),
(1353, 10002, 7, 'oPFFWOwGFN15pt531cwCQn5b0N2bhpyQ', '', 1, 1563063977, NULL, NULL),
(1354, 10002, 7, 'eDjAbodfo3yx5xQw6VdnPxY1YlK70YEO', '', 1, 1563063977, NULL, NULL),
(1355, 10002, 7, 'xWmoOqceWLPgoEGHYmf41SOShm9QlUHC', '', 1, 1563063977, NULL, NULL),
(1356, 10002, 7, '2WdqpnQPmCk5jXQyzvuMvk1NBhAmVaa2', '', 1, 1563063977, NULL, NULL),
(1357, 10002, 7, 'NnEUM5RvwNEd113MBVobmdEECXJQOzaR', '', 1, 1563063977, NULL, NULL),
(1358, 10002, 7, 'gQ1m2oDl7rsY9kvHLsqomVxEwCsmuKa1', '', 1, 1563063977, NULL, NULL),
(1359, 10002, 7, 'paL7inLVOlorP4bNBTvu1J9x0AEGEaKu', '', 1, 1563063977, NULL, NULL),
(1360, 10002, 7, 'f9IiHN4Br2dIzhls14oMcvTKjgDdujjY', '', 1, 1563063977, NULL, NULL),
(1361, 10002, 7, 'E861uadpltEbt8W6HyOrWiZA4Dshrxw9', '', 1, 1563063977, NULL, NULL),
(1362, 10002, 7, 'sUQ5r154Ome8RwCQNQw3c9VeIzpwFMuI', '', 1, 1563063977, NULL, NULL),
(1363, 10002, 7, 'R2qY6qceCPQuzccjl8yQF63JweSaeWrD', '', 1, 1563063977, NULL, NULL),
(1364, 10002, 7, 'eHccJ930tGQ1f31fOcppknP0UTgTgM97', '', 1, 1563063977, NULL, NULL),
(1365, 10002, 7, '9cfVVel49x6Tt7w9pA3T1UDxhwNbOhCG', '', 1, 1563063977, NULL, NULL),
(1366, 10002, 7, 'lnmMfPHzmursrAYbGQWOe1xoQd9KwatJ', '', 1, 1563063977, NULL, NULL),
(1367, 10002, 7, 'AKsvYdpU03YgQPyHSxa1WpsWVBcAbCo3', '', 1, 1563063977, NULL, NULL),
(1368, 10002, 7, 'mpOeiWNK0BDwnJOqNzxtxWmyPekxCKto', '', 1, 1563063977, NULL, NULL),
(1369, 10002, 7, 'JGWTasWocDU1CyWGW8iQ8K7GXgTOK1Wm', '', 1, 1563063977, NULL, NULL),
(1370, 10002, 7, '6kVdEqdsXskwDGSk7AvqrpfzHx5HFnyD', '', 1, 1563063977, NULL, NULL),
(1371, 10002, 7, 'nUKE2hNU9SwEPlBr67BqV6tRS844F6fp', '', 1, 1563063977, NULL, NULL),
(1372, 10002, 7, 'fdUySLEH2lFDFuAqQO0tLfy9DtVVCdmV', '', 1, 1563063977, NULL, NULL),
(1373, 10002, 7, 'k0cWO9XCcD6zRyRzl8WcbN3ahPiV6jpR', '', 1, 1563063977, NULL, NULL),
(1374, 10002, 7, 'bry81RQsLKlGGRaGXENbyRxyZxjqsTbN', '', 1, 1563063977, NULL, NULL),
(1375, 10002, 7, 'eayEGUhD3SeI6lBlyKQLtOqIeUg59xmK', '', 1, 1563063977, NULL, NULL),
(1376, 10002, 7, '2I4A23dFI6KS0ETB01G4my0dQQsDXpKN', '', 1, 1563063977, NULL, NULL),
(1377, 10002, 7, 'R4JVWQ5By93UN9Ilw3FPXeIz8iWXMtta', '', 1, 1563063977, NULL, NULL),
(1378, 10002, 7, 'FRMSqNMhcQ38X7oDmcVFyxzMiOFMoEU7', '', 1, 1563063977, NULL, NULL),
(1379, 10002, 7, 'cKURO9Y2nWLIbi8DVT21zdWJohPXHn3k', '', 1, 1563063977, NULL, NULL),
(1380, 10002, 7, 'tWjX5r9df68x9rfv6DSxQJcgkCw7gu1E', '', 1, 1563063977, NULL, NULL),
(1381, 10002, 7, 'aI7opCKezNMGUs6iTWPd2MKs9SZV3rV9', '', 1, 1563063977, NULL, NULL),
(1382, 10002, 7, 'GKvoNLS6H3pgoW7oToFK6z8lPELx3KVj', '', 1, 1563063977, NULL, NULL),
(1383, 10002, 7, 'BJsDNMaIh41FxhCKkKBiIolaseskXusb', '', 1, 1563063977, NULL, NULL),
(1384, 10002, 7, 'cF5pYkIVRqhYbhFcjgqHfZx5bEFiyzb6', '', 1, 1563063977, NULL, NULL),
(1385, 10002, 7, '439PveXNoX1jDBC1atrEt96wT1t6xMVD', '', 1, 1563063977, NULL, NULL),
(1386, 10002, 7, 'iDaBaH1eA8CekltuT33o5kxcL5GItKxm', '', 1, 1563063977, NULL, NULL),
(1387, 10002, 7, 'iiBi2evv45lNwpCJ9FZvBSE7FKG2c0bP', '', 1, 1563063977, NULL, NULL),
(1388, 10002, 7, 'IjB2FtDfvXniRhimfCcKBaNI2WDfzqMg', '', 1, 1563063977, NULL, NULL),
(1389, 10002, 7, 'f42oLPpgsCzHJtWgxkpzjyj8sZP6HA3O', '', 1, 1563063977, NULL, NULL),
(1390, 10002, 7, 'Oam0qS2xdn3BSnDlnnKPDEeslKkR8WCn', '', 1, 1563063977, NULL, NULL),
(1391, 10002, 7, '6T1JorFf6YUTvCdsXh2VnKyqXsspgHo5', '', 1, 1563063977, NULL, NULL),
(1392, 10002, 7, 'GBh5Wjoad8GpkDfoWC6dyDPRL4RAQGni', '', 1, 1563063977, NULL, NULL),
(1393, 10002, 7, 'UAH430BhtM2GliSAIPXz2UBU1B26XEBL', '', 1, 1563063977, NULL, NULL),
(1394, 10002, 7, 'NUmU2OsoemNTLiql0TAXWN4hAuJvB5K3', '', 1, 1563063977, NULL, NULL),
(1395, 10002, 7, '4hJ95URYhhPyHkaZVP7Hua1s2fdi3qqS', '', 1, 1563063977, NULL, NULL),
(1396, 10002, 7, 'nllkdhf8AIt42oncm65s2YV4jHsYa5ZU', '', 1, 1563063977, NULL, NULL),
(1397, 10002, 7, '8sEQKG3FxW2DdL21EOOZmCSd33jlAH09', '', 1, 1563063977, NULL, NULL),
(1398, 10002, 7, '7vIgSdWpCVeMjbHP6fOq72dgts1o81Dq', '', 1, 1563063977, NULL, NULL),
(1399, 10002, 7, 'ncudOhQEuD1fq5idbPL0mJ83c4oiFNXu', '', 1, 1563063977, NULL, NULL),
(1400, 10002, 7, 'QsLdSXcg6i9qcGlUBRK3yGmzRk1cSH5i', '', 1, 1563063977, NULL, NULL),
(1401, 10002, 7, 'cH9xZkEG6aKoREciwJXK4Lt0g2TLMwWM', '', 1, 1563063977, NULL, NULL),
(1402, 10002, 7, '3GHYgxLOfUmrhu4tsFAFylmbZFM7142t', '', 1, 1563063977, NULL, NULL),
(1403, 10002, 7, 'GGTYIydqwhurlxlJp2uzcpJ6EDagDm7g', '', 1, 1563063977, NULL, NULL),
(1404, 10002, 7, 'iXzcoJplknArdWDcRYAv2jipn77bWw3g', '', 1, 1563063977, NULL, NULL),
(1405, 10002, 7, 'gm5Qg9t7YoETurNHCpRTMCDeDaVSPnmN', '', 1, 1563063977, NULL, NULL),
(1406, 10002, 7, 'wT6mSdv23hrzyyOwVeHQ4DIbS9xd60RF', '', 1, 1563063977, NULL, NULL),
(1407, 10002, 7, 'NX3cm1WrjSjuPG1EGQpNihFeJGCcSSNd', '', 1, 1563063977, NULL, NULL),
(1408, 10002, 7, 'DiEogwO0JAmV4FbbKHPWK4fIFHCUVgjp', '', 1, 1563063977, NULL, NULL),
(1409, 10002, 7, 'Cm3hTuMD5ofppV3lm1iHvNaWAyl53sB9', '', 1, 1563063977, NULL, NULL),
(1410, 10002, 7, 'pitWe3TTMK9H2thB3oDEOAEk1zshCigZ', '', 1, 1563063977, NULL, NULL),
(1411, 10002, 7, 'jFE17LfmPTkL5E6RvgiKnx8DcHokEwES', '', 1, 1563063977, NULL, NULL),
(1412, 10002, 7, 'UFXvEOPW1vAQIXeV6zRyhuENzyx156U2', '', 1, 1563063977, NULL, NULL),
(1413, 10002, 7, 'W57TGOVYYzfpVr6jV9JhqzS41WuMktlz', '', 1, 1563063977, NULL, NULL),
(1414, 10002, 7, 'A7igbECk8S7NYEzHvpy17WA5dnsFnwl7', '', 1, 1563063977, NULL, NULL),
(1415, 10002, 7, 'nkv1EBucOIwZUPrPvUcwphNVMzpeGhm6', '', 1, 1563063977, NULL, NULL),
(1416, 10002, 7, '4eB8EVkFzY888Jd8HnqT1fxcdEDA3VBa', '', 1, 1563063977, NULL, NULL),
(1417, 10002, 7, 'MvgI9PcbtAHgeuvMyu6nI5m1oTv5hYMv', '', 1, 1563063977, NULL, NULL),
(1418, 10002, 7, 'Ii4Xy8k88xZUHvjIl3Czi3WzIfRnKEvW', '', 1, 1563063977, NULL, NULL),
(1419, 10002, 7, 'ig6xGLhhqFWMsBDR3kJUQgDBir8nCB6f', '', 1, 1563063977, NULL, NULL),
(1420, 10002, 7, 'EXqAVowBUCvevwxco8MfaJL6SUetdHnV', '', 1, 1563063977, NULL, NULL),
(1421, 10002, 7, 'Z6DKQCbZctXIn1YWWLxO5AriB1RZG2xR', '', 1, 1563063977, NULL, NULL),
(1422, 10002, 7, 'KrBf3fgA522FFXZi28oUU5YgIMVrOrxR', '', 1, 1563063977, NULL, NULL),
(1423, 10002, 7, 'io2ehEvU5VT6YqPnesqBV2rVCPdXClKM', '', 1, 1563063977, NULL, NULL),
(1424, 10002, 7, 'ARhwEDK5M5yUkIhPhuSfYTCeyUqgC3HJ', '', 1, 1563063977, NULL, NULL),
(1425, 10002, 7, 'pdpjOXpPpQ17Qz0ja5KwXsg6XyAVVHbg', '', 1, 1563063977, NULL, NULL),
(1426, 10002, 7, 'rktotzYVLI6AOjiQLatIxmb76AHPP49q', '', 1, 1563063977, NULL, NULL),
(1427, 10002, 7, 'R3CpXvjlSHGGStUKgWDskdsRVCmQMaAE', '', 1, 1563063977, NULL, NULL),
(1428, 10002, 7, 'zn8JTFDdrGhCsCATtphBoEJHCCzILPfO', '', 1, 1563063977, NULL, NULL),
(1429, 10002, 7, '2f7fD7OerljnDc1luTOVwwkqU9DuLXMb', '', 1, 1563063977, NULL, NULL),
(1430, 10002, 7, 'BLYhqCYR3iIo7mzOgJcLCw9vI5vycXgz', '', 1, 1563063977, NULL, NULL),
(1431, 10002, 7, 'lYJs0XDdaPD9kxP4duCEaBXrLa7z3SCR', '', 1, 1563063977, NULL, NULL),
(1432, 10002, 7, '6OXxfml1xbJwcxsEMK1RG77LcR7u9L3U', '', 1, 1563063977, NULL, NULL),
(1433, 10002, 7, 'sMIOLa2xqPyUfhfXJScFOrTGP0zNXbEn', '', 1, 1563063977, NULL, NULL),
(1434, 10002, 7, '6O5kQvXvX8bwQFfYEuC7g7yLSi4W7QWw', '', 1, 1563063977, NULL, NULL),
(1435, 10002, 7, 'Dcd1hxBfkd9kqDPDHUGfM5B67TjyRSbO', '', 1, 1563063977, NULL, NULL),
(1436, 10002, 7, 'Ao6g2zOkBZnZ5QWJIBpgunb1xJvr8RAa', '', 1, 1563063977, NULL, NULL),
(1437, 10002, 7, '696uPSI5BRBVhrXypqTpIMzjSkgLAMMw', '', 1, 1563063977, NULL, NULL),
(1438, 10002, 7, 'p3sY7Q7L4K5NQxlOk1S74pEzareSfTA9', '', 1, 1563063977, NULL, NULL),
(1439, 10002, 7, 'A66IFnxGv9XUef87DWbIQeq9zNJQeHAM', '', 1, 1563063977, NULL, NULL),
(1440, 10002, 7, '8s4z6dGkG9XpYEi81vGEqlyqs2Du2hy8', '', 1, 1563063977, NULL, NULL),
(1441, 10002, 7, 'OzAUyQV4mXmMyFlIqb7cdkU3e13SIspT', '', 1, 1563063977, NULL, NULL),
(1442, 10002, 7, 'WQ0c3VPCRLKLsrAIQKEU3EOHzMGyW42G', '', 1, 1563063977, NULL, NULL),
(1443, 10002, 7, 'FEHYnOsdDejdeNAH07s1xCd1hAXr8VM3', '', 1, 1563063977, NULL, NULL),
(1444, 10002, 7, 'eVvTeCltprNS8y0kwqAqsi9xclkuLXTH', '', 1, 1563063977, NULL, NULL),
(1445, 10002, 7, 'aYHIXUqkoM3V3IyNk7Gisyjg4x9eLyuZ', '', 1, 1563063977, NULL, NULL),
(1446, 10002, 7, 'BGpB35BxDgHCCJGzUVED2sdB2vKXJqOc', '', 1, 1563063977, NULL, NULL),
(1447, 10002, 7, 'xEUVJJdBImEHACrrWnyGZ5el5UshWMgU', '', 1, 1563063977, NULL, NULL),
(1448, 10002, 7, 'PAELvErv5nnFFGNfwItyXTeBzR5fl3Xr', '', 1, 1563063977, NULL, NULL),
(1449, 10002, 7, 'mwRCF38TWkwJVCE5IbOTGdhfrPPAvuLe', '', 1, 1563063977, NULL, NULL),
(1450, 10002, 7, 'EFKVzhTq7ssCFjoZLDX1t3qfvAr3c4QY', '', 1, 1563063977, NULL, NULL),
(1451, 10002, 7, 'AABc1H935onQgIUfVtzol2ptiQKhqdcX', '', 1, 1563063977, NULL, NULL),
(1452, 10002, 7, 'LRPvDGdOO8Ae3xjCnDorS9KcuuwtsfEV', '', 1, 1563063977, NULL, NULL),
(1453, 10002, 7, 'gpzx007fsI4G40IG7FrYNees2qtLtnkS', '', 1, 1563063977, NULL, NULL),
(1454, 10002, 7, '4uDLnNY6Uas5Rf1gyVu0x9am45BEswOK', '', 1, 1563063977, NULL, NULL),
(1455, 10002, 7, '8pmXD6ulAwhGFqWpGp6yMSt65IPTJqyn', '', 1, 1563063977, NULL, NULL),
(1456, 10002, 7, 'fH3YtoOFLK00jcbpUGxfT8ysG24jXkD9', '', 1, 1563063977, NULL, NULL),
(1457, 10002, 7, 'a7YzfVgs1NJGATAYIyVwUL1e1HRZmA5J', '', 1, 1563063977, NULL, NULL),
(1458, 10002, 7, 'LATLXcvodm2xBAqHunawd3QBrCbeF3BU', '', 1, 1563063977, NULL, NULL),
(1459, 10002, 7, 'U39VQQTzkcX6XUn76dJFP9pgGUhi4Htm', '', 1, 1563063977, NULL, NULL),
(1460, 10002, 7, 'Sm6s6XFqEbHVvxjV81MSbBGS36cT4ZgP', '', 1, 1563063977, NULL, NULL),
(1461, 10002, 7, 'PmAREieSktPMNq82YHzto19IPbP7a4Wg', '', 1, 1563063977, NULL, NULL),
(1462, 10002, 7, 'Kv7uHpFOyUVqZ20kow8VOA6uLpQbDXU3', '', 1, 1563063977, NULL, NULL),
(1463, 10002, 7, 'x6g3NicIMYWBVmna7vVAiQUEGqg6bQIL', '', 1, 1563063977, NULL, NULL),
(1464, 10002, 7, '29IPXKMPm5DC1jBlRm4k2IU2xyTwrTXG', '', 1, 1563063977, NULL, NULL),
(1465, 10002, 7, 'hyQvpU38is7MVmOvlmJjOq1d3bXJOhpl', '', 1, 1563063977, NULL, NULL),
(1466, 10002, 7, 'RbhMVSczB49YZ28c3D5Xx2A8ao5oWano', '', 1, 1563063977, NULL, NULL),
(1467, 10002, 7, '5owa1kUmamDgMIEvGuLEFZqPA6F3q5rW', '', 1, 1563063977, NULL, NULL),
(1468, 10002, 7, 'n4BBbBpYiDYU5awPjswBYhzBn1fnuPY5', '', 1, 1563063977, NULL, NULL),
(1469, 10002, 7, 'cYshuCAISYnNlu3xOibkCKfFgx9DkiTG', '', 1, 1563063977, NULL, NULL),
(1470, 10002, 7, 'NxZmGzTM9X7GnbS1CBnjR5ILGcg6lyW2', '', 1, 1563063977, NULL, NULL),
(1471, 10002, 7, 'aLVaXh2Xd1WEaE057V2AnO2ZLLjWFp0k', '', 1, 1563063977, NULL, NULL),
(1472, 10002, 7, '42NKHSCB8o6OjYSual3DKLtm7SdLI0gI', '', 1, 1563063977, NULL, NULL),
(1473, 10002, 7, 'wGnBN4j8IrWOhRvcq6Inl9BtnoRQEr5x', '', 1, 1563063977, NULL, NULL),
(1474, 10002, 7, 'FHMHUtbEIC4RPLEOMNc4c52iHGIxL7E6', '', 1, 1563063977, NULL, NULL),
(1475, 10002, 7, 'AgedAvPnEWnVzKx5V83WGqX8qhWBzyj4', '', 1, 1563063977, NULL, NULL),
(1476, 10002, 7, 'hnC4w51WH4lJ2hcy2GM7sBURsql1bglw', '', 1, 1563063977, NULL, NULL),
(1477, 10002, 7, 'hdnAULGlx7FTrzfFyoj6WohXLRK1wpIP', '', 1, 1563063977, NULL, NULL),
(1478, 10002, 7, 'J5GAsx2HLJ5KC1TavVaKpUc4kmazVurd', '', 1, 1563063977, NULL, NULL),
(1479, 10002, 7, 'exNUtoTlNxBt019988hbIHbUVwIzC3a9', '', 1, 1563063977, NULL, NULL),
(1480, 10002, 7, 'fDM9xnrGYE9Yf4IaYhubEtwYtHR7Zv51', '', 1, 1563063977, NULL, NULL),
(1481, 10002, 7, 'NVF8VIPKQSyPrZA7pebzVBJSwNHnpBR0', '', 1, 1563063977, NULL, NULL),
(1482, 10002, 7, 'VrJ6FBBilrawSPyCVdf7KqSATTJYMmty', '', 1, 1563063977, NULL, NULL),
(1483, 10002, 7, 'kFRbfYJQFtmQ2vy2mbdtqU4MJv4J8nSh', '', 1, 1563063977, NULL, NULL),
(1484, 10002, 7, 'kC3egeOsBSNmauczNVeRXOYg4Y3r32Eu', '', 1, 1563063977, NULL, NULL),
(1485, 10002, 7, '9loQP29hbnXJDYbnmyjKd72jNlDHIK69', '', 1, 1563063977, NULL, NULL),
(1486, 10002, 7, 'rX4MKLpSIGoAHg2x4wbI3JaKYvUtibIA', '', 1, 1563063977, NULL, NULL),
(1487, 10002, 7, 'HX3bn3IoBXMSp3WUhyDab5hyONGsMEpL', '', 1, 1563063977, NULL, NULL),
(1488, 10002, 7, 'y7cdolOXShHl39A164r4puhXV5hcitwo', '', 1, 1563063977, NULL, NULL),
(1489, 10002, 7, 'AsWuHNRKzHuwnAVzhSKGlPUQk6RrccNS', '', 1, 1563063977, NULL, NULL),
(1490, 10002, 7, 'lKEHerUuPLN6aOq0XjEvHkgTGTeNZsBk', '', 1, 1563063977, NULL, NULL),
(1491, 10002, 7, 'bgtf3It2ug9ijWBH9x27nqEgwXj4HH2Q', '', 1, 1563063977, NULL, NULL),
(1492, 10002, 7, '0eC2SFrknkW5ujAe4YoEK2nVI2kVkXlO', '', 1, 1563063977, NULL, NULL),
(1493, 10002, 7, 'llev5YpCruM5wXIkoaey2O6bYbvzH4GQ', '', 1, 1563063977, NULL, NULL),
(1494, 10002, 7, 'b8oKCbUJNvzTvLReKPYYJT29i3Wvh15K', '', 1, 1563063977, NULL, NULL),
(1495, 10002, 7, 'fRceDdMBXLtjLjxVoAUrK1LVRpx3gvur', '', 1, 1563063977, NULL, NULL),
(1496, 10002, 7, '9BMB6rzY7uPqtWvfUZFxyDAfA4YIqME4', '', 1, 1563063977, NULL, NULL),
(1497, 10002, 7, 'xJH3qZKrlyIfp8TJjos42KGOKblOmPvy', '', 1, 1563063977, NULL, NULL),
(1498, 10002, 7, 'BUf3f1k6epS9sGUqYfkDsm7ggq3rob4n', '', 1, 1563063977, NULL, NULL),
(1499, 10002, 7, 'n45NfCV3OPTZjiOboaFH7tQUhGOcuQ8Y', '', 1, 1563063977, NULL, NULL),
(1500, 10002, 7, 'dqYgHJJuS4uSOmWBZNYCySXWaYvBIsrH', '', 1, 1563063977, NULL, NULL),
(1501, 10002, 7, '6SXlUiPVDc7mIbHertR4y9Umg8nQQbOx', '', 1, 1563063977, NULL, NULL),
(1502, 10002, 7, 'wXPFogwPV73JTl5AL4Lb7xdvm2LBaHbS', '', 1, 1563063977, NULL, NULL),
(1503, 10002, 7, '37POTiwyL035RQ7zl14Rpwc8om8S33HW', '', 1, 1563063977, NULL, NULL),
(1504, 10002, 7, 'DPbCS49n4ttMUaXdbUFTaegUVpjzSKcA', '', 1, 1563063977, NULL, NULL),
(1505, 10002, 7, 'gEpcgEadfkvd7f4Gf1wzGkcvyLBecDsw', '', 1, 1563063977, NULL, NULL),
(1506, 10002, 7, 'lXKLmIF4DPAatdGhTze4ydgaMbPtXL35', '', 1, 1563063977, NULL, NULL),
(1507, 10002, 7, '55fNj6CXCsIZlfi6WqLvOWkxAmLzHy6w', '', 1, 1563063977, NULL, NULL),
(1508, 10002, 7, 'GOauwW5yOapcW3y1iB3Mo44TBmyRYFbf', '', 1, 1563063977, NULL, NULL),
(1509, 10002, 7, 'yxF9p5EWTy61pNgIA9BaoARjKJx12Umf', '', 1, 1563063977, NULL, NULL),
(1510, 10002, 7, 'M7dA9kHUfC2tgie3X61BQTMfARo7E1yE', '', 1, 1563063977, NULL, NULL),
(1511, 10002, 7, 'hb44MGxfnS4JzWnVKpkmIQgFvrjTuiNn', '', 1, 1563063977, NULL, NULL),
(1512, 10002, 7, 'xuc0cLfn8NRYaP6Y2flhqpyOsF7NKo48', '', 1, 1563063977, NULL, NULL),
(1513, 10002, 7, 'LuNyBvSTFjNYhVxIiMfO1mKNZatXhOIe', '', 1, 1563063977, NULL, NULL),
(1514, 10002, 7, '6sQ9FJmg41jBEJI1e44H7VdR1uEiyBpl', '', 1, 1563063977, NULL, NULL),
(1515, 10002, 7, 'XL4ueNxC1dai2YIPNl1y7jgycwhsiqXb', '', 1, 1563063977, NULL, NULL),
(1516, 10002, 7, 'NDWApavIipEai8VcscEOX7goVTRtDwMw', '', 1, 1563063977, NULL, NULL),
(1517, 10002, 7, 'mojSTShE3sKWyFpfSia0JjgC1Ner7x5o', '', 1, 1563063977, NULL, NULL),
(1518, 10002, 7, 'VdLx4DubdS99NvxyVtBhZclnUNZOQmuF', '', 1, 1563063977, NULL, NULL),
(1519, 10002, 7, '1bKXLRKOz9gJ1rg2ApJ9g3VwzrzCNLfs', '', 1, 1563063977, NULL, NULL),
(1520, 10002, 7, '33gVolm1OXBsJKTxuUnL3z54JCufYmsY', '', 1, 1563063977, NULL, NULL),
(1521, 10002, 7, 'aMPcAKap6ibj7asqn8is5cdv6xRR8Te1', '', 1, 1563063977, NULL, NULL),
(1522, 10002, 7, 'Aqm7iXjMhwqUvL3PTSCN2PkesGCOqchF', '', 1, 1563063977, NULL, NULL),
(1523, 10002, 7, 'R37yxxsngZh38ldY1TDsMVT1xYbqPZHa', '', 1, 1563063977, NULL, NULL),
(1524, 10002, 7, 'LnmpHG83C6aFSkuTQ0mnlDtvdaf3WifJ', '', 1, 1563063977, NULL, NULL),
(1525, 10002, 7, 'Y22DMWDDZWQcCkiuR1cK6KRttM4K3wdn', '', 1, 1563063977, NULL, NULL),
(1526, 10002, 7, 'spSGUMAygtguQwUgedeuIKowy5WcPKDU', '', 1, 1563063977, NULL, NULL),
(1527, 10002, 7, '2n95gD2rBOX9f1sFAunbj3bVNsV83WmW', '', 1, 1563063977, NULL, NULL),
(1528, 10002, 7, 'fPT7mDLDxUJKPtXb9wEE7cjrd43B3ioQ', '', 1, 1563063977, NULL, NULL),
(1529, 10002, 7, 'oSg3F8oI5Ul6l3ldYucqr8NAe7MzoBxY', '', 1, 1563063977, NULL, NULL),
(1530, 10002, 7, 'Uw0SaAo5n9y4qaGGy0LlLBzIBWCV83cW', '', 1, 1563063977, NULL, NULL),
(1531, 10002, 7, 'NmuV68vZeVABiOCWENS7TedTVYXQCP3h', '', 1, 1563063977, NULL, NULL),
(1532, 10002, 7, 'Kn9TPXiMb9JFIlPHfNItUAVlXqWSFILa', '', 1, 1563063977, NULL, NULL),
(1533, 10002, 7, 'n2o8IOuP5MJBGbRJtD1xzPrf3w4A3X7H', '', 1, 1563063977, NULL, NULL),
(1534, 10002, 7, 'IkhV3RMc2MbulXCzjAtEEx21Cik1Ty9V', '', 1, 1563063977, NULL, NULL),
(1535, 10002, 7, 'RLMKiidOuqgQbIQvvYDSSqMMfQgqXKIK', '', 1, 1563063977, NULL, NULL),
(1536, 10002, 7, 'MYPE0QVj2JpL3CqEYPQdC5wFGknyyVBF', '', 1, 1563063977, NULL, NULL),
(1537, 10002, 7, 'hErOCJY5q8UlOX538Nn8OME8ykR3dI1r', '', 1, 1563063977, NULL, NULL),
(1538, 10002, 7, 'O2WJTq4xUxYbuLvgTqGqXwBAMvrXOuXm', '', 1, 1563063977, NULL, NULL),
(1539, 10002, 7, '66trKHR7Hlluhl50FfE4fJTcBraraDp2', '', 1, 1563063977, NULL, NULL),
(1540, 10002, 7, 'ZHOPMlYRY3vGbMaC1J5DTi1Gggh8seYI', '', 1, 1563063977, NULL, NULL),
(1541, 10002, 7, 'Erl0vtgrO8qMKSjK6SrTxvDZBnxGKz6T', '', 1, 1563063977, NULL, NULL),
(1542, 10002, 7, 'PAwIu3SDHjp88T2RGOxnzMlvJW6wfGUr', '', 1, 1563063977, NULL, NULL),
(1543, 10002, 7, 'CLMmWU8BBIEIVAdzzzThph3IjQfiwiCo', '', 1, 1563063977, NULL, NULL),
(1544, 10002, 7, 'CCEg6kLEynWped2559CTkOjd7fM95QNq', '', 1, 1563063977, NULL, NULL),
(1545, 10002, 7, 'CIoEo8vOoqjUwXQszmHwGihua3Q1I2yc', '', 1, 1563063977, NULL, NULL),
(1546, 10002, 7, 'OUWGaTGyjFDqPgYO95SQwhjnzoe2C5NV', '', 1, 1563063977, NULL, NULL),
(1547, 10002, 7, 'xxhk5hNegycEabRSy7e6r5Avzze3Ev1t', '', 1, 1563063977, NULL, NULL),
(1548, 10002, 7, 'tf7iUiyHe0SrvrzX1lVd9V4afRgecAho', '', 1, 1563063977, NULL, NULL),
(1549, 10002, 7, '8AmLmJvEqCEmPNYvkmJCDbB8BM4FeDOi', '', 1, 1563063977, NULL, NULL),
(1550, 10002, 7, '7nRqQ2C3Kxo2xzxcewvYC814AcxyHlsE', '', 1, 1563063977, NULL, NULL),
(1551, 10002, 7, 'rbiQeAjky2Eb84PQYevl8ysHleU8tom6', '', 1, 1563063977, NULL, NULL),
(1552, 10002, 7, '6RQUlCzXoL2bP6OQzt9QDkzVRPN1449b', '', 1, 1563063977, NULL, NULL),
(1553, 10002, 7, 'CL3KWauuhWaLd8hq8dDBn1KQsyEIRZff', '', 1, 1563063977, NULL, NULL),
(1554, 10002, 7, 'aRIchHSFjXwQcsLmbxPzEGAboMOTmgrH', '', 1, 1563063977, NULL, NULL),
(1555, 10002, 7, 'dBIJfUdR5AShbCpMe8FaDIbGCATAYWhf', '', 1, 1563063977, NULL, NULL),
(1556, 10002, 7, 'ifJTcKKOxMViqEMB4MjK9P6HUN5oRztE', '', 1, 1563063977, NULL, NULL),
(1557, 10002, 7, 'nQ2SNqnfpsSa4Mxn28XLMOtIKi3J4oHK', '', 1, 1563063977, NULL, NULL),
(1558, 10002, 7, '3kLMU87PiRM0VWpAxvG7ThMk8lMkyFt8', '', 1, 1563063977, NULL, NULL),
(1559, 10002, 7, 'SNjN1HlnNdJ1chr8JICntaYwJ8gdfnwi', '', 1, 1563063977, NULL, NULL),
(1560, 10002, 7, 'DC7UChvYgNzix9CXo5WlzB9XMNO8xmsU', '', 1, 1563063977, NULL, NULL),
(1561, 10002, 7, 'TO8BvDSO7SsMM3jUGvlJahDcAJq7c1Hu', '', 1, 1563063977, NULL, NULL),
(1562, 10002, 7, '1n1wsrLSn2U6vKC4TqQjHMAol8hFtYXq', '', 1, 1563063977, NULL, NULL),
(1563, 10002, 7, 'YptboNDgpxKAkhgVBVsto2gCr40PJ985', '', 1, 1563063977, NULL, NULL),
(1564, 10002, 7, 'XO4QAgKkNUJYkKwxDFzPXCEFRW0faXBN', '', 1, 1563063977, NULL, NULL),
(1565, 10002, 7, 'bsEuNuFWpt5anheWCeM9bx9w47OsPE8i', '', 1, 1563063977, NULL, NULL),
(1566, 10002, 7, 'BGh3uaCaj1L4dXdAN4QlLOP6uHsYKH7G', '', 1, 1563063977, NULL, NULL),
(1567, 10002, 7, 'Dye1v7wkgoTfJywlZoqUw4yVsudzLfI5', '', 1, 1563063977, NULL, NULL),
(1568, 10002, 7, 'RyvVlEEDnqAiYARdiKHA7uYnPe0l6lXj', '', 1, 1563063977, NULL, NULL),
(1569, 10002, 7, 'OXjbYsjwEOpq26d6r9XpqOzpNIrc3zeM', '', 1, 1563063977, NULL, NULL),
(1570, 10002, 7, 'hfaRfKowFOoLoCGw9SHRiGSEWXqY8seQ', '', 1, 1563063977, NULL, NULL),
(1571, 10002, 7, 'JoRCb8FgCTg8LV5hXhnzfNI2s9HSLqeU', '', 1, 1563063977, NULL, NULL),
(1572, 10002, 7, '6j3HfKHeilJxTqxvoLULYexxj1oNrAwL', '', 1, 1563063977, NULL, NULL),
(1573, 10002, 7, 'Ut1SEJGHKB2Qw3cEb7nxDphJpM5MYMSH', '', 1, 1563063977, NULL, NULL),
(1574, 10002, 7, 'hnQ93TPN2ezbKm3lt3dquxYAjAuDky4M', '', 1, 1563063977, NULL, NULL),
(1575, 10002, 7, 'QWAdAcwjs9ORNo0lm9cTkNJBBawptNve', '', 1, 1563063977, NULL, NULL),
(1576, 10002, 7, 'Y04xZ77G2sw3zLT7WSOSdwplBFuo1vbU', '', 1, 1563063977, NULL, NULL),
(1577, 10002, 7, '0nL098F8x7iYwAuglPuSiTgo5FjIq24V', '', 1, 1563063977, NULL, NULL),
(1578, 10002, 7, 'YkvEz8xaOVdPTiJdjwVo0xPLoHQaO6p0', '', 1, 1563063977, NULL, NULL),
(1579, 10002, 7, '5sB9tLaBikQxFmKS5s5eZlvhlA9EWhlj', '', 1, 1563063977, NULL, NULL),
(1580, 10002, 7, 'hPYIS8q8msQcabcTJdAQlMtYqTZZ7nFy', '', 1, 1563063977, NULL, NULL),
(1581, 10002, 7, 'kRnpdiEXYh9qBMlSd5BjxdttMZMBAMh6', '', 1, 1563063977, NULL, NULL),
(1582, 10002, 7, 'ZUrm07HH47BbB4BwvJXlEkGc693hvsnG', '', 1, 1563063977, NULL, NULL),
(1583, 10002, 7, 'iuphBy68qZrmEgUtPMXB5WeyonleMnEI', '', 1, 1563063977, NULL, NULL),
(1584, 10002, 7, 'KXz3VJX5Zley4UmpafUY5rUDppNsBlAj', '', 1, 1563063977, NULL, NULL),
(1585, 10002, 7, 'hFf4MrUtzSxgnKzDPwMS3QjoJmqoCbLj', '', 1, 1563063977, NULL, NULL),
(1586, 10002, 7, 'JuBppdTr3AJLqtYiyZOs5fwXPBVnKaKF', '', 1, 1563063977, NULL, NULL),
(1587, 10002, 7, 'T2w5kQs7eE4s3a1zzJBeL73CMAt4PTTv', '', 1, 1563063977, NULL, NULL),
(1588, 10002, 7, 'sRqYVWPaFAbtN8zDLEUjpEBBhfIftDy7', '', 1, 1563063977, NULL, NULL),
(1589, 10002, 7, 'vvTpS6hrUtiBWVODWEmMb1Stfj4DKXRF', '', 1, 1563063977, NULL, NULL),
(1590, 10002, 7, 'nJl9hXXj6RbIuWPY5GKFsu7uScIh1rET', '', 1, 1563063977, NULL, NULL),
(1591, 10002, 7, 'qkm8o9RT6lexTHYzxPDCvvMM7jH7X5Vd', '', 1, 1563063977, NULL, NULL),
(1592, 10002, 7, 'GSM7mcwCE5eSQi50lIPCA02ThvN4qAVc', '', 1, 1563063977, NULL, NULL),
(1593, 10002, 7, 'buRAId5SLCZVsJ82VG8KIUmH8tMSa7jG', '', 1, 1563063977, NULL, NULL),
(1594, 10002, 7, 'hw6Pr8enRU45xCy1ElWbY4FiyFXDUwfN', '', 1, 1563063977, NULL, NULL),
(1595, 10002, 7, 'fKlFrtFlS81I5kmTD6tEHqiWdaBWnZeH', '', 1, 1563063977, NULL, NULL),
(1596, 10002, 7, 'PxEstq6JgS4fQVMmiriyE2gHFhxwx8H4', '', 1, 1563063977, NULL, NULL),
(1597, 10002, 7, 'w8cDS0nXhfVAWnO5DMU512ym1ccHFw6J', '', 1, 1563063977, NULL, NULL),
(1598, 10002, 7, 'aKgInPpvBV5K470Wreek8zApHoYvC3nR', '', 1, 1563063977, NULL, NULL),
(1599, 10002, 7, '6iSqYmimC2zfsAl3QN51R0x8vHU9xEqa', '', 1, 1563063977, NULL, NULL),
(1600, 10002, 7, 'PKtRylm1Xqk2NSbsBowOWKtyCsg6YhKC', '', 1, 1563063977, NULL, NULL),
(1601, 10002, 7, '3qPACVFf50avLX7MdX2jWHd8BYPxKfnn', '', 1, 1563063977, NULL, NULL),
(1602, 10002, 7, '5RP0a7mWsElTMRUmHDSR1q133S9WxPK4', '', 1, 1563063977, NULL, NULL),
(1603, 10002, 7, 'l0Cacf2SE54Vk6DCgKQQwRo1g6EIi5pv', '', 1, 1563063977, NULL, NULL),
(1604, 10002, 7, '6RlxVAXOqkYYYGo7zm4oOXngbmJvjVXb', '', 1, 1563063977, NULL, NULL),
(1605, 10002, 7, 'RpLJ5vs9PVO7XzNiUpmTFi4Pi8GV82Y1', '', 1, 1563063977, NULL, NULL),
(1606, 10002, 7, 'Ciao8kNX24JkFrvseE4jBkv4wJ9FtVnI', '', 1, 1563063977, NULL, NULL),
(1607, 10002, 7, 'y22Xl46amt8xiGTcb8l7jEYipebMjhCQ', '', 1, 1563063977, NULL, NULL),
(1608, 10002, 7, 's3YTKIn34ByiQD1a4udezFA3HQK91hGL', '', 1, 1563063977, NULL, NULL),
(1609, 10002, 7, 'GQj2iVXkiafaA5tU7Jd9LV7M62DJuY0f', '', 1, 1563063977, NULL, NULL),
(1610, 10002, 7, '98e0iV66ibtHrDqQVHERmVTl15EOB2CB', '', 1, 1563063977, NULL, NULL),
(1611, 10002, 7, '0pb246xy33I2JyFswwcfWHq7hxw4RSnN', '', 1, 1563063977, NULL, NULL),
(1612, 10002, 7, 'dsjIMbtZosyxaczKXZZOlIaHWIbhdEjd', '', 1, 1563063977, NULL, NULL),
(1613, 10002, 7, 'NVnFhYx9WKEdXYT7r9CEOeWAeEo4u3bH', '', 1, 1563063977, NULL, NULL),
(1614, 10002, 7, 'Lz6P7aoLmVNbizqBFtKQiYyEaFVkqnJ2', '', 1, 1563063977, NULL, NULL),
(1615, 10002, 7, 'pO7md4xBnIKf8VIzRjEXghTh8RUmJboR', '', 1, 1563063977, NULL, NULL),
(1616, 10002, 7, 'X7QK5mU7pGrM7LvM0lzmEEYfLRdQ8gI1', '', 1, 1563063977, NULL, NULL),
(1617, 10002, 7, '3L8gHGMOLHlJnQTKM1g5pQpLPcLqj1Zb', '', 1, 1563063977, NULL, NULL),
(1618, 10002, 7, 'YW6numtE68lThkyb7OoOdSJx2J777w65', '', 1, 1563063977, NULL, NULL),
(1619, 10002, 8, '2we2QzrFOH0tMOW', '', 1, 1563073390, NULL, NULL),
(1620, 10002, 8, 'uUuimjMSUA0gaUR', '', 1, 1563073390, NULL, NULL),
(1621, 10002, 8, 'BG5uait4Xt9YHyl', '', 1, 1563073390, NULL, NULL),
(1622, 10002, 8, 'j2DsAMAzGPHSE3k', '', 1, 1563073390, NULL, NULL),
(1623, 10002, 8, 'zIEhcsves5SUoA8', '', 1, 1563073390, NULL, NULL),
(1624, 10002, 8, '3DrT6qvselltfiK', '', 1, 1563073390, NULL, NULL),
(1625, 10002, 8, '6MTIoy8DozikeHV', '', 1, 1563073390, NULL, NULL),
(1626, 10002, 8, 'saBjkspUhqS0rDo', '', 1, 1563073390, NULL, NULL),
(1627, 10002, 8, 'FxhVcQVyGwbiDrt', '', 1, 1563073390, NULL, NULL),
(1628, 10002, 8, 'BYTO7ufjDWpTBMD', '', 1, 1563073390, NULL, NULL),
(1629, 10002, 8, '3BKBN0D6iUSQfm0', '', 1, 1563073390, NULL, NULL),
(1630, 10002, 8, '22vGcibTDLkhbdk', '', 1, 1563073390, NULL, NULL),
(1631, 10002, 8, 'QnFdcVfVqAS11Bn', '', 1, 1563073390, NULL, NULL),
(1632, 10002, 8, 'ZoqRgpGt0sK8sal', '', 1, 1563073390, NULL, NULL),
(1633, 10002, 8, 'GOhgn9H7uDz4UnE', '', 1, 1563073390, NULL, NULL),
(1634, 10002, 8, 'NPehQ5imGHEY1xp', '', 1, 1563073390, NULL, NULL),
(1635, 10002, 8, 'tlbgFBoUiXA0drh', '', 1, 1563073390, NULL, NULL),
(1636, 10002, 8, 'wPgaVYfB79WyRRD', '', 1, 1563073390, NULL, NULL),
(1637, 10002, 8, '7e8ArOfUV1i1rHv', '', 1, 1563073390, NULL, NULL),
(1638, 10002, 8, 'ENwrGLjxQqevD6t', '', 1, 1563073390, NULL, NULL),
(1639, 10002, 8, 'aHHZm0urdqVjt3T', '', 1, 1563073390, NULL, NULL),
(1640, 10002, 8, 'lMf8Et5MAOP5tA8', '', 1, 1563073390, NULL, NULL),
(1641, 10002, 8, 'OxfeHmju5DkZyjO', '', 1, 1563073390, NULL, NULL),
(1642, 10002, 8, 'crRe295YqxwySpg', '', 1, 1563073390, NULL, NULL),
(1643, 10002, 8, 'Yk5h4wiuGBStXGb', '', 1, 1563073390, NULL, NULL),
(1644, 10002, 8, '0qMOLQm1HodhtQb', '', 1, 1563073390, NULL, NULL),
(1645, 10002, 8, 'zLHRjG0mkWSHO0l', '', 1, 1563073390, NULL, NULL),
(1646, 10002, 8, 'gD7DTzD7PFbgggb', '', 1, 1563073390, NULL, NULL),
(1647, 10002, 8, '2Z1topiDppNt2b6', '', 1, 1563073390, NULL, NULL),
(1648, 10002, 8, 'reeS1cODaMhGGGL', '', 1, 1563073390, NULL, NULL),
(1649, 10002, 8, 'NeNnrzjVoOSkpyx', '', 1, 1563073390, NULL, NULL),
(1650, 10002, 8, 'pCstu5xdTrhigDX', '', 1, 1563073390, NULL, NULL),
(1651, 10002, 8, 'CuiN3wBosTGzIkd', '', 1, 1563073390, NULL, NULL),
(1652, 10002, 8, 'wbLtGGqY0thA5SH', '', 1, 1563073390, NULL, NULL),
(1653, 10002, 8, 'kC9Xd5fGtgARq36', '', 1, 1563073390, NULL, NULL),
(1654, 10002, 8, 'CbdbK9u9jsxtBjX', '', 1, 1563073390, NULL, NULL),
(1655, 10002, 8, 'I3S5fOeseb1SgBq', '', 1, 1563073390, NULL, NULL),
(1656, 10002, 8, 't4tGRb4bXe3IfBE', '', 1, 1563073390, NULL, NULL),
(1657, 10002, 8, 'jTRLZyEPGxXkz4n', '', 1, 1563073390, NULL, NULL),
(1658, 10002, 8, 'LlAzqRGZlKeCZcv', '', 1, 1563073390, NULL, NULL),
(1659, 10002, 8, 'X885eYc8H7m5jQT', '', 1, 1563073390, NULL, NULL),
(1660, 10002, 8, '2ydCvy8e5WbxNmE', '', 1, 1563073390, NULL, NULL),
(1661, 10002, 8, 'TwbKun2gHhWrEbW', '', 1, 1563073390, NULL, NULL),
(1662, 10002, 8, 'UjuOHKZHr6xUGD6', '', 1, 1563073390, NULL, NULL),
(1663, 10002, 8, '0A1R4WWYXqGqy6d', '', 1, 1563073390, NULL, NULL),
(1664, 10002, 8, 'ys1BV4Z8ZidR0kk', '', 1, 1563073390, NULL, NULL),
(1665, 10002, 8, 'zL2wLgumnLSPFx3', '', 1, 1563073390, NULL, NULL),
(1666, 10002, 8, 'd1hofUmnzDCCumf', '', 1, 1563073390, NULL, NULL),
(1667, 10002, 8, 'bOk29R9gBI5Ddco', '', 1, 1563073390, NULL, NULL),
(1668, 10002, 8, 'u62p4EbpG5Gd08h', '', 1, 1563073390, NULL, NULL),
(1669, 10002, 8, '4U1F4V9OEEisbjp', '', 1, 1563073390, NULL, NULL),
(1670, 10002, 8, 'LVtXLpusSIBD8Su', '', 1, 1563073390, NULL, NULL),
(1671, 10002, 8, '0Wad5TNN9nSUDhE', '', 1, 1563073390, NULL, NULL),
(1672, 10002, 8, 'vuyLxufDEEMnyZn', '', 1, 1563073390, NULL, NULL),
(1673, 10002, 8, 'rYtG7tw5xiAOiOx', '', 1, 1563073390, NULL, NULL),
(1674, 10002, 8, 'zbpHxnwhTH8RXQ9', '', 1, 1563073390, NULL, NULL),
(1675, 10002, 8, 'lgIYMnZzbSRN1YL', '', 1, 1563073390, NULL, NULL),
(1676, 10002, 8, 'gH0TGxyk3kMIZXX', '', 1, 1563073390, NULL, NULL),
(1677, 10002, 8, 'ZK9xgrNli2irnuu', '', 1, 1563073390, NULL, NULL),
(1678, 10002, 8, 'dqpYjzs7rUkQ1E9', '', 1, 1563073390, NULL, NULL),
(1679, 10002, 8, 'AtQFjHM5MDVQPbE', '', 1, 1563073390, NULL, NULL),
(1680, 10002, 8, 'ow2hgAjRpavqKQG', '', 1, 1563073390, NULL, NULL),
(1681, 10002, 8, 'mVkBorLmF5MDxgy', '', 1, 1563073390, NULL, NULL),
(1682, 10002, 8, 'vRC01VgOXEOeBN5', '', 1, 1563073390, NULL, NULL),
(1683, 10002, 8, 'h1H6KYhpZSwLfTL', '', 1, 1563073390, NULL, NULL),
(1684, 10002, 8, 'hOeRVHAtuZEtGId', '', 1, 1563073390, NULL, NULL),
(1685, 10002, 8, 'fC3zgczo1UR5D9W', '', 1, 1563073390, NULL, NULL),
(1686, 10002, 8, 'cZg8GOsybH9DfUd', '', 1, 1563073390, NULL, NULL),
(1687, 10002, 8, 'sSrkvm3UD1zeBiv', '', 1, 1563073390, NULL, NULL),
(1688, 10002, 8, 'nKUMGrCzemE3Wsw', '', 1, 1563073390, NULL, NULL),
(1689, 10002, 8, 'cKLbKmxWLrd38Dd', '', 1, 1563073390, NULL, NULL),
(1690, 10002, 8, 'kOdI9n3Xcxpkeju', '', 1, 1563073390, NULL, NULL),
(1691, 10002, 8, 'PzZgwjCfXG6Q6fq', '', 1, 1563073390, NULL, NULL),
(1692, 10002, 8, 'cxOZ402FX1ZG2Ld', '', 1, 1563073390, NULL, NULL),
(1693, 10002, 8, 'q2iNnRsAgjErPCA', '', 1, 1563073390, NULL, NULL),
(1694, 10002, 8, 'p1AzodpqoTD77OX', '', 1, 1563073390, NULL, NULL),
(1695, 10002, 8, 't490G0c0mBcprX0', '', 1, 1563073390, NULL, NULL),
(1696, 10002, 8, 'HgkHof4703lUWWX', '', 1, 1563073390, NULL, NULL),
(1697, 10002, 8, 'aUmhH1fpoXGwu6s', '', 1, 1563073390, NULL, NULL),
(1698, 10002, 8, 'NtEpvl3UMyQxUjE', '', 1, 1563073390, NULL, NULL),
(1699, 10002, 8, 'roFa36CTOpZTugk', '', 1, 1563073390, NULL, NULL),
(1700, 10002, 8, '3gfrX1Xm6OeleIg', '', 1, 1563073390, NULL, NULL),
(1701, 10002, 8, 'g5WGPVaEomYrFj3', '', 1, 1563073390, NULL, NULL),
(1702, 10002, 8, 'bSnWigw0n1m8XTk', '', 1, 1563073390, NULL, NULL),
(1703, 10002, 8, '2prCfdFt4zeyzsG', '', 1, 1563073390, NULL, NULL),
(1704, 10002, 8, 'D7eE8alwblnPCgq', '', 1, 1563073390, NULL, NULL),
(1705, 10002, 8, '46H3VLBlIa0buXU', '', 1, 1563073390, NULL, NULL),
(1706, 10002, 8, 'gcicvlKmoQfagim', '', 1, 1563073390, NULL, NULL),
(1707, 10002, 8, 'qmH1FKlVUVN7NjR', '', 1, 1563073390, NULL, NULL),
(1708, 10002, 8, 'IQyeyYbIOo3ccnb', '', 1, 1563073390, NULL, NULL),
(1709, 10002, 8, 'XxYS97Ov2XqCCYV', '', 1, 1563073390, NULL, NULL),
(1710, 10002, 8, 'GXTIpbl9ThoxTwP', '', 1, 1563073390, NULL, NULL),
(1711, 10002, 8, 'pNFuVdGggM2YGm6', '', 1, 1563073390, NULL, NULL),
(1712, 10002, 8, 'GGM91fBKx2gxB28', '', 1, 1563073390, NULL, NULL),
(1713, 10002, 8, 'k8D8kGbsCjdcuUG', '', 1, 1563073390, NULL, NULL),
(1714, 10002, 8, 'owSMDXQCEnybWxS', '', 1, 1563073390, NULL, NULL),
(1715, 10002, 8, 'jPgEpxecZ01yiDF', '', 1, 1563073390, NULL, NULL),
(1716, 10002, 8, 'cGgegsqeFqyPIAt', '', 1, 1563073390, NULL, NULL),
(1717, 10002, 8, 'IZ9fHxtrUQbKF3h', '', 1, 1563073390, NULL, NULL),
(1718, 10002, 8, 'zOnrxHpb6LG4mnh', '', 1, 1563073390, NULL, NULL),
(1719, 10002, 8, 'AjzAtEXWiA3B0IV', '', 1, 1563073390, NULL, NULL),
(1720, 10002, 8, 'MT6CY61yYwhIiGY', '', 1, 1563073390, NULL, NULL),
(1721, 10002, 8, 'd6dfiOQCbiBnvoK', '', 1, 1563073390, NULL, NULL),
(1722, 10002, 8, 'g21f6nKupT9q3qU', '', 1, 1563073390, NULL, NULL),
(1723, 10002, 8, 'YNdbeKBnbxwYDo8', '', 1, 1563073390, NULL, NULL),
(1724, 10002, 8, 'oZjjCr45kTZYhoZ', '', 1, 1563073390, NULL, NULL),
(1725, 10002, 8, 'CS3O8xGMvRAvou0', '', 1, 1563073390, NULL, NULL),
(1726, 10002, 8, '4Bv2Yn3IavBaxZm', '', 1, 1563073390, NULL, NULL),
(1727, 10002, 8, 'UW5BPhzp0f1ziwB', '', 1, 1563073390, NULL, NULL),
(1728, 10002, 8, 'tmcW2PjSRG2TzZS', '', 1, 1563073390, NULL, NULL),
(1729, 10002, 8, 'KohnRIq3XnlvDG5', '', 1, 1563073390, NULL, NULL),
(1730, 10002, 8, '2vIGxVzFP76TvcP', '', 1, 1563073390, NULL, NULL),
(1731, 10002, 8, 'SsCGTHGlcqDk7NI', '', 1, 1563073390, NULL, NULL),
(1732, 10002, 8, 'hHCgnnkWzp6EzD5', '', 1, 1563073390, NULL, NULL),
(1733, 10002, 8, 'Nq7xXviIcGbln21', '', 1, 1563073390, NULL, NULL),
(1734, 10002, 8, 'PiO2rGBNTRHsRiU', '', 1, 1563073390, NULL, NULL),
(1735, 10002, 8, 'f7I719p2Rcvca3d', '', 1, 1563073390, NULL, NULL),
(1736, 10002, 8, 'YPYeuLRWVNv2NzL', '', 1, 1563073390, NULL, NULL),
(1737, 10002, 8, 'Xr5G5VHSDfNMR0Z', '', 1, 1563073390, NULL, NULL),
(1738, 10002, 8, 'ZNvx0Hf1euoOpPx', '', 1, 1563073390, NULL, NULL),
(1739, 10002, 8, 'r4eD1cN20CHrhGG', '', 1, 1563073390, NULL, NULL),
(1740, 10002, 8, '7HP3O3zP1LSmIMD', '', 1, 1563073390, NULL, NULL),
(1741, 10002, 8, 'kgw2gQA6Axxt4jZ', '', 1, 1563073390, NULL, NULL),
(1742, 10002, 8, 'rRMXzyrBbCHhWEl', '', 1, 1563073390, NULL, NULL),
(1743, 10002, 8, 'F3nvdsghsgipgyk', '', 1, 1563073390, NULL, NULL),
(1744, 10002, 8, 'ExHOHDPKGUg3RqQ', '', 1, 1563073390, NULL, NULL),
(1745, 10002, 8, 'Ckcwz8bA4iG1Zep', '', 1, 1563073390, NULL, NULL),
(1746, 10002, 8, 'DemobasmR1TcIPB', '', 1, 1563073390, NULL, NULL),
(1747, 10002, 8, '1XbpLqE6FsezQHB', '', 1, 1563073390, NULL, NULL),
(1748, 10002, 8, 'Mkh47p5Xu6USYkM', '', 1, 1563073390, NULL, NULL),
(1749, 10002, 8, 'atGRNTRnxgOr1Y1', '', 1, 1563073390, NULL, NULL),
(1750, 10002, 8, 'iZNbd2okR18G6Hb', '', 1, 1563073390, NULL, NULL),
(1751, 10002, 8, '6fK86W18mZGnhWN', '', 1, 1563073390, NULL, NULL),
(1752, 10002, 8, 'FKEU6DaLS4kFp0u', '', 1, 1563073390, NULL, NULL),
(1753, 10002, 8, 'WiVbkD8rbhKVru3', '', 1, 1563073390, NULL, NULL),
(1754, 10002, 8, 'o62tc86kINwuu97', '', 1, 1563073390, NULL, NULL),
(1755, 10002, 8, 'nNlWPGGA7GtyeF1', '', 1, 1563073390, NULL, NULL),
(1756, 10002, 8, '4kftH4AeND5rTUr', '', 1, 1563073390, NULL, NULL),
(1757, 10002, 8, 'M5AYGQxoLoZifOl', '', 1, 1563073390, NULL, NULL),
(1758, 10002, 8, 'Frs2M5pAaGzSpwQ', '', 1, 1563073390, NULL, NULL),
(1759, 10002, 8, 'i3gUa9lRn3n4zQF', '', 1, 1563073390, NULL, NULL),
(1760, 10002, 8, 'AiEb3h3QELLcGyN', '', 1, 1563073390, NULL, NULL),
(1761, 10002, 8, '7L3BQ0mrxIyFETX', '', 1, 1563073390, NULL, NULL),
(1762, 10002, 8, 'GAfLQMWCfnVa4rd', '', 1, 1563073390, NULL, NULL),
(1763, 10002, 8, 'm05mMoboKVm7EPc', '', 1, 1563073390, NULL, NULL),
(1764, 10002, 8, 'I8WaGoxfBwrOGP7', '', 1, 1563073390, NULL, NULL),
(1765, 10002, 8, 'QlUQ3CFz94gDrYY', '', 1, 1563073390, NULL, NULL),
(1766, 10002, 8, 'tGYpA92jddMBypd', '', 1, 1563073390, NULL, NULL),
(1767, 10002, 8, 'WlDocMZn0cQjUI0', '', 1, 1563073390, NULL, NULL),
(1768, 10002, 8, 'HrfjYIyIkhaiWI3', '', 1, 1563073390, NULL, NULL),
(1769, 10002, 8, 'efG650ZUvdY2T4d', '', 1, 1563073390, NULL, NULL),
(1770, 10002, 8, 'k5nGz8rnRULAjaq', '', 1, 1563073390, NULL, NULL),
(1771, 10002, 8, 'TS9mmhvxHns16dW', '', 1, 1563073390, NULL, NULL),
(1772, 10002, 8, 'uMmS635fsdbWa04', '', 1, 1563073390, NULL, NULL),
(1773, 10002, 8, '3ruFfzcqQXFz20n', '', 1, 1563073390, NULL, NULL),
(1774, 10002, 8, 'YaL3Q2EyxLbVzqc', '', 1, 1563073390, NULL, NULL),
(1775, 10002, 8, '7Vsdm6vZWo2y6Oi', '', 1, 1563073390, NULL, NULL),
(1776, 10002, 8, 'S10ZvIFiBTF8mST', '', 1, 1563073390, NULL, NULL),
(1777, 10002, 8, '56GbTcwwyGj18dW', '', 1, 1563073390, NULL, NULL),
(1778, 10002, 8, 'G5NAdUdrj37Y7K6', '', 1, 1563073390, NULL, NULL),
(1779, 10002, 8, 'PpxOHhGgSwAzGxi', '', 1, 1563073390, NULL, NULL),
(1780, 10002, 8, 'V6jD5UjMLaHwQmp', '', 1, 1563073390, NULL, NULL),
(1781, 10002, 8, 'qG3kHEeqNLGiCmT', '', 1, 1563073390, NULL, NULL),
(1782, 10002, 8, '8e4FziVKTPdeRjG', '', 1, 1563073390, NULL, NULL),
(1783, 10002, 8, 'tntCKGULyGo5mZU', '', 1, 1563073390, NULL, NULL),
(1784, 10002, 8, 'ZOltc8IAbbmEFTy', '', 1, 1563073390, NULL, NULL),
(1785, 10002, 8, 'EvGkK1cWqsnwmKz', '', 1, 1563073390, NULL, NULL),
(1786, 10002, 8, 'XqvrY9BK7wFUVpl', '', 1, 1563073390, NULL, NULL),
(1787, 10002, 8, 'edtVyie4yGxglc1', '', 1, 1563073390, NULL, NULL),
(1788, 10002, 8, 'jxiSQEBFWkxsUmW', '', 1, 1563073390, NULL, NULL),
(1789, 10002, 8, 'yOR3WmIg1lQl9Sl', '', 1, 1563073390, NULL, NULL),
(1790, 10002, 8, 'EvQ4EwzIYqckNE0', '', 1, 1563073390, NULL, NULL),
(1791, 10002, 8, '3ic8gZ7zjeEEXgD', '', 1, 1563073390, NULL, NULL),
(1792, 10002, 8, '1YCBDUzrLA6h5ON', '', 1, 1563073390, NULL, NULL),
(1793, 10002, 8, 'eUIOC6NbqZkMCdj', '', 1, 1563073390, NULL, NULL),
(1794, 10002, 8, 'wvdhlzIcwupIQC9', '', 1, 1563073390, NULL, NULL),
(1795, 10002, 8, 'IfI9lNYSkIDFMk2', '', 1, 1563073390, NULL, NULL),
(1796, 10002, 8, 'plXpPCcm2Qx3zdf', '', 1, 1563073390, NULL, NULL),
(1797, 10002, 8, 'ckwNdPASU2goY5e', '', 1, 1563073390, NULL, NULL),
(1798, 10002, 8, '0rP4wGljXTdtQgI', '', 1, 1563073390, NULL, NULL),
(1799, 10002, 8, 'zXtLbfMSpwy4fdl', '', 1, 1563073390, NULL, NULL),
(1800, 10002, 8, '0GjGVaKgRfhkdtl', '', 1, 1563073390, NULL, NULL),
(1801, 10002, 8, 'qlKPG2bCGyNTDVk', '', 1, 1563073390, NULL, NULL),
(1802, 10002, 8, 'eGVS04Gfod2HKnI', '', 1, 1563073390, NULL, NULL),
(1803, 10002, 8, 'V2CLhSGUjgi27cI', '', 1, 1563073390, NULL, NULL),
(1804, 10002, 8, 'Tr7MKCPGYH8PAr4', '', 1, 1563073390, NULL, NULL),
(1805, 10002, 8, 'hD7yh51s1dui4MI', '', 1, 1563073390, NULL, NULL),
(1806, 10002, 8, '0W74TnkpuUGEs8l', '', 1, 1563073390, NULL, NULL),
(1807, 10002, 8, 'fISWTTGUCcd1MiM', '', 1, 1563073390, NULL, NULL),
(1808, 10002, 8, 'clE9pTQweER1HWQ', '', 1, 1563073390, NULL, NULL),
(1809, 10002, 8, 'zfm3QARuOFOPdLx', '', 1, 1563073390, NULL, NULL),
(1810, 10002, 8, 'ZdjwMTgR2XsTXGk', '', 1, 1563073390, NULL, NULL),
(1811, 10002, 8, 'Emh96MZomaxsleH', '', 1, 1563073390, NULL, NULL),
(1812, 10002, 8, 'krAu3W5v68Rwj7Y', '', 1, 1563073390, NULL, NULL),
(1813, 10002, 8, 'hAcd8homQgB1Mme', '', 1, 1563073390, NULL, NULL),
(1814, 10002, 8, 'vG1iBhdBiGyshH1', '', 1, 1563073390, NULL, NULL),
(1815, 10002, 8, '3lcFMqmVrcoyelH', '', 1, 1563073390, NULL, NULL),
(1816, 10002, 8, 'yORn11lp8Q9CKAt', '', 1, 1563073390, NULL, NULL),
(1817, 10002, 8, 'G4Xb0zdaMG0BRmg', '', 1, 1563073390, NULL, NULL),
(1818, 10002, 8, 'kqzwIyNAm3UIKed', '', 1, 1563073390, NULL, NULL),
(1819, 10002, 8, '8lbHbyQFh4fcuFT', '', 1, 1563073390, NULL, NULL),
(1820, 10002, 8, 'mLENsaI0bN0GjZc', '', 1, 1563073390, NULL, NULL),
(1821, 10002, 8, 'hvlMSKGt9plpAyG', '', 1, 1563073390, NULL, NULL),
(1822, 10002, 8, 'dLbUbh3ZUKm8UiI', '', 1, 1563073390, NULL, NULL),
(1823, 10002, 8, '8Mx3bPMxh2Oe3wb', '', 1, 1563073390, NULL, NULL),
(1824, 10002, 8, '6QnEmSTekFeEDta', '', 1, 1563073390, NULL, NULL),
(1825, 10002, 8, '8vaUtS8tWUU2lQi', '', 1, 1563073390, NULL, NULL),
(1826, 10002, 8, 'fpR7Or2nFRlDyPG', '', 1, 1563073390, NULL, NULL),
(1827, 10002, 8, 'vXjOLGK69mmje7G', '', 1, 1563073390, NULL, NULL),
(1828, 10002, 8, '2SaEmNU9MF9wXie', '', 1, 1563073390, NULL, NULL),
(1829, 10002, 8, '7gM6l7HE3ArFGAv', '', 1, 1563073390, NULL, NULL),
(1830, 10002, 8, 'pG1gSn8a02GPrDB', '', 1, 1563073390, NULL, NULL),
(1831, 10002, 8, '2OMGxPsqGhTvR1P', '', 1, 1563073390, NULL, NULL),
(1832, 10002, 8, 'GHC9DszuWKjAQH8', '', 1, 1563073390, NULL, NULL),
(1833, 10002, 8, '6wX2GQGA67pouPn', '', 1, 1563073390, NULL, NULL),
(1834, 10002, 8, 'x2oTF7PM2H9ezgn', '', 1, 1563073390, NULL, NULL),
(1835, 10002, 8, 'L0GRaF3siEUkM03', '', 1, 1563073390, NULL, NULL),
(1836, 10002, 8, 'BS2q8mBIMRi9epY', '', 1, 1563073390, NULL, NULL),
(1837, 10002, 8, '0zXBMKQyri7nMWp', '', 1, 1563073390, NULL, NULL),
(1838, 10002, 8, 'rIFdG5UBjho6rxe', '', 1, 1563073390, NULL, NULL),
(1839, 10002, 8, 'Up2WTrIFmKvVpgy', '', 1, 1563073390, NULL, NULL),
(1840, 10002, 8, 'rc1tioq8VQrUgtY', '', 1, 1563073390, NULL, NULL),
(1841, 10002, 8, 'OjKkYbXG3WxlmNx', '', 1, 1563073390, NULL, NULL),
(1842, 10002, 8, 'ym0ddBvf9ZQUSDQ', '', 1, 1563073390, NULL, NULL),
(1843, 10002, 8, 'FtG68gdxUqMrhGg', '', 1, 1563073390, NULL, NULL),
(1844, 10002, 8, 'qsS0cdMGkxBOGxF', '', 1, 1563073390, NULL, NULL),
(1845, 10002, 8, 'oTNFinuHP4nPzSx', '', 1, 1563073390, NULL, NULL),
(1846, 10002, 8, 'xz9dnKFaStxSe6l', '', 1, 1563073390, NULL, NULL),
(1847, 10002, 8, 'QvCLAFKG3UVvsgN', '', 1, 1563073390, NULL, NULL),
(1848, 10002, 8, 'lIZMmDTTWDWaTeg', '', 1, 1563073390, NULL, NULL),
(1849, 10002, 8, 'wNpBZr5PNHMxAvi', '', 1, 1563073390, NULL, NULL),
(1850, 10002, 8, 'eTvutt1EjVePVGe', '', 1, 1563073390, NULL, NULL),
(1851, 10002, 8, 'xozQbZaTUZtyjwd', '', 1, 1563073390, NULL, NULL),
(1852, 10002, 8, 'bAN9lZ5b7dbVWrQ', '', 1, 1563073390, NULL, NULL),
(1853, 10002, 8, 'meHWTjOB07OVktT', '', 1, 1563073390, NULL, NULL),
(1854, 10002, 8, 'hrGDlVSG4mWnGog', '', 1, 1563073390, NULL, NULL),
(1855, 10002, 8, 'O2eGm4jSFH3juxD', '', 1, 1563073390, NULL, NULL),
(1856, 10002, 8, 'XtOWEOXgQmoaqgu', '', 1, 1563073390, NULL, NULL),
(1857, 10002, 8, 'adke5qsC8bqRwMM', '', 1, 1563073390, NULL, NULL),
(1858, 10002, 8, 'YNZoHjAP4GTjW8l', '', 1, 1563073390, NULL, NULL),
(1859, 10002, 8, 'NmxBGsCHuDMBxgu', '', 1, 1563073390, NULL, NULL),
(1860, 10002, 8, '50rCoOUsD6om3Xn', '', 1, 1563073390, NULL, NULL),
(1861, 10002, 8, 'sO9ANNj8x9cqmgO', '', 1, 1563073390, NULL, NULL),
(1862, 10002, 8, 'oqAFhRvKZzG95LL', '', 1, 1563073390, NULL, NULL),
(1863, 10002, 8, 'pdNdSV6ppbUGx2l', '', 1, 1563073390, NULL, NULL),
(1864, 10002, 8, '8CdcxWzoa5L219A', '', 1, 1563073390, NULL, NULL),
(1865, 10002, 8, 'a81B9w539F5rBPX', '', 1, 1563073390, NULL, NULL),
(1866, 10002, 8, 'WV5hasq8ZS7GtvY', '', 1, 1563073390, NULL, NULL),
(1867, 10002, 8, 'RAGUapHUaAh0Xfu', '', 1, 1563073390, NULL, NULL),
(1868, 10002, 8, '4ffq8xL6aF3tOeU', '', 1, 1563073390, NULL, NULL),
(1869, 10002, 8, 'WKtUnGxu9deL1T3', '', 1, 1563073390, NULL, NULL),
(1870, 10002, 8, 'WwTGEFgKg5vNZIY', '', 1, 1563073390, NULL, NULL),
(1871, 10002, 8, 'xx89OrwIfcWlXHZ', '', 1, 1563073390, NULL, NULL),
(1872, 10002, 8, '5W02OWZ6G9G1DBM', '', 1, 1563073390, NULL, NULL),
(1873, 10002, 8, 'gBGwoOS47tGYqMa', '', 1, 1563073390, NULL, NULL),
(1874, 10002, 8, 'STcGrf0ScpyCcXy', '', 1, 1563073390, NULL, NULL),
(1875, 10002, 8, 'rCzPoEdYlzfIfKy', '', 1, 1563073390, NULL, NULL),
(1876, 10002, 8, 'yDUVBGjZMZxQl7G', '', 1, 1563073390, NULL, NULL),
(1877, 10002, 8, '5Yqt23k89zwBGRk', '', 1, 1563073390, NULL, NULL),
(1878, 10002, 8, 'qjF2BbYACrPdIUx', '', 1, 1563073390, NULL, NULL),
(1879, 10002, 8, 'aor5nE66cOQPvzB', '', 1, 1563073390, NULL, NULL),
(1880, 10002, 8, '26lFgGToCWI540g', '', 1, 1563073390, NULL, NULL),
(1881, 10002, 8, 'G7LuVCUjzPrGBLS', '', 1, 1563073390, NULL, NULL),
(1882, 10002, 8, '4PNQhBIkD6E4npP', '', 1, 1563073390, NULL, NULL),
(1883, 10002, 8, 'wfjhQWOwQO8xmow', '', 1, 1563073390, NULL, NULL),
(1884, 10002, 8, 'I0E0lI3gwZAbNWB', '', 1, 1563073390, NULL, NULL),
(1885, 10002, 8, 'W1psDCpP4L0kD5R', '', 1, 1563073390, NULL, NULL),
(1886, 10002, 8, 'QORP47VW1GLHwbu', '', 1, 1563073390, NULL, NULL),
(1887, 10002, 8, '6FyADhCXwwpnOMC', '', 1, 1563073390, NULL, NULL),
(1888, 10002, 8, 'sekhSlgUOeGKe4K', '', 1, 1563073390, NULL, NULL),
(1889, 10002, 8, '2IKQGMQ1bLxI2OQ', '', 1, 1563073390, NULL, NULL),
(1890, 10002, 8, 'hE89PuQtv9S3Tm1', '', 1, 1563073390, NULL, NULL),
(1891, 10002, 8, 'Y6PIRd90FEaesMC', '', 1, 1563073390, NULL, NULL),
(1892, 10002, 8, 'MEobXldduYRYfXy', '', 1, 1563073390, NULL, NULL),
(1893, 10002, 8, 'ILLqR7TgG6kY51W', '', 1, 1563073390, NULL, NULL),
(1894, 10002, 8, 'mRtshebr9moG9BP', '', 1, 1563073390, NULL, NULL),
(1895, 10002, 8, '2H0CDWTT3kvyLXU', '', 1, 1563073390, NULL, NULL),
(1896, 10002, 8, '1qGyXBUtrgMnx4I', '', 1, 1563073390, NULL, NULL),
(1897, 10002, 8, 'G2GmqHU8d4BKUyG', '', 1, 1563073390, NULL, NULL),
(1898, 10002, 8, 'Bw5wlIuW4Ljg7Ul', '', 1, 1563073390, NULL, NULL),
(1899, 10002, 8, 'EgNY7HGrhP4Oo3f', '', 1, 1563073390, NULL, NULL),
(1900, 10002, 8, 'anNVm0apxD8SVhw', '', 1, 1563073390, NULL, NULL),
(1901, 10002, 8, '8ZYLUV4zGgIio70', '', 1, 1563073390, NULL, NULL),
(1902, 10002, 8, 'IEPQX3BhdkGGTG5', '', 1, 1563073390, NULL, NULL),
(1903, 10002, 8, 'NpovaiU7OOigYpg', '', 1, 1563073390, NULL, NULL),
(1904, 10002, 8, '7vi0XfFevRKOFiK', '', 1, 1563073390, NULL, NULL),
(1905, 10002, 8, 'bDMdGve57pvAlM1', '', 1, 1563073390, NULL, NULL),
(1906, 10002, 8, 'ObmFpGiwgSIcPru', '', 1, 1563073390, NULL, NULL),
(1907, 10002, 8, 'q3VVx5jEHMfkhGH', '', 1, 1563073390, NULL, NULL),
(1908, 10002, 8, 'iw8so4O4DZNmpFT', '', 1, 1563073390, NULL, NULL),
(1909, 10002, 8, 'PEeVcUzlPEUHM47', '', 1, 1563073390, NULL, NULL),
(1910, 10002, 8, 'hqay5OragpTv1Nj', '', 1, 1563073390, NULL, NULL),
(1911, 10002, 8, 'uKzLe5QV9rGQovC', '', 1, 1563073390, NULL, NULL),
(1912, 10002, 8, '8dASdcCR5Y1wMVZ', '', 1, 1563073390, NULL, NULL),
(1913, 10002, 8, 'dCXBqTS7OqQBtu5', '', 1, 1563073390, NULL, NULL),
(1914, 10002, 8, 'PTMXA5FGUQBCRgn', '', 1, 1563073390, NULL, NULL),
(1915, 10002, 8, '0mtz1MEEohy38ii', '', 1, 1563073390, NULL, NULL),
(1916, 10002, 8, 'kw4bGIPIgmGrpTE', '', 1, 1563073390, NULL, NULL),
(1917, 10002, 8, '0LnhHQyKwqvuCRm', '', 1, 1563073390, NULL, NULL),
(1918, 10002, 8, 'wQry1w8aT5YOAHH', '', 1, 1563073390, NULL, NULL),
(1919, 10002, 8, 'RI1DKWBoFGG2RFe', '', 1, 1563073390, NULL, NULL),
(1920, 10002, 8, '2TGrkf7gQhKPGkZ', '', 1, 1563073390, NULL, NULL),
(1921, 10002, 8, 'VgGQQdWPS8eqeDZ', '', 1, 1563073390, NULL, NULL),
(1922, 10002, 8, 'MGEyy9Q74dmH5ny', '', 1, 1563073390, NULL, NULL),
(1923, 10002, 8, '11Y77CzqhTM7yF3', '', 1, 1563073390, NULL, NULL),
(1924, 10002, 8, 'v3EHDKPAHC2qjGx', '', 1, 1563073390, NULL, NULL),
(1925, 10002, 8, 'FgmX5vZLI3LwjOo', '', 1, 1563073390, NULL, NULL),
(1926, 10002, 8, 'Xpiqv0KM13ZwXaa', '', 1, 1563073390, NULL, NULL),
(1927, 10002, 8, 'l3mMti7lgGBA9Mo', '', 1, 1563073390, NULL, NULL),
(1928, 10002, 8, 'mp8pASBjQTsKyMq', '', 1, 1563073390, NULL, NULL),
(1929, 10002, 8, '74xA9WH90wdRGlE', '', 1, 1563073390, NULL, NULL),
(1930, 10002, 8, 'GUc0bcnUtZS5Twe', '', 1, 1563073390, NULL, NULL),
(1931, 10002, 8, 'yDNhZsXWDTmMHZ5', '', 1, 1563073390, NULL, NULL),
(1932, 10002, 8, 'hhnmHXBqGhsNCTP', '', 1, 1563073390, NULL, NULL),
(1933, 10002, 8, 'Asqf4dkspiWvUxj', '', 1, 1563073390, NULL, NULL),
(1934, 10002, 8, '2UxTCBiAhRDLna9', '', 1, 1563073390, NULL, NULL),
(1935, 10002, 8, 'D0BVM7Pz2HRI41G', '', 1, 1563073390, NULL, NULL),
(1936, 10002, 8, 'hlN6UiXCk7wRYMg', '', 1, 1563073390, NULL, NULL),
(1937, 10002, 8, 'VdbGUdcjp7KDCm1', '', 1, 1563073390, NULL, NULL),
(1938, 10002, 8, 'KkWGvlabWnFxUi1', '', 1, 1563073390, NULL, NULL),
(1939, 10002, 8, '4rjzunXr7vohdvs', '', 1, 1563073390, NULL, NULL),
(1940, 10002, 8, 'kEWuuPa6NW8txnr', '', 1, 1563073390, NULL, NULL),
(1941, 10002, 8, 'dmYMhA0jOmGzeOY', '', 1, 1563073390, NULL, NULL),
(1942, 10002, 8, '9mGD01leibmGdHo', '', 1, 1563073390, NULL, NULL),
(1943, 10002, 8, 'GtTjoaltVg1nxBT', '', 1, 1563073390, NULL, NULL),
(1944, 10002, 8, 'zoITGoWGalHzHyO', '', 1, 1563073390, NULL, NULL),
(1945, 10002, 8, '4Prq0BUyx04XBXy', '', 1, 1563073390, NULL, NULL),
(1946, 10002, 8, '2k6BjQ5lHmHSVlI', '', 1, 1563073390, NULL, NULL),
(1947, 10002, 8, 'MXycMmqoKX4GGL3', '', 1, 1563073390, NULL, NULL),
(1948, 10002, 8, 'fCyMjNdMXbWT6pK', '', 1, 1563073390, NULL, NULL),
(1949, 10002, 8, 'Gl5dLM4FmOvO8Zd', '', 1, 1563073390, NULL, NULL),
(1950, 10002, 8, 'g4QXYUZgUfD7VTj', '', 1, 1563073390, NULL, NULL),
(1951, 10002, 8, 'HLbg5oOS4CqPbHB', '', 1, 1563073390, NULL, NULL),
(1952, 10002, 8, 'mBfHOcsm3mZyMRq', '', 1, 1563073390, NULL, NULL),
(1953, 10002, 8, 'xLBu8b4Rq907GG0', '', 1, 1563073390, NULL, NULL),
(1954, 10002, 8, 'Px8RwtmQlpOikct', '', 1, 1563073390, NULL, NULL),
(1955, 10002, 8, 'M4xaIMXujX8oQ9S', '', 1, 1563073390, NULL, NULL),
(1956, 10002, 8, 'czZ9X0G6fM7MFZK', '', 1, 1563073390, NULL, NULL),
(1957, 10002, 8, 'TZWnbORer2SGHo9', '', 1, 1563073390, NULL, NULL),
(1958, 10002, 8, 'KzAGIIGLfUN9pK0', '', 1, 1563073390, NULL, NULL),
(1959, 10002, 8, '6UsSYHZzLrBcGGs', '', 1, 1563073390, NULL, NULL),
(1960, 10002, 8, 'KWNVLobE3mHRnOt', '', 1, 1563073390, NULL, NULL),
(1961, 10002, 8, '5iZsOqsXXScvr6h', '', 1, 1563073390, NULL, NULL),
(1962, 10002, 8, '9TWGG7Plspt6zSi', '', 1, 1563073390, NULL, NULL),
(1963, 10002, 8, '59matiDtGQHqs1I', '', 1, 1563073390, NULL, NULL),
(1964, 10002, 8, '7su44jj41GLQMLl', '', 1, 1563073390, NULL, NULL),
(1965, 10002, 8, 'R8kSvqRulxw6gMm', '', 1, 1563073390, NULL, NULL),
(1966, 10002, 8, 'YLG9X3FLGsdaRWj', '', 1, 1563073390, NULL, NULL),
(1967, 10002, 8, 'hfCjoKb65l53n8Q', '', 1, 1563073390, NULL, NULL),
(1968, 10002, 8, 'lpfh5kwMiwo5X0d', '', 1, 1563073390, NULL, NULL),
(1969, 10002, 8, 'sSzIuhg7undbNnz', '', 1, 1563073390, NULL, NULL),
(1970, 10002, 8, 'Lzjnohkrbpoi8EC', '', 1, 1563073390, NULL, NULL),
(1971, 10002, 8, 'PkFGR21sYbX4zlV', '', 1, 1563073390, NULL, NULL),
(1972, 10002, 8, 'oPXHrOjMfRZXjW7', '', 1, 1563073390, NULL, NULL),
(1973, 10002, 8, 'ihnGNA2Tt231GHi', '', 1, 1563073390, NULL, NULL),
(1974, 10002, 8, 'lHZi30el7aB8eeR', '', 1, 1563073390, NULL, NULL),
(1975, 10002, 8, 'NowYrBWYjbNod1h', '', 1, 1563073390, NULL, NULL),
(1976, 10002, 8, 'bnBB0IK1bE5Bcxx', '', 1, 1563073390, NULL, NULL),
(1977, 10002, 8, 'ZQGKVE7MZBdRgXf', '', 1, 1563073390, NULL, NULL),
(1978, 10002, 8, '1u5BkFNh9BY07gA', '', 1, 1563073390, NULL, NULL),
(1979, 10002, 8, 'wCTiN9P0TOifgTp', '', 1, 1563073390, NULL, NULL),
(1980, 10002, 8, 'zW2iUEjfDxr3ZdV', '', 1, 1563073390, NULL, NULL),
(1981, 10002, 8, 'F7Ozs0cq00fo70D', '', 1, 1563073390, NULL, NULL),
(1982, 10002, 8, 'WDPMgaPMPFDpEZ1', '', 1, 1563073390, NULL, NULL),
(1983, 10002, 8, 'HGsi0ubVoXh3lWs', '', 1, 1563073390, NULL, NULL),
(1984, 10002, 8, '4Q17SEYWjOntugO', '', 1, 1563073390, NULL, NULL),
(1985, 10002, 8, 'VCCH3amF9LpqKKQ', '', 1, 1563073390, NULL, NULL),
(1986, 10002, 8, 'CGojwucYKuoDipw', '', 1, 1563073390, NULL, NULL),
(1987, 10002, 8, 'sGmTmIyyV1uPYCs', '', 1, 1563073390, NULL, NULL),
(1988, 10002, 8, 'qxm9aAEiVGVfVZN', '', 1, 1563073390, NULL, NULL),
(1989, 10002, 8, 'LC9UGb5cGKhWkyL', '', 1, 1563073390, NULL, NULL),
(1990, 10002, 8, 'TxMEzgjGFOqRGQo', '', 1, 1563073390, NULL, NULL),
(1991, 10002, 8, 'b2fpvxE9kYdyHKO', '', 1, 1563073390, NULL, NULL),
(1992, 10002, 8, 'hhu1juhFPnFYOoq', '', 1, 1563073390, NULL, NULL),
(1993, 10002, 8, 'g8Ai0zYu3Y054D4', '', 1, 1563073390, NULL, NULL),
(1994, 10002, 8, '0CkyouGlyIlm904', '', 1, 1563073390, NULL, NULL),
(1995, 10002, 8, 'sWmxhoSGeirKWcX', '', 1, 1563073390, NULL, NULL),
(1996, 10002, 8, 'SDTYEU8ZcmFNArC', '', 1, 1563073390, NULL, NULL),
(1997, 10002, 8, 'hqOFCB5reOcGoeH', '', 1, 1563073390, NULL, NULL),
(1998, 10002, 8, 'Nif10UESLAZBxrf', '', 1, 1563073390, NULL, NULL),
(1999, 10002, 8, 'HxlNCI2axa9N0rK', '', 1, 1563073390, NULL, NULL),
(2000, 10002, 8, 'WPYdTniHM4dl4SG', '', 1, 1563073390, NULL, NULL),
(2001, 10002, 8, 'lmNd6W2fFbYN6fy', '', 1, 1563073390, NULL, NULL),
(2002, 10002, 8, 'nqW6Rrjka6N5UDD', '', 1, 1563073390, NULL, NULL),
(2003, 10002, 8, 'mqnZFtOZyFXgCSg', '', 1, 1563073390, NULL, NULL),
(2004, 10002, 8, 'h3Rl58TdknXwkbB', '', 1, 1563073390, NULL, NULL),
(2005, 10002, 8, 'Ph6NLN6RCyeQ9C1', '', 1, 1563073390, NULL, NULL);
INSERT INTO `goods_card` (`id`, `user_id`, `goods_id`, `number`, `secret`, `status`, `create_at`, `delete_at`, `sell_time`) VALUES
(2006, 10002, 8, 'DbIRoPLM2g4qoh2', '', 1, 1563073390, NULL, NULL),
(2007, 10002, 8, 'i8QTPcVB61mj7p4', '', 1, 1563073390, NULL, NULL),
(2008, 10002, 8, 'EhT5g6tmO2U3PiX', '', 1, 1563073390, NULL, NULL),
(2009, 10002, 8, 'wb1q5NAVi4a15od', '', 1, 1563073390, NULL, NULL),
(2010, 10002, 8, 'FQ1Ank6ly6aIAP8', '', 1, 1563073390, NULL, NULL),
(2011, 10002, 8, 'O2SKMYAe0XU9ETN', '', 1, 1563073390, NULL, NULL),
(2012, 10002, 8, 'QFr8UpG4DW0Ht1x', '', 1, 1563073390, NULL, NULL),
(2013, 10002, 8, 'DsWuDFx308KRPdn', '', 1, 1563073390, NULL, NULL),
(2014, 10002, 8, 'IuA0Vrw7K80z5Nc', '', 1, 1563073390, NULL, NULL),
(2015, 10002, 8, '9HxzviAZjS8aSO0', '', 1, 1563073390, NULL, NULL),
(2016, 10002, 8, 'SNTvQfBT4tRWGNz', '', 1, 1563073390, NULL, NULL),
(2017, 10002, 8, 'Cdiv9OWpGIrdCEX', '', 1, 1563073390, NULL, NULL),
(2018, 10002, 8, 'Z7jklE3tRMSgjYu', '', 1, 1563073390, NULL, NULL),
(2019, 10002, 8, '9NSG2X9OrqDipLl', '', 1, 1563073390, NULL, NULL),
(2020, 10002, 8, 'ipXQSN4NflyMXPk', '', 1, 1563073390, NULL, NULL),
(2021, 10002, 8, 'kyXLHOdKGYeSIWA', '', 1, 1563073390, NULL, NULL),
(2022, 10002, 8, 'T6bqtULAuCxwD1o', '', 1, 1563073390, NULL, NULL),
(2023, 10002, 8, 'ZKGnANZQF6I2a5g', '', 1, 1563073390, NULL, NULL),
(2024, 10002, 8, 'PC01b8WxgW1X5av', '', 1, 1563073390, NULL, NULL),
(2025, 10002, 8, 'vsM78e75uORsggx', '', 1, 1563073390, NULL, NULL),
(2026, 10002, 8, 'YlW6LpZ4ZzbEuSi', '', 1, 1563073390, NULL, NULL),
(2027, 10002, 8, 'Me9sg2dKueSbj8t', '', 1, 1563073390, NULL, NULL),
(2028, 10002, 8, 'v6Mle0vjkAMnRKN', '', 1, 1563073390, NULL, NULL),
(2029, 10002, 8, 'lDHQW7LSiGtFwZP', '', 1, 1563073390, NULL, NULL),
(2030, 10002, 8, 'GXhi6mV8YGRvH12', '', 1, 1563073390, NULL, NULL),
(2031, 10002, 8, '5ernQ97bMwxLFGd', '', 1, 1563073390, NULL, NULL),
(2032, 10002, 8, 'UQoAZd2OOGQr4iG', '', 1, 1563073390, NULL, NULL),
(2033, 10002, 8, 'P17uAs9p2IysPxy', '', 1, 1563073390, NULL, NULL),
(2034, 10002, 8, 'rckNza86mveRCTG', '', 1, 1563073390, NULL, NULL),
(2035, 10002, 8, 'V9G1mLbbEIKaiBT', '', 1, 1563073390, NULL, NULL),
(2036, 10002, 8, 'rScQlKSOopNd6Bv', '', 1, 1563073390, NULL, NULL),
(2037, 10002, 8, 'gtsyYiRzxPENZlU', '', 1, 1563073390, NULL, NULL),
(2038, 10002, 8, 'OoYvme97e0fUxxh', '', 1, 1563073390, NULL, NULL),
(2039, 10002, 8, 'HcGwxGBCHUq4CIF', '', 1, 1563073390, NULL, NULL),
(2040, 10002, 8, 'QNEKTmn0LKa3zkD', '', 1, 1563073390, NULL, NULL),
(2041, 10002, 8, 'WRuRDa1sEaCjPzW', '', 1, 1563073390, NULL, NULL),
(2042, 10002, 8, 'kH9yquPbF9CURKZ', '', 1, 1563073390, NULL, NULL),
(2043, 10002, 8, 'SpE8rRV4h1BD2vk', '', 1, 1563073390, NULL, NULL),
(2044, 10002, 8, '83ovPxjdWYK3jZu', '', 1, 1563073390, NULL, NULL),
(2045, 10002, 8, 'Tl8H2PAF3QccnLt', '', 1, 1563073390, NULL, NULL),
(2046, 10002, 8, 'ZjpbNIttevE3XMP', '', 1, 1563073390, NULL, NULL),
(2047, 10002, 8, 'kd0LDWcDDHYV5lr', '', 1, 1563073390, NULL, NULL),
(2048, 10002, 8, '756GfqWCkev9p4a', '', 1, 1563073390, NULL, NULL),
(2049, 10002, 8, 'MS7OI6VSgsvi2PO', '', 1, 1563073390, NULL, NULL),
(2050, 10002, 8, '6NDW9d8OVKn5uKO', '', 1, 1563073390, NULL, NULL),
(2051, 10002, 8, '5folY13j31eF3aP', '', 1, 1563073390, NULL, NULL),
(2052, 10002, 8, '2Prt6VB8LL1estd', '', 1, 1563073390, NULL, NULL),
(2053, 10002, 8, 'ojKdqT8UGFXTulj', '', 1, 1563073390, NULL, NULL),
(2054, 10002, 8, 'qCRv5Xt0GSWE9SC', '', 1, 1563073390, NULL, NULL),
(2055, 10002, 8, 'YKNqIhvoLfBS4qy', '', 1, 1563073390, NULL, NULL),
(2056, 10002, 8, 'hQKUEsg0zOn3ajZ', '', 1, 1563073390, NULL, NULL),
(2057, 10002, 8, 'bq3vnDIWpUfCvFG', '', 1, 1563073390, NULL, NULL),
(2058, 10002, 8, 'xgS0bCM9mmOor65', '', 1, 1563073390, NULL, NULL),
(2059, 10002, 8, 'bCpiLurB3dvVBgj', '', 1, 1563073390, NULL, NULL),
(2060, 10002, 8, 'A3i51ech8S6tLYl', '', 1, 1563073390, NULL, NULL),
(2061, 10002, 8, 'PXNa4BhehANFIQv', '', 1, 1563073390, NULL, NULL),
(2062, 10002, 8, 'htP7ir1uUHaGsbP', '', 1, 1563073390, NULL, NULL),
(2063, 10002, 8, 'BbjHFm5eTIFteqS', '', 1, 1563073390, NULL, NULL),
(2064, 10002, 8, 'xYYQw0XIiGltcT6', '', 1, 1563073390, NULL, NULL),
(2065, 10002, 8, 'M0IY1nbQKqptOub', '', 1, 1563073390, NULL, NULL),
(2066, 10002, 8, 'SE3cbadjo7L67gF', '', 1, 1563073390, NULL, NULL),
(2067, 10002, 8, 'eGVXQKSEPmb8mQD', '', 1, 1563073390, NULL, NULL),
(2068, 10002, 8, 'GnSoI3GRqnT3You', '', 1, 1563073390, NULL, NULL),
(2069, 10002, 8, 'UG6FnoIZHXGMMYD', '', 1, 1563073390, NULL, NULL),
(2070, 10002, 8, 'SxlH2UrXcEP5hfG', '', 1, 1563073390, NULL, NULL),
(2071, 10002, 8, 'yQzvDlIMcVdxK5L', '', 1, 1563073390, NULL, NULL),
(2072, 10002, 8, 'K1GxbqGSjGvvWuk', '', 1, 1563073390, NULL, NULL),
(2073, 10002, 8, 'Qe6xm5Nl6BMNMfa', '', 1, 1563073390, NULL, NULL),
(2074, 10002, 8, 'f8L1FNvUqld2pGH', '', 1, 1563073390, NULL, NULL),
(2075, 10002, 8, 'GgqPL07iQREpujX', '', 1, 1563073390, NULL, NULL),
(2076, 10002, 8, '78XYl35Yc5eviWl', '', 1, 1563073390, NULL, NULL),
(2077, 10002, 8, 'frONq3NckonWEqp', '', 1, 1563073390, NULL, NULL),
(2078, 10002, 8, 'a2Ue8qtfNTsemTl', '', 1, 1563073390, NULL, NULL),
(2079, 10002, 8, '0nXC33Yot0EdGQG', '', 1, 1563073390, NULL, NULL),
(2080, 10002, 8, 'YBBXBmfTIZg35GX', '', 1, 1563073390, NULL, NULL),
(2081, 10002, 8, 'KzpZbi9up0NBcuV', '', 1, 1563073390, NULL, NULL),
(2082, 10002, 8, '9eQ5wMNXzLK0jBB', '', 1, 1563073390, NULL, NULL),
(2083, 10002, 8, 'je7dwtbRPOsdmCP', '', 1, 1563073390, NULL, NULL),
(2084, 10002, 8, 'MewRt6q3OB8PRLU', '', 1, 1563073390, NULL, NULL),
(2085, 10002, 8, 'coAFsgKntjUpfl3', '', 1, 1563073390, NULL, NULL),
(2086, 10002, 8, 'vuT6BjjCsZ8CAvH', '', 1, 1563073390, NULL, NULL),
(2087, 10002, 8, 'jv3KyqDkUS09UIR', '', 1, 1563073390, NULL, NULL),
(2088, 10002, 8, 'YUx8GiQAgwVWrjR', '', 1, 1563073390, NULL, NULL),
(2089, 10002, 8, 'Tw6jRespG00G4iL', '', 1, 1563073390, NULL, NULL),
(2090, 10002, 8, 'rSwMSdgtVMDOB7d', '', 1, 1563073390, NULL, NULL),
(2091, 10002, 8, 'aRS6uUp3iqu09HK', '', 1, 1563073390, NULL, NULL),
(2092, 10002, 8, 'vgdeM5jGdzviFN4', '', 1, 1563073390, NULL, NULL),
(2093, 10002, 8, 'Z2oisggvk5rO5dE', '', 1, 1563073390, NULL, NULL),
(2094, 10002, 8, 'DQx9QnpuwEbnYtK', '', 1, 1563073390, NULL, NULL),
(2095, 10002, 8, 'kL5eR7YNcrbwQ4k', '', 1, 1563073390, NULL, NULL),
(2096, 10002, 8, 'yfnaNIIAmIo5Fck', '', 1, 1563073390, NULL, NULL),
(2097, 10002, 8, 'NnKDOpAQymDF7Sa', '', 1, 1563073390, NULL, NULL),
(2098, 10002, 8, 'yVPisOOcqnHgVwk', '', 1, 1563073390, NULL, NULL),
(2099, 10002, 8, 'h3S5A95YA7IypbG', '', 1, 1563073390, NULL, NULL),
(2100, 10002, 8, '9FZ6SreK3kkIF4h', '', 1, 1563073390, NULL, NULL),
(2101, 10002, 8, '77BarB5wgiOVQep', '', 1, 1563073390, NULL, NULL),
(2102, 10002, 8, 'Z4Q2XShghpzSgtZ', '', 1, 1563073390, NULL, NULL),
(2103, 10002, 8, 'd1ybNKG7GKVzFno', '', 1, 1563073390, NULL, NULL),
(2104, 10002, 8, 'hf0GRHdGo3IY7gL', '', 1, 1563073390, NULL, NULL),
(2105, 10002, 8, 'P3GgeaLwqnsOxF6', '', 1, 1563073390, NULL, NULL),
(2106, 10002, 8, 'wQBMge5yRBPz3eK', '', 1, 1563073390, NULL, NULL),
(2107, 10002, 8, 'kswiD76alvZhSjS', '', 1, 1563073390, NULL, NULL),
(2108, 10002, 8, 'VZSrYtIsj3GzSPB', '', 1, 1563073390, NULL, NULL),
(2109, 10002, 8, 'eYK8qcy27UaMSOQ', '', 1, 1563073390, NULL, NULL),
(2110, 10002, 8, '2x9UOLO536Vrswp', '', 1, 1563073390, NULL, NULL),
(2111, 10002, 8, '2KCXc9Uypntqcpg', '', 1, 1563073390, NULL, NULL),
(2112, 10002, 8, 'm1hUG2b5coUSIes', '', 1, 1563073390, NULL, NULL),
(2113, 10002, 8, 'ftCAI4Pk7eHWeoU', '', 1, 1563073390, NULL, NULL),
(2114, 10002, 8, 'j4j0VBvDNOE55E8', '', 1, 1563073390, NULL, NULL),
(2115, 10002, 8, 'sQQD6iyWn5vXGHD', '', 1, 1563073390, NULL, NULL),
(2116, 10002, 8, '5uRMsn6Pn1ijPOW', '', 1, 1563073390, NULL, NULL),
(2117, 10002, 8, 'yD3THpW4YiNtiiS', '', 1, 1563073390, NULL, NULL),
(2118, 10002, 8, 'yTesbYQlyMYFiuv', '', 1, 1563073390, NULL, NULL);

-- --------------------------------------------------------

--
-- 表的结构 `goods_category`
--

CREATE TABLE `goods_category` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(100) NOT NULL,
  `sort` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `status` tinyint(4) NOT NULL,
  `create_at` int(10) UNSIGNED NOT NULL,
  `theme` varchar(15) NOT NULL DEFAULT 'default' COMMENT '主题'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `goods_category`
--

INSERT INTO `goods_category` (`id`, `user_id`, `name`, `sort`, `status`, `create_at`, `theme`) VALUES
(2, 10002, '天赐传奇团队', 0, 1, 1562845308, 'default'),
(3, 10002, 'SM号Vx，带支付密码可进大群', 1, 1, 1563032586, 'default'),
(4, 10002, '半年设备SM老号Vx，带支付密码可进大群', 2, 1, 1563032713, 'default'),
(5, 10002, '一年设备SM老号Vx，带支付密码可进大群', 3, 1, 1563032809, 'default'),
(6, 10002, 'Vx绑卡服务-直绑', 4, 1, 1563072915, 'default');

-- --------------------------------------------------------

--
-- 表的结构 `goods_coupon`
--

CREATE TABLE `goods_coupon` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `cate_id` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT '全部',
  `type` tinyint(3) UNSIGNED NOT NULL DEFAULT '1' COMMENT '类型 1、元  100、%',
  `amount` decimal(10,2) UNSIGNED NOT NULL DEFAULT '0.00',
  `code` varchar(255) NOT NULL DEFAULT '',
  `remark` varchar(100) NOT NULL DEFAULT '' COMMENT '备注',
  `status` tinyint(3) UNSIGNED NOT NULL DEFAULT '1' COMMENT '1未使用 2已用',
  `expire_at` int(10) UNSIGNED NOT NULL,
  `create_at` int(10) UNSIGNED NOT NULL,
  `delete_at` int(11) DEFAULT NULL COMMENT '删除标记'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `invite_code`
--

CREATE TABLE `invite_code` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL COMMENT '所有者ID',
  `code` char(32) NOT NULL DEFAULT '' COMMENT '邀请码',
  `status` tinyint(3) UNSIGNED NOT NULL DEFAULT '0' COMMENT '使用状态 0未使用 1已使用',
  `invite_uid` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT '受邀用户ID',
  `invite_at` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT '邀请时间',
  `create_at` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT '创建时间',
  `expire_at` int(10) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `link`
--

CREATE TABLE `link` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `relation_type` varchar(20) NOT NULL DEFAULT '',
  `relation_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `token` char(16) NOT NULL DEFAULT '',
  `url` varchar(100) NOT NULL DEFAULT '',
  `short_url` varchar(30) NOT NULL,
  `status` tinyint(3) UNSIGNED NOT NULL,
  `create_at` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `link`
--

INSERT INTO `link` (`id`, `user_id`, `relation_type`, `relation_id`, `token`, `url`, `short_url`, `status`, `create_at`) VALUES
(2, 10001, 'goods', 1, '4A9BAC76', '', 'http://t.cn/EhdF9gu', 1, 1538990191),
(3, 10001, 'goods', 2, '9512A536', '', 'http://t.cn/EhdsVgM', 1, 1538990308),
(4, 10001, 'goods', 3, '410BB69A', '', 'http://t.cn/EhdsnoQ', 1, 1538990374),
(5, 10001, 'user', 10001, '3320B5AF', '', 'http://t.cn/EhgZPLY', 1, 1538991202),
(8, 10002, 'goods', 4, 'F619F925', '', 'http://t.cn/AiWZawjT', 1, 1562845348),
(9, 10002, 'goods_category', 2, 'A3E5077605429A22', '', 'http://t.cn/AiWZTur6', 1, 1562847895),
(10, 10002, 'user', 10002, '89C96E18', '', 'http://t.cn/AiW6XdIb', 1, 1563032895),
(11, 10002, 'goods', 5, 'DDC0377A', '', 'http://t.cn/AiWaMrv6', 1, 1563063705),
(12, 10002, 'goods', 6, '80620795', '', 'http://t.cn/AiWaxxVm', 1, 1563063795),
(13, 10002, 'goods', 7, '9DDD2C17', '', 'http://t.cn/AiWax8ev', 1, 1563063848),
(14, 10002, 'goods', 8, '2F6AB710', '', 'http://t.cn/AiWS1gBO', 1, 1563073129),
(15, 10002, 'goods', 9, '4BD5F0D6', '', 'http://t.cn/AiWSBUS6', 1, 1563073158);

-- --------------------------------------------------------

--
-- 表的结构 `log`
--

CREATE TABLE `log` (
  `id` int(10) UNSIGNED NOT NULL,
  `business_type` varchar(20) NOT NULL DEFAULT '' COMMENT '业务类型',
  `content` text NOT NULL COMMENT '内容',
  `ua` varchar(255) NOT NULL DEFAULT '',
  `uri` varchar(255) NOT NULL DEFAULT '',
  `create_at` int(10) UNSIGNED NOT NULL COMMENT '记录时间',
  `create_ip` varchar(15) NOT NULL DEFAULT '' COMMENT 'ip'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `merchant_log`
--

CREATE TABLE `merchant_log` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `ip` char(15) NOT NULL DEFAULT '' COMMENT '操作者IP地址',
  `node` char(200) NOT NULL DEFAULT '' COMMENT '当前操作节点',
  `user_id` int(11) NOT NULL DEFAULT '0' COMMENT '操作人用户ID',
  `username` varchar(32) NOT NULL DEFAULT '' COMMENT '用户名',
  `action` varchar(200) NOT NULL DEFAULT '' COMMENT '操作行为',
  `content` text NOT NULL COMMENT '操作内容描述',
  `create_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='系统操作日志表';

-- --------------------------------------------------------

--
-- 表的结构 `message`
--

CREATE TABLE `message` (
  `id` int(10) UNSIGNED NOT NULL,
  `from_id` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT '0为管理员',
  `to_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `title` varchar(60) NOT NULL DEFAULT '',
  `content` varchar(1024) NOT NULL DEFAULT '',
  `status` tinyint(3) NOT NULL DEFAULT '0' COMMENT '0未读  1已读',
  `create_at` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT '发送时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- 转存表中的数据 `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(53, '20180428_alter_migrations_table.sql', 0),
(54, '20180428_create_foo_table.sql', 0),
(55, '20180428_drop_foo_table.sql', 0),
(56, '20180502_create_foo_table.sql', 1),
(57, '20180502_drop_foo_table.sql', 2),
(58, '20180507_alter_table.sql', 3),
(59, '20180509_alter_order_table_goods_table.sql', 3),
(60, '20180512_alter_user_money_log_table.sql', 4),
(61, '20180512_alter_user_table.sql', 4),
(62, '20180513_add_auto_unfreeze_table.sql', 4),
(63, '20180513_alter_user_table.sql', 5),
(64, '20180513alter_goods_card_table.sql', 5),
(65, '20180515_alter_auto_unfreeze_table.sql', 5),
(66, '20180523alter_order_card_table.sql', 6);

-- --------------------------------------------------------

--
-- 表的结构 `nav`
--

CREATE TABLE `nav` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `pid` bigint(20) UNSIGNED NOT NULL DEFAULT '0' COMMENT '父id',
  `title` varchar(100) NOT NULL DEFAULT '' COMMENT '名称',
  `node` varchar(200) NOT NULL DEFAULT '' COMMENT '节点代码',
  `icon` varchar(100) NOT NULL DEFAULT '' COMMENT '菜单图标',
  `url` varchar(400) NOT NULL DEFAULT '' COMMENT '链接',
  `params` varchar(500) DEFAULT '' COMMENT '链接参数',
  `target` varchar(20) NOT NULL DEFAULT '_self' COMMENT '链接打开方式',
  `sort` int(11) UNSIGNED DEFAULT '0' COMMENT '菜单排序',
  `status` tinyint(1) UNSIGNED NOT NULL DEFAULT '1' COMMENT '状态(0:禁用,1:启用)',
  `create_by` bigint(20) UNSIGNED NOT NULL DEFAULT '0' COMMENT '创建人',
  `create_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='前台导航表';

--
-- 转存表中的数据 `nav`
--

INSERT INTO `nav` (`id`, `pid`, `title`, `node`, `icon`, `url`, `params`, `target`, `sort`, `status`, `create_by`, `create_at`) VALUES
(1, 0, '网站首页', '', '', '/', '', '0', 6, 1, 0, '2018-03-23 01:20:50'),
(2, 0, '常见问题', '', '', '/company/faq', '', '0', 4, 1, 0, '2018-03-23 01:21:11'),
(3, 0, '联系我们', '', '', '/company/contact', '', '0', 1, 1, 0, '2018-03-23 01:21:35'),
(4, 0, '订单查询', '', '', '/orderquery', '', '0', 3, 1, 0, '2018-03-23 01:22:09'),
(5, 0, '企业资质', '', '', '/article/14.html', '', '0', 5, 1, 0, '2018-03-23 03:28:55'),
(6, 0, '用户注册', '', '', '/register', '', '1', 0, 0, 0, '2018-05-10 04:24:37'),
(7, 0, '投诉查询', '', '', '/complaintquery', '', '_self', 2, 1, 0, '2018-06-21 02:53:00');

-- --------------------------------------------------------

--
-- 表的结构 `order`
--

CREATE TABLE `order` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `goods_id` int(10) UNSIGNED NOT NULL,
  `trade_no` varchar(255) DEFAULT NULL,
  `transaction_id` varchar(60) NOT NULL DEFAULT '' COMMENT '流水号',
  `paytype` tinyint(3) UNSIGNED NOT NULL DEFAULT '0',
  `channel_id` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT '支付渠道',
  `channel_account_id` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT '渠道账号',
  `pay_url` varchar(10240) NOT NULL DEFAULT '' COMMENT '付款地址',
  `pay_content_type` tinyint(1) NOT NULL DEFAULT '0' COMMENT '支付参数类型 1：二维码 2：跳转链接 3：表单',
  `goods_name` varchar(500) NOT NULL DEFAULT '' COMMENT '商品名称',
  `goods_price` decimal(10,2) UNSIGNED NOT NULL DEFAULT '0.00' COMMENT '商品单价',
  `goods_cost_price` decimal(10,2) UNSIGNED NOT NULL DEFAULT '0.00' COMMENT '成本价',
  `quantity` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT '数量',
  `coupon_type` tinyint(3) UNSIGNED NOT NULL DEFAULT '0' COMMENT '是否使用优惠券',
  `coupon_id` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT '优惠券ID',
  `coupon_price` decimal(10,2) UNSIGNED NOT NULL DEFAULT '0.00' COMMENT '优惠价格',
  `total_price` decimal(10,2) UNSIGNED NOT NULL DEFAULT '0.00' COMMENT '总价（买家实付款）',
  `total_cost_price` decimal(10,2) UNSIGNED NOT NULL DEFAULT '0.00' COMMENT '总成本价',
  `sold_notify` tinyint(3) UNSIGNED NOT NULL DEFAULT '0' COMMENT '售出通知（买家）',
  `take_card_type` tinyint(3) UNSIGNED NOT NULL DEFAULT '0' COMMENT '是否需要取卡密码',
  `take_card_password` varchar(20) NOT NULL DEFAULT '' COMMENT '取卡密码',
  `contact` varchar(20) NOT NULL DEFAULT '' COMMENT '联系方式',
  `email_notify` tinyint(3) UNSIGNED NOT NULL DEFAULT '0' COMMENT '是否邮件通知',
  `email` varchar(60) NOT NULL DEFAULT '' COMMENT '邮箱号',
  `sms_notify` tinyint(3) UNSIGNED NOT NULL DEFAULT '0' COMMENT '是否短信通知',
  `rate` decimal(10,4) UNSIGNED NOT NULL DEFAULT '0.0000' COMMENT '手续费率',
  `fee` decimal(10,3) UNSIGNED NOT NULL DEFAULT '0.000' COMMENT '手续费',
  `agent_rate` decimal(10,4) UNSIGNED NOT NULL DEFAULT '0.0000' COMMENT '代理费率',
  `agent_fee` decimal(10,3) UNSIGNED NOT NULL DEFAULT '0.000' COMMENT '代理佣金',
  `sms_price` decimal(10,2) NOT NULL DEFAULT '0.00',
  `status` tinyint(3) NOT NULL DEFAULT '0' COMMENT '订单状态 0未支付 1已支付 2已关闭',
  `is_freeze` tinyint(4) NOT NULL DEFAULT '0',
  `create_at` int(10) UNSIGNED NOT NULL COMMENT '创建时间',
  `create_ip` varchar(15) NOT NULL DEFAULT '',
  `success_at` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT '支付成功时间',
  `first_query` tinyint(4) NOT NULL DEFAULT '0' COMMENT '订单第一次查询无需验证码',
  `sms_payer` tinyint(3) UNSIGNED NOT NULL DEFAULT '0' COMMENT '短信付费方：0买家 1商户',
  `total_product_price` decimal(10,2) UNSIGNED NOT NULL DEFAULT '0.00' COMMENT '商品总价（不含短信费）',
  `sendout` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT '已发货数量',
  `fee_payer` tinyint(4) NOT NULL DEFAULT '1' COMMENT '订单手续费支付方，1：商家承担，2买家承担',
  `settlement_type` tinyint(4) UNSIGNED NOT NULL DEFAULT '1' COMMENT '结算方式，1:T1结算，0:T0结算',
  `finally_money` decimal(10,4) NOT NULL DEFAULT '0.0000' COMMENT '商户订单最终收入（已扣除短信费，手续费）'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `order_card`
--

CREATE TABLE `order_card` (
  `id` int(10) UNSIGNED NOT NULL,
  `order_id` int(10) UNSIGNED NOT NULL,
  `number` varchar(500) NOT NULL DEFAULT '',
  `secret` varchar(500) NOT NULL DEFAULT '',
  `card_id` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `order_card`
--

INSERT INTO `order_card` (`id`, `order_id`, `number`, `secret`, `card_id`) VALUES
(1, 1, 'AAAAAAAAAAA', 'BBBBBBBBBBBB\n', 1),
(2, 4, '111111111', '', 114),
(3, 6, 'adaadadfgffffffffffffffffff', '', 115),
(4, 14, 'dadadadadadssss', '', 116),
(5, 13, 'ssfdfafcxvxvxxvxvx', '', 117);

-- --------------------------------------------------------

--
-- 表的结构 `pay_type`
--

CREATE TABLE `pay_type` (
  `id` int(11) UNSIGNED NOT NULL,
  `name` varchar(100) NOT NULL COMMENT '支付名',
  `product_id` tinyint(3) NOT NULL DEFAULT '0' COMMENT '支付类型',
  `logo` varchar(100) NOT NULL DEFAULT '' COMMENT 'logo',
  `ico` varchar(100) NOT NULL DEFAULT '' COMMENT '图标',
  `is_mobile` tinyint(3) NOT NULL DEFAULT '0' COMMENT '是否手机支付',
  `is_form_data` tinyint(3) NOT NULL DEFAULT '0' COMMENT '是否 form 提交',
  `target` tinyint(3) NOT NULL DEFAULT '0' COMMENT '银行支付使用，未知作用',
  `sub_lists` text COMMENT '银行支付使用，指定支持的银行列表'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='支付类型表';

--
-- 转存表中的数据 `pay_type`
--

INSERT INTO `pay_type` (`id`, `name`, `product_id`, `logo`, `ico`, `is_mobile`, `is_form_data`, `target`, `sub_lists`) VALUES
(1, '微信扫码', 2, 'icon_wx', 'icon_wx', 0, 0, 0, ''),
(2, '微信H5', 2, 'icon_wx', 'icon_wx', 1, 0, 0, ''),
(3, '支付宝扫码', 1, 'icon_zfb', 'icon_zfb', 0, 1, 0, ''),
(4, '支付宝H5', 1, 'icon_zfb', 'icon_zfb', 1, 0, 0, ''),
(5, '网银跳转', 6, 'icon_bank', 'icon_bank', 0, 0, 0, ''),
(6, '网银直连', 6, 'icon_bank', 'icon_bank', 0, 0, 0, ''),
(7, '百度钱包', 5, 'baidu_logo', 'baidu_ico', 0, 0, 0, ''),
(8, 'QQ钱包扫码', 3, 'qqrcode', 'qqrcode', 0, 0, 0, ''),
(9, '京东钱包', 4, 'jd_logo', 'jd_ico', 0, 0, 0, ''),
(10, 'QQ钱包H5', 3, 'icon_qq', 'icon_qq', 1, 0, 0, ''),
(11, '支付宝PC', 1, 'icon_zfb', 'icon_zfb', 0, 0, 0, ''),
(12, '微信刷卡', 2, 'icon_wx', 'icon_wx', 0, 0, 0, ''),
(13, '支付宝刷卡', 1, 'icon_zfb', 'icon_zfb', 0, 0, 0, ''),
(14, '支付宝免签', 1, 'icon_zfb', 'icon_zfb', 0, 0, 0, ''),
(15, '微信免签', 2, 'icon_wx', 'icon_wx', 0, 0, 0, ''),
(16, '微极速微信支付', 2, 'icon_wx', 'icon_wx', 1, 1, 0, ''),
(17, '15173PC微信支付', 2, 'icon_wx', 'icon_wx', 0, 0, 0, ''),
(18, '15173Wap微信支付', 2, 'icon_wx', 'icon_wx', 1, 0, 0, ''),
(19, '拉卡微信扫码支付', 2, 'icon_wx', 'icon_wx', 0, 0, 0, ''),
(20, '拉卡微信h5支付', 2, 'icon_wx', 'icon_wx', 1, 0, 0, ''),
(21, '拉卡支付宝扫码支付', 1, 'icon_zfb', 'icon_zfb', 0, 0, 0, ''),
(22, '快接微信扫码支付', 2, 'icon_wx', 'icon_wx', 0, 0, 0, ''),
(23, '快接微信H5支付', 2, 'icon_wx', 'icon_wx', 1, 0, 0, ''),
(24, '快接支付宝扫码支付', 1, 'icon_zfb', 'icon_zfb', 0, 0, 0, ''),
(25, '快接支付宝扫码支付(线上二维码)', 1, 'icon_zfb', 'icon_zfb', 0, 0, 0, ''),
(26, '快接支付宝H5支付', 1, 'icon_zfb', 'icon_zfb', 1, 0, 0, ''),
(27, '微信官方H5支付', 2, 'icon_wx', 'icon_wx', 1, 0, 0, ''),
(28, '拉卡QQ支付', 3, 'icon_qq', 'icon_qq', 0, 0, 0, ''),
(29, '拉卡银联快捷', 6, 'icon_bank', 'icon_bank', 0, 0, 0, '{\"1\":{\"name\":\"\\u4e2d\\u56fd\\u5de5\\u5546\\u94f6\\u884c\",\"logo\":\"icon_bank\",\"ico\":\"icon_bank\",\"is_mobile\":0,\"target\":0,\"is_form_data\":0,\"code\":10001},\"2\":{\"name\":\"\\u4e2d\\u56fd\\u519c\\u4e1a\\u94f6\\u884c\",\"logo\":\"icon_zgnyyh\",\"ico\":\"icon_zgnyyh\",\"is_mobile\":0,\"target\":0,\"is_form_data\":0,\"code\":10002}}'),
(30, '12kaQQ钱包扫码', 3, 'icon_qq', 'icon_qq', 0, 0, 0, ''),
(31, '12kaQQWap', 3, 'icon_qq', 'icon_qq', 0, 0, 0, ''),
(32, '12ka网银快捷', 6, 'icon_bank', 'icon_bank', 0, 0, 0, ''),
(33, '12ka网银Wap', 6, 'icon_bank', 'icon_bank', 0, 0, 0, ''),
(34, '12ka支付宝扫码', 1, 'icon_zfb', 'icon_zfb', 0, 0, 0, ''),
(35, '12ka支付宝wap', 1, 'icon_zfb', 'icon_zfb', 0, 0, 0, ''),
(36, '12ka微信扫码', 2, 'icon_wx', 'icon_wx', 0, 0, 0, ''),
(37, '12ka微信wap', 2, 'icon_wx', 'icon_wx', 0, 0, 0, ''),
(38, '码支付微信扫码', 2, 'icon_wx', 'icon_wx', 0, 0, 0, ''),
(39, '15173Qq扫码支付', 3, 'icon_qq', 'icon_qq', 0, 0, 0, ''),
(40, '码支付QQ扫码', 3, 'icon_qq', 'icon_qq', 0, 0, 0, ''),
(41, '码支付支付宝扫码', 1, 'icon_zfb', 'icon_zfb', 0, 0, 0, ''),
(42, '蜂鸟支付宝扫码', 1, 'icon_zfb', 'icon_zfb', 0, 0, 0, ''),
(43, '蜂鸟支付宝WAP', 1, 'icon_zfb', 'icon_zfb', 0, 0, 0, ''),
(44, '蜂鸟微信扫码', 2, 'icon_wx', 'icon_wx', 0, 0, 0, ''),
(45, '蜂鸟微信公众号支付', 2, 'icon_wx', 'icon_wx', 0, 0, 0, ''),
(46, '蜂鸟QQ钱包扫码', 3, 'icon_qq', 'icon_qq', 0, 0, 0, ''),
(47, '掌灵付微信扫码', 2, 'icon_wx', 'icon_wx', 0, 1, 0, ''),
(48, '掌灵付QQ扫码', 3, 'qqrcode', 'qqrcode', 0, 1, 0, ''),
(49, '掌灵付支付宝扫码', 1, 'icon_zfb', 'icon_zfb', 0, 1, 0, ''),
(50, '掌灵付微信公众号支付', 2, 'icon_wx', 'icon_wx', 0, 1, 0, ''),
(51, '黔贵金服支付宝扫码', 1, 'icon_zfb', 'icon_zfb', 0, 1, 0, ''),
(52, '黔贵金服微信扫码', 2, 'icon_wx', 'icon_wx', 0, 1, 0, ''),
(53, '黔贵金服QQ扫码', 3, 'qqrcode', 'qqrcode', 0, 1, 0, ''),
(54, '黔贵金服微信公众号支付', 2, 'icon_wx', 'icon_wx', 0, 1, 0, ''),
(55, '黔贵金服支付宝Wap', 1, 'icon_zfb', 'icon_zfb', 0, 1, 0, ''),
(56, '黔贵金服微信WAP', 2, 'icon_wx', 'icon_wx', 0, 1, 0, ''),
(57, '点缀支付宝即时到账', 1, 'icon_zfb', 'icon_zfb', 0, 0, 0, ''),
(58, '点缀支付京东钱包扫码', 4, 'icon_jd', 'icon_jd', 0, 0, 0, ''),
(59, '掌灵付微信H5', 2, 'icon_wx', 'icon_wx', 0, 0, 0, ''),
(60, '掌灵付京东H5', 4, 'icon_jd', 'icon_jd', 0, 0, 0, ''),
(61, '点缀微信 H5', 2, 'icon_wx', 'icon_wx', 0, 0, 0, ''),
(62, '优畅上海微信 H5', 2, 'icon_wx', 'icon_wx', 0, 0, 0, ''),
(63, '优畅上海微信扫码', 2, 'icon_wx', 'icon_wx', 0, 0, 0, ''),
(64, '优畅上海微信公众号', 2, 'icon_wx', 'icon_wx', 0, 0, 0, ''),
(65, 'Topay微信', 2, 'icon_wx', 'icon_wx', 0, 0, 0, ''),
(66, 'Topay 支付宝', 1, 'icon_zfb', 'icon_zfb', 0, 0, 0, ''),
(67, '海鸟微信公众号', 2, 'icon_wx', 'icon_wx', 0, 0, 0, ''),
(68, '海鸟微信扫码', 2, 'icon_wx', 'icon_wx', 0, 0, 0, ''),
(69, '海鸟微信 H5', 2, 'icon_wx', 'icon_wx', 0, 0, 0, ''),
(70, '海鸟 QQ 扫码', 3, 'icon_qq', 'icon_qq', 0, 0, 0, ''),
(71, '海鸟支付宝扫码', 1, 'icon_zfb', 'icon_zfb', 0, 0, 0, ''),
(72, '淘米支付宝扫码', 1, 'icon_zfb', 'icon_zfb', 0, 1, 0, ''),
(73, '淘米微信扫码', 2, 'icon_wx', 'icon_wx', 0, 1, 0, ''),
(74, '淘米QQ扫码', 3, 'qqrcode', 'qqrcode', 0, 1, 0, ''),
(75, '淘米微信公众号支付', 2, 'icon_wx', 'icon_wx', 0, 1, 0, ''),
(76, '淘米支付宝Wap', 1, 'icon_zfb', 'icon_zfb', 0, 1, 0, ''),
(77, '淘米微信WAP', 2, 'icon_wx', 'icon_wx', 0, 1, 0, ''),
(78, '完美数卡微信扫码', 2, 'icon_wx', 'icon_wx', 0, 1, 0, ''),
(79, '完美数卡支付宝扫码', 1, 'icon_zfb', 'icon_zfb', 0, 1, 0, ''),
(80, '完美数卡QQ扫码', 3, 'icon_qq', 'icon_qq', 0, 1, 0, ''),
(81, '完美数卡QQWap', 3, 'icon_qq', 'icon_qq', 0, 1, 0, ''),
(82, '完美数卡微信Wap', 2, 'icon_wx', 'icon_wx', 0, 1, 0, ''),
(83, '完美数卡支付宝Wap', 1, 'icon_zfb', 'icon_zfb', 0, 1, 0, ''),
(84, '支付宝免签', 1, 'icon_zfb', 'icon_zfb', 0, 1, 0, ''),
(85, 'qq免签', 3, 'icon_qq', 'icon_qq', 0, 1, 0, ''),
(86, '微信免签', 2, 'icon_wx', 'icon_wx', 0, 1, 0, ''),
(87, '掌灵付支付宝扫码', 1, 'icon_zfb', 'icon_zfb', 0, 1, 0, ''),
(88, '牛支付支付宝扫码', 1, 'icon_zfb', 'icon_zfb', 0, 1, 0, ''),
(89, '掌灵付京东扫码', 4, 'icon_jd', 'icon_jd', 0, 0, 0, ''),
(90, '优畅上海支付宝扫码', 1, 'icon_zfb', 'icon_zfb', 0, 0, 0, ''),
(91, '优畅上海支付宝 Wap', 1, 'icon_zfb', 'icon_zfb', 0, 0, 0, ''),
(92, '威富通支付宝扫码', 1, 'icon_zfb', 'icon_zfb', 0, 0, 0, ''),
(93, '威富通支付宝Wap', 1, 'icon_zfb', 'icon_zfb', 0, 0, 0, ''),
(94, '威富通微信扫码', 2, 'icon_wx', 'icon_wx', 0, 0, 0, ''),
(95, '威富通京东扫码', 4, 'icon_jd', 'icon_jd', 0, 0, 0, ''),
(96, '威富通微信wap', 2, 'icon_wx', 'icon_wx', 0, 0, 0, ''),
(97, '威富通微信公众号', 2, 'icon_wx', 'icon_wx', 0, 0, 0, ''),
(98, '汉口银行微信公众号', 2, 'icon_wx', 'icon_wx', 0, 0, 0, ''),
(99, '汉口银行微信扫码', 2, 'icon_wx', 'icon_wx', 0, 0, 0, ''),
(100, '汉口银行微信wap', 2, 'icon_wx', 'icon_wx', 0, 0, 0, ''),
(101, '汉口银行支付宝扫码', 1, 'icon_zfb', 'icon_zfb', 0, 0, 0, ''),
(102, '汉口银行支付宝Wap', 1, 'icon_zfb', 'icon_zfb', 0, 0, 0, ''),
(103, '优畅上海QQ扫码', 3, 'icon_qq', 'icon_qq', 0, 0, 0, ''),
(104, '优畅上海QQWap', 3, 'icon_qq', 'icon_qq', 0, 0, 0, ''),
(105, '平安付微信扫码', 2, 'icon_wx', 'icon_wx', 0, 0, 0, ''),
(106, '平安付支付宝扫码', 1, 'icon_zfb', 'icon_zfb', 0, 0, 0, ''),
(107, '平安付支付宝wap', 1, 'icon_zfb', 'icon_zfb', 0, 0, 0, ''),
(108, '平安付微信wap', 2, 'icon_wx', 'icon_wx', 0, 0, 0, ''),
(109, '网商银行微信', 2, 'icon_wx', 'icon_wx', 0, 0, 0, ''),
(110, '网商银行支付宝', 1, 'icon_zfb', 'icon_zfb', 0, 0, 0, ''),
(111, '吉易支付微信扫码', 2, 'icon_wx', 'icon_wx', 0, 0, 0, ''),
(112, '吉易支付微信公众号', 2, 'icon_wx', 'icon_wx', 0, 0, 0, ''),
(113, 'PayApi支付宝', 2, 'icon_zfb', 'icon_zfb', 0, 0, 0, ''),
(114, 'PayApi微信', 2, 'icon_wx', 'icon_wx', 0, 0, 0, ''),
(115, '易云支付宝扫码', 1, 'icon_zfb', 'icon_zfb', 0, 0, 0, ''),
(116, '易云支付宝WAP', 1, 'icon_zfb', 'icon_zfb', 0, 0, 0, ''),
(117, '易云微信扫码', 2, 'icon_wx', 'icon_wx', 0, 0, 0, ''),
(118, '易云微信WAP', 2, 'icon_wx', 'icon_wx', 0, 0, 0, ''),
(119, '易云微信公众号', 2, 'icon_wx', 'icon_wx', 0, 0, 0, ''),
(120, '恒隆支付宝扫码', 1, 'icon_zfb', 'icon_zfb', 0, 0, 0, ''),
(121, '恒隆支付宝WAP', 1, 'icon_zfb', 'icon_zfb', 0, 0, 0, ''),
(122, '恒隆微信公众号支付', 2, 'icon_wx', 'icon_wx', 0, 0, 0, ''),
(123, '恒隆微信扫码', 2, 'icon_wx', 'icon_wx', 0, 0, 0, ''),
(124, '深度支付宝扫码', 1, 'icon_zfb', 'icon_zfb', 0, 0, 0, ''),
(125, '深度支付宝服务窗支付', 1, 'icon_zfb', 'icon_zfb', 0, 0, 0, ''),
(126, '深度微信扫码', 2, 'icon_wx', 'icon_wx', 0, 0, 0, ''),
(127, '深度微信公众号', 2, 'icon_wx', 'icon_wx', 0, 0, 0, ''),
(128, '聚合支付', 1, 'icon_zfb', 'icon_zfb', 0, 0, 0, '');

-- --------------------------------------------------------

--
-- 表的结构 `product`
--

CREATE TABLE `product` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) NOT NULL COMMENT '通道名称',
  `code` varchar(50) NOT NULL COMMENT '通道代码',
  `status` tinyint(1) UNSIGNED NOT NULL DEFAULT '0' COMMENT '状态 1 开启 0 关闭',
  `polling` tinyint(1) UNSIGNED NOT NULL DEFAULT '0' COMMENT '接口模式 0单独 1轮询',
  `channel_id` int(10) UNSIGNED NOT NULL,
  `weight` text NOT NULL COMMENT '权重',
  `paytype` tinyint(1) UNSIGNED NOT NULL DEFAULT '0' COMMENT '支付类型 1 微信扫码 2 微信公众号 3 支付宝扫码 4 支付宝手机 5 网银支付 6'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `product`
--

INSERT INTO `product` (`id`, `title`, `code`, `status`, `polling`, `channel_id`, `weight`, `paytype`) VALUES
(901, '微信公众号', 'WXJSAPI', 0, 0, 0, '[]', 2),
(902, '微信扫码支付', 'WXSCAN', 0, 0, 0, '[]', 1),
(903, '支付宝扫码支付', 'ALISCAN', 0, 0, 14, '[]', 3),
(904, '支付宝手机', 'ALIWAP', 0, 0, 15, '[]', 4),
(907, '网银支付', 'DBANK', 0, 0, 0, '[]', 5);

-- --------------------------------------------------------

--
-- 表的结构 `rate_group`
--

CREATE TABLE `rate_group` (
  `id` int(11) UNSIGNED NOT NULL,
  `title` varchar(20) NOT NULL COMMENT '分组名',
  `create_at` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT '创建时间',
  `delete_at` int(10) UNSIGNED DEFAULT NULL COMMENT '删除时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='费率分组表';

-- --------------------------------------------------------

--
-- 表的结构 `rate_group_rule`
--

CREATE TABLE `rate_group_rule` (
  `id` int(11) UNSIGNED NOT NULL,
  `group_id` int(11) NOT NULL COMMENT '分组 ID',
  `channel_id` int(11) NOT NULL COMMENT '渠道 ID',
  `rate` decimal(10,4) NOT NULL DEFAULT '0.0000' COMMENT '渠道费率',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '状态 1：开启 0：关闭'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='分组费率规则';

-- --------------------------------------------------------

--
-- 表的结构 `rate_group_user`
--

CREATE TABLE `rate_group_user` (
  `id` int(11) UNSIGNED NOT NULL,
  `group_id` int(11) NOT NULL COMMENT '分组 ID',
  `user_id` int(11) NOT NULL COMMENT '用户 ID'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='分组内用户表';

-- --------------------------------------------------------

--
-- 表的结构 `sms_code`
--

CREATE TABLE `sms_code` (
  `id` int(10) UNSIGNED NOT NULL,
  `mobile` varchar(15) NOT NULL DEFAULT '' COMMENT '手机号',
  `screen` varchar(30) NOT NULL DEFAULT '' COMMENT '场景',
  `code` char(6) NOT NULL DEFAULT '' COMMENT '验证码',
  `status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '状态：0：未使用 1：已使用',
  `create_at` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT '创建时间',
  `create_ip` varchar(255) NOT NULL DEFAULT '' COMMENT '请求 ip'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `system_auth`
--

CREATE TABLE `system_auth` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(20) NOT NULL COMMENT '权限名称',
  `status` tinyint(1) UNSIGNED DEFAULT '1' COMMENT '状态(1:禁用,2:启用)',
  `sort` smallint(6) UNSIGNED DEFAULT '0' COMMENT '排序权重',
  `desc` varchar(255) DEFAULT NULL COMMENT '备注说明',
  `create_by` bigint(11) UNSIGNED DEFAULT '0' COMMENT '创建人',
  `create_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='系统权限表';

--
-- 转存表中的数据 `system_auth`
--

INSERT INTO `system_auth` (`id`, `title`, `status`, `sort`, `desc`, `create_by`, `create_at`) VALUES
(3, '超级管理员', 1, 0, '超级管理员', 0, '2021-04-10 19:24:49');

-- --------------------------------------------------------

--
-- 表的结构 `system_auth_node`
--

CREATE TABLE `system_auth_node` (
  `auth` bigint(20) UNSIGNED DEFAULT NULL COMMENT '角色ID',
  `node` varchar(200) DEFAULT NULL COMMENT '节点路径'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='角色与节点关系表';

--
-- 转存表中的数据 `system_auth_node`
--

INSERT INTO `system_auth_node` (`auth`, `node`) VALUES
(3, 'admin'),
(3, 'admin/config'),
(3, 'admin/config/index'),
(3, 'admin/config/file'),
(3, 'admin/log'),
(3, 'admin/log/index'),
(3, 'admin/log/del'),
(3, 'admin/menu'),
(3, 'admin/menu/index'),
(3, 'admin/menu/add'),
(3, 'admin/menu/edit'),
(3, 'admin/menu/del'),
(3, 'admin/menu/forbid'),
(3, 'admin/menu/resume'),
(3, 'admin/mlog'),
(3, 'admin/mlog/index'),
(3, 'admin/mlog/del'),
(3, 'admin/nav'),
(3, 'admin/nav/index'),
(3, 'admin/nav/add'),
(3, 'admin/nav/edit'),
(3, 'admin/nav/del'),
(3, 'admin/nav/forbid'),
(3, 'admin/nav/resume'),
(3, 'admin/user'),
(3, 'admin/user/index'),
(3, 'admin/user/auth'),
(3, 'admin/user/add'),
(3, 'admin/user/edit'),
(3, 'admin/user/del'),
(3, 'admin/user/forbid'),
(3, 'admin/user/resume'),
(3, 'manage'),
(3, 'manage/article'),
(3, 'manage/article/index'),
(3, 'manage/article/add'),
(3, 'manage/article/edit'),
(3, 'manage/article/change_status'),
(3, 'manage/article/del'),
(3, 'manage/articlecategory'),
(3, 'manage/articlecategory/index'),
(3, 'manage/articlecategory/add'),
(3, 'manage/articlecategory/edit'),
(3, 'manage/articlecategory/change_status'),
(3, 'manage/articlecategory/del'),
(3, 'manage/backup'),
(3, 'manage/backup/index'),
(3, 'manage/backup/tablist'),
(3, 'manage/backup/backall'),
(3, 'manage/backup/backtables'),
(3, 'manage/backup/recover'),
(3, 'manage/backup/deletebak'),
(3, 'manage/backup/downloadbak'),
(3, 'manage/cash'),
(3, 'manage/cash/index'),
(3, 'manage/cash/config'),
(3, 'manage/cash/detail'),
(3, 'manage/channel'),
(3, 'manage/channel/index'),
(3, 'manage/channel/add'),
(3, 'manage/channel/edit'),
(3, 'manage/channel/del'),
(3, 'manage/channel/change_status'),
(3, 'manage/channel/getlistbypaytype'),
(3, 'manage/channelaccount'),
(3, 'manage/channelaccount/index'),
(3, 'manage/channelaccount/add'),
(3, 'manage/channelaccount/edit'),
(3, 'manage/channelaccount/del'),
(3, 'manage/channelaccount/clear'),
(3, 'manage/channelaccount/change_status'),
(3, 'manage/channelaccount/getfields'),
(3, 'manage/complaint'),
(3, 'manage/complaint/index'),
(3, 'manage/complaint/change_status'),
(3, 'manage/complaint/change_admin_read'),
(3, 'manage/complaint/del'),
(3, 'manage/email'),
(3, 'manage/email/index'),
(3, 'manage/email/test'),
(3, 'manage/goods'),
(3, 'manage/goods/index'),
(3, 'manage/goods/change_status'),
(3, 'manage/index'),
(3, 'manage/index/main'),
(3, 'manage/invitecode'),
(3, 'manage/invitecode/index'),
(3, 'manage/invitecode/add'),
(3, 'manage/invitecode/del'),
(3, 'manage/invitecode/delnum'),
(3, 'manage/log'),
(3, 'manage/log/user_money'),
(3, 'manage/order'),
(3, 'manage/order/index'),
(3, 'manage/order/detail'),
(3, 'manage/order/merchant'),
(3, 'manage/order/channel'),
(3, 'manage/order/hour'),
(3, 'manage/order/change_freeze_status'),
(3, 'manage/order/del'),
(3, 'manage/order/del_batch'),
(3, 'manage/product'),
(3, 'manage/product/index'),
(3, 'manage/product/add'),
(3, 'manage/product/edit'),
(3, 'manage/product/del'),
(3, 'manage/product/change_status'),
(3, 'manage/site'),
(3, 'manage/site/info'),
(3, 'manage/site/domain'),
(3, 'manage/site/register'),
(3, 'manage/site/wordfilter'),
(3, 'manage/site/spread'),
(3, 'manage/sms'),
(3, 'manage/sms/index'),
(3, 'manage/user'),
(3, 'manage/user/index'),
(3, 'manage/user/change_status'),
(3, 'manage/user/change_freeze_status'),
(3, 'manage/user/detail'),
(3, 'manage/user/add'),
(3, 'manage/user/edit'),
(3, 'manage/user/del'),
(3, 'manage/user/manage_money'),
(3, 'manage/user/rate'),
(3, 'manage/user/login'),
(3, 'manage/user/message'),
(3, 'manage/user/loginlog'),
(3, 'manage/user/unlock'),
(3, 'wechat'),
(3, 'wechat/config'),
(3, 'wechat/config/index'),
(3, 'wechat/config/pay'),
(3, 'wechat/fans'),
(3, 'wechat/fans/index'),
(3, 'wechat/fans/back'),
(3, 'wechat/fans/backadd'),
(3, 'wechat/fans/tagset'),
(3, 'wechat/fans/backdel'),
(3, 'wechat/fans/tagadd'),
(3, 'wechat/fans/tagdel'),
(3, 'wechat/fans/sync'),
(3, 'wechat/keys'),
(3, 'wechat/keys/index'),
(3, 'wechat/keys/add'),
(3, 'wechat/keys/edit'),
(3, 'wechat/keys/del'),
(3, 'wechat/keys/forbid'),
(3, 'wechat/keys/resume'),
(3, 'wechat/keys/subscribe'),
(3, 'wechat/keys/defaults'),
(3, 'wechat/menu'),
(3, 'wechat/menu/index'),
(3, 'wechat/menu/edit'),
(3, 'wechat/menu/cancel'),
(3, 'wechat/news'),
(3, 'wechat/news/index'),
(3, 'wechat/news/select'),
(3, 'wechat/news/image'),
(3, 'wechat/news/add'),
(3, 'wechat/news/edit'),
(3, 'wechat/news/del'),
(3, 'wechat/news/push'),
(3, 'wechat/tags'),
(3, 'wechat/tags/index'),
(3, 'wechat/tags/add'),
(3, 'wechat/tags/edit'),
(3, 'wechat/tags/sync'),
(3, 'admin/node'),
(3, 'admin/node/save'),
(3, 'admin/node/save'),
(3, 'admin/auth'),
(3, 'admin/auth/index'),
(3, 'admin/auth/apply'),
(3, 'admin/auth/add'),
(3, 'admin/auth/edit'),
(3, 'admin/auth/forbid'),
(3, 'admin/auth/resume'),
(3, 'admin/auth/del'),
(3, 'admin/auth/bindgoogle'),
(3, 'admin/node'),
(3, 'admin/node/save'),
(3, 'admin/node/index');

-- --------------------------------------------------------

--
-- 表的结构 `system_config`
--

CREATE TABLE `system_config` (
  `id` int(11) UNSIGNED NOT NULL,
  `name` varchar(100) DEFAULT NULL COMMENT '配置编码',
  `value` varchar(1024) DEFAULT NULL COMMENT '配置值'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='系统参数配置' ROW_FORMAT=DYNAMIC;

--
-- 转存表中的数据 `system_config`
--

INSERT INTO `system_config` (`id`, `name`, `value`) VALUES
(148, 'site_name', '企业自动发卡系统'),
(149, 'site_copy', 'Copyright © 企业自动发卡系统 All rights reserved. '),
(164, 'storage_type', 'local'),
(165, 'storage_qiniu_is_https', '1'),
(166, 'storage_qiniu_bucket', 'static'),
(167, 'storage_qiniu_domain', 'static.ctolog.com'),
(168, 'storage_qiniu_access_key', 'OAFHGzCgZjod2-s4xr-g5ptkXsNbxDO_t2fozIEC'),
(169, 'storage_qiniu_secret_key', 'gy0aYdSFMSayQ4kMkgUeEeJRLThVjLpUJoPFxd-Z'),
(170, 'storage_qiniu_region', '华东'),
(173, 'app_name', '企业自动发卡平台'),
(174, 'app_version', '2.00 dev'),
(176, 'browser_icon', '/static/img/c23d983c0e1a7682.ico'),
(184, 'wechat_appid', 'wxbd2715b339faa4b6'),
(185, 'wechat_appsecret', '7e8055384f9c4ff5991c46cacd336ad9'),
(186, 'wechat_token', 'mytoken'),
(187, 'wechat_encodingaeskey', 'KHyoWLoS7oLZYkB4PokMTfA5sm6Hrqc9Tzgn9iXc0YH'),
(188, 'wechat_mch_id', '1332187001'),
(189, 'wechat_partnerkey', 'A82DC5BD1F3359081049C568D8502BC5'),
(194, 'wechat_cert_key', ''),
(196, 'wechat_cert_cert', ''),
(197, 'tongji_baidu_key', ''),
(198, 'tongji_cnzz_key', '1261854404'),
(199, 'storage_oss_bucket', 'think-oss'),
(200, 'storage_oss_keyid', 'WjeX0AYSfgy5VbXQ'),
(201, 'storage_oss_secret', 'hQTENHy6MYVUTgwjcgfOCq5gckm2Lp'),
(202, 'storage_oss_domain', 'think-oss.oss-cn-shanghai.aliyuncs.com'),
(203, 'storage_oss_is_https', '1'),
(204, 'sms_channel', ''),
(205, 'smsbao_user', 'smsbaousername'),
(206, 'smsbao_pass', 'smsbaopassword'),
(207, 'smsbao_signature', '自动发卡'),
(208, 'alidayu_key', 'alidayu_key'),
(209, 'alidayu_secret', 'alidayu_secret'),
(210, 'alidayu_smstpl', 'SMS_111795375'),
(211, 'alidayu_signature', '自动发卡'),
(212, 'yixin_sms_user', 'account'),
(213, 'yixin_sms_pass', 'password'),
(214, 'yixin_sms_signature', '自动发卡'),
(215, 'email_name', '自动发卡'),
(216, 'email_smtp', 'smtp.qq.com'),
(217, 'email_port', '465'),
(218, 'email_user', '123456@qq.com'),
(219, 'email_pass', '123456'),
(220, 'cash_min_money', '10'),
(221, 'transaction_min_fee', '0.01'),
(222, 'cash_fee_type', '100'),
(223, 'cash_fee', '2'),
(224, 'spread_rebate_rate', '0.3'),
(225, 'site_keywords', '自动发卡平台,发卡平台,自动发卡'),
(226, 'site_desc', '本站致力于提供卡密商品的在线寄售服务，为卡密商户打造专业快速的发卡渠道，为卡密买家带来舒适愉快的购卡体验。'),
(227, 'site_status', '1'),
(228, 'site_subtitle', '-一站式的卡密寄售平台'),
(229, 'site_close_tips', '站点维护中'),
(230, 'complaint_limit_num', '1'),
(231, 'cash_status', '1'),
(232, 'cash_close_tips', '满100每天12点自动结算，无须手动结算。'),
(233, 'cash_limit_time_start', '0'),
(234, 'cash_limit_time_end', '23'),
(235, 'cash_limit_num', '1'),
(236, 'cash_limit_num_tips', '满100每天12点自动结算，无须手动结算。'),
(237, 'site_info_tel', '13666666666'),
(238, 'site_info_qq', '4001234567'),
(239, 'site_info_address', '四川省成都市锦江区春熙路街道123号'),
(240, 'site_info_email', '123456@qq.com'),
(241, 'site_info_copyright', 'Copyright © 企业自动发卡系统 All rights reserved. '),
(242, 'site_info_icp', '蜀ICP备12345678号'),
(243, 'site_domain', 'http://www.123.com'),
(244, 'site_domain_res', '/static'),
(245, 'site_register_verify', '1'),
(246, 'site_register_status', '1'),
(247, 'site_register_smscode_status', '0'),
(248, 'site_wordfilter_status', '1'),
(249, 'site_wordfilter_danger', '习近平|毛泽东|胡锦涛|江泽民|援交|傻逼|二逼|SB|脑残!111|徐才厚|郭伯雄'),
(250, 'disabled_domains', 'www|ftp|bbs|blog|tes'),
(251, 'site_domain_short', ''),
(252, 'storage_local_exts', 'jpg,jpeg,gif,png,ico'),
(253, 'site_logo', '/static/img/logo.png'),
(254, 'site_shop_domain', 'http://www.123.com'),
(255, 'yixin_sms_cost', '0.2'),
(256, 'smsbao_cost', '0.2'),
(257, 'alidayu_cost', '0.2'),
(258, 'index_theme', 't4'),
(259, '1cloudsp_key', 'AccessKey'),
(260, '1cloudsp_secret', 'AccessSecret'),
(261, '1cloudsp_smstpl', '3934'),
(262, '1cloudsp_signature', '自动发卡'),
(263, '1cloudsp_cost', '0.2'),
(264, '253sms_key', 'api_account'),
(265, '253sms_secret', 'api_password'),
(266, '253sms_signature', '自动发卡'),
(267, '253sms_cost', '0.2'),
(268, 'ip_register_limit', '5'),
(269, 'is_google_auth', '0'),
(270, 'sms_error_limit', '5'),
(271, 'sms_error_time', '10'),
(272, 'wrong_password_times', '20'),
(273, 'site_statistics', ''),
(274, 'admin_login_path', 'admin'),
(276, 'announce_push', '0'),
(278, 'spread_reward', '0'),
(279, 'spread_reward_money', '5'),
(280, 'spread_invite_code', '1'),
(281, 'contact_us', '&lt;dl&gt;\r\n	&lt;dt style=&quot;text-align: center;&quot;&gt;联系电话： &lt;font&gt;133-1234-1234&lt;/font&gt;&lt;/dt&gt;\r\n	&lt;dd style=&quot;text-align: center;&quot;&gt;客服 QQ： &lt;font&gt;12345678&lt;/font&gt;&lt;/dd&gt;\r\n	&lt;dd style=&quot;text-align: center;&quot;&gt;公司地址： 自动发卡系统&lt;/dd&gt;\r\n	&lt;dd style=&quot;text-align: center;&quot;&gt;公司名称： 自动发卡系统&lt;/dd&gt;\r\n&lt;/dl&gt;\r\n'),
(282, 'qqgroup', ''),
(283, 'wx_auto_login', '0'),
(284, 'is_need_invite_code', '0'),
(285, 'site_register_code_type', 'email'),
(286, 'auto_cash', '1'),
(287, 'auto_cash_money', '100'),
(288, 'sms_notify_channel', ''),
(289, 'merchant_logo', '/static/img/logo2.png'),
(290, 'site_info_qrcode', '/static/img/qrcode.png'),
(291, 'cash_type', '[\"1\",\"2\",\"3\"]'),
(292, 'goods_min_price', '0'),
(293, 'goods_max_price', '5000'),
(294, 'auto_cash_time', '1'),
(295, 'auto_cash_fee_type', '1'),
(296, 'auto_cash_fee', '0'),
(297, 'order_query_chkcode', '1'),
(298, 'buy_protocol', '&lt;p&gt;&lt;span style=&quot;color:#e74c3c;&quot;&gt;&lt;strong&gt;本站不提供任何性质和形式的担保服务，卡密买卖请在双方充分信任情况下进行。&lt;/strong&gt;&lt;/span&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;1、本站仅&lt;span style=&quot;color:#990000;&quot;&gt;&lt;strong&gt;提供自动发卡和寄售服务&lt;/strong&gt;&lt;/span&gt;，并非卡密销售商，也不清楚卡密的真实用途。&lt;/p&gt;\r\n\r\n&lt;p&gt;2、建议首先咨询卡密卖家，明白卡密用途后购买，非卡密错误等问题，本站有权驳回一切投诉与退款。&lt;/p&gt;\r\n\r\n&lt;p&gt;3、如果遇到假卡/欺诈/钓鱼等情形，请在当天24点前，联系平台客服协助处理，或者在线提交订单投诉。&lt;/p&gt;\r\n'),
(299, 'login_auth', '0'),
(300, 'login_auth_type', '0'),
(301, 'fee_payer', '1'),
(302, 'available_email', '0'),
(303, 'idcard_auth_type', '0'),
(304, 'idcard_auth_path', ''),
(305, 'idcard_auth_appcode', ''),
(306, 'idcard_auth_status_field', 'status'),
(307, 'idcard_auth_status_code', '01'),
(308, 'settlement_type', '1'),
(309, 'settlement_type', '1'),
(310, 'settlement_type', '1'),
(311, 'settlement_type', '1'),
(312, 'email_smtp1', ''),
(313, 'email_port1', ''),
(314, 'email_user1', ''),
(315, 'email_pass1', ''),
(316, 'email_smtp2', ''),
(317, 'email_port2', ''),
(318, 'email_user2', ''),
(319, 'email_pass2', ''),
(320, 'site_info_tel_desc', ''),
(321, 'site_info_qq_desc', ''),
(322, 'site_info_qrcode_desc', ''),
(323, 'site_iphone_qrcode', ''),
(324, 'site_android_qrcode', ''),
(325, 'sms_complaint_channel', ''),
(326, 'sms_complaint_notify_channel', ''),
(327, 'alidayu_complaint_smstpl', 'SMS_111795376'),
(328, 'alidayu_complaint_notify_smstpl', 'SMS_111795377'),
(329, 'alidayu_order_smstpl', 'SMS_111795378'),
(330, '1cloudsp_complaint_smstpl', '3935'),
(331, '1cloudsp_complaint_notify_smstpl', '3936'),
(332, '1cloudsp_order_smstpl', '3937'),
(333, 'order_type', '1'),
(334, 'user_order_profix', 'Tc');

-- --------------------------------------------------------

--
-- 表的结构 `system_log`
--

CREATE TABLE `system_log` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `ip` char(15) NOT NULL DEFAULT '' COMMENT '操作者IP地址',
  `node` char(200) NOT NULL DEFAULT '' COMMENT '当前操作节点',
  `username` varchar(32) NOT NULL DEFAULT '' COMMENT '操作人用户名',
  `action` varchar(200) NOT NULL DEFAULT '' COMMENT '操作行为',
  `content` text NOT NULL COMMENT '操作内容描述',
  `create_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='系统操作日志表';

-- --------------------------------------------------------

--
-- 表的结构 `system_menu`
--

CREATE TABLE `system_menu` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `pid` bigint(20) UNSIGNED NOT NULL DEFAULT '0' COMMENT '父id',
  `title` varchar(100) NOT NULL DEFAULT '' COMMENT '名称',
  `node` varchar(200) NOT NULL DEFAULT '' COMMENT '节点代码',
  `icon` varchar(100) NOT NULL DEFAULT '' COMMENT '菜单图标',
  `url` varchar(400) NOT NULL DEFAULT '' COMMENT '链接',
  `params` varchar(500) DEFAULT '' COMMENT '链接参数',
  `target` varchar(20) NOT NULL DEFAULT '_self' COMMENT '链接打开方式',
  `sort` int(11) UNSIGNED DEFAULT '0' COMMENT '菜单排序',
  `status` tinyint(1) UNSIGNED NOT NULL DEFAULT '1' COMMENT '状态(0:禁用,1:启用)',
  `create_by` bigint(20) UNSIGNED NOT NULL DEFAULT '0' COMMENT '创建人',
  `create_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='系统菜单表';

--
-- 转存表中的数据 `system_menu`
--

INSERT INTO `system_menu` (`id`, `pid`, `title`, `node`, `icon`, `url`, `params`, `target`, `sort`, `status`, `create_by`, `create_at`) VALUES
(2, 0, '系统管理', '', 'fa fa-gear', '#', '', '_self', 1000, 1, 0, '2015-11-16 03:15:38'),
(4, 2, '系统配置', '', '', '#', '', '_self', 100, 1, 0, '2016-03-14 02:12:55'),
(5, 4, '网站参数', '', 'fa fa-apple', 'admin/config/index', '', '_self', 20, 1, 0, '2016-05-05 22:36:49'),
(6, 4, '文件存储', '', 'fa fa-save', 'admin/config/file', '', '_self', 30, 1, 0, '2016-05-05 22:39:43'),
(9, 20, '后台操作日志', '', 'glyphicon glyphicon-console', 'admin/log/index', '', '_self', 50, 1, 0, '2017-03-23 23:49:31'),
(19, 20, '权限管理', '', 'fa fa-user-secret', 'admin/auth/index', '', '_self', 10, 1, 0, '2015-11-16 21:18:12'),
(20, 2, '系统权限', '', '', '#', '', '_self', 200, 1, 0, '2016-03-14 02:11:41'),
(21, 20, '系统菜单', '', 'glyphicon glyphicon-menu-hamburger', 'admin/menu/index', '', '_self', 30, 1, 0, '2015-11-16 03:16:16'),
(22, 20, '节点管理', '', 'fa fa-ellipsis-v', 'admin/node/index', '', '_self', 20, 1, 0, '2015-11-16 03:16:16'),
(29, 20, '系统用户', '', 'fa fa-users', 'admin/user/index', '', '_self', 40, 1, 0, '2016-10-30 22:31:40'),
(61, 0, '微信管理', '', 'fa fa-wechat', '#', '', '_self', 2000, 1, 0, '2017-03-28 19:00:21'),
(62, 61, '微信对接配置', '', '', '#', '', '_self', 100, 1, 0, '2017-03-28 19:03:38'),
(63, 62, '微信接口配置\r\n', '', 'fa fa-usb', 'wechat/config/index', '', '_self', 10, 1, 0, '2017-03-28 19:04:44'),
(65, 61, '微信粉丝管理', '', '', '#', '', '_self', 300, 1, 0, '2017-03-28 19:08:32'),
(66, 65, '粉丝标签', '', 'fa fa-tags', 'wechat/tags/index', '', '_self', 10, 1, 0, '2017-03-28 19:09:41'),
(67, 65, '已关注粉丝', '', 'fa fa-wechat', 'wechat/fans/index', '', '_self', 20, 1, 0, '2017-03-28 19:10:07'),
(68, 61, '微信订制', '', '', '#', '', '_self', 200, 1, 0, '2017-03-28 19:10:39'),
(69, 68, '微信菜单定制', '', 'glyphicon glyphicon-phone', 'wechat/menu/index', '', '_self', 40, 1, 0, '2017-03-28 19:11:08'),
(70, 68, '关键字管理', '', 'fa fa-paw', 'wechat/keys/index', '', '_self', 10, 1, 0, '2017-03-28 19:11:49'),
(71, 68, '关注自动回复', '', 'fa fa-commenting-o', 'wechat/keys/subscribe', '', '_self', 20, 1, 0, '2017-03-28 19:12:32'),
(81, 68, '无配置默认回复', '', 'fa fa-commenting-o', 'wechat/keys/defaults', '', '_self', 30, 1, 0, '2017-04-20 22:48:25'),
(82, 61, '素材资源管理', '', '', '#', '', '_self', 300, 1, 0, '2017-04-23 19:23:18'),
(83, 82, '添加图文', '', 'fa fa-folder-open-o', 'wechat/news/add?id=1', '', '_self', 20, 1, 0, '2017-04-23 19:23:40'),
(85, 82, '图文列表', '', 'fa fa-file-pdf-o', 'wechat/news/index', '', '_self', 10, 1, 0, '2017-04-23 19:25:45'),
(86, 65, '粉丝黑名单', '', 'fa fa-reddit-alien', 'wechat/fans/back', '', '_self', 30, 1, 0, '2017-05-05 00:17:03'),
(87, 0, '插件案例', '', '', '#', '', '_self', 3000, 0, 0, '2017-07-09 23:10:16'),
(88, 87, '第三方插件', '', '', '#', '', '_self', 0, 0, 0, '2017-07-09 23:10:37'),
(90, 88, 'PCAS 省市区', '', '', 'demo/plugs/region', '', '_self', 0, 0, 0, '2017-07-10 02:45:47'),
(91, 87, '内置插件', '', '', '#', '', '_self', 0, 0, 0, '2017-07-10 02:56:59'),
(92, 91, '文件上传', '', '', 'demo/plugs/file', '', '_self', 0, 0, 0, '2017-07-10 02:57:22'),
(93, 88, '富文本编辑器', '', '', 'demo/plugs/editor', '', '_self', 0, 0, 0, '2017-07-27 23:19:44'),
(95, 0, '网关通道', '', 'fa fa-product-hunt', '#', '', '_self', 4000, 1, 0, '2018-01-17 19:08:57'),
(97, 95, '支付接口管理', '', 'fa fa-futbol-o', 'manage/channel/index', '', '_self', 0, 1, 0, '2018-01-17 19:09:53'),
(99, 4, '短信配置', '', 'fa fa-envelope-o', 'manage/sms/index', '', '_self', 50, 1, 0, '2018-01-17 19:36:42'),
(100, 4, '邮件配置', '', 'fa fa-envelope-o', 'manage/email/index', '', '_self', 60, 1, 0, '2018-01-18 21:45:19'),
(101, 0, '用户管理', '', 'fa fa-users', '#', '', '_self', 5000, 1, 0, '2018-01-22 18:46:59'),
(102, 101, '商户管理', '', 'fa fa-users', 'manage/user/index', '', '_self', 0, 1, 0, '2018-01-22 18:47:40'),
(103, 0, '交易明细', '', 'fa fa-bar-chart', '#', '', '_self', 6000, 1, 0, '2018-01-23 00:47:46'),
(104, 103, '订单列表', '', 'fa fa-list-ol', 'manage/order/index', '', '_self', 0, 1, 0, '2018-01-23 00:48:09'),
(105, 103, '金额变动记录', '', 'fa fa-exchange', 'manage/log/user_money', '', '_self', 100, 1, 0, '2018-01-23 23:02:50'),
(106, 0, '财务管理', '', 'fa fa-google-wallet', '#', '', '_self', 8000, 1, 0, '2018-01-23 23:17:39'),
(107, 106, '提现管理', '', 'fa fa-cny', 'manage/cash/index', '', '_self', 0, 1, 0, '2018-01-23 23:18:06'),
(108, 4, '后台主页', '', 'fa fa-area-chart', 'manage/index/main', '', '_self', 0, 1, 0, '2018-01-25 22:19:38'),
(109, 106, '提现配置', '', 'fa fa-google-wallet', 'manage/cash/config', '', '_self', 70, 1, 0, '2018-01-28 23:29:43'),
(110, 4, '站点信息', '', 'glyphicon glyphicon-info-sign', 'manage/site/info', '', '_self', 21, 1, 0, '2018-01-29 00:26:24'),
(111, 4, '域名设置', '', 'fa fa-at', 'manage/site/domain', '', '_self', 25, 1, 0, '2018-01-29 00:47:15'),
(112, 4, '注册设置', '', 'fa fa-cog', 'manage/site/register', '', '_self', 27, 1, 0, '2018-01-29 02:45:43'),
(113, 4, '字词过滤', '', 'fa fa-filter', 'manage/site/wordfilter', '', '_self', 90, 1, 0, '2018-01-29 22:50:48'),
(114, 103, '商户分析', '', 'fa fa-area-chart', 'manage/order/merchant', '', '_self', 40, 1, 0, '2018-01-30 19:32:00'),
(115, 103, '渠道分析', '', 'fa fa-area-chart', 'manage/order/channel', '', '_self', 50, 1, 0, '2018-01-30 19:32:29'),
(116, 103, '实时数据', '', 'fa fa-area-chart', 'manage/order/hour', '', '_self', 60, 1, 0, '2018-01-30 19:32:57'),
(117, 0, '商品管理', '', 'fa fa-shopping-bag', '#', '', '_self', 5500, 1, 0, '2018-02-01 02:43:51'),
(118, 117, '商品管理', '', 'fa fa-shopping-bag', 'manage/goods/index', '', '_self', 0, 1, 0, '2018-02-01 02:44:10'),
(119, 103, '投诉管理', '', 'fa fa-bullhorn', 'manage/complaint/index', '', '_self', 20, 1, 0, '2018-02-01 23:34:06'),
(121, 0, '内容管理', '', 'fa fa-file-text', '#', '', '_self', 4500, 1, 0, '2018-02-08 18:38:43'),
(122, 121, '内容管理', '', 'fa fa-file-text', 'manage/article/index', '', '_self', 0, 1, 0, '2018-02-08 18:44:51'),
(123, 121, '分类管理', '', 'fa fa-bars', 'manage/article_category/index', '', '_self', 0, 1, 0, '2018-03-05 08:13:26'),
(124, 4, '备份管理', '', 'fa fa-database', 'manage/backup/index', '', '_self', 100, 1, 0, '2018-03-12 03:31:11'),
(125, 4, '前台导航', '', 'fa fa-navicon', 'admin/nav/index', '', '_self', 110, 1, 0, '2018-03-23 01:08:38'),
(126, 101, '登录解锁', '', 'fa fa-unlock', '/manage/user/unlock', '', '_self', 0, 1, 0, '2018-03-26 19:15:00'),
(127, 20, '商户操作日志', '', 'fa fa-thumb-tack', '/admin/mlog/index', '', '_self', 60, 1, 0, '2018-03-27 00:19:10'),
(128, 4, '推广设置', '', 'fa fa-bullhorn', '/manage/site/spread', '', '_self', 120, 1, 0, '2018-03-27 03:19:29'),
(129, 101, '邀请码管理', '', 'fa fa-user-plus', '/manage/invite_code/index', '', '_self', 0, 1, 0, '2018-03-27 03:54:29'),
(130, 117, '订单自定义', '', 'fa fa-credit-card-alt', 'manage/goods/change_trade_no_status', '', '_self', 0, 1, 0, '2018-04-19 17:03:50'),
(131, 101, '费率分组管理', '', '', 'manage/rate/index', '', '_self', 0, 1, 0, '2018-06-19 18:53:02'),
(132, 4, '数据删除', '', '', 'manage/content/del', '', '_self', 0, 1, 0, '2018-10-08 08:49:42'),
(133, 95, '安装支付接口', '', 'fa fa-futbol-o', 'manage/channel/index?is_install=0', '', '_self', 0, 1, 0, '2018-01-17 19:09:53'),
(135, 106, '代付渠道管理', '', 'fa fa-futbol-o', 'manage/cashChannel/index', '', '_self', 1000, 1, 0, '2018-08-07 07:50:38');

-- --------------------------------------------------------

--
-- 表的结构 `system_node`
--

CREATE TABLE `system_node` (
  `id` int(11) UNSIGNED NOT NULL,
  `node` varchar(100) DEFAULT NULL COMMENT '节点代码',
  `title` varchar(500) DEFAULT NULL COMMENT '节点标题',
  `is_menu` tinyint(1) UNSIGNED DEFAULT '0' COMMENT '是否可设置为菜单',
  `is_auth` tinyint(1) UNSIGNED DEFAULT '1' COMMENT '是否启动RBAC权限控制',
  `is_login` tinyint(1) UNSIGNED DEFAULT '1' COMMENT '是否启动登录控制',
  `create_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='系统节点表' ROW_FORMAT=DYNAMIC;

--
-- 转存表中的数据 `system_node`
--

INSERT INTO `system_node` (`id`, `node`, `title`, `is_menu`, `is_auth`, `is_login`, `create_at`) VALUES
(131, 'admin/auth/index', '权限列表', 1, 1, 1, '2017-08-22 23:45:42'),
(132, 'admin', '后台管理', 0, 1, 1, '2017-08-22 23:45:44'),
(133, 'admin/auth/apply', '节点授权', 0, 1, 1, '2017-08-23 00:05:18'),
(134, 'admin/auth/add', '添加授权', 0, 1, 1, '2017-08-23 00:05:19'),
(135, 'admin/auth/edit', '编辑权限', 0, 1, 1, '2017-08-23 00:05:19'),
(136, 'admin/auth/forbid', '禁用权限', 0, 1, 1, '2017-08-23 00:05:20'),
(137, 'admin/auth/resume', '启用权限', 0, 1, 1, '2017-08-23 00:05:20'),
(138, 'admin/auth/del', '删除权限', 0, 1, 1, '2017-08-23 00:05:21'),
(139, 'admin/config/index', '参数配置', 1, 1, 1, '2017-08-23 00:05:22'),
(140, 'admin/config/file', '文件配置', 1, 1, 1, '2017-08-23 00:05:22'),
(141, 'admin/log/index', '日志列表', 1, 1, 1, '2017-08-23 00:05:23'),
(142, 'admin/log/del', '删除日志', 0, 1, 1, '2017-08-23 00:05:24'),
(143, 'admin/menu/index', '菜单列表', 1, 1, 1, '2017-08-23 00:05:25'),
(144, 'admin/menu/add', '添加菜单', 0, 1, 1, '2017-08-23 00:05:25'),
(145, 'admin/menu/edit', '编辑菜单', 0, 1, 1, '2017-08-23 00:05:26'),
(146, 'admin/menu/del', '删除菜单', 0, 1, 1, '2017-08-23 00:05:26'),
(147, 'admin/menu/forbid', '禁用菜单', 0, 1, 1, '2017-08-23 00:05:27'),
(148, 'admin/menu/resume', '启用菜单', 0, 1, 1, '2017-08-23 00:05:28'),
(149, 'admin/node/index', '节点列表', 1, 1, 1, '2017-08-23 00:05:29'),
(150, 'admin/node/save', '节点更新', 0, 1, 1, '2017-08-23 00:05:30'),
(151, 'admin/user/index', '用户管理', 1, 1, 1, '2017-08-23 00:05:31'),
(152, 'admin/user/auth', '用户授权', 0, 1, 1, '2017-08-23 00:05:32'),
(153, 'admin/user/add', '添加用户', 0, 1, 1, '2017-08-23 00:05:33'),
(154, 'admin/user/edit', '编辑用户', 0, 1, 1, '2017-08-23 00:05:33'),
(155, 'admin/user/pass', '用户密码', 0, 1, 1, '2017-08-23 00:05:34'),
(156, 'admin/user/del', '删除用户', 0, 1, 1, '2017-08-23 00:05:34'),
(157, 'admin/user/forbid', '禁用用户', 0, 1, 1, '2017-08-23 00:05:34'),
(158, 'admin/user/resume', '启用用户', 0, 1, 1, '2017-08-23 00:05:35'),
(159, 'demo/plugs/file', '文件上传', 1, 0, 0, '2017-08-23 00:05:36'),
(160, 'demo/plugs/region', '区域选择', 1, 0, 0, '2017-08-23 00:05:36'),
(161, 'demo/plugs/editor', '富文本器', 1, 0, 0, '2017-08-23 00:05:37'),
(162, 'wechat/config/index', '微信参数', 1, 1, 1, '2017-08-23 00:05:37'),
(163, 'wechat/config/pay', '微信支付', 0, 1, 1, '2017-08-23 00:05:38'),
(164, 'wechat/fans/index', '粉丝列表', 1, 1, 1, '2017-08-23 00:05:39'),
(165, 'wechat/fans/back', '粉丝黑名单', 1, 1, 1, '2017-08-23 00:05:39'),
(166, 'wechat/fans/backadd', '移入黑名单', 0, 1, 1, '2017-08-23 00:05:40'),
(167, 'wechat/fans/tagset', '设置标签', 0, 1, 1, '2017-08-23 00:05:41'),
(168, 'wechat/fans/backdel', '移出黑名单', 0, 1, 1, '2017-08-23 00:05:41'),
(169, 'wechat/fans/tagadd', '添加标签', 0, 1, 1, '2017-08-23 00:05:41'),
(170, 'wechat/fans/tagdel', '删除标签', 0, 1, 1, '2017-08-23 00:05:42'),
(171, 'wechat/fans/sync', '同步粉丝', 0, 1, 1, '2017-08-23 00:05:43'),
(172, 'wechat/keys/index', '关键字列表', 1, 1, 1, '2017-08-23 00:05:44'),
(173, 'wechat/keys/add', '添加关键字', 0, 1, 1, '2017-08-23 00:05:44'),
(174, 'wechat/keys/edit', '编辑关键字', 0, 1, 1, '2017-08-23 00:05:45'),
(175, 'wechat/keys/del', '删除关键字', 0, 1, 1, '2017-08-23 00:05:45'),
(176, 'wechat/keys/forbid', '禁用关键字', 0, 1, 1, '2017-08-23 00:05:46'),
(177, 'wechat/keys/resume', '启用关键字', 0, 1, 1, '2017-08-23 00:05:46'),
(178, 'wechat/keys/subscribe', '关注默认回复', 0, 1, 1, '2017-08-23 00:05:48'),
(179, 'wechat/keys/defaults', '默认响应回复', 0, 1, 1, '2017-08-23 00:05:49'),
(180, 'wechat/menu/index', '微信菜单', 1, 1, 1, '2017-08-23 00:05:51'),
(181, 'wechat/menu/edit', '发布微信菜单', 0, 1, 1, '2017-08-23 00:05:51'),
(182, 'wechat/menu/cancel', '取消微信菜单', 0, 1, 1, '2017-08-23 00:05:52'),
(183, 'wechat/news/index', '微信图文列表', 1, 1, 1, '2017-08-23 00:05:52'),
(184, 'wechat/news/select', '微信图文选择', 0, 1, 1, '2017-08-23 00:05:53'),
(185, 'wechat/news/image', '微信图片选择', 0, 1, 1, '2017-08-23 00:05:53'),
(186, 'wechat/news/add', '添加图文', 0, 1, 1, '2017-08-23 00:05:54'),
(187, 'wechat/news/edit', '编辑图文', 0, 1, 1, '2017-08-23 00:05:56'),
(188, 'wechat/news/del', '删除图文', 0, 1, 1, '2017-08-23 00:05:56'),
(189, 'wechat/news/push', '推送图文', 0, 1, 1, '2017-08-23 00:05:56'),
(190, 'wechat/tags/index', '微信标签列表', 1, 1, 1, '2017-08-23 00:05:58'),
(191, 'wechat/tags/add', '添加微信标签', 0, 1, 1, '2017-08-23 00:05:58'),
(192, 'wechat/tags/edit', '编辑微信标签', 0, 1, 1, '2017-08-23 00:05:58'),
(193, 'wechat/tags/sync', '同步微信标签', 0, 1, 1, '2017-08-23 00:05:59'),
(194, 'admin/auth', '权限管理', 0, 1, 1, '2017-08-23 00:06:58'),
(195, 'admin/config', '系统配置', 0, 1, 1, '2017-08-23 00:07:34'),
(196, 'admin/log', '系统日志', 0, 1, 1, '2017-08-23 00:07:46'),
(197, 'admin/menu', '系统菜单', 0, 1, 1, '2017-08-23 00:08:02'),
(198, 'admin/node', '系统节点', 0, 1, 1, '2017-08-23 00:08:44'),
(199, 'admin/user', '系统用户', 0, 1, 1, '2017-08-23 00:09:43'),
(200, 'demo', '插件案例', 0, 1, 1, '2017-08-23 00:10:43'),
(201, 'demo/plugs', '插件案例', 0, 1, 1, '2017-08-23 00:10:51'),
(202, 'wechat', '微信管理', 0, 1, 1, '2017-08-23 00:11:13'),
(203, 'wechat/config', '微信配置', 0, 1, 1, '2017-08-23 00:11:19'),
(204, 'wechat/fans', '粉丝管理', 0, 1, 1, '2017-08-23 00:11:36'),
(205, 'wechat/keys', '关键字管理', 0, 1, 1, '2017-08-23 00:13:00'),
(206, 'wechat/menu', '微信菜单管理', 0, 1, 1, '2017-08-23 00:14:11'),
(207, 'wechat/news', '微信图文管理', 0, 1, 1, '2017-08-23 00:14:40'),
(208, 'wechat/tags', '微信标签管理', 0, 1, 1, '2017-08-23 00:15:25'),
(209, 'manage/channel/index', '供应商管理', 0, 1, 1, '2018-01-18 21:53:53'),
(210, 'manage/channel/add', '添加供应商', 0, 1, 1, '2018-01-18 21:53:54'),
(211, 'manage/channel/edit', '修改供应商', 0, 1, 1, '2018-01-18 21:53:54'),
(212, 'manage/channel/del', '删除供应商', 0, 1, 1, '2018-01-18 21:53:54'),
(213, 'manage/channel/change_status', '修改供应商状态', 0, 1, 1, '2018-01-18 21:53:55'),
(214, 'manage/channel/getlistbypaytype', '根据支付类型获取支付供应商列表', 0, 1, 1, '2018-01-18 21:53:55'),
(215, 'manage/channelaccount/add', '添加供应商账号', 0, 1, 1, '2018-01-18 21:54:03'),
(216, 'manage/channelaccount/index', '供应商账号管理', 0, 1, 1, '2018-01-18 21:54:04'),
(217, 'manage/channelaccount/edit', '修改供应商账号', 0, 1, 1, '2018-01-18 21:54:05'),
(218, 'manage/channelaccount/del', '删除供应商账号', 0, 1, 1, '2018-01-18 21:54:06'),
(219, 'manage/channelaccount/clear', '清除供应商账号额度', 0, 1, 1, '2018-01-18 21:54:07'),
(220, 'manage/channelaccount/change_status', '修改供应商账号状态', 0, 1, 1, '2018-01-18 21:54:07'),
(221, 'manage/channelaccount/getfields', '获取渠道账户字段', 0, 1, 1, '2018-01-18 21:54:08'),
(222, 'manage/email/index', '邮件配置', 0, 1, 1, '2018-01-18 21:54:10'),
(223, 'manage/email/test', '发信测试', 0, 1, 1, '2018-01-18 21:54:10'),
(224, 'manage/product/index', '支付产品管理', 0, 1, 1, '2018-01-18 21:54:11'),
(225, 'manage/product/add', '添加支付产品', 0, 1, 1, '2018-01-18 21:54:12'),
(226, 'manage/product/edit', '编辑支付产品', 0, 1, 1, '2018-01-18 21:54:12'),
(227, 'manage/product/del', '删除支付产品', 0, 1, 1, '2018-01-18 21:54:14'),
(228, 'manage/product/change_status', '修改支付产品状态', 0, 1, 1, '2018-01-18 21:54:14'),
(229, 'manage/sms/index', '短信配置', 0, 1, 1, '2018-01-18 21:54:15'),
(230, 'manage/cash/index', '提现列表', 0, 1, 1, '2018-01-25 00:47:20'),
(231, 'manage/cash/detail', '提现申请详情', 0, 1, 1, '2018-01-25 00:47:20'),
(232, 'manage/cash/payqrcode', '', 0, 1, 1, '2018-01-25 00:47:21'),
(233, 'manage/log/user_money', '金额变动记录', 0, 1, 1, '2018-01-25 00:47:24'),
(234, 'manage/order/index', '订单列表', 0, 1, 1, '2018-01-25 00:47:25'),
(235, 'manage/order/detail', '订单详情', 0, 1, 1, '2018-01-25 00:47:26'),
(236, 'manage/user/index', '商户管理', 0, 1, 1, '2018-01-25 00:47:29'),
(237, 'manage/user/change_status', '修改商户审核状态', 0, 1, 1, '2018-01-25 00:47:30'),
(238, 'manage/user/detail', '查看商户详情', 0, 1, 1, '2018-01-25 00:47:30'),
(239, 'manage/user/add', '添加商户', 0, 1, 1, '2018-01-25 00:47:31'),
(240, 'manage/user/edit', '编辑商户', 0, 1, 1, '2018-01-25 00:47:31'),
(241, 'manage/user/del', '删除商户', 0, 1, 1, '2018-01-25 00:47:32'),
(242, 'manage/user/manage_money', '商户资金管理', 0, 1, 1, '2018-01-25 00:47:32'),
(243, 'manage/user/rate', '设置商户费率', 0, 1, 1, '2018-01-25 00:47:33'),
(244, 'manage/cash/config', '提现配置', 0, 1, 1, '2018-02-01 01:00:28'),
(245, 'manage/index/main', '主页', 0, 1, 1, '2018-02-01 01:00:33'),
(246, 'manage/order/merchant', '商户分析', 0, 1, 1, '2018-02-01 01:00:35'),
(247, 'manage/order/channel', '渠道分析', 0, 1, 1, '2018-02-01 01:00:36'),
(248, 'manage/order/hour', '实时数据', 0, 1, 1, '2018-02-01 01:00:36'),
(249, 'manage/site/info', '站点信息配置', 0, 1, 1, '2018-02-01 01:00:40'),
(250, 'manage/site/domain', '域名设置', 0, 1, 1, '2018-02-01 01:00:40'),
(251, 'manage/site/register', '注册设置', 0, 1, 1, '2018-02-01 01:00:41'),
(252, 'manage/site/wordfilter', '字词过滤', 0, 1, 1, '2018-02-01 01:00:41'),
(253, 'manage/user/change_freeze_status', '修改商户冻结状态', 0, 1, 1, '2018-02-01 01:00:43'),
(254, 'manage/user/login', '商户登录', 0, 1, 1, '2018-02-01 01:00:45'),
(255, 'manage/user/message', '商户站内信', 0, 1, 1, '2018-02-01 01:00:45'),
(256, 'merchant/cash/index', '', 0, 0, 0, '2018-02-01 01:00:48'),
(257, 'manage/goods/index', '商品管理', 0, 1, 1, '2018-02-01 03:33:28'),
(258, 'manage/goods/change_status', '修改商品上架状态', 0, 1, 1, '2018-02-01 03:33:29'),
(259, 'manage/complaint/index', '投诉管理', 0, 1, 1, '2018-02-02 03:46:10'),
(260, 'manage/complaint/change_status', '修改投诉处理状态', 0, 1, 1, '2018-02-02 03:46:11'),
(261, 'manage/complaint/change_admin_read', '修改投诉读取状态', 0, 1, 1, '2018-02-02 03:46:11'),
(262, 'manage/complaint/del', '删除投诉', 0, 1, 1, '2018-02-02 03:46:12'),
(263, 'manage/order/change_freeze_status', '修改订单冻结状态', 0, 1, 1, '2018-02-05 02:24:23'),
(264, 'manage/user/loginlog', '商户登录日志', 0, 1, 1, '2018-02-05 02:24:31'),
(265, 'merchant/user/closelink', '', 0, 0, 0, '2018-03-19 22:22:03'),
(266, 'merchant/goodscategory', '', 0, 0, 0, '2018-03-19 22:22:32'),
(267, 'merchant/cash/apply', '', 0, 0, 0, '2018-03-19 22:22:35'),
(268, 'merchant/cash', '', 0, 0, 0, '2018-03-19 22:22:38'),
(269, 'merchant', '', 0, 0, 0, '2018-03-19 22:23:00'),
(270, 'manage/article/add', '添加文章', 0, 1, 1, '2018-03-19 22:23:38'),
(271, 'manage/article/edit', '编辑文章', 0, 1, 1, '2018-03-19 22:23:39'),
(272, 'manage/article/index', '内容管理', 0, 1, 1, '2018-03-19 22:23:39'),
(273, 'manage/article/change_status', '修改文章状态', 0, 1, 1, '2018-03-19 22:23:40'),
(274, 'manage/article/del', '删除文章', 0, 1, 1, '2018-03-19 22:23:41'),
(275, 'manage/articlecategory/index', '文章分类管理', 0, 1, 1, '2018-03-19 22:23:53'),
(276, 'manage/articlecategory/add', '添加文章分类', 0, 1, 1, '2018-03-19 22:23:53'),
(277, 'manage/articlecategory/edit', '编辑文章分类', 0, 1, 1, '2018-03-19 22:23:54'),
(278, 'manage/articlecategory/change_status', '修改文章分类状态', 0, 1, 1, '2018-03-19 22:23:54'),
(279, 'manage/articlecategory/del', '删除文章分类', 0, 1, 1, '2018-03-19 22:23:55'),
(280, 'manage/backup/index', '备份管理', 0, 1, 1, '2018-03-19 22:24:04'),
(281, 'manage/backup/tablist', '获取数据表', 0, 1, 1, '2018-03-19 22:24:05'),
(282, 'manage/backup/backall', '备份数据库', 0, 1, 1, '2018-03-19 22:24:06'),
(283, 'manage/backup/backtables', '按表备份', 0, 1, 1, '2018-03-19 22:24:07'),
(284, 'manage/backup/recover', '还原数据库', 0, 1, 1, '2018-03-19 22:24:07'),
(285, 'manage/backup/downloadbak', '下载备份文件', 0, 1, 1, '2018-03-19 22:24:08'),
(286, 'manage/backup/deletebak', '删除备份', 0, 1, 1, '2018-03-19 22:24:09'),
(287, 'manage/article', '内容管理', 0, 1, 1, '2018-03-22 00:32:51'),
(288, 'admin/auth/google', '', 0, 0, 0, '2018-03-22 00:33:13'),
(289, 'admin/auth/bindgoogle', '生成绑定谷歌身份验证器二维码', 0, 0, 0, '2018-03-22 00:39:13'),
(290, 'manage/user', '用户管理', 0, 1, 1, '2018-03-22 00:41:20'),
(291, 'manage/sms', '短信配置', 0, 1, 1, '2018-03-22 00:44:54'),
(292, 'manage/site', '站点信息', 0, 1, 1, '2018-03-22 00:45:04'),
(293, 'manage/product', '支付产品管理', 0, 1, 1, '2018-03-22 00:47:47'),
(294, 'manage/order/del_batch', '批量删除无效订单', 0, 1, 1, '2018-03-22 00:48:42'),
(295, 'manage/order/del', '删除无效订单', 0, 1, 1, '2018-03-22 00:48:43'),
(296, 'manage/order', '交易明细', 0, 1, 1, '2018-03-22 00:50:10'),
(297, 'manage/log', '金额变动记录', 0, 1, 1, '2018-03-22 00:51:25'),
(298, 'manage/index', '主页', 0, 1, 1, '2018-03-22 00:51:55'),
(299, 'manage/goods', '商品管理', 0, 1, 1, '2018-03-22 00:52:09'),
(300, 'manage/email', '邮件配置', 0, 1, 1, '2018-03-22 00:53:07'),
(301, 'manage/complaint', '投诉管理', 0, 1, 1, '2018-03-22 00:54:06'),
(302, 'manage/channelaccount', '供应商账号管理', 0, 1, 1, '2018-03-22 00:54:52'),
(303, 'manage/channel', '供应商管理', 0, 1, 1, '2018-03-22 02:45:06'),
(304, 'manage/cash', '提现管理', 0, 1, 1, '2018-03-22 02:46:43'),
(305, 'manage/backup', '备份管理', 0, 1, 1, '2018-03-22 02:49:14'),
(306, 'manage/articlecategory', '文章分类管理', 0, 1, 1, '2018-03-22 02:53:43'),
(307, 'manage/goods/change_trade_no_status', '', 0, 1, 1, '2018-04-19 17:04:48'),
(308, 'shop/shop/index', '', 0, 0, 0, '2018-06-21 02:19:27'),
(309, 'shop/shop/category', '', 0, 0, 0, '2018-06-21 02:19:28'),
(310, 'shop/shop/goods', '', 0, 0, 0, '2018-06-21 02:20:39'),
(311, 'shop/shop/getgoodslist', '', 0, 0, 0, '2018-06-21 02:20:40'),
(312, 'shop/shop/getgoodsinfo', '', 0, 0, 0, '2018-06-21 02:20:41'),
(313, 'shop/shop/getrate', '', 0, 0, 0, '2018-06-21 02:20:41'),
(314, 'shop/shop/getdiscounts', '', 0, 0, 0, '2018-06-21 02:20:42'),
(315, 'shop/shop/getdiscount', '', 0, 0, 0, '2018-06-21 02:20:43'),
(316, 'shop/shop/checkvisitpassword', '', 0, 0, 0, '2018-06-21 02:20:43'),
(317, 'shop/shop/checkcoupon', '', 0, 0, 0, '2018-06-21 02:20:44'),
(318, 'admin/app/index', '', 0, 1, 1, '2022-12-09 12:33:35'),
(319, 'admin/app', '', 0, 1, 1, '2022-12-09 12:33:48');

-- --------------------------------------------------------

--
-- 表的结构 `system_sequence`
--

CREATE TABLE `system_sequence` (
  `id` bigint(20) NOT NULL,
  `type` varchar(20) DEFAULT NULL COMMENT '序号类型',
  `sequence` char(50) NOT NULL COMMENT '序号值',
  `create_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='系统序号表';

-- --------------------------------------------------------

--
-- 表的结构 `system_user`
--

CREATE TABLE `system_user` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `username` varchar(50) NOT NULL DEFAULT '' COMMENT '用户登录名',
  `password` char(32) NOT NULL DEFAULT '' COMMENT '用户登录密码',
  `qq` varchar(16) DEFAULT NULL COMMENT '联系QQ',
  `mail` varchar(32) DEFAULT NULL COMMENT '联系邮箱',
  `phone` varchar(16) DEFAULT NULL COMMENT '联系手机号',
  `desc` varchar(255) DEFAULT '' COMMENT '备注说明',
  `login_num` bigint(20) UNSIGNED DEFAULT '0' COMMENT '登录次数',
  `login_at` datetime DEFAULT NULL,
  `status` tinyint(1) UNSIGNED NOT NULL DEFAULT '1' COMMENT '状态(0:禁用,1:启用)',
  `authorize` varchar(255) DEFAULT NULL,
  `is_deleted` tinyint(1) UNSIGNED DEFAULT '0' COMMENT '删除状态(1:删除,0:未删)',
  `create_by` bigint(20) UNSIGNED DEFAULT NULL COMMENT '创建人',
  `create_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `google_secret_key` varchar(128) DEFAULT '' COMMENT '谷歌令牌密钥'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='系统用户表';

--
-- 转存表中的数据 `system_user`
--

INSERT INTO `system_user` (`id`, `username`, `password`, `qq`, `mail`, `phone`, `desc`, `login_num`, `login_at`, `status`, `authorize`, `is_deleted`, `create_by`, `create_at`, `google_secret_key`) VALUES
(10005, 'admin', '21232f297a57a5a743894a0e4a801fc3', '', '123456@qq.com', '13666666666', 'demo', 282, '2022-12-10 06:40:38', 1, '3', 0, 0, '2018-05-02 00:40:09', '');

-- --------------------------------------------------------

--
-- 表的结构 `unique_orderno`
--

CREATE TABLE `unique_orderno` (
  `id` int(11) NOT NULL,
  `trade_no` varchar(255) COLLATE utf8_bin NOT NULL COMMENT '订单号'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- 转存表中的数据 `unique_orderno`
--

INSERT INTO `unique_orderno` (`id`, `trade_no`) VALUES
(1, 'T190711194528200042'),
(2, 'T190711194702200079'),
(3, 'T190711195130200059'),
(4, 'T190711200007200022'),
(5, 'T190711200223200070'),
(6, 'T190711200325200014'),
(7, 'T190711200401200063'),
(8, 'T190711203145200020'),
(9, 'T190711203151200079'),
(10, 'T190711203156200097'),
(11, 'T190711212217200045'),
(12, 'T190711212234200012'),
(13, 'T190711212243200025'),
(14, 'Tc190712174046200059'),
(15, 'Tc190712174409200012'),
(16, 'Tc190714153746200050');

-- --------------------------------------------------------

--
-- 表的结构 `user`
--

CREATE TABLE `user` (
  `id` int(10) UNSIGNED NOT NULL,
  `parent_id` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT '上级ID',
  `openid` varchar(50) NOT NULL DEFAULT '' COMMENT '微信openid',
  `username` varchar(50) NOT NULL,
  `password` char(32) NOT NULL,
  `mobile` varchar(15) NOT NULL DEFAULT '' COMMENT '手机号',
  `qq` varchar(16) NOT NULL,
  `email` varchar(50) NOT NULL,
  `subdomain` varchar(250) NOT NULL DEFAULT '' COMMENT '子域名',
  `shop_name` varchar(20) NOT NULL DEFAULT '' COMMENT '店铺名称',
  `shop_notice` varchar(200) NOT NULL DEFAULT '' COMMENT '公告通知',
  `statis_code` varchar(1024) NOT NULL DEFAULT '' COMMENT '统计代码',
  `pay_theme` varchar(255) NOT NULL DEFAULT 'default' COMMENT '支付页风格',
  `stock_display` tinyint(3) UNSIGNED NOT NULL DEFAULT '2' COMMENT '库存展示方式 1实际库存 2库存范围',
  `money` decimal(10,3) NOT NULL DEFAULT '0.000',
  `rebate` decimal(10,3) UNSIGNED NOT NULL DEFAULT '0.000',
  `freeze_money` decimal(10,3) NOT NULL DEFAULT '0.000',
  `status` tinyint(3) UNSIGNED NOT NULL DEFAULT '1' COMMENT '0未审核 1已审核',
  `is_freeze` tinyint(3) UNSIGNED NOT NULL DEFAULT '0' COMMENT '是否冻结 0否 1是',
  `create_at` int(10) UNSIGNED NOT NULL,
  `ip` varchar(50) DEFAULT '' COMMENT 'IP地址',
  `website` varchar(255) NOT NULL DEFAULT '' COMMENT '商户网站',
  `is_close` tinyint(3) UNSIGNED NOT NULL DEFAULT '0' COMMENT '是否关闭店铺 0否 1是',
  `shop_notice_auto_pop` tinyint(4) NOT NULL DEFAULT '1' COMMENT '商家公告是否自动弹出',
  `cash_type` tinyint(4) NOT NULL DEFAULT '1' COMMENT '提现方式',
  `login_auth` tinyint(4) NOT NULL DEFAULT '0' COMMENT '是否开启安全登录',
  `login_auth_type` tinyint(4) NOT NULL DEFAULT '1' COMMENT '安全登录验证方式，1：短信，2：邮件，3：谷歌密码验证',
  `google_secret_key` varchar(128) DEFAULT '' COMMENT '谷歌令牌密钥',
  `shop_gouka_protocol_pop` tinyint(4) NOT NULL DEFAULT '1' COMMENT '购卡协议是否自动弹出',
  `user_notice_auto_pop` tinyint(4) NOT NULL DEFAULT '1' COMMENT '商家是否自动弹出',
  `login_key` int(11) NOT NULL DEFAULT '0' COMMENT '用户登录标记',
  `fee_payer` tinyint(4) NOT NULL DEFAULT '0' COMMENT '订单手续费支付方，0：跟随系统，1：商家承担，2买家承担',
  `settlement_type` tinyint(4) NOT NULL DEFAULT '-1' COMMENT '结算方式，-1：跟随系统，1:T1结算，0:T0结算'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `user`
--

INSERT INTO `user` (`id`, `parent_id`, `openid`, `username`, `password`, `mobile`, `qq`, `email`, `subdomain`, `shop_name`, `shop_notice`, `statis_code`, `pay_theme`, `stock_display`, `money`, `rebate`, `freeze_money`, `status`, `is_freeze`, `create_at`, `ip`, `website`, `is_close`, `shop_notice_auto_pop`, `cash_type`, `login_auth`, `login_auth_type`, `google_secret_key`, `shop_gouka_protocol_pop`, `user_notice_auto_pop`, `login_key`, `fee_payer`, `settlement_type`) VALUES
(10001, 0, '', 'admin', '25d55ad283aa400af464c76d713c07ad', '13666666666', '123456', '123456@qq.com', '', '', '', '', 'default', 2, '0.000', '0.000', '0.090', 1, 0, 1633684365, '', '', 0, 1, 1, 0, 1, '', 1, 1, 1104206, 0, -1);

-- --------------------------------------------------------

--
-- 表的结构 `user_channel`
--

CREATE TABLE `user_channel` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `channel_id` int(10) UNSIGNED NOT NULL,
  `status` tinyint(3) UNSIGNED NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `user_collect`
--

CREATE TABLE `user_collect` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `type` tinyint(3) UNSIGNED NOT NULL DEFAULT '1' COMMENT '类型 1支付宝 2微信 3银行卡',
  `info` text NOT NULL,
  `create_at` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `collect_img` tinytext,
  `allow_update` tinyint(4) NOT NULL DEFAULT '0' COMMENT '1为允许用户修改收款信息'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `user_collect`
--

INSERT INTO `user_collect` (`id`, `user_id`, `type`, `info`, `create_at`, `collect_img`, `allow_update`) VALUES
(1, 10002, 1, '{\"account\":\"123@321.lc\",\"realname\":\"321\",\"idcard_number\":\"445221199901075916\"}', 1562911949, 'http://70aa.cn/static/upload/5d2824d0ccf1e/5d2824d0ccf8f.png', 0);

-- --------------------------------------------------------

--
-- 表的结构 `user_login_error_log`
--

CREATE TABLE `user_login_error_log` (
  `id` int(11) NOT NULL,
  `login_name` varchar(50) NOT NULL DEFAULT '' COMMENT '登录名',
  `password` varchar(255) NOT NULL DEFAULT '' COMMENT '尝试密码',
  `user_type` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0：普通用户 1：后台管理员账号',
  `login_from` int(1) NOT NULL DEFAULT '0' COMMENT '登录来源：0：前台 1：总后台',
  `login_time` int(11) NOT NULL DEFAULT '0' COMMENT '登录时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `user_login_log`
--

CREATE TABLE `user_login_log` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `ip` varchar(15) NOT NULL DEFAULT '',
  `create_at` int(10) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `user_login_log`
--

INSERT INTO `user_login_log` (`id`, `user_id`, `ip`, `create_at`) VALUES
(1, 10001, '61.242.114.84', 1538990543),
(2, 10001, '113.250.89.43', 1560915339),
(3, 10002, '121.13.21.254', 1562843735),
(4, 10002, '121.13.21.254', 1562849812),
(5, 10002, '36.23.250.57', 1562857240),
(6, 10002, '113.77.147.55', 1562911059),
(7, 10002, '113.77.147.55', 1562911842),
(8, 10002, '116.18.22.82', 1562923842),
(9, 10002, '36.23.97.104', 1562946147),
(10, 10002, '36.23.191.105', 1563032529),
(11, 10002, '121.13.197.40', 1563063414),
(12, 10002, '36.17.86.116', 1563071090),
(13, 10002, '121.13.197.40', 1563072828),
(14, 10002, '36.23.79.98', 1563087027),
(15, 10002, '116.18.22.128', 1563089585),
(16, 10002, '36.19.125.2', 1563094623),
(17, 10002, '116.18.22.128', 1563119458),
(18, 10002, '121.13.197.185', 1563189641),
(19, 10002, '121.13.197.28', 1563282461),
(20, 10002, '113.77.147.199', 1563858375),
(21, 10002, '113.77.147.199', 1563863209),
(22, 10002, '113.77.147.199', 1563863211),
(23, 10002, '223.104.254.240', 1569326979),
(24, 10002, '42.200.244.66', 1569895526),
(25, 10002, '112.97.213.115', 1572707550);

-- --------------------------------------------------------

--
-- 表的结构 `user_money_log`
--

CREATE TABLE `user_money_log` (
  `id` int(11) NOT NULL,
  `business_type` enum('sub_sold_rebate','sub_fee_rebate','cash_notpass','cash_success','apply_cash','admin_dec','admin_inc','goods_sold','fee','sub_register','freeze','unfreeze') NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL COMMENT '用户ID',
  `money` decimal(10,3) NOT NULL COMMENT '变动金额',
  `balance` decimal(10,3) NOT NULL COMMENT '剩余',
  `reason` varchar(255) NOT NULL DEFAULT '' COMMENT '变动原因',
  `create_at` int(10) UNSIGNED NOT NULL COMMENT '创建时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `user_rate`
--

CREATE TABLE `user_rate` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL COMMENT '用户ID',
  `channel_id` int(10) UNSIGNED NOT NULL COMMENT '渠道ID',
  `rate` decimal(10,4) UNSIGNED NOT NULL COMMENT '费率'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `user_token`
--

CREATE TABLE `user_token` (
  `user_id` int(10) UNSIGNED NOT NULL COMMENT '用户 id',
  `token` varchar(255) NOT NULL COMMENT '用户登录凭证',
  `platform` varchar(20) NOT NULL COMMENT '用户登录平台',
  `refresh_token` varchar(255) NOT NULL COMMENT '登录凭证刷新凭证',
  `created_at` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT '创建时间，即登录时间',
  `expire_at` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT '凭证过期时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `verify_email_error_log`
--

CREATE TABLE `verify_email_error_log` (
  `id` int(11) NOT NULL,
  `username` varchar(50) DEFAULT '' COMMENT '前台用户名',
  `admin` varchar(50) DEFAULT '' COMMENT '管理员用户名',
  `email` varchar(20) DEFAULT '' COMMENT '邮箱',
  `code` varchar(10) DEFAULT '' COMMENT '输入验证码',
  `screen` varchar(20) DEFAULT '' COMMENT '使用场景',
  `type` tinyint(1) DEFAULT '0' COMMENT '1：短信验证码 2：谷歌身份验证, 0:邮箱',
  `ctime` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `verify_error_log`
--

CREATE TABLE `verify_error_log` (
  `id` int(11) NOT NULL,
  `username` varchar(50) DEFAULT '' COMMENT '前台用户名',
  `admin` varchar(50) DEFAULT '' COMMENT '管理员用户名',
  `mobile` varchar(20) DEFAULT '' COMMENT '手机号码',
  `code` varchar(10) DEFAULT '' COMMENT '输入验证码',
  `screen` varchar(20) DEFAULT '' COMMENT '使用场景',
  `type` tinyint(1) DEFAULT '0' COMMENT '1：短信验证码 2：谷歌身份验证',
  `ctime` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `verify_error_log`
--

INSERT INTO `verify_error_log` (`id`, `username`, `admin`, `mobile`, `code`, `screen`, `type`, `ctime`) VALUES
(1, '', '', '18130030906', '', 'register', 1, 1561816663),
(2, '', '', '18130030906', '', 'register', 1, 1561816676);

-- --------------------------------------------------------

--
-- 表的结构 `wechat_fans`
--

CREATE TABLE `wechat_fans` (
  `id` bigint(20) UNSIGNED NOT NULL COMMENT '粉丝表ID',
  `appid` varchar(50) DEFAULT NULL COMMENT '公众号Appid',
  `groupid` bigint(20) UNSIGNED DEFAULT NULL COMMENT '分组ID',
  `tagid_list` varchar(100) DEFAULT '' COMMENT '标签id',
  `is_back` tinyint(1) UNSIGNED DEFAULT '0' COMMENT '是否为黑名单用户',
  `subscribe` tinyint(1) UNSIGNED NOT NULL DEFAULT '0' COMMENT '用户是否订阅该公众号，0：未关注，1：已关注',
  `openid` char(100) NOT NULL DEFAULT '' COMMENT '用户的标识，对当前公众号唯一',
  `spread_openid` char(100) DEFAULT NULL COMMENT '推荐人OPENID',
  `spread_at` datetime DEFAULT NULL,
  `nickname` varchar(100) DEFAULT NULL COMMENT '用户的昵称',
  `sex` tinyint(1) UNSIGNED DEFAULT NULL COMMENT '用户的性别，值为1时是男性，值为2时是女性，值为0时是未知',
  `country` varchar(50) DEFAULT NULL COMMENT '用户所在国家',
  `province` varchar(50) DEFAULT NULL COMMENT '用户所在省份',
  `city` varchar(50) DEFAULT NULL COMMENT '用户所在城市',
  `language` varchar(50) DEFAULT NULL COMMENT '用户的语言，简体中文为zh_CN',
  `headimgurl` varchar(500) DEFAULT NULL COMMENT '用户头像',
  `subscribe_time` bigint(20) UNSIGNED DEFAULT NULL COMMENT '用户关注时间',
  `subscribe_at` datetime DEFAULT NULL COMMENT '关注时间',
  `unionid` varchar(100) DEFAULT NULL COMMENT 'unionid',
  `remark` varchar(50) DEFAULT NULL COMMENT '备注',
  `expires_in` bigint(20) UNSIGNED DEFAULT '0' COMMENT '有效时间',
  `refresh_token` varchar(200) DEFAULT NULL COMMENT '刷新token',
  `access_token` varchar(200) DEFAULT NULL COMMENT '访问token',
  `create_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='微信粉丝';

-- --------------------------------------------------------

--
-- 表的结构 `wechat_fans_tags`
--

CREATE TABLE `wechat_fans_tags` (
  `id` bigint(20) UNSIGNED NOT NULL COMMENT '标签ID',
  `appid` char(50) DEFAULT NULL COMMENT '公众号APPID',
  `name` varchar(35) DEFAULT NULL COMMENT '标签名称',
  `count` int(11) UNSIGNED DEFAULT NULL COMMENT '总数',
  `create_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建日期'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='微信会员标签';

-- --------------------------------------------------------

--
-- 表的结构 `wechat_keys`
--

CREATE TABLE `wechat_keys` (
  `id` bigint(20) NOT NULL,
  `appid` char(50) DEFAULT NULL COMMENT '公众号APPID',
  `type` varchar(20) DEFAULT NULL COMMENT '类型，text 文件消息，image 图片消息，news 图文消息',
  `keys` varchar(100) DEFAULT NULL COMMENT '关键字',
  `content` text COMMENT '文本内容',
  `image_url` varchar(255) DEFAULT NULL COMMENT '图片链接',
  `voice_url` varchar(255) DEFAULT NULL COMMENT '语音链接',
  `music_title` varchar(100) DEFAULT NULL COMMENT '音乐标题',
  `music_url` varchar(255) DEFAULT NULL COMMENT '音乐链接',
  `music_image` varchar(255) DEFAULT NULL COMMENT '音乐缩略图链接',
  `music_desc` varchar(255) DEFAULT NULL COMMENT '音乐描述',
  `video_title` varchar(100) DEFAULT NULL COMMENT '视频标题',
  `video_url` varchar(255) DEFAULT NULL COMMENT '视频URL',
  `video_desc` varchar(255) DEFAULT NULL COMMENT '视频描述',
  `news_id` bigint(20) UNSIGNED DEFAULT NULL COMMENT '图文ID',
  `sort` bigint(20) UNSIGNED DEFAULT '0' COMMENT '排序字段',
  `status` tinyint(1) UNSIGNED DEFAULT '1' COMMENT '0 禁用，1 启用',
  `create_by` bigint(20) UNSIGNED DEFAULT NULL COMMENT '创建人',
  `create_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '创建时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT=' 微信关键字';

-- --------------------------------------------------------

--
-- 表的结构 `wechat_menu`
--

CREATE TABLE `wechat_menu` (
  `id` bigint(16) UNSIGNED NOT NULL,
  `index` bigint(20) DEFAULT NULL,
  `pindex` bigint(20) UNSIGNED NOT NULL DEFAULT '0' COMMENT '父id',
  `type` varchar(24) NOT NULL DEFAULT '' COMMENT '菜单类型 null主菜单 link链接 keys关键字',
  `name` varchar(256) NOT NULL DEFAULT '' COMMENT '菜单名称',
  `content` text NOT NULL COMMENT '文字内容',
  `sort` bigint(20) UNSIGNED DEFAULT '0' COMMENT '排序',
  `status` tinyint(1) UNSIGNED DEFAULT '1' COMMENT '状态(0禁用1启用)',
  `create_by` bigint(20) UNSIGNED NOT NULL DEFAULT '0' COMMENT '创建人',
  `create_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='微信菜单配置';

-- --------------------------------------------------------

--
-- 表的结构 `wechat_news`
--

CREATE TABLE `wechat_news` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `media_id` varchar(100) DEFAULT NULL COMMENT '永久素材MediaID',
  `local_url` varchar(300) DEFAULT NULL COMMENT '永久素材显示URL',
  `article_id` varchar(60) DEFAULT NULL COMMENT '关联图文ID，用，号做分割',
  `is_deleted` tinyint(1) UNSIGNED DEFAULT '0' COMMENT '是否删除',
  `create_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `create_by` bigint(20) DEFAULT NULL COMMENT '创建人'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='微信图文表';

-- --------------------------------------------------------

--
-- 表的结构 `wechat_news_article`
--

CREATE TABLE `wechat_news_article` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(50) DEFAULT NULL COMMENT '素材标题',
  `local_url` varchar(300) DEFAULT NULL COMMENT '永久素材显示URL',
  `show_cover_pic` tinyint(4) UNSIGNED DEFAULT '0' COMMENT '是否显示封面 0不显示，1 显示',
  `author` varchar(20) DEFAULT NULL COMMENT '作者',
  `digest` varchar(300) DEFAULT NULL COMMENT '摘要内容',
  `content` longtext COMMENT '图文内容',
  `content_source_url` varchar(200) DEFAULT NULL COMMENT '图文消息原文地址',
  `create_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `create_by` bigint(20) DEFAULT NULL COMMENT '创建人'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='微信素材表';

-- --------------------------------------------------------

--
-- 表的结构 `wechat_news_image`
--

CREATE TABLE `wechat_news_image` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `md5` varchar(32) DEFAULT NULL COMMENT '文件md5',
  `local_url` varchar(300) DEFAULT NULL COMMENT '本地文件链接',
  `media_url` varchar(300) DEFAULT NULL COMMENT '远程图片链接',
  `create_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='微信服务器图片';

-- --------------------------------------------------------

--
-- 表的结构 `wechat_news_media`
--

CREATE TABLE `wechat_news_media` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `appid` varchar(200) DEFAULT NULL COMMENT '公众号ID',
  `md5` varchar(32) DEFAULT NULL COMMENT '文件md5',
  `type` varchar(20) DEFAULT NULL COMMENT '媒体类型',
  `media_id` varchar(100) DEFAULT NULL COMMENT '永久素材MediaID',
  `local_url` varchar(300) DEFAULT NULL COMMENT '本地文件链接',
  `media_url` varchar(300) DEFAULT NULL COMMENT '远程图片链接',
  `create_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='微信素材表';

-- --------------------------------------------------------

--
-- 表的结构 `wechat_pay_notify`
--

CREATE TABLE `wechat_pay_notify` (
  `id` int(20) NOT NULL,
  `appid` varchar(50) DEFAULT NULL COMMENT '公众号Appid',
  `bank_type` varchar(50) DEFAULT NULL COMMENT '银行类型',
  `cash_fee` bigint(20) DEFAULT NULL COMMENT '现金价',
  `fee_type` char(20) DEFAULT NULL COMMENT '币种，1人民币',
  `is_subscribe` char(1) DEFAULT 'N' COMMENT '是否关注，Y为关注，N为未关注',
  `mch_id` varchar(50) DEFAULT NULL COMMENT '商户MCH_ID',
  `nonce_str` varchar(32) DEFAULT NULL COMMENT '随机串',
  `openid` varchar(50) DEFAULT NULL COMMENT '微信用户openid',
  `out_trade_no` varchar(50) DEFAULT NULL COMMENT '支付平台退款交易号',
  `sign` varchar(100) DEFAULT NULL COMMENT '签名',
  `time_end` datetime DEFAULT NULL COMMENT '结束时间',
  `result_code` varchar(10) DEFAULT NULL,
  `return_code` varchar(10) DEFAULT NULL,
  `total_fee` varchar(11) DEFAULT NULL COMMENT '支付总金额，单位为分',
  `trade_type` varchar(20) DEFAULT NULL COMMENT '支付方式',
  `transaction_id` varchar(64) DEFAULT NULL COMMENT '订单号',
  `create_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '本地系统时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='支付日志表';

-- --------------------------------------------------------

--
-- 表的结构 `wechat_pay_prepayid`
--

CREATE TABLE `wechat_pay_prepayid` (
  `id` int(20) NOT NULL,
  `appid` char(50) DEFAULT NULL COMMENT '公众号APPID',
  `from` char(32) DEFAULT 'shop' COMMENT '支付来源',
  `fee` bigint(20) UNSIGNED DEFAULT NULL COMMENT '支付费用(分)',
  `trade_type` varchar(20) DEFAULT NULL COMMENT '订单类型',
  `order_no` varchar(50) DEFAULT NULL COMMENT '内部订单号',
  `out_trade_no` varchar(50) DEFAULT NULL COMMENT '外部订单号',
  `prepayid` varchar(500) DEFAULT NULL COMMENT '预支付码',
  `expires_in` bigint(20) UNSIGNED DEFAULT NULL COMMENT '有效时间',
  `transaction_id` varchar(64) DEFAULT NULL COMMENT '微信平台订单号',
  `is_pay` tinyint(1) UNSIGNED DEFAULT '0' COMMENT '1已支付，0未支退款',
  `pay_at` datetime DEFAULT NULL COMMENT '支付时间',
  `is_refund` tinyint(1) UNSIGNED DEFAULT '0' COMMENT '是否退款，退款单号(T+原来订单)',
  `refund_at` datetime DEFAULT NULL COMMENT '退款时间',
  `create_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '本地系统时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='支付订单号对应表';

--
-- 转储表的索引
--

--
-- 表的索引 `announce_log`
--
ALTER TABLE `announce_log`
  ADD PRIMARY KEY (`id`);

--
-- 表的索引 `app_menu`
--
ALTER TABLE `app_menu`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `app_menu_id_uindex` (`id`),
  ADD UNIQUE KEY `app_menu_function_id_uindex` (`function_id`);

--
-- 表的索引 `app_version`
--
ALTER TABLE `app_version`
  ADD PRIMARY KEY (`id`);

--
-- 表的索引 `article`
--
ALTER TABLE `article`
  ADD PRIMARY KEY (`id`);

--
-- 表的索引 `article_category`
--
ALTER TABLE `article_category`
  ADD PRIMARY KEY (`id`);

--
-- 表的索引 `auto_unfreeze`
--
ALTER TABLE `auto_unfreeze`
  ADD PRIMARY KEY (`id`),
  ADD KEY `unfreeze_time` (`unfreeze_time`);

--
-- 表的索引 `cash`
--
ALTER TABLE `cash`
  ADD PRIMARY KEY (`id`);

--
-- 表的索引 `cash_channel`
--
ALTER TABLE `cash_channel`
  ADD PRIMARY KEY (`id`);

--
-- 表的索引 `cash_channel_account`
--
ALTER TABLE `cash_channel_account`
  ADD PRIMARY KEY (`id`);

--
-- 表的索引 `channel`
--
ALTER TABLE `channel`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `channel_code_uindex` (`code`);

--
-- 表的索引 `channel_account`
--
ALTER TABLE `channel_account`
  ADD PRIMARY KEY (`id`);

--
-- 表的索引 `complaint`
--
ALTER TABLE `complaint`
  ADD PRIMARY KEY (`id`);

--
-- 表的索引 `complaint_message`
--
ALTER TABLE `complaint_message`
  ADD PRIMARY KEY (`id`);

--
-- 表的索引 `email_code`
--
ALTER TABLE `email_code`
  ADD PRIMARY KEY (`id`);

--
-- 表的索引 `goods`
--
ALTER TABLE `goods`
  ADD PRIMARY KEY (`id`);

--
-- 表的索引 `goods_card`
--
ALTER TABLE `goods_card`
  ADD PRIMARY KEY (`id`),
  ADD KEY `goods_card_user_id_index` (`user_id`),
  ADD KEY `goods_card_goods_id_index` (`goods_id`);

--
-- 表的索引 `goods_category`
--
ALTER TABLE `goods_category`
  ADD PRIMARY KEY (`id`);

--
-- 表的索引 `goods_coupon`
--
ALTER TABLE `goods_coupon`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `user_id` (`user_id`,`code`) USING BTREE;

--
-- 表的索引 `invite_code`
--
ALTER TABLE `invite_code`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `code` (`code`);

--
-- 表的索引 `link`
--
ALTER TABLE `link`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `user_id` (`user_id`,`relation_type`,`relation_id`),
  ADD UNIQUE KEY `token_uindex` (`token`),
  ADD UNIQUE KEY `user_link_index` (`relation_id`,`relation_type`);

--
-- 表的索引 `log`
--
ALTER TABLE `log`
  ADD PRIMARY KEY (`id`);

--
-- 表的索引 `merchant_log`
--
ALTER TABLE `merchant_log`
  ADD PRIMARY KEY (`id`);

--
-- 表的索引 `message`
--
ALTER TABLE `message`
  ADD PRIMARY KEY (`id`);

--
-- 表的索引 `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `migration` (`migration`);

--
-- 表的索引 `nav`
--
ALTER TABLE `nav`
  ADD PRIMARY KEY (`id`),
  ADD KEY `index_system_menu_node` (`node`) USING BTREE;

--
-- 表的索引 `order`
--
ALTER TABLE `order`
  ADD PRIMARY KEY (`id`),
  ADD KEY `order_create_at_index` (`create_at`);

--
-- 表的索引 `order_card`
--
ALTER TABLE `order_card`
  ADD PRIMARY KEY (`id`),
  ADD KEY `order_card_order_id_index` (`order_id`),
  ADD KEY `order_card_card_id_index` (`card_id`);

--
-- 表的索引 `pay_type`
--
ALTER TABLE `pay_type`
  ADD PRIMARY KEY (`id`);

--
-- 表的索引 `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`id`);

--
-- 表的索引 `rate_group`
--
ALTER TABLE `rate_group`
  ADD PRIMARY KEY (`id`);

--
-- 表的索引 `rate_group_rule`
--
ALTER TABLE `rate_group_rule`
  ADD PRIMARY KEY (`id`),
  ADD KEY `group_id` (`group_id`) USING BTREE;

--
-- 表的索引 `rate_group_user`
--
ALTER TABLE `rate_group_user`
  ADD PRIMARY KEY (`id`);

--
-- 表的索引 `sms_code`
--
ALTER TABLE `sms_code`
  ADD PRIMARY KEY (`id`);

--
-- 表的索引 `system_auth`
--
ALTER TABLE `system_auth`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `index_system_auth_title` (`title`) USING BTREE,
  ADD KEY `index_system_auth_status` (`status`) USING BTREE;

--
-- 表的索引 `system_auth_node`
--
ALTER TABLE `system_auth_node`
  ADD KEY `index_system_auth_auth` (`auth`) USING BTREE,
  ADD KEY `index_system_auth_node` (`node`) USING BTREE;

--
-- 表的索引 `system_config`
--
ALTER TABLE `system_config`
  ADD PRIMARY KEY (`id`),
  ADD KEY `index_system_config_name` (`name`);

--
-- 表的索引 `system_log`
--
ALTER TABLE `system_log`
  ADD PRIMARY KEY (`id`);

--
-- 表的索引 `system_menu`
--
ALTER TABLE `system_menu`
  ADD PRIMARY KEY (`id`),
  ADD KEY `index_system_menu_node` (`node`) USING BTREE;

--
-- 表的索引 `system_node`
--
ALTER TABLE `system_node`
  ADD PRIMARY KEY (`id`),
  ADD KEY `index_system_node_node` (`node`);

--
-- 表的索引 `system_sequence`
--
ALTER TABLE `system_sequence`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `index_system_sequence_unique` (`type`,`sequence`) USING BTREE,
  ADD KEY `index_system_sequence_type` (`type`),
  ADD KEY `index_system_sequence_number` (`sequence`);

--
-- 表的索引 `system_user`
--
ALTER TABLE `system_user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `index_system_user_username` (`username`) USING BTREE;

--
-- 表的索引 `unique_orderno`
--
ALTER TABLE `unique_orderno`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `index_trade_no` (`trade_no`) USING BTREE;

--
-- 表的索引 `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `user_mobile_uindex` (`mobile`),
  ADD UNIQUE KEY `user_email_uindex` (`email`);

--
-- 表的索引 `user_channel`
--
ALTER TABLE `user_channel`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `user_id` (`user_id`,`channel_id`);

--
-- 表的索引 `user_collect`
--
ALTER TABLE `user_collect`
  ADD PRIMARY KEY (`id`);

--
-- 表的索引 `user_login_error_log`
--
ALTER TABLE `user_login_error_log`
  ADD PRIMARY KEY (`id`);

--
-- 表的索引 `user_login_log`
--
ALTER TABLE `user_login_log`
  ADD PRIMARY KEY (`id`);

--
-- 表的索引 `user_money_log`
--
ALTER TABLE `user_money_log`
  ADD PRIMARY KEY (`id`);

--
-- 表的索引 `user_rate`
--
ALTER TABLE `user_rate`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `user_id` (`user_id`,`channel_id`);

--
-- 表的索引 `user_token`
--
ALTER TABLE `user_token`
  ADD KEY `index_login_user` (`user_id`) USING BTREE,
  ADD KEY `index_login_token` (`token`) USING BTREE,
  ADD KEY `index_login_platform` (`platform`) USING BTREE;

--
-- 表的索引 `verify_email_error_log`
--
ALTER TABLE `verify_email_error_log`
  ADD PRIMARY KEY (`id`);

--
-- 表的索引 `verify_error_log`
--
ALTER TABLE `verify_error_log`
  ADD PRIMARY KEY (`id`);

--
-- 表的索引 `wechat_fans`
--
ALTER TABLE `wechat_fans`
  ADD PRIMARY KEY (`id`),
  ADD KEY `index_wechat_fans_spread_openid` (`spread_openid`) USING BTREE,
  ADD KEY `index_wechat_fans_openid` (`openid`) USING BTREE;

--
-- 表的索引 `wechat_fans_tags`
--
ALTER TABLE `wechat_fans_tags`
  ADD KEY `index_wechat_fans_tags_id` (`id`) USING BTREE,
  ADD KEY `index_wechat_fans_tags_appid` (`appid`) USING BTREE;

--
-- 表的索引 `wechat_keys`
--
ALTER TABLE `wechat_keys`
  ADD PRIMARY KEY (`id`);

--
-- 表的索引 `wechat_menu`
--
ALTER TABLE `wechat_menu`
  ADD PRIMARY KEY (`id`),
  ADD KEY `index_wechat_menu_pindex` (`pindex`) USING BTREE;

--
-- 表的索引 `wechat_news`
--
ALTER TABLE `wechat_news`
  ADD PRIMARY KEY (`id`),
  ADD KEY `index_wechat_new_artcle_id` (`article_id`) USING BTREE;

--
-- 表的索引 `wechat_news_article`
--
ALTER TABLE `wechat_news_article`
  ADD PRIMARY KEY (`id`);

--
-- 表的索引 `wechat_news_image`
--
ALTER TABLE `wechat_news_image`
  ADD PRIMARY KEY (`id`),
  ADD KEY `index_wechat_news_image_md5` (`md5`);

--
-- 表的索引 `wechat_news_media`
--
ALTER TABLE `wechat_news_media`
  ADD PRIMARY KEY (`id`);

--
-- 表的索引 `wechat_pay_notify`
--
ALTER TABLE `wechat_pay_notify`
  ADD PRIMARY KEY (`id`),
  ADD KEY `index_wechat_pay_notify_openid` (`openid`) USING BTREE,
  ADD KEY `index_wechat_pay_notify_out_trade_no` (`out_trade_no`) USING BTREE,
  ADD KEY `index_wechat_pay_notify_transaction_id` (`transaction_id`) USING BTREE;

--
-- 表的索引 `wechat_pay_prepayid`
--
ALTER TABLE `wechat_pay_prepayid`
  ADD PRIMARY KEY (`id`),
  ADD KEY `index_wechat_pay_prepayid_outer_no` (`out_trade_no`) USING BTREE,
  ADD KEY `index_wechat_pay_prepayid_order_no` (`order_no`) USING BTREE,
  ADD KEY `index_wechat_pay_is_pay` (`is_pay`) USING BTREE,
  ADD KEY `index_wechat_pay_is_refund` (`is_refund`) USING BTREE;

--
-- 在导出的表使用AUTO_INCREMENT
--

--
-- 使用表AUTO_INCREMENT `announce_log`
--
ALTER TABLE `announce_log`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- 使用表AUTO_INCREMENT `app_menu`
--
ALTER TABLE `app_menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- 使用表AUTO_INCREMENT `app_version`
--
ALTER TABLE `app_version`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- 使用表AUTO_INCREMENT `article`
--
ALTER TABLE `article`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- 使用表AUTO_INCREMENT `article_category`
--
ALTER TABLE `article_category`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- 使用表AUTO_INCREMENT `auto_unfreeze`
--
ALTER TABLE `auto_unfreeze`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- 使用表AUTO_INCREMENT `cash`
--
ALTER TABLE `cash`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- 使用表AUTO_INCREMENT `cash_channel`
--
ALTER TABLE `cash_channel`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- 使用表AUTO_INCREMENT `cash_channel_account`
--
ALTER TABLE `cash_channel_account`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- 使用表AUTO_INCREMENT `channel`
--
ALTER TABLE `channel`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '通道ID', AUTO_INCREMENT=176;

--
-- 使用表AUTO_INCREMENT `channel_account`
--
ALTER TABLE `channel_account`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- 使用表AUTO_INCREMENT `complaint`
--
ALTER TABLE `complaint`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- 使用表AUTO_INCREMENT `complaint_message`
--
ALTER TABLE `complaint_message`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- 使用表AUTO_INCREMENT `email_code`
--
ALTER TABLE `email_code`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- 使用表AUTO_INCREMENT `goods`
--
ALTER TABLE `goods`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- 使用表AUTO_INCREMENT `goods_card`
--
ALTER TABLE `goods_card`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2119;

--
-- 使用表AUTO_INCREMENT `goods_category`
--
ALTER TABLE `goods_category`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- 使用表AUTO_INCREMENT `goods_coupon`
--
ALTER TABLE `goods_coupon`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- 使用表AUTO_INCREMENT `invite_code`
--
ALTER TABLE `invite_code`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- 使用表AUTO_INCREMENT `link`
--
ALTER TABLE `link`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- 使用表AUTO_INCREMENT `log`
--
ALTER TABLE `log`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- 使用表AUTO_INCREMENT `merchant_log`
--
ALTER TABLE `merchant_log`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=194;

--
-- 使用表AUTO_INCREMENT `message`
--
ALTER TABLE `message`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- 使用表AUTO_INCREMENT `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=67;

--
-- 使用表AUTO_INCREMENT `nav`
--
ALTER TABLE `nav`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- 使用表AUTO_INCREMENT `order`
--
ALTER TABLE `order`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- 使用表AUTO_INCREMENT `order_card`
--
ALTER TABLE `order_card`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- 使用表AUTO_INCREMENT `pay_type`
--
ALTER TABLE `pay_type`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=129;

--
-- 使用表AUTO_INCREMENT `product`
--
ALTER TABLE `product`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=908;

--
-- 使用表AUTO_INCREMENT `rate_group`
--
ALTER TABLE `rate_group`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- 使用表AUTO_INCREMENT `rate_group_rule`
--
ALTER TABLE `rate_group_rule`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- 使用表AUTO_INCREMENT `rate_group_user`
--
ALTER TABLE `rate_group_user`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- 使用表AUTO_INCREMENT `sms_code`
--
ALTER TABLE `sms_code`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- 使用表AUTO_INCREMENT `system_auth`
--
ALTER TABLE `system_auth`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- 使用表AUTO_INCREMENT `system_config`
--
ALTER TABLE `system_config`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=335;

--
-- 使用表AUTO_INCREMENT `system_log`
--
ALTER TABLE `system_log`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=207;

--
-- 使用表AUTO_INCREMENT `system_menu`
--
ALTER TABLE `system_menu`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=136;

--
-- 使用表AUTO_INCREMENT `system_node`
--
ALTER TABLE `system_node`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=320;

--
-- 使用表AUTO_INCREMENT `system_sequence`
--
ALTER TABLE `system_sequence`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- 使用表AUTO_INCREMENT `system_user`
--
ALTER TABLE `system_user`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10006;

--
-- 使用表AUTO_INCREMENT `unique_orderno`
--
ALTER TABLE `unique_orderno`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- 使用表AUTO_INCREMENT `user`
--
ALTER TABLE `user`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10003;

--
-- 使用表AUTO_INCREMENT `user_channel`
--
ALTER TABLE `user_channel`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- 使用表AUTO_INCREMENT `user_collect`
--
ALTER TABLE `user_collect`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- 使用表AUTO_INCREMENT `user_login_error_log`
--
ALTER TABLE `user_login_error_log`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- 使用表AUTO_INCREMENT `user_login_log`
--
ALTER TABLE `user_login_log`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- 使用表AUTO_INCREMENT `user_money_log`
--
ALTER TABLE `user_money_log`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- 使用表AUTO_INCREMENT `user_rate`
--
ALTER TABLE `user_rate`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- 使用表AUTO_INCREMENT `verify_email_error_log`
--
ALTER TABLE `verify_email_error_log`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- 使用表AUTO_INCREMENT `verify_error_log`
--
ALTER TABLE `verify_error_log`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- 使用表AUTO_INCREMENT `wechat_fans`
--
ALTER TABLE `wechat_fans`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '粉丝表ID';

--
-- 使用表AUTO_INCREMENT `wechat_keys`
--
ALTER TABLE `wechat_keys`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- 使用表AUTO_INCREMENT `wechat_menu`
--
ALTER TABLE `wechat_menu`
  MODIFY `id` bigint(16) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- 使用表AUTO_INCREMENT `wechat_news`
--
ALTER TABLE `wechat_news`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- 使用表AUTO_INCREMENT `wechat_news_article`
--
ALTER TABLE `wechat_news_article`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- 使用表AUTO_INCREMENT `wechat_news_image`
--
ALTER TABLE `wechat_news_image`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- 使用表AUTO_INCREMENT `wechat_news_media`
--
ALTER TABLE `wechat_news_media`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- 使用表AUTO_INCREMENT `wechat_pay_notify`
--
ALTER TABLE `wechat_pay_notify`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT;

--
-- 使用表AUTO_INCREMENT `wechat_pay_prepayid`
--
ALTER TABLE `wechat_pay_prepayid`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
